﻿namespace Store.Messages.Payment
{
    public class CheckoutData
    {
        public decimal PaidAmount { get; private set; }

        public string PaymentProviderReference { get; private set; }

        public CheckoutData(decimal paidAmount, string paymentProviderReference)
        {
            this.PaidAmount = paidAmount;
            this.PaymentProviderReference = paymentProviderReference;
        }
    }
}