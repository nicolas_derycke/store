﻿namespace Store.Messages.Payment
{
    public enum CheckoutType
    {
        Free,
        BankTransfer,
        CreditCardPayment,
        DebitCardPayment,
        DirectActivation
    }
}