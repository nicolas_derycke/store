﻿namespace Store.Messages.Commands
{
    public class ValidateBasket
    {
        public string OrderCode { get; private set; }
        public bool ForceRevalidation { get; private set; }

        public ValidateBasket(string orderCode, bool forceRevalidation)
        {
            this.OrderCode = orderCode;
            this.ForceRevalidation = forceRevalidation;
        }
    }
}
