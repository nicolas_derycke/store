﻿namespace Store.Messages.Events
{
    public class EmailQueuedForOrder
    {
        public int MailQueueId { get; private set; }
        public string OrderCode { get; private set; }
        public int TicketId { get; private set; }
        public string TicketNumber { get; private set; }
        
        public EmailQueuedForOrder(int mailQueueId, string orderCode, int ticketId, string ticketNumber)
        {
            this.MailQueueId = mailQueueId;
            this.OrderCode = orderCode;
            this.TicketId = ticketId;
            this.TicketNumber = ticketNumber;
        }
    }
}