﻿using System;
using Store.Messages.Payment;

namespace Store.Messages.Events
{
    public class OrderCreated
    {
        public string OrderCode { get; private set; }

        public CheckoutType CheckoutType { get; private set; }

        public int CustomerId { get; private set; }

        public string SalesReference { get; private set; }

        public DateTime CreatedOn { get; private set; }

        public OrderCreated(string orderCode, CheckoutType checkoutType, int customerId, string salesReference, DateTime createdOn)
        {
            this.OrderCode = orderCode;
            this.CheckoutType = checkoutType;
            this.CustomerId = customerId;
            this.SalesReference = salesReference;
            this.CreatedOn = createdOn;
        }
    }
}