﻿using System;
using Store.Messages.Payment;

namespace Store.Messages.Events
{
    public class CheckoutCompleted
    {
        public string OrderCode { get; private set; }
        public int CustomerId { get; private set; }
        public CheckoutType CheckoutType { get; private set; }
        public CheckoutData CheckoutData { get; private set; }
        public string CustomerReference { get; private set; }
        public DateTime CheckoutCompletedOn { get; private set; }
        public int CheckedoutById { get; private set; }
        public string CheckedoutBy { get; private set; }
        public bool IsCheckedoutByEmployee { get; private set; }

        public CheckoutCompleted(string orderCode, int customerId, CheckoutType checkoutType, CheckoutData checkoutData, string customerReference, DateTime checkoutCompletedOn, int checkedoutById, string checkedoutBy, bool isCheckedoutByEmployee)
        {
            this.OrderCode = orderCode;
            this.CustomerId = customerId;
            this.CheckoutType = checkoutType;
            this.CheckoutData = checkoutData;
            this.CustomerReference = customerReference;
            this.CheckoutCompletedOn = checkoutCompletedOn;
            this.CheckedoutById = checkedoutById;
            this.CheckedoutBy = checkedoutBy;
            this.IsCheckedoutByEmployee = isCheckedoutByEmployee;
        }
    }
}