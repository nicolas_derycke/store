﻿using System;

namespace Store.Messages.Events
{
    public class OrderCancelled
    {
        public string OrderCode { get; private set; }
        public DateTime CancelledOn { get; private set; }
        public int CancelledById { get; private set; }
        public string CancelledBy { get; private set; }
        public string Reason { get; private set; }


        public OrderCancelled(string orderCode, string reason, DateTime cancelledOn, int cancelledById, string cancelledBy)
        {
            OrderCode = orderCode;
            CancelledOn = cancelledOn;
            CancelledById = cancelledById;
            CancelledBy = cancelledBy;
            Reason = reason;
        }
    }
}