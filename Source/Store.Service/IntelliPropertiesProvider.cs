using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.Service
{
    public class IntelliPropertiesProvider : IIntelliPropertiesProvider
    {
        public Func<IEnumerable<KeyValuePair<string, string>>> IntelliPropertiesAccessor
        {
            get
            {
                return Enumerable.Empty<KeyValuePair<string, string>>;
            }
        }
    }
}