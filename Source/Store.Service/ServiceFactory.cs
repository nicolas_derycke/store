﻿using System.Configuration;
using Core.Security;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Messaging.MassTransit3;
using Intelligent.Shared.Messaging.MassTransit3.Serilog;
using Intelligent.Shared.Reflection;
using Intelligent.Shared.ServiceModel.Client;
using Intelligent.Shared.Services.Topshelf;
using MassTransit;
using Microsoft.Practices.Unity;
using Serilog;
using Store.Helpers;
using Store.Mapping;
using Store.Services;
using Store.Validation;
using Ticketing.API.OrderQueue.Interface.Contracts.Services;
using System.Runtime.Remoting.Messaging;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace Store.Service
{
    public class ServiceFactory : UnityTopshelfServiceFactory
    {
        public override void ConfigureContainer(IUnityContainer container)
        {
            container.RegisterType<ILogger>(new ContainerControlledLifetimeManager(), new InjectionFactory(x => Log.Logger));
            container.RegisterType<ICommandHandler, CommandHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IAppSettingsConfiguration, AppSettingsConfiguration>(new ContainerControlledLifetimeManager());
            container.RegisterType<ISalesReferenceService, SalesReferenceService>(new ContainerControlledLifetimeManager());
            container.RegisterInstance<ITokenProvider>(new TokenProvider(Scopes.Customer, "combell.store", "store"));
            container.RegisterType<IHttpClientFactory, StoreHttpClientFactory>(new PerResolveLifetimeManager());
            container.RegisterInstance<IServiceInvoker<ITicketingService>>(new ServiceInvoker<ITicketingService>("Ticketing_OQ_TicketingServiceConfiguration"), new ContainerControlledLifetimeManager());
            container.RegisterType<ITicketService, TicketService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IIntelliPropertiesProvider, IntelliPropertiesProvider>(new ContainerControlledLifetimeManager());

            container.RegisterTypesWhichImplement<IBasketItemValidator>();

            UnityHelper.ConfigureContainer(container);

            MappingConfiguration.ConfigureMappings();

            var busControl = BusControlBuilder.New(
                    ConfigurationManager.AppSettings["MassTransit_HostAddress"],
                    ConfigurationManager.AppSettings["MassTransit_UserName"],
                    ConfigurationManager.AppSettings["MassTransit_Password"]
                )
                .WithAddedBusConfiguration(cfg =>
                {
                    cfg.ConfigurePublish(publishCfg => publishCfg.UseSendExecute(context =>
                    {
                        var intelliProperties = (IEnumerable<KeyValuePair<string,string>>)CallContext.LogicalGetData("IntelliProperties");
                        foreach (var intelliProperty in intelliProperties)
                        {
                            context.Headers.Set(intelliProperty.Key, intelliProperty.Value);
                        }
                    }));
                })
                .AddConsumerEndpoint("orderdomain.store.service", ep =>
                {
                    ep.WithConcurrency(new ConcurrencySetting(1))
                        .ForTypes(
                            TypeFinder.FindTypesWhichImplement(
                                typeof(ServiceFactory).Assembly,
                                typeof(IConsumer<>)),
                            type => container.Resolve(type));
                    ep.WithConsumerMw(endpointCfg =>
                    {
                        endpointCfg.UseExecute(context =>
                        {
                            var convertedHeaders =
                                context.Headers
                                    .GetAll()
                                    .Where(h => h.Key.StartsWith("Intelli"))
                                    .Select(h => new KeyValuePair<string, string>(h.Key, h.Value.ToString()));
                            CallContext.LogicalSetData("IntelliProperties", convertedHeaders);
                        });
                    });
                })
                .UseSeriLog()
                .Build();

            container.RegisterBusControl(busControl);
        }
    }
}
