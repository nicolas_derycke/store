﻿using Billing.Messages;
using Intelligent.Shared.Core.Queries;
using Serilog;
using Store.Data;
using Store.SqlCommands;

namespace Store.Service.EventHandlers
{
    internal class OrderInvoicedHandler : OrderPaymentStatusChangedHandler<OrderInvoiced>
    {
        public OrderInvoicedHandler(ISqlCommandHandler sqlCommandHandler, IQueryHandler queryHandler, ILogger logger) 
            : base(sqlCommandHandler, queryHandler, logger)
        {
        }

        protected override string GetOrderCode(OrderInvoiced message)
        {
            return message.OrderCode;
        }

        protected override OrderPaymentData GetOrderPaymentData(OrderInvoiced message)
        {
            return new OrderPaymentData(
                message.EpayCode, 
                message.InvoiceType != InvoiceType.NewOrdersProforma && message.InvoiceType != InvoiceType.PeriodFreeProforma && message.InvoiceType != InvoiceType.PermanentFreeProforma);
        }
    }
}