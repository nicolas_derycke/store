﻿using Intelligent.Shared.Core.Queries;
using OrderTracking.Messages;
using Store.API.Contracts;
using Store.SqlCommands;

namespace Store.Service.EventHandlers
{
    public class OrderProcessingStartedHandler : OrderStatusChangedHandler<OrderProcessingStarted>
    {
        public OrderProcessingStartedHandler(ISqlCommandHandler sqlCommandHandler, IQueryHandler queryHandler)
            : base(sqlCommandHandler, queryHandler)
        {
        }

        protected override string GetOrderCode(OrderProcessingStarted message)
        {
            return message.OrderCode;
        }

        protected override OrderStatus GetOrderStatus()
        {
            return OrderStatus.Processing;
        }
    }
}