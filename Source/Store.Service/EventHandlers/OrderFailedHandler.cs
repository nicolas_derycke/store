﻿using Intelligent.Shared.Core.Queries;
using OrderTracking.Messages;
using Store.API.Contracts;
using Store.SqlCommands;

namespace Store.Service.EventHandlers
{
    public class OrderFailedHandler : OrderStatusChangedHandler<OrderFailed>
    {
        public OrderFailedHandler(ISqlCommandHandler sqlCommandHandler, IQueryHandler queryHandler)
            : base(sqlCommandHandler, queryHandler)
        {
        }

        protected override string GetOrderCode(OrderFailed message)
        {
            return message.OrderCode;
        }

        protected override OrderStatus GetOrderStatus()
        {
            return OrderStatus.Failed;
        }
    }
}