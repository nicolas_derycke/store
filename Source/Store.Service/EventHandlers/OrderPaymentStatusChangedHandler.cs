﻿using System;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Core.Users;
using Intelligent.Shared.Core.Queries;
using MassTransit;
using Serilog;
using Store.Data;
using Store.Model.Exceptions;
using Store.SqlCommands;
using Store.SqlCommands.Orders;
using Store.SqlQueries;

namespace Store.Service.EventHandlers
{
    public abstract class OrderPaymentStatusChangedHandler<T> : IConsumer<T> where T : class
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IQueryHandler _queryHandler;
        private readonly ILogger _logger;

        public OrderPaymentStatusChangedHandler(ISqlCommandHandler sqlCommandHandler, IQueryHandler queryHandler, ILogger logger)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (logger == null) throw new ArgumentNullException("logger");

            this._sqlCommandHandler = sqlCommandHandler;
            this._queryHandler = queryHandler;
            this._logger = logger;
        }

        public async Task Consume(ConsumeContext<T> context)
        {
            var message = context.Message;
            
            var orderCode = new OrderCode(GetOrderCode(message));

            _logger.Information("Received message {messageName} for {orderCode}", message.GetType().Name, orderCode);
            
            if (orderCode.Type == OrderCodeType.Provisioning)
            {
                _logger.Information("OrderCode '{orderCode}' is of type provisioning", orderCode.Code);
                return;
            }

            var order = await _queryHandler.ExecuteAsync(new GetOrderByOrderCodeQuery(orderCode.Code));
            if (!order.HasValue)
                throw new OrderNotFoundException(orderCode.Code);

            var orderPaymentData = GetOrderPaymentData(message);
            _sqlCommandHandler.Execute(new UpdateOrderPaymentData(order.Value.OrderId, orderPaymentData, SystemUser.Id));
            _logger.Information("Updated order {orderId} with payment data {@paymentData}", order.Value.OrderId, orderPaymentData);
        }

        abstract protected string GetOrderCode(T message);

        abstract protected OrderPaymentData GetOrderPaymentData(T message);
    }
}