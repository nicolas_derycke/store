﻿using System;
using System.Threading.Tasks;
using Intelligent.Shared.Messaging.CommandBus;
using MassTransit;
using Payments.Messages;
using Payments.Messages.Events;
using Serilog;
using Store.Commands.Baskets;
using Store.Helpers;
using Store.Messages.Payment;
using Store.Services;

namespace Store.Service.EventHandlers
{
    public class PaymentTransactionProcessedHandler : IConsumer<PaymentTransactionProcessed>
    {
        private readonly ILogger _logger;
        private readonly IBasketService _basketService;
        private readonly ICommandBus _commandBus;

        public PaymentTransactionProcessedHandler(ILogger logger,
            IBasketService basketService,
            ICommandBus commandBus
            )
        {
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (logger == null) throw new ArgumentNullException("logger");

            _logger = logger;
            _basketService = basketService;
            _commandBus = commandBus;
        }

        public async Task Consume(ConsumeContext<PaymentTransactionProcessed> context)
        {
            // only listen to messages with ordercode that are accepted
            if (context.Message.PaymentReferenceType != PaymentReferenceType.OrderCode || context.Message.TransactionStatus != TransactionStatus.Accepted)
                return;
            
            _logger.Information("{name}: Received message {@message}", typeof(PaymentTransactionProcessed).Name, context.Message);

            // we wait 10 seconds if the message was not redelivered
            // checkout by store is prefered as we loose the customerreference in this case
            if (!context.ReceiveContext.Redelivered)
            {
                _logger.Information("{name}: Waiting to process message {@message}", typeof(PaymentTransactionProcessed).Name, context.Message);
                await Task.Delay(TimeSpan.FromSeconds(10));
            }

            var orderCode = context.Message.TransactionReference;
            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                var basket = await _basketService.GetBasketByOrderCodeToValidate(orderCode);

                if (basket == null)
                { 
                    _logger.Information("No basket found for ordercode {orderCode}", orderCode);
                    transaction.Complete();
                    return;
                }

                if (basket.CheckedOut)
                    _logger.Information("Basket already checkedout, ordercode {orderCode}", orderCode);
                else
                {
                    var checkoutType = CheckoutType.DebitCardPayment;
                    if (context.Message.PaymentMethodId == 2 || context.Message.PaymentMethodId == 3) // visa or mastercard
                        checkoutType = CheckoutType.CreditCardPayment;
                    
                    var checkoutData = new CheckoutData(context.Message.Amount, context.Message.TransactionReference);         
                    _commandBus.Send(new CheckoutBasketCommand(basket, checkoutType, checkoutData, basket.CustomerReference, 6, "system", false));
                    _logger.Information("{name}: Order {orderCode} was checked out by ogone payment", typeof(PaymentTransactionProcessedHandler).Name, orderCode);
                }
                
                transaction.Complete();
            }
        }
    }
}