﻿using System;
using System.Threading.Tasks;
using Intelligent.Core.Users;
using Intelligent.Shared.Core.Queries;
using MassTransit;
using Store.API.Contracts;
using Store.Model.Exceptions;
using Store.SqlCommands;
using Store.SqlCommands.Orders;
using Store.SqlQueries;

namespace Store.Service.EventHandlers
{
    public abstract class OrderStatusChangedHandler<T> : IConsumer<T> where T : class
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IQueryHandler _queryHandler;

        public OrderStatusChangedHandler(ISqlCommandHandler sqlCommandHandler, IQueryHandler queryHandler)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            _sqlCommandHandler = sqlCommandHandler;
            _queryHandler = queryHandler;
        }

        public async Task Consume(ConsumeContext<T> context)
        {
            var orderCode = GetOrderCode(context.Message);

            var order = await _queryHandler.ExecuteAsync(new GetOrderByOrderCodeQuery(orderCode));
            if (!order.HasValue)
                throw new OrderNotFoundException(orderCode);

            _sqlCommandHandler.Execute(new UpdateOrderStatus(order.Value.OrderId, GetOrderStatus(), SystemUser.Id));
        }

        abstract protected string GetOrderCode(T message);

        abstract protected OrderStatus GetOrderStatus();
    }
}