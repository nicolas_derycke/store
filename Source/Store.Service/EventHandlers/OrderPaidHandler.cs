﻿using Billing.Messages;
using Intelligent.Shared.Core.Queries;
using Serilog;
using Store.Data;
using Store.SqlCommands;

namespace Store.Service.EventHandlers
{
    internal class OrderPaidHandler : OrderPaymentStatusChangedHandler<OrderPaid>
    {
        public OrderPaidHandler(ISqlCommandHandler sqlCommandHandler, IQueryHandler queryHandler, ILogger logger) 
            : base(sqlCommandHandler, queryHandler, logger)
        {
        }

        protected override string GetOrderCode(OrderPaid message)
        {
            return message.OrderCode;
        }

        protected override OrderPaymentData GetOrderPaymentData(OrderPaid message)
        {
            return new OrderPaymentData(null, false);
        }
    }
}