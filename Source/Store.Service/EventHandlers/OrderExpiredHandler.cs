﻿using System;
using System.Threading.Tasks;
using Billing.Messages;
using Intelligent.Core.Users;
using Intelligent.Shared.Core.Queries;
using MassTransit;
using Serilog;
using Store.API.Contracts;
using Store.Data;
using Store.Helpers;
using Store.Messages.Events;
using Store.Model.Exceptions;
using Store.SqlCommands;
using Store.SqlCommands.Orders;
using Store.SqlQueries;

namespace Store.Service.EventHandlers
{
    public class OrderExpiredHandler : IConsumer<OrderExpired>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IQueryHandler _queryHandler;
        private readonly IBus _bus;
        private readonly ILogger _logger;

        public OrderExpiredHandler(ILogger logger,
            ISqlCommandHandler sqlCommandHandler,
            IQueryHandler queryHandler,
            IBus bus
            )
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (bus == null) throw new ArgumentNullException("bus");
            if (logger == null) throw new ArgumentNullException("logger");

            _logger = logger;
            _sqlCommandHandler = sqlCommandHandler;
            _queryHandler = queryHandler;
            _bus = bus;
        }

        public async Task Consume(ConsumeContext<OrderExpired> context)
        {
            _logger.Information("{name}: Received message {@message}", typeof(OrderExpiredHandler).Name, context.Message);
            var orderCode = context.Message.OrderCode;

            var result = await _queryHandler.ExecuteAsync(new GetOrderByOrderCodeQuery(orderCode));
            if (!result.HasValue)
                return;

            var order = result.Value;

            if(order.Status == OrderStatus.Cancelled)
                throw new InvalidOrderStatusException(order.Status);

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _sqlCommandHandler.Execute(new UpdateOrderStatus(order.OrderId, OrderStatus.Cancelled, SystemUser.Id));
                _sqlCommandHandler.Execute(new UpdateOrderPaymentData(order.OrderId,
                    new OrderPaymentData(order.EpayCode, false), SystemUser.Id));

                await _bus.Publish(new OrderCancelled(orderCode, "Order expired", DateTime.Now, SystemUser.Id, SystemUser.Name));

                transaction.Complete();
            }

            _logger.Information("{name}: Order {orderCode} was cancelled", typeof(OrderExpiredHandler).Name, orderCode);
        }
    }
}
