﻿using System;
using System.Threading.Tasks;
using Core.Mailing.Messages.Events;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using MassTransit;
using Serilog;
using Store.Messages.Events;
using Store.SqlQueries;

namespace Store.Service.EventHandlers
{
    public class EmailQueuedEventHandler : IConsumer<EmailQueuedEvent>
    {
        private readonly IQueryHandler _queryHandler;
        private readonly ILogger _logger;

        public EmailQueuedEventHandler(IQueryHandler queryHandler, ILogger logger)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (logger == null) throw new ArgumentNullException("logger");

            _queryHandler = queryHandler;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<EmailQueuedEvent> context)
        {
            var message = context.Message;

            _logger.Information("{name}: Received message {@message}", typeof(EmailQueuedEventHandler).Name, message);

            if (string.IsNullOrWhiteSpace(message.OrderCode))
            {
                _logger.Information("{name}: No order code supplied for mail queue id {mailQueueId}", typeof(EmailQueuedEventHandler).Name, message.MailQueueId);
                return;
            }

            if (new OrderCode(message.OrderCode).Type != OrderCodeType.Store)
            {
                _logger.Information("{name}: The supplied order code {orderCode} is not a store order", typeof(EmailQueuedEventHandler).Name, message.OrderCode);
                return;
            }

            var order = await _queryHandler.ExecuteAsync(new GetOrderByOrderCodeQuery(message.OrderCode));
            if (!order.HasValue)
            {
                _logger.Warning("{name}: No order found for {orderCode}", typeof(EmailQueuedEventHandler).Name, message.OrderCode);
                return;
            }

            await context.Publish(new EmailQueuedForOrder(message.MailQueueId, message.OrderCode, order.Value.TicketId, order.Value.TicketNumber));
            _logger.Information("{name}: EmailQueuedForOrder publish for {ticketNumber}", typeof(EmailQueuedEventHandler).Name, order.Value.TicketNumber);
        }
    }
}