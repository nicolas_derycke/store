﻿using Intelligent.Shared.Core.Queries;
using OrderTracking.Messages;
using Store.API.Contracts;
using Store.SqlCommands;

namespace Store.Service.EventHandlers
{
    public class OrderFinishedHandler : OrderStatusChangedHandler<OrderFinished>
    {
        public OrderFinishedHandler(ISqlCommandHandler sqlCommandHandler, IQueryHandler queryHandler)
            : base(sqlCommandHandler, queryHandler)
        {
        }

        protected override string GetOrderCode(OrderFinished message)
        {
            return message.OrderCode;
        }

        protected override OrderStatus GetOrderStatus()
        {
            return OrderStatus.Finished;
        }
    }
}