﻿using System;
using System.Threading.Tasks;
using Intelligent.Core.Users;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using MassTransit;
using Serilog;
using Store.Commands.Orders;
using Store.Helpers;
using Store.Messages.Events;
using Store.Model.Orders;
using Store.Services;
using Store.SqlQueries;

namespace Store.Service.EventHandlers
{
    public class CheckoutCompletedHandler : IConsumer<CheckoutCompleted>
    {
        private readonly ILogger _logger;
        private readonly IBus _bus;
        private readonly ICommandBus _commandBus;
        private readonly IQueryHandler _queryHandler;
        private readonly IBasketService _basketService;
        private readonly ISalesReferenceService _salesReferenceService;
        private readonly ITicketService _ticketService;

        public CheckoutCompletedHandler(
            ILogger logger, 
            IBus bus, 
            ICommandBus commandBus,
            IQueryHandler queryHandler,
            IBasketService basketService, 
            ISalesReferenceService salesReferenceService,
            ITicketService ticketService)
        {
            if (logger == null) throw new ArgumentNullException("logger");
            if (bus == null) throw new ArgumentNullException("bus");
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (salesReferenceService == null) throw new ArgumentNullException("salesReferenceService");
            if (ticketService == null) throw new ArgumentNullException("ticketService");

            _logger = logger;
            _bus = bus;
            _commandBus = commandBus;
            _queryHandler = queryHandler;
            _basketService = basketService;
            _salesReferenceService = salesReferenceService;
            _ticketService = ticketService;
        }

        public async Task Consume(ConsumeContext<CheckoutCompleted> context)
        {
            _logger.Information("{name}: Received message {@message}", typeof(CheckoutCompletedHandler).Name, context.Message);

            var message = context.Message;
            var orderCode = message.OrderCode;

            var result = await _queryHandler.ExecuteAsync(new GetOrderByOrderCodeQuery(orderCode));
            if (result.HasValue)
            {
                _logger.Information("Order {orderCode} already created, nothing to do here", result.Value.OrderCode);
                return;
            }

            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            var order = basket.MapTo<Order>();
            order.CheckoutData = message.CheckoutData != null ? message.CheckoutData.MapTo<CheckoutData>() : null;
            order.CheckoutType = message.CheckoutType.MapTo<CheckoutType>();
            order.CustomerReference = message.CustomerReference;

            var salesReference = await _salesReferenceService.Generate(order.CustomerIdentifier, order.Source, message.IsCheckedoutByEmployee);

            var ticket = await _ticketService.CreateTicketForOrder(order.OrderCode, order.CustomerIdentifier, order.Provider);
            order.AssignTicket(ticket.TicketId, ticket.TicketNumber);

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(new CreateOrderCommand(order, context.Message.CheckedoutById));

                await _bus.Publish(new OrderCreated(orderCode, message.CheckoutType, order.CustomerIdentifier.CustomerId, salesReference, DateTime.Now));

                transaction.Complete();
            }

            _logger.Information("{name}: Basket {orderCode} was converted to an order", typeof(CheckoutCompletedHandler).Name, orderCode);
        }
    }
}
