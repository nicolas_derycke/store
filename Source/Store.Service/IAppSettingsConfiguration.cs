namespace Store.Service
{
    public interface IAppSettingsConfiguration
    {
        int RevalidationOffsetInDays { get; }
        int RevalidationPeriodInDays { get; }
        int ExpiredRegularBasketOffsetInDays { get; }
        int ExpiredUpgradeBasketOffsetInDays { get; }
        int ExpireAnonymousBasketOffsetInDays { get; }
    }
}