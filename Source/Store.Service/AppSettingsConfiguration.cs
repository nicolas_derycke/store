﻿using System.Configuration;

namespace Store.Service
{
    public class AppSettingsConfiguration : IAppSettingsConfiguration
    {
        public AppSettingsConfiguration()
        {
            RevalidationOffsetInDays = int.Parse(ConfigurationManager.AppSettings["RevalidationOffsetInDays"]);
            RevalidationPeriodInDays = int.Parse(ConfigurationManager.AppSettings["RevalidationPeriodInDays"]);

            ExpiredRegularBasketOffsetInDays   = int.Parse(ConfigurationManager.AppSettings["ExpiredRegularBasketOffsetInDays"]);
            ExpiredUpgradeBasketOffsetInDays   = int.Parse(ConfigurationManager.AppSettings["ExpiredUpgradeBasketOffsetInDays"]);
            ExpireAnonymousBasketOffsetInDays = int.Parse(ConfigurationManager.AppSettings["ExpireAnonymousBasketOffsetInDays"]);
        }

        public int RevalidationOffsetInDays { get; private set; }

        public int RevalidationPeriodInDays { get; private set; }

        public int ExpiredRegularBasketOffsetInDays { get; private set; }

        public int ExpiredUpgradeBasketOffsetInDays { get; private set; }

        public int ExpireAnonymousBasketOffsetInDays { get; private set; }
    }
}
