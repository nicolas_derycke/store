﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Intelligent.Core.Users;
using Intelligent.Shared.Messaging.CommandBus;
using MassTransit;
using Serilog;
using Store.Commands.Baskets;
using Store.Messages.Commands;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.SqlCommands.Baskets;
using Store.Validation;
using Store.Warnings;

namespace Store.Service.CommandHandlers
{
    public class ValidateBasketHandler : IConsumer<ValidateBasket>
    {
        private readonly IAppSettingsConfiguration _appSettingsConfiguration;
        private readonly ILogger _logger;
        private readonly IEnumerable<IBasketItemValidator> _validators;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IEnumerable<IWarningProvider> _warningProviders;
        private readonly IBasketService _basketService;
        private readonly ICommandBus _commandBus;

        public ValidateBasketHandler(
            IAppSettingsConfiguration appSettingsConfiguration,
            ILogger logger,
            IEnumerable<IBasketItemValidator> validators,
            ISqlCommandHandler sqlCommandHandler,
            IEnumerable<IWarningProvider> warningProviders,
            IBasketService basketService,
            ICommandBus commandBus)
        {
            if (appSettingsConfiguration == null) throw new ArgumentNullException("appSettingsConfiguration");
            if (logger == null) throw new ArgumentNullException("logger");
            if (validators == null) throw new ArgumentNullException("validators");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (warningProviders == null) throw new ArgumentNullException("warningProviders");
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (commandBus == null) throw new ArgumentNullException("commandBus");

            _appSettingsConfiguration = appSettingsConfiguration;
            _logger = logger;
            _validators = validators;
            _sqlCommandHandler = sqlCommandHandler;
            _warningProviders = warningProviders;
            _basketService = basketService;
            _commandBus = commandBus;
        }

        public async Task Consume(ConsumeContext<ValidateBasket> context)
        {
            var message = context.Message;

            _logger.Information(
                "{eventHandlerName}: Validating basket {orderCode}",
                typeof(ValidateBasketHandler).Name,
                message.OrderCode);

            var basket = await _basketService.GetBasketByOrderCodeToValidate(message.OrderCode);

            if (basket.Deleted)
            {
                _logger.Warning("Basket {orderCode} will not be validated. The basket has been deleted.");
                return;
            }

            // we won't validate the basket if the validation has already happened (e.g. multiple messages for the same basket)
            if (!basket.IsRevalidationRequired(_appSettingsConfiguration.RevalidationOffsetInDays) &&
                !message.ForceRevalidation)
            {
                _logger.Warning(
                    "Basket {orderCode} will not be validated as the last validation date {lastValidationDate} is not within the revalidation offset.",
                    basket.OrderCode,
                    basket.LastValidationDate);
                return;
            }

            if (basket.IsRecalculating &&
                !basket.IsRevalidationRequired(_appSettingsConfiguration.RevalidationOffsetInDays + 1) && // Don't skip validation when it has been running for more that 1 day
                !message.ForceRevalidation)
            {
                _logger.Warning(
                    "Basket {orderCode} will not be validated as validation is currently running.",
                    basket.OrderCode);
                return;
            }

            try
            {
                _sqlCommandHandler.Execute(new UpdateBasketIsRecalculating(basket.BasketId, true, SystemUser.Id));

                // get applied promocodes to re-apply them after the re-calculation
                var appliedPromoCodes = basket.Items.Where(i => i.IsPromo).Select(i => i.PromoCode).ToList();

                // validate
                foreach (var basketItem in basket.Items)
                    basketItem.SetBasketItemValidationErrors(
                        _validators.SelectMany(v => v.Validate(basketItem, basket)).ToList()); // to list is important, to avoid multiple executions of the validators!
                basket.LastValidationDate = DateTime.Now;

                // check for warnings
                foreach (var basketItem in basket.Items)
                    basketItem.SetBasketItemWarnings(_warningProviders.SelectMany(w => w.Validate(basketItem, basket)).ToList());

                // store validation state
                foreach (var basketItem in basket.Items)
                    _sqlCommandHandler.Execute(new UpdateBasketItem(basketItem, SystemUser.Id));

                // re-apply promocodes to basket
                foreach (var promoCode in appliedPromoCodes)
                    _commandBus.Send(new ApplyPromoCodeToBasketCommand(promoCode, basket, SystemUser.Id));

                _sqlCommandHandler.Execute(new UpdateBasketLastValidationDate(basket.BasketId, basket.LastValidationDate, SystemUser.Id));
                _sqlCommandHandler.Execute(new UpdateBasketIsRecalculationRequested(basket.OrderCode, false, SystemUser.Id));
            }
            finally
            {
                _sqlCommandHandler.Execute(new UpdateBasketIsRecalculating(basket.BasketId, false, SystemUser.Id));
            }
        }
    }
}