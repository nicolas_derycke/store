﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Intelligent.Core.Users;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Threading;
using MassTransit;
using Serilog;
using Store.Helpers;
using Store.Messages.Commands;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;

namespace Store.Service.CommandHandlers
{
    public class RevalidateBasketsHandler : IConsumer<RevalidateBaskets>
    {
        private readonly IQueryHandler _queryHandler;
        private readonly ILogger _logger;
        private readonly IAppSettingsConfiguration _appSettingsConfiguration;
        private readonly ISqlCommandHandler _sqlCommandHandler;

        public RevalidateBasketsHandler(IQueryHandler queryHandler, ILogger logger, IAppSettingsConfiguration appSettingsConfiguration, ISqlCommandHandler sqlCommandHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (logger == null) throw new ArgumentNullException("logger");
            if (appSettingsConfiguration == null) throw new ArgumentNullException("appSettingsConfiguration");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");

            _queryHandler = queryHandler;
            _logger = logger;
            _appSettingsConfiguration = appSettingsConfiguration;
            _sqlCommandHandler = sqlCommandHandler;
        }

        public async Task Consume(ConsumeContext<RevalidateBaskets> context)
        {
            _logger.Information(
                "{eventHandlerName}: Revalidating all baskets which haven't been validated within {days} day(s) and are not older then {period} days.",
                typeof(RevalidateBasketsHandler).Name,
                _appSettingsConfiguration.RevalidationOffsetInDays,
                _appSettingsConfiguration.RevalidationPeriodInDays);

            var basketsToValidateQuery = new GetBasketOrderCodesExceedingLastValidationDateQuery(_appSettingsConfiguration.RevalidationOffsetInDays, _appSettingsConfiguration.RevalidationPeriodInDays);
            var basketsToValidate = await _queryHandler.ExecuteAsync(basketsToValidateQuery);

            _logger.Information(
                "{eventHandlerName}: {count} items found to revalidate.",
                typeof(RevalidateBasketsHandler).Name,
                basketsToValidate.Count());

            foreach (var basket in basketsToValidate)
            {
                if (basket.IsRecalculationRequested)
                {
                    _logger.Information("Recalculation is already requested for order {orderCode}, not publishing validate basket event.", basket.OrderCode);
                    continue;
                }

                using (var transaction = TransactionHelper.GetNewTransactionScope())
                {
                    _sqlCommandHandler.Execute(new UpdateBasketIsRecalculationRequested(basket.OrderCode, true, SystemUser.Id));
                    AsyncPump.Await(async () => await context.Publish(new ValidateBasket(basket.OrderCode, false)));

                    transaction.Complete();
                }
            }
        }
    }
}
