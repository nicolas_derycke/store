﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Intelligent.Core.Users;
using Intelligent.Shared.Core.Queries;
using MassTransit;
using Serilog;
using Store.API.Contracts.Baskets;
using Store.Messages.Commands;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;

namespace Store.Service.CommandHandlers
{
    public class DeleteExpiredBasketsHandler : IConsumer<DeleteExpiredBaskets>
    {
        private readonly IQueryHandler _queryHandler;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly ILogger _logger;
        private readonly IAppSettingsConfiguration _appSettingsConfiguration;

        public DeleteExpiredBasketsHandler(IQueryHandler queryHandler, ISqlCommandHandler sqlCommandHandler, ILogger logger, IAppSettingsConfiguration appSettingsConfiguration)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (logger == null) throw new ArgumentNullException("logger");
            if (appSettingsConfiguration == null) throw new ArgumentNullException("appSettingsConfiguration");

            _queryHandler = queryHandler;
            _sqlCommandHandler = sqlCommandHandler;
            _logger = logger;
            _appSettingsConfiguration = appSettingsConfiguration;
        }

        public async Task Consume(ConsumeContext<DeleteExpiredBaskets> context)
        {
            await DeleteExpiredBaskets(BasketType.Regular, _appSettingsConfiguration.ExpiredRegularBasketOffsetInDays);
            await DeleteExpiredBaskets(BasketType.Standalone, _appSettingsConfiguration.ExpiredUpgradeBasketOffsetInDays);
            await DeleteAnonymousBaskets();
        }

        private async Task DeleteExpiredBaskets(BasketType basketType, int offsetInDays)
        {
            _logger.Information(
                "{eventHandlerName}: Deleting all {basketType} baskets which haven't been updated in the last {days} days.",
                typeof(RevalidateBasketsHandler).Name,
                basketType,
                offsetInDays);

            var basketIds = await _queryHandler.ExecuteAsync(new GetBasketIdsExceedingLastUpdateDateQuery(DateTime.Now.AddDays(-offsetInDays), basketType));
            DeleteBaskets(basketIds);
        }

        private async Task DeleteAnonymousBaskets()
        {
            var offSetInDays = _appSettingsConfiguration.ExpireAnonymousBasketOffsetInDays;
            _logger.Information(
                "{eventHandlerName}: Deleting all anonymous baskets which haven't been updated in the last {days} days.",
                typeof(RevalidateBasketsHandler).Name,
                offSetInDays);

            var basketIds = await _queryHandler.ExecuteAsync(new GetAnonymousBasketIdsExceedingLastUpdateDateQuery(DateTime.Now.AddDays(-offSetInDays)));
            DeleteBaskets(basketIds);
        }

        private void DeleteBaskets(IEnumerable<int> basketIds)
        {
            foreach (var basketId in basketIds)
            {
                _sqlCommandHandler.Execute(new DeleteBasket(basketId, SystemUser.Id));
                _logger.Information("{eventHandlerName}: Deleted basket {basketId}", typeof(RevalidateBasketsHandler).Name, basketId);
            }
        }
    }
}