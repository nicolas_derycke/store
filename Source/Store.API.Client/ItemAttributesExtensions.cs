﻿using System;
using Intelligent.Shared.Core;
using Store.API.Contracts;

namespace Store.API.Client
{
    public static class ItemAttributesExtensions
    {
        public static T GetBasketItemAttribute<T>(this ItemAttributeDictionary itemAttributes, AttributeKey key)
        {
            if (!itemAttributes.ContainsKey(key))
                throw new Exception(string.Format("AttributeKey '{0}' was not found.", key));

            return (T)itemAttributes[key];
        }

        public static Optional<T> GetOptionalBasketItemAttribute<T>(this ItemAttributeDictionary itemAttributes, AttributeKey key)
        {
            if (!itemAttributes.ContainsKey(key))
                return Optional<T>.Empty;

            return (T)itemAttributes[key];
        }
    }
}
