﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using Intelligent.Core.Models;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using MassTransit.Pipeline.ConsumerFactories;
using MassTransit.Testing;
using MassTransit.Testing.Factories;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using Store.API.Contracts.Baskets;
using Store.Messages.Commands;
using Store.Model.Baskets;
using Store.Service;
using Store.Service.CommandHandlers;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.Validation;
using Store.Warnings;

namespace Store.API.Tests.Handlers
{
    public class WhenValidateBasket : GivenWhenThen
    {
        private ValidateBasketHandler _validateBasketHandler;
        private GenericGiven _given;
        private Basket _basket;

        private ISqlCommandHandler _sqlCommandHandler;
        private ILogger _logger;

        protected override void Given()
        {
            _given = new GenericGiven();

            _basket =
               _given.ABasket(new List<BasketItem>
               {
                    _given.ABasketItemForProduct(
                        _given.ALinuxExpressProduct(12),
                        12,
                        1,
                        _given.GetHostingAttributes(),
                        1234,promoCode:"test")
                }, DateTime.MinValue);
            
            _sqlCommandHandler = A.Fake<ISqlCommandHandler>();
            _logger = A.Fake<ILogger>();

            _validateBasketHandler = _given.CreateValidateBasketHandler(_basket, _sqlCommandHandler, _logger);
        }

        protected override void When()
        {
            var consumeContext = A.Fake<ConsumeContext<ValidateBasket>>();
            var validateBasketCommand = new ValidateBasket(_basket.OrderCode, false);
            A.CallTo(() => consumeContext.Message).Returns(validateBasketCommand);
            _validateBasketHandler.Consume(consumeContext).Wait();
        }

        [Test]
        public void BasketItemShouldContainWarningAndValidationError()
        {
            A.CallTo(() => _sqlCommandHandler.Execute(A<UpdateBasketItem>.That.Matches(c =>
                c.BasketItem.BasketItemId == 1234 &&
                c.BasketItem.Warning == BasketItemWarning.ExtraFieldsRequired &&
                c.BasketItem.ValidationError == BasketItemValidationError.AccountBelongsToOtherCustomer)))
                .MustHaveHappened();
        }
    }
}
