﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using Store.API.Contracts.Baskets;
using Store.Messages.Commands;
using Store.Service.CommandHandlers;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;

namespace Store.API.Tests.Handlers
{
    public class WhenDeleteExpiredBasket : GivenWhenThen
    {
        private GenericGiven _given;
        private DeleteExpiredBasketsHandler _deleteExpiredBasketsHandler;
        private ISqlCommandHandler _sqlCommandHandler;
        private ILogger _logger;
        private readonly IEnumerable<int> _regularBasketIds = new List<int> { 1,2,3 };
        private readonly IEnumerable<int> _standaloneBasketIds = new List<int> { 4, 5, 6 };

        protected override void Given()
        {
            _given = new GenericGiven();
            _sqlCommandHandler = A.Fake<ISqlCommandHandler>();
            _logger = A.Fake<ILogger>();
            var queryHandler = A.Fake<IQueryHandler>();

            A.CallTo(() => queryHandler.ExecuteAsync(A<GetBasketIdsExceedingLastUpdateDateQuery>.Ignored, CancellationToken.None))
                .Returns(Task.FromResult<IEnumerable<int>>(_regularBasketIds));
                
            _deleteExpiredBasketsHandler = _given.CreateDeleteExpiredBasketsHandler(queryHandler,_sqlCommandHandler, _logger);
        }

        protected override void When()
        {
            var consumeContextDeleteExpiredBaskets = A.Fake<ConsumeContext<DeleteExpiredBaskets>>();
            _deleteExpiredBasketsHandler.Consume(consumeContextDeleteExpiredBaskets).Wait();
        }

        [Test]
        public void ShouldDeleteBaskets()
        {
            A.CallTo(
                () =>
                    _sqlCommandHandler.Execute(A<DeleteBasket>.That.Matches(d => _regularBasketIds.Contains(d.BasketId)))).MustHaveHappened();
        }
    }
}
