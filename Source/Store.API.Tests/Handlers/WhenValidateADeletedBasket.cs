﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using Serilog;
using Store.Messages.Commands;
using Store.Model.Baskets;
using Store.Service.CommandHandlers;
using Store.SqlCommands;

namespace Store.API.Tests.Handlers
{
    public class WhenValidateADeletedBasket : GivenWhenThen
    {
        private ValidateBasketHandler _deletedBasketValidateHandler;
        private Basket _deletedBasket;

        private ISqlCommandHandler _sqlCommandHandler;
        private ILogger _logger;

        private GenericGiven _given;

        protected override void Given()
        {
            _given = new GenericGiven();

            _deletedBasket = _given.ADeletedBasket(new List<BasketItem>
               {
                    _given.ABasketItemForProduct(
                        _given.ALinuxExpressProduct(12),
                        12,
                        1,
                        _given.GetHostingAttributes(),
                        1235)
                },
                DateTime.MinValue);

            _sqlCommandHandler = A.Fake<ISqlCommandHandler>();
            _logger = A.Fake<ILogger>();

            _deletedBasketValidateHandler = _given.CreateValidateBasketHandler(_deletedBasket, _sqlCommandHandler, _logger);

        }

        protected override void When()
        {
            var consumeContextDeletedBasket = A.Fake<ConsumeContext<ValidateBasket>>();
            var validateDeletedBasketCommand = new ValidateBasket(_deletedBasket.OrderCode, false);
            A.CallTo(() => consumeContextDeletedBasket.Message).Returns(validateDeletedBasketCommand);
            _deletedBasketValidateHandler.Consume(consumeContextDeletedBasket).Wait();
        }


        [Test]
        public void BasketItemShouldNotContainWarningAndValidationError()
        {
            A.CallTo(() =>
              _logger.Warning(A<string>
                                .That
                                .Matches(c => c == "Basket {orderCode} will not be validated. The basket has been deleted.")))
                                .MustHaveHappened(Repeated.AtLeast.Once);
        }
    }
}
