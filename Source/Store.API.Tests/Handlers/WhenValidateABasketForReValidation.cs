﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using Serilog;
using Store.Messages.Commands;
using Store.Model.Baskets;
using Store.Service.CommandHandlers;
using Store.SqlCommands;

namespace Store.API.Tests.Handlers
{
    public class WhenValidateABasketForReValidation : GivenWhenThen
    {
        private ValidateBasketHandler _revalidateBasketValidateHandler;
        private Basket _revalidateBasket;
        private GenericGiven _given;

        private ISqlCommandHandler _sqlCommandHandler;
        private ILogger _logger;

        protected override void Given()
        {
            _given = new GenericGiven();
            _revalidateBasket = _given.ABasket(new List<BasketItem> { _given.ABasketItemForProduct(
                _given.ALinuxExpressProduct(12),
                12,
                1,
                _given.GetHostingAttributes(),
                1236)}, DateTime.Now);

            _sqlCommandHandler = A.Fake<ISqlCommandHandler>();
            _logger = A.Fake<ILogger>();

            _revalidateBasketValidateHandler = _given.CreateValidateBasketHandler(_revalidateBasket, _sqlCommandHandler, _logger);
        }

        protected override void When()
        {
            var consumeContextRevalidateBasket = A.Fake<ConsumeContext<ValidateBasket>>();
            var revalidateDeletedBasketCommand = new ValidateBasket(_revalidateBasket.OrderCode, false);
            A.CallTo(() => consumeContextRevalidateBasket.Message).Returns(revalidateDeletedBasketCommand);
            _revalidateBasketValidateHandler.Consume(consumeContextRevalidateBasket).Wait();
        }

        [Test]
        public void BasketItemShouldNotBeReValidated()
        {
            A.CallTo(() =>
             _logger.Warning(A<string>
                                .That
                                .Matches(c => c == "Basket {orderCode} will not be validated as the last validation date {lastValidationDate} is not within the revalidation offset."), A<object[]>._))
                                .MustHaveHappened();
        }
    }
}
