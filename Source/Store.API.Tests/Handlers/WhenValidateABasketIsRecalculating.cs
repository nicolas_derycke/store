﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using Intelligent.Shared.Testing.NUnit;
using MassTransit;
using NUnit.Framework;
using Serilog;
using Store.Messages.Commands;
using Store.Model.Baskets;
using Store.Service.CommandHandlers;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.API.Tests.Handlers
{
    public class WhenValidateABasketIsRecalculating : GivenWhenThen
    {
        private ValidateBasketHandler _recalculatingBasketValidateHandler;
        private Basket _basketIsRecalculating;
        private GenericGiven _given;

        private ISqlCommandHandler _sqlCommandHandler;
        private ILogger _logger;

        protected override void Given()
        {
            _given = new GenericGiven();
            _basketIsRecalculating = _given.ABasketIsRecalculating(new List<BasketItem>
            {
                _given.ABasketItemForProduct(
                    _given.ALinuxExpressProduct(12),
                    12,
                    1,
                    _given.GetHostingAttributes(),
                    1237)
            }, DateTime.Now.AddDays(-1));

            _sqlCommandHandler = A.Fake<ISqlCommandHandler>();
            _logger = A.Fake<ILogger>();

            _recalculatingBasketValidateHandler = _given.CreateValidateBasketHandler(_basketIsRecalculating, _sqlCommandHandler, _logger);

        }

        protected override void When()
        {
            var consumeContextRecalculatingBasket = A.Fake<ConsumeContext<ValidateBasket>>();
            var recalculatingBasketCommand = new ValidateBasket(_basketIsRecalculating.OrderCode, false);
            A.CallTo(() => consumeContextRecalculatingBasket.Message).Returns(recalculatingBasketCommand);
            _recalculatingBasketValidateHandler.Consume(consumeContextRecalculatingBasket).Wait();
        }

        [Test]
        public void BasketItemShouldNotBeRevalidatedBasketIsRecalculating()
        {
            A.CallTo(() => _sqlCommandHandler.Execute(A<UpdateBasketItem>.That.Matches(c =>
                c.BasketItem.BasketItemId == 1237))).MustNotHaveHappened();
        }
    }
}
