﻿using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Converters;
using Store.API.Contracts.TypedItemAttributes;
using System;

namespace Store.API.Tests.Converters
{
    [TestFixture]
    public class WhenCreatingPreregistrationAttributesWithAStageDateOfFormatIso8601 : GivenWhenThen
    {
        private AttributeKey _attributeKey;
        private string _attributeValueJsonIso8601;
        private string _attributeValueJsonIso8601WithoutMilliseconds;
        private string _attributeValueJsonWithNormalDate;
        private IBasketItemAttribute _basketItemAttributeIso8601;
        private IBasketItemAttribute _basketItemAttributeIso8601WithoutMilliseconds;
        private IBasketItemAttribute _basketItemAttributeWithNormalDate;

        protected override void Given()
        {
            _attributeKey = AttributeKey.Preregistration;
            _attributeValueJsonIso8601 = @"{
                                               ""isLandRush"" : true,
                                               ""isGoLive"" : false,
                                               ""stageDate"": ""2016-08-01T10:00:00.000Z""
                                           }";
            _attributeValueJsonIso8601WithoutMilliseconds = @"{
                                                                  ""isLandRush"" : true,
                                                                  ""isGoLive"" : false,
                                                                  ""stageDate"": ""2016-08-01T10:00:00Z""
                                                              }";

            _attributeValueJsonWithNormalDate = @"{
                                                      ""isLandRush"" : true,
                                                      ""isGoLive"" : false,
                                                      ""stageDate"": ""2016-09-26T14:59:00+02:00""
                                                  }";
        }

        protected override void When()
        {
            _basketItemAttributeIso8601 = AttributeValueFactory.Create(_attributeKey, _attributeValueJsonIso8601);
            _basketItemAttributeIso8601WithoutMilliseconds = AttributeValueFactory.Create(_attributeKey, _attributeValueJsonIso8601WithoutMilliseconds);
            _basketItemAttributeWithNormalDate = AttributeValueFactory.Create(_attributeKey, _attributeValueJsonWithNormalDate);
        }

        [Test]
        public void PreregistrationAttributeShouldBeCreated()
        {
            _basketItemAttributeIso8601.Should().BeOfType<PreregistrationAttributes>();
            var basketItemAttribute = (PreregistrationAttributes)_basketItemAttributeIso8601;
            basketItemAttribute.IsLandRush.Should().Be(true);
            basketItemAttribute.IsGoLive.Should().Be(false);
            basketItemAttribute.StageDate.Should().Be(new DateTime(2016, 8, 1, 10, 0, 0));
        }

        [Test]
        public void PreregistrationAttributeWithoutMillisecondsShouldBeCreated()
        {
            _basketItemAttributeIso8601WithoutMilliseconds.Should().BeOfType<PreregistrationAttributes>();
            var basketItemAttribute = (PreregistrationAttributes)_basketItemAttributeIso8601WithoutMilliseconds;
            basketItemAttribute.IsLandRush.Should().Be(true);
            basketItemAttribute.IsGoLive.Should().Be(false);
            basketItemAttribute.StageDate.Should().Be(new DateTime(2016, 8, 1, 10, 0, 0));
        }

        [Test]
        public void PreregistrationAttributeWithNotValidDateShouldHaveWrongStageDate()
        {
            _basketItemAttributeWithNormalDate.Should().BeOfType<PreregistrationAttributes>();
            var basketItemAttribute = (PreregistrationAttributes)_basketItemAttributeWithNormalDate;
            basketItemAttribute.IsLandRush.Should().Be(true);
            basketItemAttribute.IsGoLive.Should().Be(false);
            basketItemAttribute.StageDate.Should().Be(new DateTime(2016, 9, 26, 14, 59, 0));
        }
    }
}