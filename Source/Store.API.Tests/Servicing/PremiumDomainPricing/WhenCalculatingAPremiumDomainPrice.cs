﻿using System;
using FluentAssertions;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using Store.Model;
using Store.Proxies;
using Store.Proxies.Data;
using Store.Services;

namespace Store.API.Tests.Servicing.PremiumDomainPricing
{
    public class WhenCalculatingAPremiumDomainPrice : GivenWhenThen
    {
        private PremiumDomainPriceService _premiumDomainPriceService;

        private IRrpProxy _rrpProxy;
        private ILogger _logger;
        private IQueryHandler _queryHandler;

        private RrpProxyDomainPrice _rrpProxyDomainPrice;
        private int _periodInMonths;
        private decimal _defaultPeriodPrice;

        private PremiumDomainPrice _premiumDomainPrice;

        protected override void Given()
        {
            _rrpProxy = MockRepository.GenerateMock<IRrpProxy>();
            _logger = MockRepository.GenerateMock<ILogger>();
            _queryHandler = MockRepository.GenerateMock<IQueryHandler>();

            _premiumDomainPriceService = new PremiumDomainPriceService(_rrpProxy, _logger, _queryHandler);

            _defaultPeriodPrice = 100.0m;
            _periodInMonths = 12;
            _rrpProxyDomainPrice = new RrpProxyDomainPrice(5000, 5000, 5000, 1000, _periodInMonths / 12, true, "eur");
        }

        protected override void When()
        {
            _premiumDomainPrice = _premiumDomainPriceService.CalculatePremiumDomainPrice(_rrpProxyDomainPrice, _defaultPeriodPrice, _periodInMonths);
        }

        [Test]
        public void ShouldCorrectlyCalculatePrices()
        {
            _premiumDomainPrice.Period.Should().Be(12);
            _premiumDomainPrice.PeriodPrice.Should().Be(5000m * 1.25m);
            _premiumDomainPrice.SetupPrice.Should().Be(1000m * 1.25m);
        }
    }
}