﻿using System.Collections.Generic;
using System.Linq;
using Billing.API.Contracts;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts.Baskets;
using Store.Model.Baskets;
using Store.SqlCommands.BasketItems;

namespace Store.API.Tests.Servicing
{
    [TestFixture]
    public class WhenChangingBasketItemsQuantityForValidCorrections : AbstractWhenCreatingBasketItemsForCorrections
    {
        protected override InvoiceCorrectionStatusContract InvoiceCorrectionStatus
        {
            get
            {
                return InvoiceCorrectionStatusContract.ValidCorrection;
            }
        }

        protected override BasketItemValidationError ExpectedValidationError
        {
            get
            {
                return BasketItemValidationError.None;
            }
        }

        protected override Basket CreateBasket(List<BasketItem> basketItems)
        {
            basketItems.Add(CreateABasketItemWithId(884, basketItems.First().BasketItemId));
            return _given.ABasket(basketItems);
        }

        protected override void ConfigureSqlCommandHandlerExpectations()
        {
            _sqlCommandHandler.Expect(x => x.Execute(Arg<RemoveBasketItem>.Matches(arg => arg.BasketItemId == 884)));
        }

        [Test]
        public void ShouldCreateBasketItems()
        {
            _commandBus.VerifyAllExpectations();
        }

        [Test]
        public void ShouldDeletePreviousCompensations()
        {
            _basket.Items.Any(i => i.BasketItemId == 884).Should().BeFalse();
            _sqlCommandHandler.VerifyAllExpectations();
        }
    }
}