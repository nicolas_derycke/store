﻿namespace Store.API.Tests.Servicing.SalesReferences
{
    public class WhenGeneratingForAnExistingCustomer : AbstractGeneratingSalesReference
    {
        protected override string Source
        {
            get
            {
                return "Combell.store";
            }
        }

        protected override bool IsCheckedoutByEmployee
        {
            get
            {
                return false;
            }
        }

        protected override bool OneInvoicePerMonthForNewOrders
        {
            get
            {
                return false;
            }
        }

        protected override bool IsNewCustomer
        {
            get
            {
                return false;
            }
        }

        protected override string ExpectedSalesReference
        {
            get
            {
                return "BB1188";
            }
        }
    }
}
