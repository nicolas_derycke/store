﻿namespace Store.API.Tests.Servicing.SalesReferences
{
    public class WhenGeneratingForANewCustomerOnMyc : AbstractGeneratingSalesReference
    {
        protected override string Source
        {
            get
            {
                return "Combell.controlpanel";
            }
        }

        protected override bool IsCheckedoutByEmployee
        {
            get
            {
                return false;
            }
        }

        protected override bool OneInvoicePerMonthForNewOrders
        {
            get
            {
                return false;
            }
        }

        protected override bool IsNewCustomer
        {
            get
            {
                return true;
            }
        }

        protected override string ExpectedSalesReference
        {
            get
            {
                return "AA1155";
            }
        }
    }
}
