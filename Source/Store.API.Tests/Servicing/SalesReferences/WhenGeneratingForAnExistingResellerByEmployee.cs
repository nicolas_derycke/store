﻿namespace Store.API.Tests.Servicing.SalesReferences
{
    public class WhenGeneratingForAnExistingResellerByEmployee : AbstractGeneratingSalesReference
    {
        protected override string Source
        {
            get
            {
                return "Combell.controlpanel";
            }
        }

        protected override bool IsCheckedoutByEmployee
        {
            get
            {
                return true;
            }
        }

        protected override bool OneInvoicePerMonthForNewOrders
        {
            get
            {
                return true;
            }
        }

        protected override bool IsNewCustomer
        {
            get
            {
                return false;
            }
        }

        protected override string ExpectedSalesReference
        {
            get
            {
                return "BB2299";
            }
        }
    }
}
