﻿using System.Threading.Tasks;
using Billing.API.Common.Interface.Contracts.Data;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.Proxies;
using Store.Services;

namespace Store.API.Tests.Servicing.SalesReferences
{
    [TestFixture]
    public abstract class AbstractGeneratingSalesReference : GivenWhenThen
    {
        private SalesReferenceService _salesReferenceService;

        private ICustomerProxy _customerProxy;
        private ICustomerInvoiceCriteriaProxy _customerInvoiceCriteriaProxy;

        private string _salesReference;

        private CustomerIdentifier _customerIdentifier;

        protected abstract string Source { get; }
        protected abstract bool IsCheckedoutByEmployee { get; }
        protected abstract bool OneInvoicePerMonthForNewOrders { get; }
        protected abstract bool IsNewCustomer { get; }
        protected abstract string ExpectedSalesReference { get; }

        protected override void Given()
        {
            _customerProxy = MockRepository.GenerateMock<ICustomerProxy>();
            _customerInvoiceCriteriaProxy = MockRepository.GenerateMock<ICustomerInvoiceCriteriaProxy>();

            _salesReferenceService = new SalesReferenceService(_customerProxy, _customerInvoiceCriteriaProxy);

            _customerIdentifier = CustomerIdentifier.FromCustomerNumber(129829);

            _customerProxy.
                Expect(x => x.IsNewCustomer(_customerIdentifier)).
                Return(Task.FromResult(IsNewCustomer)).
                Repeat.Once();

            _customerInvoiceCriteriaProxy.
                Expect(x => x.GetByCustomer(_customerIdentifier)).
                Return(new CustomerInvoiceCriteriaDataContract() { OneInvoicePerMonthForNewOrders = OneInvoicePerMonthForNewOrders }).
                Repeat.Once();
        }

        protected override void When()
        {
            _salesReference = _salesReferenceService.Generate(_customerIdentifier, Source, IsCheckedoutByEmployee).Result;
        }

        [Test]
        public void ShouldBeGenerated()
        {
            _salesReference.Should().Be(ExpectedSalesReference);
        }
    }
}