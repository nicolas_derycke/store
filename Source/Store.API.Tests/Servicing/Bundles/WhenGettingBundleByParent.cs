﻿namespace Store.API.Tests.Servicing.Bundles
{
    public class WhenGettingBundleByParent : AbstractWhenGettingBundleWithMultipleChilds
    {
        protected override void When()
        {
            _bundle = _bundleService.GetBundle(_basket, _parent);
        }
    }
}
