﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Core;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.Model;
using Store.Model.Baskets;
using Store.Services;

namespace Store.API.Tests.Servicing.Bundles
{
    [TestFixture]
    public abstract class AbstractWhenGettingBundleWithMultipleChilds : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aProduct;

        protected Basket _basket;
        protected BasketItem _child;
        protected BasketItem _parent;

        protected BundleService _bundleService;

        protected Optional<Bundle> _bundle;

        protected override void Given()
        {
            _given = new GenericGiven();
            var basketItems = new List<BasketItem>();

            _aProduct = _given.AProduct("linux-express", ProductTypes.LinuxSharedHosting);
            _parent = CreateABasketItemWithId(1, "zeno.be");
            basketItems.Add(_parent);

            _aProduct = _given.AProduct("domeinnaam-be", "domainname");
            _child = CreateABasketItemWithId(2, "zeno.be");
            basketItems.Add(_child);
            
            _aProduct = _given.AProduct("domeinnaam-be", "domainname"); 
            basketItems.Add(CreateABasketItemWithId(4, "zenodierick.be"));

            //_aProduct = _given.AProduct("comodo-positive-ssl-certificaat", "ssl-certs");
            //basketItems.Add(CreateABasketItemWithId(5, "dierick.be"));

            //_aProduct = _given.AProduct("comodo-positive-ssl-certificaat", "ssl-certs", 0); // it's already free
            //basketItems.Add(CreateABasketItemWithId(7, "zeno.be"));

            _basket = _given.ABasket(basketItems);

            _bundleService = new BundleService();
        }

        private BasketItem CreateABasketItemWithId(int id, string identifier)
        {
            return _given.ABasketItemForProduct(_aProduct, _aProduct.Period, 1, _given.GetHostingAttributes(identifier), id);
        }

        abstract protected override void When();

        [Test]
        public void ShouldFindBundle()
        {
            _bundle.HasValue.Should().BeTrue();
        }

        [Test]
        public void ShouldHaveCorrectParent()
        {
            var bundle = _bundle.Value;
            bundle.Parent.BasketItemId.Should().Be(1);
        }

        [Test]
        public void ShouldHaveCorrectChilds()
        {
            var bundle = _bundle.Value;
            bundle.Childs.Should().HaveCount(1);
            bundle.Childs.Should().Contain(c => c.BasketItemId == 2);
            //bundle.Childs.Should().Contain(c => c.BasketItemId == 7);
        }

        [Test]
        public void ShouldHaveCorrectFreeChild()
        {
            var bundle = _bundle.Value;
            bundle.FreeChild.Should().NotBeNull();
            //bundle.FreeChild.BasketItemId.Should().Be(7);
        }
    }
}