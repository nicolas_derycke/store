﻿namespace Store.API.Tests.Servicing.Bundles
{
    public class WhenGettingBundleByChild : AbstractWhenGettingBundleWithMultipleChilds
    {
        protected override void When()
        {
            _bundle = _bundleService.GetBundle(_basket, _child);
        }
    }
}
