﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Core;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.Model;
using Store.Model.Baskets;
using Store.Services;

namespace Store.API.Tests.Servicing.Bundles
{
    [TestFixture]
    public class WhenGettingBundleForNonBundleChild : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aProduct;

        private Basket _basket;
        private BasketItem _child;

        private BundleService _bundleService;

        private Optional<Bundle> _bundle;

        protected override void Given()
        {
            _given = new GenericGiven();
            var basketItems = new List<BasketItem>();

            _aProduct = _given.AProduct("linux-express", ProductTypes.LinuxSharedHosting);
            basketItems.Add(CreateABasketItemWithId(1, "zeno.be"));

            _aProduct = _given.AProduct("domeinnaam-fr", "domainname");
            _child = CreateABasketItemWithId(2, "zeno.be");
            basketItems.Add(_child);
            
            _aProduct = _given.AProduct("domeinnaam-be", "domainname"); 
            basketItems.Add(CreateABasketItemWithId(4, "zenodierick.be"));

            _aProduct = _given.AProduct("comodo-positive-ssl-certificaat", "ssl-certs");
            basketItems.Add(CreateABasketItemWithId(5, "dierick.be"));

            _aProduct = _given.AProduct("comodo-positive-ssl-certificaat", "ssl-certs");
            basketItems.Add(CreateABasketItemWithId(7, "zeno.be"));

            _basket = _given.ABasket(basketItems);

            _bundleService = new BundleService();
        }

        private BasketItem CreateABasketItemWithId(int id, string identifier)
        {
            return _given.ABasketItemForProduct(_aProduct, _aProduct.Period, 1, _given.GetHostingAttributes(identifier), id);
        }

        protected override void When()
        {
            _bundle = _bundleService.GetBundle(_basket, _child);
        }

        [Test]
        public void ShouldNotFindBundle()
        {
            _bundle.HasValue.Should().BeFalse();
        }
    }
}