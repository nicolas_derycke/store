﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Core;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Services;

namespace Store.API.Tests.Servicing.Bundles
{
    [TestFixture]
    public class WhenGettingBundleForFixedPriceChild : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aProduct;

        private Basket _basket;
        private BasketItem _child;
        protected BasketItem _parent;

        private BundleService _bundleService;

        private Optional<Bundle> _bundle;

        protected override void Given()
        {
            _given = new GenericGiven();
            var basketItems = new List<BasketItem>();

            _aProduct = _given.AProduct("linux-express", ProductTypes.LinuxSharedHosting);
            _parent = CreateABasketItemWithId(1, "zeno.be", false);
            basketItems.Add(_parent);
            
            _aProduct = _given.AProduct("domeinnaam-be", "domainname");
            _child = CreateABasketItemWithId(4, "zeno.be", true);
            _child.Attributes.Add(AttributeKey.PremiumDomain, new PremiumDomainAttribute());
            basketItems.Add(_child);

            _basket = _given.ABasket(basketItems);

            _bundleService = new BundleService();
        }

        private BasketItem CreateABasketItemWithId(int id, string identifier, bool isFixedPrice)
        {
            return _given.ABasketItemForProduct(_aProduct, _aProduct.Period, 1, _given.GetHostingAttributes(identifier), id, isFixedPrice: isFixedPrice);
        }

        protected override void When()
        {
            _bundle = _bundleService.GetBundle(_basket, _child);
        }

        [Test]
        public void ShouldNotFindBundle()
        {
            _bundle.HasValue.Should().BeFalse();
        }
    }
}