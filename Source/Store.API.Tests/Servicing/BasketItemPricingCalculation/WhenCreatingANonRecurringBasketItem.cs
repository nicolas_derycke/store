﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Providers;
using Store.Services;

namespace Store.API.Tests.Servicing.BasketItemPricingCalculation
{
    [TestFixture]
    public class WhenCreatingANonRecurringBasketItem : GivenWhenThen
    {
        private BasketItemPricingService _basketItemPricingService;

        private IProductInfoProvider _productInfoProvider;

        private List<ProductInfo> _productInfos;

        private CustomerIdentifier _customerIdentifier;
        private Provider _provider;
        private Language _language;
        private decimal _vatRatePercentage;

        private BasketItem _basketItem;

        protected override void Given()
        {
            _productInfoProvider = MockRepository.GenerateMock<IProductInfoProvider>();
            _basketItemPricingService = new BasketItemPricingService(_productInfoProvider);

            _customerIdentifier = CustomerIdentifier.FromCustomerNumber(129829);
            _provider = Provider.Combell;
            _language = Language.English;
            var period = 12;
            _vatRatePercentage = 0.21m;

            _basketItem = new BasketItem(
                "productCode",
                string.Empty,
                period,
                1,
                new ItemAttributeDictionary()
                {
                    { AttributeKey.Domain, new DomainItemAttributes("zeno.be") },
                    { AttributeKey.TransferDomain, new TransferDomainAttributes(string.Empty, true, true) }
                },
                _vatRatePercentage,
                false);

            _productInfos = new List<ProductInfo>()
                    {
                    new ProductInfo(
                        456,
                        string.Empty,
                        ProductTypes.DomainName,
                        string.Empty,
                        "Domeinnaam .be",
                        ProductTypes.DomainName,
                        852m,
                        753,
                        98.5m,
                        period,
                        ProductAction.Renewal,
                        true),
                    new ProductInfo(
                        4561,
                        string.Empty,
                        ProductTypes.DomainName,
                        string.Empty,
                        "Domeinnaam .be",
                        ProductTypes.DomainName,
                        8521m,
                        7531,
                        981.5m,
                        period,
                        ProductAction.Transfer,
                        true),
                    new ProductInfo(
                        4562,
                        string.Empty,
                        ProductTypes.DomainName,
                        string.Empty,
                        "Domeinnaam .be",
                        ProductTypes.DomainName,
                        8522m,
                        7532,
                        982.5m,
                        period,
                        ProductAction.Register,
                        true)
                };

            _productInfoProvider.
                Expect(x => x.GetProductInfo(_basketItem, _customerIdentifier, _provider.Id, _language)).
                Return(_productInfos).
            Repeat.Once();
        }

        protected override void When()
        {
            _basketItemPricingService.SetProductInfoAndPrice(_basketItem, _customerIdentifier, _provider, _language, _vatRatePercentage);
        }

        [Test]
        public void ShouldStillBeNonRecurring()
        {
            _basketItem.IsRecurring.Should().BeFalse();
        }
    }
}