﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Providers;
using Store.Services;

namespace Store.API.Tests.Servicing.BasketItemPricingCalculation
{
    [TestFixture]
    public class WhenCreatingADomainBasketItem : GivenWhenThen
    {
        private BasketItemPricingService _basketItemPricingService;

        private IProductInfoProvider _productDetailsProvider;
        private IProductGroupService _productGroupService;

        private BasketItem _createdBasketItem;

        private CustomerIdentifier _customerIdentifier;
        private Provider _provider;
        private decimal _vatRatePercentage;
        private BasketItemContract _basketItemContract;
        private Language _language;
        private int _parentId;
        private bool _isRecurring;
        private ProductInfo _productInfo;

        protected override void Given()
        {
            _productDetailsProvider = MockRepository.GenerateMock<IProductInfoProvider>();
            _productGroupService = MockRepository.GenerateMock<IProductGroupService>();

            _basketItemPricingService = new BasketItemPricingService(_productDetailsProvider);

            _customerIdentifier = CustomerIdentifier.FromCustomerNumber(129829);
            _provider = Provider.Combell;
            _vatRatePercentage = 0.99m;
            _basketItemContract =
                new BasketItemContract(
                    1,
                    "domeinnaam-be",
                    "be",
                    "",
                    "Domeinnaam .be",
                    12,
                    1,
                    new ItemAttributeDictionary()
                    {
                        { AttributeKey.Domain, new DomainItemAttributes("zeno.be") }
                    },
                    true,
                    987,
                    null);
            _language = Language.German;
            _parentId = 123;
            _isRecurring = true;

            _productInfo =
                new ProductInfo(
                    456,
                    _basketItemContract.ProductCode,
                    _basketItemContract.ProductType,
                    _basketItemContract.PromoProductCode,
                    _basketItemContract.ProductName,
                    ProductTypes.DomainName,
                    852m,
                    753,
                    98.5m,
                    _basketItemContract.Period,
                    ProductAction.Renewal,
                    true);

            _createdBasketItem = new BasketItem(
                _basketItemContract.ProductCode,
                _basketItemContract.PromoProductCode,
                _basketItemContract.Period,
                _basketItemContract.Quantity,
                _basketItemContract.Attributes,
                _vatRatePercentage,
                _basketItemContract.IsRecurring,
                _parentId);
            _createdBasketItem.ValidationError = BasketItemValidationError.InactiveProductPeriod;

            _productDetailsProvider.
                Expect(
                x => x.GetProductInfo(
                    _createdBasketItem,
                    _customerIdentifier,
                    _provider.Id,
                    _language)).
                Return(new List<ProductInfo>() { _productInfo }).
                Repeat.Once();
        }

        protected override void When()
        {
            _basketItemPricingService.SetProductInfoAndPrice(_createdBasketItem, _customerIdentifier, _provider, _language, _vatRatePercentage);
        }

        [Test]
        public void ShouldContainAllData()
        {
            _productDetailsProvider.VerifyAllExpectations();

            _createdBasketItem.Should().NotBeNull();

            _createdBasketItem.ActualProductCode.Should().Be(_basketItemContract.ProductCode);
            _createdBasketItem.BasketItemId.Should().Be(0);
            _createdBasketItem.ParentId.Should().Be(_parentId);
            _createdBasketItem.IsRecurring.Should().Be(_isRecurring);
            _createdBasketItem.Period.Should().Be(_basketItemContract.Period);
            _createdBasketItem.PeriodId.Should().Be(_productInfo.PeriodId);
            _createdBasketItem.ProductCode.Should().Be(_basketItemContract.ProductCode);
            _createdBasketItem.ProductId.Should().Be(_productInfo.ProductId);
            _createdBasketItem.ProductName.Should().Be(_productInfo.ProductName);
            _createdBasketItem.PromoProductCode.Should().Be(_basketItemContract.PromoProductCode);
            _createdBasketItem.Quantity.Should().Be(_basketItemContract.Quantity);

            _createdBasketItem.Price.Should().NotBeNull();
            _createdBasketItem.Price.ExclVat.Should().Be(_productInfo.PeriodPrice);
            _createdBasketItem.Price.ReductionExclVat.Should().Be(_productInfo.Reduction);
        }

        [Test]
        public void ShouldTheProductTypeBeDomainName()
        {
            _createdBasketItem.ProductType.Should().Be(ProductTypes.DomainName);
        }

        [Test]
        public void ShouldNotHaveInactiveProductPeriodError()
        {
            _createdBasketItem.ValidationError = BasketItemValidationError.None;
        }
    }
}
