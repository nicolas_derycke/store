﻿using System.Linq;
using Billing.API.Contracts;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts.Baskets;
using Store.Commands.BasketItems;

namespace Store.API.Tests.Servicing
{
    [TestFixture]
    public class WhenCreatingBasketItemsForCurrentInvoiceLineNotFoundCorrections : AbstractWhenCreatingBasketItemsForCorrections
    {
        protected override InvoiceCorrectionStatusContract InvoiceCorrectionStatus
        {
            get
            {
                return InvoiceCorrectionStatusContract.CurrentInvoiceLineNotFound;
            }
        }

        protected override BasketItemValidationError ExpectedValidationError
        {
            get
            {
                return BasketItemValidationError.None;
            }
        }

        protected override void ConfigureCommandBusExpectations()
        {
            _commandBus.Expect(x => x.Send((Arg<CreateBasketItemCommand>.Is.Anything))).Repeat.Never();
        }

        [Test]
        public void ShouldNotCreateBasketItems()
        {
            _commandBus.VerifyAllExpectations();
        }

        [Test]
        public void ShouldNotAddBasketItems()
        {
            _basket.Items.Should().HaveCount(1);
            _basket.Items.Any(i => i.PeriodId == _periodId && i.ValidationError == ExpectedValidationError).Should().BeFalse();
        }
    }
}