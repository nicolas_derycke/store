﻿using System;
using System.Collections.Generic;
using Billing.API.Contracts;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using Rhino.Mocks;
using Store.API.Contracts.Baskets;
using Store.Commands.BasketItems;
using Store.Model;
using Store.Model.Baskets;
using Store.Services;
using Store.SqlCommands;

namespace Store.API.Tests.Servicing
{
    abstract public class AbstractWhenCreatingBasketItemsForCorrections : GivenWhenThen
    {
        protected GenericGiven _given;
        private ProductInfo _aLinuxPromo;

        protected ICommandBus _commandBus;
        protected ISqlCommandHandler _sqlCommandHandler; 

        private InvoiceCorrectionService _invoiceCorrectionService;

        private InvoiceCorrectionContract _invoiceCorrectionContract;
        protected Basket _basket;
        private string _identifier;
        private BasketItem _basketItem;

        protected int _periodId;

        abstract protected InvoiceCorrectionStatusContract InvoiceCorrectionStatus { get; }

        abstract protected BasketItemValidationError ExpectedValidationError { get; }

        protected override void Given()
        {
            _periodId = 123;

            _commandBus = MockRepository.GenerateMock<ICommandBus>();
            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();

            _invoiceCorrectionService = new InvoiceCorrectionService(_commandBus, _sqlCommandHandler);

            _invoiceCorrectionContract =
                new InvoiceCorrectionContract(
                    InvoiceCorrectionStatus,
                    new List<InvoiceCorrectionLineContract>()
                    {
                        new InvoiceCorrectionLineContract(new DateTime(2015, 01, 01), new DateTime(2016, 01, 01), 36, 25.6m, _periodId, 987654, "Linux express", "linux-express", "linux-webhosting", 951)
                    });

            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            _basketItem = CreateABasketItemWithId(111);
            var basketItems = new List<BasketItem>
            {
                _basketItem
            };

            _identifier = "zeno.be";

            _basket = CreateBasket(basketItems);

            ConfigureCommandBusExpectations();
        }

        protected virtual Basket CreateBasket(List<BasketItem> basketItems)
        {
            return _given.ABasket(basketItems);
        }

        protected virtual void ConfigureSqlCommandHandlerExpectations()
        {
            // none
        }

        protected virtual void ConfigureCommandBusExpectations()
        {
            _commandBus.Expect(x => x.Send(Arg<CreateBasketItemCommand>.Matches(arg =>
                  arg.Basket == _basket &&
                  arg.BasketItem.ActualProductCode == "linux-express" &&
                  arg.BasketItem.IsRecurring == false &&
                  arg.BasketItem.IsUpgrade == false &&
                  arg.BasketItem.ParentId == 111 &&
                  arg.BasketItem.Period == 36 &&
                  arg.BasketItem.PeriodId == _periodId &&
                  arg.BasketItem.Price.ExclVat == 25.6m &&
                  arg.BasketItem.ProductId == 951 &&
                  arg.BasketItem.ProductName == "Linux express" &&
                  arg.BasketItem.ProductType == "linux-webhosting" &&
                  arg.BasketItem.Quantity == -1 &&
                  arg.BasketItem.ValidationError == ExpectedValidationError &&
                  arg.BasketItem.Warning == BasketItemWarning.None))).
                  Repeat.Once();
        }

        protected BasketItem CreateABasketItemWithId(int id, int? parentId = null)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, _given.GetHostingAttributes(), id, parentId);
        }

        protected override void When()
        {
            _invoiceCorrectionService.CreateBasketItems(_invoiceCorrectionContract, _basket, _identifier, _basketItem);
        }
    }
}