﻿using System.Linq;
using Billing.API.Contracts;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts.Baskets;

namespace Store.API.Tests.Servicing
{
    [TestFixture]
    public class WhenCreatingBasketItemsForNegativeCorrections : AbstractWhenCreatingBasketItemsForCorrections
    {
        protected override InvoiceCorrectionStatusContract InvoiceCorrectionStatus
        {
            get
            {
                return InvoiceCorrectionStatusContract.NegativeCorrection;
            }
        }

        protected override BasketItemValidationError ExpectedValidationError
        {
            get
            {
                return BasketItemValidationError.NegativeBasketItemPrice;
            }
        }

        [Test]
        public void ShouldCreateBasketItems()
        {
            _commandBus.VerifyAllExpectations();
        }

        [Test]
        public void ShouldAddBasketItems()
        {
            _basket.Items.Should().HaveCount(2);
            _basket.Items.Any(i => i.PeriodId == _periodId && i.ValidationError == ExpectedValidationError).Should().BeTrue();
        }
    }
}