﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Validation;

namespace Store.API.Tests.Validating
{
    public class WhenValidatingDuplicateProductCombinationSameIdentifierSameProductCode : GivenWhenThen
    {
        private Basket _duplicateProductCombinationBasket;
        private Basket _duplicateProductCombinationBasketWithoutIdentifier;
        private BasketItem _duplicateBasketItem;
        private BasketItem _duplicateBasketItemWithoutIdentifier;
        private GenericGiven _given;
        private DuplicateProductCombinationValidator _duplicateProductCombinationValidator;
        private DuplicateProductCombinationValidator _duplicateProductCombinationValidatorWithoutIdentifier;
        private IEnumerable<BasketItemValidationError> _basketItemValidationErrors;
        private IEnumerable<BasketItemValidationError> _basketItemValidationErrorsWithoutIdentifier;
        protected override void Given()
        {
            _given = new GenericGiven();
            _duplicateProductCombinationValidator = new DuplicateProductCombinationValidator();
            _duplicateProductCombinationValidatorWithoutIdentifier = new DuplicateProductCombinationValidator();

            _duplicateProductCombinationBasket = _given.ABasket(new List<BasketItem>
            {
                _given.ABasketItemForProduct(_given.AProduct("basic-e-mail-25-gb", ProductTypes.BasicEmail),1,1,new ItemAttributeDictionary { new ItemAttributeKeyValuePair(AttributeKey.Domain, new DomainItemAttributes("test.be")) }),
            });

            _duplicateBasketItem = _given.ABasketItemForProduct(
                _given.AProduct("basic-e-mail-25-gb", ProductTypes.BasicEmail),
                1,
                1,
                new ItemAttributeDictionary
                {
                    new ItemAttributeKeyValuePair(AttributeKey.Domain, new DomainItemAttributes("test.be"))
                });


            _duplicateProductCombinationBasketWithoutIdentifier = _given.ABasket(new List<BasketItem>
            {
                _given.ABasketItemForProduct(_given.AProduct("basic-e-mail-25-gb", ProductTypes.BasicEmail),1,1,new ItemAttributeDictionary { }),
            });

            _duplicateBasketItemWithoutIdentifier = _given.ABasketItemForProduct(
                _given.AProduct("basic-e-mail-25-gb", ProductTypes.BasicEmail),
                1,
                1,
                new ItemAttributeDictionary{});
        }

        protected override void When()
        {
            _basketItemValidationErrors = _duplicateProductCombinationValidator.Validate(_duplicateBasketItem, _duplicateProductCombinationBasket);
            _basketItemValidationErrorsWithoutIdentifier =
                _duplicateProductCombinationValidatorWithoutIdentifier.Validate(_duplicateBasketItemWithoutIdentifier,
                    _duplicateProductCombinationBasketWithoutIdentifier);
        }

        [Test]
        public void ValidatorShouldReturnDuplicateProductCombinationForSameIdentifierOnSameProductCode()
        {
            _basketItemValidationErrors.Should().Contain(BasketItemValidationError.DuplicateProductCombination);
            _basketItemValidationErrorsWithoutIdentifier.Should()
                .Contain(BasketItemValidationError.DuplicateProductCombination);
        }
    }
}
