﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billing.API.Common.Interface.Contracts.Commands;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.Validation;

namespace Store.API.Tests.Validating
{
    public class WhenValidatingQuantityOfProductTypeInABasket : GivenWhenThen
    {
        private BasketItem _linuxHostingBasketItem;
        private Action _quantityHigherThenAllowed;
        private Action _quantityZero;
        private Action _quantityLowerThenAllowed;
        private GenericGiven _given;
        protected override void Given()
        {
            _given = new GenericGiven();
            _linuxHostingBasketItem =
                _given.ABasketItemForProduct(_given.ALinuxExpressProduct(12), 12, 2,
                    new ItemAttributeDictionary(), 1234);

        }

        protected override void When()
        {
            _quantityHigherThenAllowed = () => QuantityValidator.ThrowIfInvalid(_linuxHostingBasketItem, 2);
            _quantityZero = () => QuantityValidator.ThrowIfInvalid(_linuxHostingBasketItem, 0);
            _quantityLowerThenAllowed = () => QuantityValidator.ThrowIfInvalid(_linuxHostingBasketItem, -1);
        }

        [Test]
        public void ShouldThrowInvalidBasketItemQuantity()
        {
            _quantityHigherThenAllowed.ShouldThrow<InvalidBasketItemQuantityException>();
            _quantityZero.ShouldThrow<InvalidBasketItemQuantityException>();
            _quantityLowerThenAllowed.ShouldThrow<InvalidBasketItemQuantityException>();
        }
    }
}
