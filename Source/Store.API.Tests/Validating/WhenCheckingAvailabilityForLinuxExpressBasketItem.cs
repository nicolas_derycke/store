﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Validation;

namespace Store.API.Tests.Validating
{
    [TestFixture]
    public class WhenCheckingAvailabilityForLinuxExpressBasketItem : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aLinuxPromo;

        private DomainAvailabilityValidator _domainAvailabilityValidator;

        private Basket _basket;
        private BasketItem _basketItem;

        private IEnumerable<BasketItemValidationError> _validationErrors;

        protected override void Given()
        {
            _domainAvailabilityValidator = new DomainAvailabilityValidator(MockRepository.GenerateMock<IIntelliPropertiesProvider>());

            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            _basketItem = CreateABasketItemWithId(1);
            var basketItems = new List<BasketItem>
            {
                _basketItem
            };
            _basket = _given.ABasket(basketItems);
        }

        private BasketItem CreateABasketItemWithId(int id)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, _given.GetHostingAttributes(), id, null);
        }

        protected override void When()
        {
            _validationErrors = _domainAvailabilityValidator.Validate(_basketItem, _basket);
        }

        [Test]
        public void ShouldSkipValidation()
        {
            _validationErrors.Should().BeEmpty();
        }
    }
}
