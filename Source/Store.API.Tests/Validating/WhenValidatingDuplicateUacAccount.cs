﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using Store.Validation;
using UAC.Queries.Model.Entities;

namespace Store.API.Tests.Validating
{
    public class WhenValidatingDuplicateUacAccount : GivenWhenThen
    {
        private IUacProxy _uacProxy;
        private DuplicateUacAccountValidator _duplicateUacValidator;
        private Basket _basket;
        private BasketItem _basketItem;
        private GenericGiven _given;
        private IEnumerable<BasketItemValidationError> _basketItemValidationErrors;

        protected override void Given()
        {
            _given =new GenericGiven();
            _uacProxy = A.Fake<IUacProxy>();
            _duplicateUacValidator = new DuplicateUacAccountValidator(_uacProxy);
            
            A.CallTo(() => _uacProxy.GetCustomersByResourceSetAndIdentifier(A<ResourceName>.That.Matches(r => r == ResourceName.Domainnames), A<string>._))
                .Returns(new List<CustomerIdentifier>{ CustomerIdentifier.FromCustomerId(123456) });

            _basket = _given.ABasket(new List<BasketItem>{}, CustomerIdentifier.FromCustomerId(123456));
            _basketItem = _given.ABasketItemForProduct(
                 _given.AProduct("testen", ProductTypes.DomainName),
                 1,
                 1,
                 new ItemAttributeDictionary
                 {
                    new ItemAttributeKeyValuePair(AttributeKey.Domain, new DomainItemAttributes("test.be"))
                 });
        }

        protected override void When()
        {
            _basketItemValidationErrors = _duplicateUacValidator.Validate(_basketItem, _basket);
        }

        [Test]
        public void ShouldContainDuplicateUacAccountError()
        {
            _basketItemValidationErrors.Should().Contain(BasketItemValidationError.UacAccountAlreadyExists);
        }
    }
}
