using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using MassTransit;
using Rhino.Mocks;
using Serilog;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Data;
using Store.Messages.Commands;
using Store.Model;
using Store.Model.Baskets;
using Store.Providers;
using Store.Service;
using Store.Service.CommandHandlers;
using Store.Services;
using Store.SqlCommands;
using Store.SqlQueries;
using Store.Validation;
using Store.Warnings;

namespace Store.API.Tests
{
    public class GenericGiven
    {
        private readonly IProductInfoProvider _productDetailsProvider;
        private CustomerIdentifier _customerIdentifier;
        private Intelligent.Core.Models.Provider _provider;

        public GenericGiven(ProviderValue providerValue = ProviderValue.Combell) : this(null, providerValue) { }

        protected GenericGiven(CustomerIdentifier customerIdentifier, ProviderValue providerValue)
        {
            _customerIdentifier = customerIdentifier ?? CustomerIdentifier.FromCustomerId(0);
            _provider = new Intelligent.Core.Models.Provider(providerValue);

            _productDetailsProvider = CreateProductInfoProvider();
            VatRate = 0.21M;
        }

        public DeleteExpiredBasketsHandler CreateDeleteExpiredBasketsHandler(IQueryHandler queryHandler, ISqlCommandHandler commandHandler, ILogger logger)
        {
            var appSettingsConfiguration = A.Fake<IAppSettingsConfiguration>();

            A.CallTo(() => appSettingsConfiguration.ExpireAnonymousBasketOffsetInDays).Returns(1);
            A.CallTo(() => appSettingsConfiguration.ExpiredRegularBasketOffsetInDays).Returns(1);
            A.CallTo(() => appSettingsConfiguration.ExpiredUpgradeBasketOffsetInDays).Returns(1);

            var deleteExpiredBasketsHandler = new DeleteExpiredBasketsHandler(queryHandler, commandHandler, logger, appSettingsConfiguration);

            return deleteExpiredBasketsHandler;
        }

        public ValidateBasketHandler CreateValidateBasketHandler(Basket basket, ISqlCommandHandler commandHandler, ILogger logger)
        {
            var appSettingsConfiguration = A.Fake<IAppSettingsConfiguration>();
            A.CallTo(() => appSettingsConfiguration.RevalidationOffsetInDays).Returns(1);

            var commandBus = A.Fake<ICommandBus>();
            var basketService = A.Fake<IBasketService>();

            var validator = A.Fake<IBasketItemValidator>();
            var warning = A.Fake<IWarningProvider>();

            A.CallTo(() => validator.Validate(basket.Items.First(), basket))
                .Returns(new List<BasketItemValidationError> { BasketItemValidationError.AccountBelongsToOtherCustomer });

            A.CallTo(() => warning.Validate(basket.Items.First(), basket))
                .Returns(new List<BasketItemWarning> { BasketItemWarning.ExtraFieldsRequired });

            var validators = new List<IBasketItemValidator>
            {
                validator
            };

            var warnings = new List<IWarningProvider>
            {
                warning
            };

            var _handler = new ValidateBasketHandler(
               appSettingsConfiguration,
               logger,
               validators,
               commandHandler,
               warnings,
               basketService,
               commandBus);

            A.CallTo(() => basketService.GetBasketByOrderCodeToValidate(basket.OrderCode))
                .Returns(basket
                    );

            return _handler;
        }

        public ProductInfoProvider CreateProductInfoProvider(int customerReductionPercentage = 0)
        {
            var queryHandler = MockRepository.GenerateMock<IQueryHandler>();
            var productGroupService = MockRepository.GenerateMock<IProductGroupService>();

            queryHandler
                .Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "linux-express" && qry.CustomerId != 0)))
                .Return(ProductDetailFactory.GetList("linux-express", customerReductionPercentage));

            queryHandler
                .Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "linux-express-promo" && qry.CustomerId != 0)))
                .Return(ProductDetailFactory.GetList("linux-express-promo", customerReductionPercentage));

            queryHandler
                .Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "performancehosting" && qry.CustomerId != 0)))
                .Return(ProductDetailFactory.GetList("performancehosting", customerReductionPercentage));

            queryHandler
                .Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "linux-express" && qry.CustomerId == 0)))
                .Return(ProductDetailFactory.GetList("linux-express"));

            queryHandler
                .Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "linux-express-promo" && qry.CustomerId == 0)))
                .Return(ProductDetailFactory.GetList("linux-express-promo"));

            queryHandler
                .Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "performancehosting" && qry.CustomerId == 0)))
                .Return(ProductDetailFactory.GetList("performancehosting"));

            queryHandler
                .Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "nullProduct")))
                .Return(Enumerable.Empty<ProductDetailsData>());

            return new ProductInfoProvider(queryHandler, productGroupService);
        }

        public GenericGiven WithCustomerIdentifier(CustomerIdentifier customerIdentifier)
        {
            _customerIdentifier = customerIdentifier;
            return this;
        }

        public GenericGiven WithProvider(Intelligent.Core.Models.Provider provider)
        {
            _provider = provider;
            return this;
        }

        public GenericGiven WithDefaultVatRate(decimal vatRate)
        {
            VatRate = vatRate;
            return this;
        }

        public decimal VatRate { get; private set; }

        public decimal VatMultiplier
        {
            get { return VatRate + 1; }
        }

        public ProductInfo APerformanceHosting(int period)
        {
            return _productDetailsProvider.GetProductInfo("performancehosting", "", period, _customerIdentifier, _provider.Id, Language.English, null).First();
        }

        public ProductInfo ALinuxExpressProduct(int period)
        {
            return _productDetailsProvider.GetProductInfo("linux-express", "", period, _customerIdentifier, _provider.Id, Language.English, null).First();
        }

        public ProductInfo ALinuxExpressPromoProduct(int period)
        {
            return _productDetailsProvider.GetProductInfo("linux-express", "linux-express-promo", period, _customerIdentifier, _provider.Id, Language.English, null).First();
        }

        public ProductInfo AProduct(string productCode, string productGroupCode, decimal periodPrice = 5, int period = 12)
        {
            return new ProductInfo(
                1,
                productCode,
                productGroupCode,
                string.Empty,
                productCode.Replace("-", ""),
                productGroupCode,
                periodPrice,
                2,
                0,
                12,
                ProductAction.Renewal, 
                true);
        }

        public Basket ABasket(IEnumerable<BasketItem> basketItems, CustomerIdentifier customerIdentifer, string orderCode, decimal vatRatePercentage = 21)
        {
            return new Basket(0, orderCode, customerIdentifer, basketItems, vatRatePercentage, DateTime.Now, false, DateTime.Now, Provider.Combell, BasketType.Regular, false, "combell.store", Language.Dutch, false,"");
        }

        public Basket ABasket(IEnumerable<BasketItem> basketItems, CustomerIdentifier customerIdentifer, decimal vatRatePercentage = 21)
        {
            if (basketItems == null) throw new ArgumentNullException("basketItems");
            return new Basket(0, "COM-20151130-100", customerIdentifer, basketItems, vatRatePercentage, DateTime.Now, false, DateTime.Now, Provider.Combell, BasketType.Regular, false, "combell.store", Language.Dutch, false,"");
        }

        public Basket ABasket(IEnumerable<BasketItem> basketItems, decimal vatRatePercentage = 21, Language language = Language.Dutch)
        {
            return new Basket(0, "COM-20151130-100", null, basketItems, vatRatePercentage, DateTime.Now, false, DateTime.Now, Provider.Combell, BasketType.Regular, false, "combell.store", language, false,"");
        }

        public Basket ADeletedBasket(IEnumerable<BasketItem> basketItems, DateTime lastValidationDate, decimal vatRatePercentage = 21, Language language = Language.Dutch)
        {
            return new Basket(0, "COM-20151130-100", null, basketItems, vatRatePercentage, DateTime.Now, false, lastValidationDate, Provider.Combell, BasketType.Regular, false, "combell.store", language, true,"");
        }

        public Basket AnUpgradeBasket(IEnumerable<BasketItem> basketItems, decimal vatRatePercentage = 21)
        {
            return new Basket(0, "COM-20151130-100", null, basketItems, vatRatePercentage, DateTime.Now, false, DateTime.Now, Provider.Combell, BasketType.Standalone, false, "combell.store", Language.Dutch, false,"");
        }

        public Basket AnUpgradeBasket(IEnumerable<BasketItem> basketItems, CustomerIdentifier customerIdentifer, string orderCode, decimal vatRatePercentage = 21)
        {
            return new Basket(0, orderCode, customerIdentifer, basketItems, vatRatePercentage, DateTime.Now, false, DateTime.Now, Provider.Combell, BasketType.Standalone, false, "combell.store", Language.Dutch, false,"");
        }

        public Basket ABasket(IEnumerable<BasketItem> basketItems, DateTime lastValidationDate)
        {
            return new Basket(0, "COM-20151130-100", null, basketItems, 21.0m, DateTime.Now, false, lastValidationDate, Provider.Combell, BasketType.Regular, false, "combell.store", Language.Dutch, false,"");
        }

        public Basket ABasketIsRecalculating(IEnumerable<BasketItem> basketItems, DateTime lastValidationDate)
        {
            return new Basket(0, "COM-20151130-100", null, basketItems, 21.0m, DateTime.Now, false, lastValidationDate, Provider.Combell, BasketType.Regular, true, "combell.store", Language.Dutch, false,"");
        }

        public IProductInfoProvider AProductProvider()
        {
            return _productDetailsProvider;
        }

        public BasketItem ABasketItemForProduct(ProductInfo productInfo, int period, int quantity, ItemAttributeDictionary attributes, int basketItemId = 0, int? parentId = null, bool isRecurring = true, BasketItemValidationError validationError = BasketItemValidationError.None, bool isFixedPrice = false, string promoCode = "")
        {
            var vatRatePercentage = 21m;

            return new BasketItem(
                basketItemId,
                productInfo.ProductId,
                productInfo.ProductCode,
                productInfo.ProductGroupCode,
                productInfo.PromoProductCode,
                productInfo.ProductName,
                period,
                productInfo.PeriodId,
                quantity,
                new Price(productInfo.PeriodPrice, productInfo.Reduction, vatRatePercentage),
                attributes,
                vatRatePercentage,
                validationError,
                BasketItemWarning.None,
                isRecurring,
                isFixedPrice,
                parentId,
                promoCode);
        }

        public ItemAttributeDictionary UpgradeAttributes
        {
            get
            {
                return new ItemAttributeDictionary(new Dictionary<AttributeKey, IBasketItemAttribute>()
                {
                    {
                        AttributeKey.Domain,
                        new DomainItemAttributes("zeno.be")
                    },
                    {
                        AttributeKey.Hosting,
                        new HostingAttributes(12356)
                    }
                });
            }
        }

        public ItemAttributeDictionary GetHostingAttributes(string identifier = "testdomain.be")
        {
            return new ItemAttributeDictionary(new Dictionary<AttributeKey, IBasketItemAttribute>()
                {
                    {
                        AttributeKey.Domain,
                        new DomainItemAttributes(identifier)
                    }
                });
        }

        public ItemAttributeDictionary AddonAttributes
        {
            get
            {
                return new ItemAttributeDictionary(new Dictionary<AttributeKey, IBasketItemAttribute>()
                {
                    {
                        AttributeKey.UacAccountAddon,
                        new UacAccountAddonAttributes(12, null)
                    }
                });
            }
        }

        public ItemAttributeDictionary AddonUpgradeAttributes
        {
            get
            {
                return new ItemAttributeDictionary(new Dictionary<AttributeKey, IBasketItemAttribute>()
                {
                    {
                        AttributeKey.Domain,
                        new DomainItemAttributes("testdomain.be")
                    },
                    {
                        AttributeKey.UacAccountAddon,
                        new UacAccountAddonAttributes(12, 5)
                    }
                });
            }
        }
    }
}