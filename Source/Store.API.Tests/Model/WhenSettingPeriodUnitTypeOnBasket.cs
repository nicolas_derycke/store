﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts.Baskets;
using Store.Mapping;
using Store.Model;
using Store.Model.Baskets;

namespace Store.API.Tests.Model
{
    [TestFixture]
    public class WhenSettingPeriodUnitTypeOnBasket : GivenWhenThen
    {
        private Basket _basket;
        private BasketItem _basketItem;
        private ProductInfo _aLinuxPromo;
        private const int _productPeriod = 3;
        private readonly PeriodUnitType _periodUnitTypeToSet = PeriodUnitType.OneMonth;
        private BasketContract _basketContract;

        protected override void Given()
        {
            MappingConfiguration.ConfigureMappings();
            var given = new GenericGiven();

            _aLinuxPromo = given.ALinuxExpressPromoProduct(_productPeriod);

            _basketItem = given.ABasketItemForProduct(_aLinuxPromo, _productPeriod, 1, given.GetHostingAttributes());
            _basket = given.ABasket(new List<BasketItem>{ _basketItem });
        }

        protected override void When()
        {
            _basket.SetPeriodUnitTypeForBasketItems(_periodUnitTypeToSet);
            _basketContract = _basket.MapTo<BasketContract>();
        }

        [Test]
        public void PeriodUnitPriceShouldBeRecalculated()
        {
            var mappedBasketItem = _basketContract.Items.First(b => b.BasketItemId == _basketItem.BasketItemId);
            var expectedResultExclVat = (_basketItem.Price.ExclVat / _productPeriod) * (int)_periodUnitTypeToSet;
            var expectedResultInclVat = (_basketItem.Price.InclVat / _productPeriod) * (int)_periodUnitTypeToSet;
            var expectedResultReductionExclVat = (_basketItem.Price.ReductionExclVat / _productPeriod) * (int)_periodUnitTypeToSet;
            var expectedResultReductionInclVat = (_basketItem.Price.ReductionInclVat / _productPeriod) * (int)_periodUnitTypeToSet;
            mappedBasketItem.PeriodUnitPrice.ExclVat.Should().Be(expectedResultExclVat);
            mappedBasketItem.PeriodUnitPrice.InclVat.Should().Be(expectedResultInclVat);
            mappedBasketItem.PeriodUnitPrice.ReductionExclVat.Should().Be(expectedResultReductionExclVat);
            mappedBasketItem.PeriodUnitPrice.ReductionInclVat.Should().Be(expectedResultReductionInclVat);
        }
    }
}
