﻿using System.Collections.Generic;
using Store.Model.ExtraFields;

namespace Store.API.Tests.Model.ExtraFieldRules
{
    public class WhenMatchingForCompanies : AbstractWhenMatchingExtraFieldRules
    {
        protected override ExtraFieldRule GetExtraFieldRule()
        {
            return new ExtraFieldRuleBuilder().ForExtension("fr").ForCompanies().Build();
        }

        protected override List<ExtraFieldRuleTest> GetExamples()
        {
            return new List<ExtraFieldRuleTest>()
            {
                new ExtraFieldRuleTest("zeno.fr", "zeno nv", "fr", true),
                new ExtraFieldRuleTest("zeno.fr", "", "fr", false),
                new ExtraFieldRuleTest("zeno.fr", "zeno nv", "be", true),
                new ExtraFieldRuleTest("zeno.fr", "", "be", false)
            };
        }
    }
}
