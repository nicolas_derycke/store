﻿using System.Collections.Generic;
using Store.Model.ExtraFields;

namespace Store.API.Tests.Model.ExtraFieldRules
{
    public class WhenMatchingForAnyoneOutside : AbstractWhenMatchingExtraFieldRules
    {
        protected override ExtraFieldRule GetExtraFieldRule()
        {
            return new ExtraFieldRuleBuilder().ForExtension("ca").ForAnyone().Outside("ca").Build();
        }

        protected override List<ExtraFieldRuleTest> GetExamples()
        {
            return new List<ExtraFieldRuleTest>()
            {
                new ExtraFieldRuleTest("zeno.ca", "zeno nv", "ca", false),
                new ExtraFieldRuleTest("zeno.ca", "", "ca", false),
                new ExtraFieldRuleTest("zeno.ca", "zeno nv", "be", true),
                new ExtraFieldRuleTest("zeno.ca", "", "be", true)
            };
        }
    }
}
