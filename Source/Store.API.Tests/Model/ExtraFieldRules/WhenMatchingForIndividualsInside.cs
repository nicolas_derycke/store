﻿using System.Collections.Generic;
using Store.Model.ExtraFields;

namespace Store.API.Tests.Model.ExtraFieldRules
{
    public class WhenMatchingForIndividualsInside : AbstractWhenMatchingExtraFieldRules
    {
        protected override ExtraFieldRule GetExtraFieldRule()
        {
            return new ExtraFieldRuleBuilder().ForExtension("it").ForIndividuals().Inside("it").Build();
        }

        protected override List<ExtraFieldRuleTest> GetExamples()
        {
            return new List<ExtraFieldRuleTest>()
            {
                new ExtraFieldRuleTest("zeno.it", "zeno nv", "it", false),
                new ExtraFieldRuleTest("zeno.it", "", "it", true),
                new ExtraFieldRuleTest("zeno.it", "zeno nv", "be", false),
                new ExtraFieldRuleTest("zeno.it", "", "be", false)
            };
        }
    }
}
