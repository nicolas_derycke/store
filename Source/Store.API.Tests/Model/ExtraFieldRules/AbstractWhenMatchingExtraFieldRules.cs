﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.Model.ExtraFields;

namespace Store.API.Tests.Model.ExtraFieldRules
{
    [TestFixture]
    public abstract class AbstractWhenMatchingExtraFieldRules : GivenWhenThen
    {
        private ExtraFieldRule _rule;

        private List<ExtraFieldRuleTest> _examples;

        protected override void Given()
        {
            _rule = GetExtraFieldRule();
            _examples = GetExamples();
        }

        abstract protected ExtraFieldRule GetExtraFieldRule();

        abstract protected List<ExtraFieldRuleTest> GetExamples();

        protected override void When()
        {
            foreach (var example in _examples)
                example.Result = _rule.IsMatch(new DomainName(example.DomainName), example.Company, example.CountryCode);
        }

        [Test]
        public void ShouldMatchTheExpectedResult()
        {
            foreach (var example in _examples)
                example.ExpectedResult.Should().Be(example.Result, "Domain {0}, company {1}, country code {2}", example.DomainName, example.Company, example.CountryCode);
        }

        protected class ExtraFieldRuleTest
        {
            public string DomainName { get; set; }
            public string Company { get; set; }
            public string CountryCode { get; set; }
            public bool ExpectedResult { get; set; }
            public bool Result { get; set; }

            public ExtraFieldRuleTest(string domainName, string company, string countryCode, bool expectedResult)
            {
                this.DomainName = domainName;
                this.Company = company;
                this.CountryCode = countryCode;
                this.ExpectedResult = expectedResult;
            }
        }
    }
}
