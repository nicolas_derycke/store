﻿using System.Collections.Generic;
using Store.Model.ExtraFields;

namespace Store.API.Tests.Model.ExtraFieldRules
{
    public class WhenMatchingForIndividuals : AbstractWhenMatchingExtraFieldRules
    {
        protected override ExtraFieldRule GetExtraFieldRule()
        {
            return new ExtraFieldRuleBuilder().ForExtension("se").ForIndividuals().Build();
        }

        protected override List<ExtraFieldRuleTest> GetExamples()
        {
            return new List<ExtraFieldRuleTest>()
            {
                new ExtraFieldRuleTest("zeno.se", "zeno nv", "fr", false),
                new ExtraFieldRuleTest("zeno.se", "", "fr", true),
                new ExtraFieldRuleTest("zeno.se", "zeno nv", "be", false),
                new ExtraFieldRuleTest("zeno.se", "", "be", true)
            };
        }
    }
}
