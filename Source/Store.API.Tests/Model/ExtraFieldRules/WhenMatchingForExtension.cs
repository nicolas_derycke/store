﻿using System.Collections.Generic;
using Store.Model.ExtraFields;

namespace Store.API.Tests.Model.ExtraFieldRules
{
    public class WhenMatchingForExtension : AbstractWhenMatchingExtraFieldRules
    {
        protected override ExtraFieldRule GetExtraFieldRule()
        {
            return new ExtraFieldRuleBuilder().ForExtension("ca").Build();
        }

        protected override List<ExtraFieldRuleTest> GetExamples()
        {
            return new List<ExtraFieldRuleTest>()
            {
                new ExtraFieldRuleTest("zeno.ca", "zeno nv", "ca", true),
                new ExtraFieldRuleTest("zeno.ca", "", "ca", true),
                new ExtraFieldRuleTest("zeno.ca", "zeno nv", "be", true),
                new ExtraFieldRuleTest("zeno.ca", "", "be", true),
                new ExtraFieldRuleTest("zeno.be", "zeno nv", "be", false),
                new ExtraFieldRuleTest("zeno.be", "", "be", false),
                new ExtraFieldRuleTest("zeno.be", "zeno nv", "ca", false),
                new ExtraFieldRuleTest("zeno.be", "", "ca", false)
            };
        }
    }
}
