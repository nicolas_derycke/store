﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.Model.Baskets;

namespace Store.API.Tests.Model
{
    [TestFixture]
    public class VatRateCalculationFromPercentage : GivenWhenThen
    {
        private Basket _basket;
        private decimal _vatRatePercentage;

        protected override void Given()
        {
            var given = new GenericGiven();
            _vatRatePercentage = 60;
            _basket = given.ABasket(new List<BasketItem>(), _vatRatePercentage);
        }

        protected override void When()
        {
        }

        [Test]
        public void ShouldBeCalculatedCorrectly()
        {
            _basket.VatRate.Should().Be(_basket.VatRatePercentage / 100);
        }
    }
}
