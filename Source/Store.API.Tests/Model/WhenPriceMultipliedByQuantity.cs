﻿using System;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.Model;

namespace Store.API.Tests.Model
{
    [TestFixture]
    public class WhenPriceMultipliedByQuantity : GivenWhenThen
    {
        private Price _totalPrice;
        private Price _price;
        private int _quantity;

        protected override void Given()
        {
            _price = new Price(20m, 30m, 21m);
            _quantity = 3;
        }

        protected override void When()
        {
            _totalPrice = _price * _quantity;
        }

        [Test]
        public void TotalPriceShouldBe()
        {
            _totalPrice.ExclVat.Should().Be(_price.ExclVat * _quantity);
            _totalPrice.InclVat.Should().Be(_price.InclVat * _quantity);
            _totalPrice.ReductionExclVat.Should().Be(_price.ReductionExclVat * _quantity);
            _totalPrice.ReductionInclVat.Should().Be(_price.ReductionInclVat * _quantity);
        }
    }

    [TestFixture]
    public class WhenPriceIsCalculated : GivenWhenThen
    {
        private Price _price;
        private decimal _excl;
        private decimal _reduction;
        private decimal _vat;

        protected override void Given()
        {
            _excl = 30.123m;
            _reduction = 20.126m;
            _vat = 20.5m;            
        }

        protected override void When()
        {
            _price = new Price(_excl, _reduction, _vat);
        }

        [Test]
        public void PriceShouldBeRoundedToTwoDecimals()
        {
            var vat = 1 + _vat/100;
            _price.ExclVat.Should().Be(Math.Round(_excl, 2));
            _price.InclVat.Should().Be(Math.Round(Math.Round(_excl, 2) * vat, 2));

            _price.ReductionExclVat.Should().Be(Math.Round(_reduction, 2));
            _price.ReductionInclVat.Should().Be(Math.Round(Math.Round(_reduction, 2) * vat, 2));

        }        
    }
}
