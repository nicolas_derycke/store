﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts.TypedItemAttributes;
using Store.Providers;

namespace Store.API.Tests.Providing
{
    [TestFixture]
    public class WhenGettingExtraFieldsForFrExtension : GivenWhenThen
    {
        private RequiredExtraFieldsProvider _requiredExtraFieldsProvider;

        private string _domainName;
        private RegistrantAttributes _registrant;

        private IEnumerable<string> _extraFields;

        protected override void Given()
        {
            _requiredExtraFieldsProvider = new RequiredExtraFieldsProvider();

            _domainName = "zeno.fr";
            _registrant = new RegistrantAttributes(string.Empty, "Zeno NV", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        protected override void When()
        {
            _extraFields = _requiredExtraFieldsProvider.Get(_domainName, _registrant);
        }

        [Test]
        public void ShouldExtraFieldsBeEmpty()
        {
            _extraFields.Should().HaveCount(1);
            _extraFields.First().Should().Be("companyNumber");
        }
    }
}
