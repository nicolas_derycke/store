﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts.TypedItemAttributes;
using Store.Providers;

namespace Store.API.Tests.Providing
{
    [TestFixture]
    public class WhenGettingExtraFieldsForEmptyDomainName : GivenWhenThen
    {
        private RequiredExtraFieldsProvider _requiredExtraFieldsProvider;

        private string _domainName;
        private RegistrantAttributes _registrant;

        private IEnumerable<string> _extraFields;

        protected override void Given()
        {
            _requiredExtraFieldsProvider = new RequiredExtraFieldsProvider();

            _domainName = string.Empty;
            _registrant = RegistrantAttributes.Empty;
        }

        protected override void When()
        {
            _extraFields = _requiredExtraFieldsProvider.Get(_domainName, _registrant);
        }

        [Test]
        public void ShouldExtraFieldsBeEmpty()
        {
            _extraFields.Should().BeEmpty();
        }
    }
}
