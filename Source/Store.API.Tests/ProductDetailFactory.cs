﻿using System.Collections.Generic;
using System.Linq;
using Store.Data;
using Store.Model;

namespace Store.API.Tests
{
    public class ProductDetailFactory
    {
        protected static Dictionary<string, ProductDetailsData> _product
           = new Dictionary<string, ProductDetailsData>() 
                    { 
                        { 
                            "linux-express", 
                            new ProductDetailsData()
                                {
                                    Period = 3,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 50m
                                }
                                                    
                        }, 
                        { 
                            "linux-express-promo", 
                            new ProductDetailsData()
                                {
                                    Period = 3,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 20m
                                }
                                                      
                        }, 
                        { 
                            "performancehosting", 
                            new ProductDetailsData()
                                {
                                    Period = 3,
                                    ProductGroupCode = "performance-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 20m
                                }
                                                      
                        }
                    };

        public static ProductDetailsData Get(string productCode)
        {
            return _product[productCode];
        }

        public static IEnumerable<ProductDetailsData> GetList(string productCode)
        {
            return _productList[productCode];
        }

        public static IEnumerable<ProductDetailsData> GetList(string productCode, int reductionPercentage)
        {
            var reduction = (100 - reductionPercentage) / 100M;
            return _productList[productCode].Select(item => new ProductDetailsData() { Period = item.Period, ProductName = item.ProductName, PeriodPrice = item.PeriodPrice * reduction, ProductGroupCode = item.ProductGroupCode, Reduction = item.Reduction, Action = item.Action }).ToList();
        }

        protected static Dictionary<string, IEnumerable<ProductDetailsData>> _productList
           = new Dictionary<string, IEnumerable<ProductDetailsData>>() 
                    { 
                        { 
                            "linux-express", 
                            new List<ProductDetailsData>(){
                                new ProductDetailsData()
                                {
                                    Period = 3,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 50m,
                                    Action = ProductAction.Renewal
                                },
                                new ProductDetailsData()
                                {
                                    Period = 12,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 60m,
                                    Action = ProductAction.Renewal
                                },
                                new ProductDetailsData()
                                {
                                    Period = 24,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 70m,
                                    Action = ProductAction.Renewal
                                }
                            }
                                                    
                        }, 
                        { 
                            "linux-express-promo", 
                            new List<ProductDetailsData>(){
                                new ProductDetailsData()
                                {
                                    Period = 3,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 20m,
                                    Action = ProductAction.Renewal
                                },
                                new ProductDetailsData()
                                {
                                    Period = 12,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 50m,
                                    Action = ProductAction.Renewal
                                },
                                new ProductDetailsData()
                                {
                                    Period = 24,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 80m,
                                    Action = ProductAction.Renewal
                                }
                             }                     
                        }, 
                        { 
                            "performancehosting", 
                            new List<ProductDetailsData>(){
                                new ProductDetailsData()
                                {
                                    Period = 3,
                                    ProductGroupCode = "performance-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 20m,
                                    Action = ProductAction.Renewal
                                },
                                new ProductDetailsData()
                                {
                                    Period = 12,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 50m,
                                    Action = ProductAction.Renewal
                                },
                                new ProductDetailsData()
                                {
                                    Period = 24,
                                    ProductGroupCode = "linux-hosting",
                                    ProductName =  "Linux hosting express",
                                    PeriodPrice = 80m,
                                    Action = ProductAction.Renewal
                                }
                             }               
                        }
                    };
    }
}
