﻿using System;
using System.Linq;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.Providers;

namespace Store.API.Tests.Baskets
{
    [TestFixture]
    public class WhenGettingProductInfoWithInvalidProductPeriod : GivenWhenThen
    {
        private Action _whenAction;
        private BasketItem _basketItem;
        private IProductInfoProvider _productProvider;

        protected override void Given()
        {
            var given = new GenericGiven();
            const int productPeriod = 5;

            _basketItem = new BasketItem(
                0,
                1,
                "performancehosting",
                "hosting",
                "linux-express-promo",
                "Linux express",
                productPeriod,
                123,
                1,
                new Price(100, 0, 21),
                given.GetHostingAttributes(),
                0.21m,
                BasketItemValidationError.None,
                BasketItemWarning.None,
                true,
                false);

            _productProvider = given.AProductProvider();
        }

        protected override void When()
        {
            _whenAction = () => _productProvider.GetProductInfo(_basketItem, CustomerIdentifier.FromCustomerId(0), 0, Language.English).ToList();
        }

        [Test]
        public void ShouldThrowInvalidProductPeriodException()
        {
            _whenAction.ShouldThrow<InvalidProductPeriodSpecifiedException>();
        }
    }
}
