﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Store.Model.Baskets;
using Store.Model.Exceptions;

namespace Store.API.Tests.Baskets.BasketItemCreation
{
    [TestFixture]
    public class WhenCreatingALinuxUpgradeBasketItemOnRegularBasket : WhenCreatingABasketItem
    {
        private Action _callHandler;

        protected override BasketItem CreateABasketItem()
        {
            var aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            return _given.ABasketItemForProduct(aLinuxPromo, aLinuxPromo.Period, 1, _given.UpgradeAttributes, 1);
        }

        protected override Basket CreateABasket(List<BasketItem> basketItems)
        {
            var aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            var secondBasketItem = _given.ABasketItemForProduct(aLinuxPromo, aLinuxPromo.Period, 1, _given.GetHostingAttributes(), 1);

            return _given.ABasket(new[] { secondBasketItem }.ToList());
        }

        protected override void When()
        {
            _callHandler = () => _createBasketItemCommandHandler.Handle(_createBasketItemCommand);
        }

        [Test]
        public void ShouldThrowException()
        {
            _callHandler.ShouldThrow<InvalidBasketTypeException>();
        }
    }
}