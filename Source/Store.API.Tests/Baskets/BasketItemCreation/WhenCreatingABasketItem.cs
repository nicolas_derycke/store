﻿using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Core;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.CommandHandlers.BasketItems;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Providers.Attributes;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.Validation;
using Store.Warnings;
using Intelligent.Shared.Core.Queries;

namespace Store.API.Tests.Baskets.BasketItemCreation
{
    abstract public class WhenCreatingABasketItem : GivenWhenThen
    {
        protected GenericGiven _given;

        protected Basket _basket;
        protected BasketItem _basketItem;
        protected CreateBasketItemCommand _createBasketItemCommand;

        protected CreateBasketItemCommandHandler _createBasketItemCommandHandler;

        protected ICommandBus _commandBus;
        protected IAttributeProvider _attributeProvider;
        protected ISqlCommandHandler _sqlCommandHandler;
        protected IWarningProvider _warningProvider;
        protected ILiveBasketItemValidator _liveValidator;
        protected IPremiumDomainPriceService _premiumDomainPriceService;
        protected IBasketItemPricingService _basketItemPricingService;
        protected IQueryHandler _queryHandler;

        protected BasketItemWarning _basketItemWarning;
        protected BasketItemValidationError _basketItemValidationError;

        protected override void Given()
        {
            _given = new GenericGiven();
            _basketItem = CreateABasketItem();
            var basketItems = new List<BasketItem>
            {
                _basketItem
            };
            _basket = CreateABasket(basketItems);

            _createBasketItemCommand = new CreateBasketItemCommand(_basket, _basketItem, 88);

            _commandBus = MockRepository.GenerateMock<ICommandBus>();
            _attributeProvider = MockRepository.GenerateMock<IAttributeProvider>();
            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _warningProvider = MockRepository.GenerateMock<IWarningProvider>();
            _liveValidator = MockRepository.GenerateMock<ILiveBasketItemValidator>();
            _premiumDomainPriceService = MockRepository.GenerateMock<IPremiumDomainPriceService>();
            _basketItemPricingService = MockRepository.GenerateMock<IBasketItemPricingService>();
            _queryHandler = MockRepository.GenerateMock<IQueryHandler>();

            _createBasketItemCommandHandler = new CreateBasketItemCommandHandler(_commandBus, new[] { _attributeProvider }, _sqlCommandHandler, new[] { _warningProvider }, new[] { _liveValidator }, _premiumDomainPriceService, _basketItemPricingService, _queryHandler);

            _attributeProvider.
                Expect(x => x.Get(_basket, _basketItem)).
                Return(new ItemAttributeKeyValuePair(AttributeKey.Registrant, RegistrantAttributes.Empty)).
                Repeat.Once();

            _basketItemWarning = BasketItemWarning.BusinessEmailAccountExists;
            _warningProvider.
                Expect(x => x.Validate(_basketItem, _basket)).
            Return(new[] { this._basketItemWarning }).
            Repeat.Once();

            _basketItemValidationError = BasketItemValidationError.InactiveProductPeriod;
            _liveValidator.
                Expect(x => x.Validate(_basketItem, _basket)).
                Return(GetBasketItemValidationErrors()).
                Repeat.Once();

            _sqlCommandHandler.Expect(x => x.Execute(Arg<CreateBasketItemAttribute>.Matches(arg => 
                arg.AttributeKey == AttributeKey.Registrant &&
                arg.BasketItemId == _basketItem.BasketItemId &&
                arg.CreationById == _createBasketItemCommand.CreationById))).Repeat.Once();

            _sqlCommandHandler.Expect(x => x.Execute(Arg<CreateBasketItemAttribute>.Matches(arg =>
                arg.AttributeKey == _basketItem.Attributes.Keys.First() &&
                arg.BasketItemId == _basketItem.BasketItemId &&
                arg.CreationById == _createBasketItemCommand.CreationById))).Repeat.Once();

            _sqlCommandHandler.Expect(x => x.Execute(Arg<CreateBasketItem>.Matches(arg =>
                arg.Basket == _basket &&
                arg.BasketItem == _basketItem &&
                arg.CreationById == _createBasketItemCommand.CreationById))).Repeat.Once();

            _commandBus.Expect(x => x.Send(Arg<AddHostingUpgradeCompensationCommand>.Matches(arg => arg.Basket == _basket && arg.BasketItem == _basketItem))).Repeat.Once();
            _commandBus.Expect(x => x.Send(Arg<AddFixedCostsCommand>.Matches(arg => arg.Basket == _basket && arg.BasketItem == _basketItem))).Repeat.Once();
            _commandBus.Expect(x => x.Send(Arg<AddAddonUpgradeCompensationCommand>.Matches(arg => arg.Basket == _basket && arg.BasketItem == _basketItem))).Repeat.Once();
        }

        protected virtual IEnumerable<BasketItemValidationError> GetBasketItemValidationErrors()
        {
            return new[] { _basketItemValidationError };
        }

        protected virtual Optional<Bundle> CreateBundle()
        {
            return Optional<Bundle>.Empty;
        }

        abstract protected BasketItem CreateABasketItem();

        abstract protected Basket CreateABasket(List<BasketItem> basketItems);

        protected override void When()
        {
            _createBasketItemCommandHandler.Handle(_createBasketItemCommand);
        }
    }
}