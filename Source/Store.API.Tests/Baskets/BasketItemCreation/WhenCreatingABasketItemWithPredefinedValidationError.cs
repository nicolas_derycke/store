﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts.Baskets;
using Store.Model.Baskets;

namespace Store.API.Tests.Baskets.BasketItemCreation
{
    [TestFixture]
    public class WhenCreatingABasketItemWithPredefinedValidationError : WhenCreatingABasketItem
    {
        private BasketItemValidationError _validationError;

        protected override BasketItem CreateABasketItem()
        {
            var aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            _validationError = BasketItemValidationError.NegativeBasketItemPrice;
            return _given.ABasketItemForProduct(aLinuxPromo, aLinuxPromo.Period, 1, _given.GetHostingAttributes(), 1, validationError: _validationError);
        }

        protected override Basket CreateABasket(List<BasketItem> basketItems)
        {
            return _given.ABasket(new List<BasketItem>());
        }

        protected override IEnumerable<BasketItemValidationError> GetBasketItemValidationErrors()
        {
            return Enumerable.Empty<BasketItemValidationError>();
        }    

        [Test]
        public void ShouldCreateBasketItemAndAttributes()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }

        [Test]
        public void ShouldAddWarning()
        {
            _warningProvider.VerifyAllExpectations();
            _basketItem.Warning.Should().Be(_basketItemWarning);
        }

        [Test]
        public void ShouldKeepPredefinedValidationError()
        {
            _liveValidator.VerifyAllExpectations();
            _basketItem.ValidationError.Should().Be(_validationError);
        }

        [Test]
        public void ShouldAddCompensationsAndFixedCosts()
        {
            _commandBus.VerifyAllExpectations();
        }

        [Test]
        public void ShouldNotUpdateBasketItemAsFree()
        {
            _basketItem.Price.ExclVat.Should().BeGreaterThan(0);
            _basketItem.Price.InclVat.Should().BeGreaterThan(0);
        }
    }
}