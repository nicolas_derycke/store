using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model.Baskets;
using Store.SqlCommands.BasketItems;

namespace Store.API.Tests.Baskets.BasketItemCreation
{
    [TestFixture]
    public class WhenCreatingDuplicateFaxBasketItem : WhenCreatingABasketItem
    {
        protected override BasketItem CreateABasketItem()
        {
            return new BasketItem(
                "fax-services",
                string.Empty,
                12,
                1,
                new ItemAttributeDictionary(new Dictionary<AttributeKey, IBasketItemAttribute> {{AttributeKey.Fax, new FaxAttributes("+32.3", null, true, "e@mail.com", null)}}),
                0,
                true);
        }

        protected override Basket CreateABasket(List<BasketItem> basketItems)
        {
            return _given.ABasket(new List<BasketItem>
            {
                CreateABasketItem()
            });
        }

        [Test]
        public void IdentifierIsMadeUnique()
        {
            Assert.That(_basket.Items.Any(x => x.Attributes.Identifier == "+32.3xxxxxxxx"));

            _sqlCommandHandler.AssertWasCalled(x => x.Execute(Arg<CreateBasketItem>.Matches(c => c.BasketItem.Attributes.Identifier == "+32.3xxxxxxxx1")));
        }
    }
}