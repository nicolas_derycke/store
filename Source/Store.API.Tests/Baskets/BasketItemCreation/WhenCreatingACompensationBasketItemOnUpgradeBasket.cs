﻿using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using Store.Model.Baskets;

namespace Store.API.Tests.Baskets.BasketItemCreation
{
    [TestFixture]
    public class WhenCreatingACompensationBasketItemOnUpgradeBasket : WhenCreatingABasketItem
    {
        protected override BasketItem CreateABasketItem()
        {
            var aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            return _given.ABasketItemForProduct(aLinuxPromo, aLinuxPromo.Period, -1, _given.GetHostingAttributes(), 1, null, false);
        }

        protected override Basket CreateABasket(List<BasketItem> basketItems)
        {
            return _given.AnUpgradeBasket(new List<BasketItem>());
        }

        protected override void When()
        {
            _createBasketItemCommandHandler.Handle(_createBasketItemCommand);
        }

        [Test]
        public void ShouldCreateBasketItemAndAttributes()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }
    }
}