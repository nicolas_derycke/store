﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using Rhino.Mocks;
using Store.Model.Baskets;

namespace Store.API.Tests.Baskets.BasketItemCreation
{
    [TestFixture]
    public class WhenCreatingALinuxBasketItem : WhenCreatingABasketItem
    {
        protected override BasketItem CreateABasketItem()
        {
            var aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            return _given.ABasketItemForProduct(aLinuxPromo, aLinuxPromo.Period, 1, _given.GetHostingAttributes(), 1);
        }

        protected override Basket CreateABasket(List<BasketItem> basketItems)
        {
            return _given.ABasket(new List<BasketItem>());
        }

        [Test]
        public void ShouldCreateBasketItemAndAttributes()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }

        [Test]
        public void ShouldAddWarnings()
        {
            _warningProvider.VerifyAllExpectations();
            _basketItem.Warning.Should().Be(_basketItemWarning);
        }

        [Test]
        public void ShouldAddLiveValidationErrors()
        {
            _liveValidator.VerifyAllExpectations();
            _basketItem.ValidationError.Should().Be(_basketItemValidationError);
        }

        [Test]
        public void ShouldAddCompensationsAndFixedCosts()
        {
            _commandBus.VerifyAllExpectations();
        }

        [Test]
        public void ShouldNotUpdateBasketItemAsFree()
        {
            _basketItem.Price.ExclVat.Should().BeGreaterThan(0);
            _basketItem.Price.InclVat.Should().BeGreaterThan(0);
        }
    }
}