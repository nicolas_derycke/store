﻿using System.Collections.Generic;
using Intelligent.Core.Models;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.CommandHandlers.BasketItems;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.API.Tests.Baskets
{
    [TestFixture]
    public class WhenChangingThePeriodOfABasketItem : GivenWhenThen
    {
        private ProductInfo _aLinuxPromo;
        private ProductInfo _aLinuxPromoOtherPeriod;
        private BasketItem _basketItemToUpdateBefore;
        private BasketItem _basketItemToUpdateAfter;
        private GenericGiven _given;
        private Basket _basket;
        private ISqlCommandHandler _mockedSqlCommandHandler;
        private ICommandBus _commandBus;
        private IBasketItemPricingService _basketItemPricingService;
        private ChangeBasketItemPeriodCommandHandler _handler;

        const int ProductPeriod = 3;
        const int UpdatedProductPeriod = 12;

        protected override void Given()
        {
            _given = new GenericGiven();

            _aLinuxPromo = _given.ALinuxExpressPromoProduct(ProductPeriod);

            _aLinuxPromoOtherPeriod = _given.ALinuxExpressPromoProduct(UpdatedProductPeriod);


            var basketItems = new List<BasketItem>
            {
                CreateABasketItemWithId(1),
                CreateABasketItemWithId(2, 4),
                CreateABasketItemWithId(3)
            };

            _basketItemToUpdateBefore = CreateABasketItemWithId(4);
            basketItems.Add(_basketItemToUpdateBefore);

            _basket = _given.ABasket(basketItems, language: Language.English);

            _basketItemToUpdateAfter = CreateBasketItemWithIdAndUpdatedPeriod(4);

            _mockedSqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _commandBus = MockRepository.GenerateMock<ICommandBus>();
            _basketItemPricingService = MockRepository.GenerateMock<IBasketItemPricingService>();

            _handler = new ChangeBasketItemPeriodCommandHandler(_basketItemPricingService, _mockedSqlCommandHandler, _commandBus);
        }

        private BasketItem CreateBasketItemWithIdAndUpdatedPeriod(int id)
        {
            return _given.ABasketItemForProduct(_aLinuxPromoOtherPeriod, UpdatedProductPeriod, 1, _given.GetHostingAttributes(), id);
        }

        private BasketItem CreateABasketItemWithId(int id, int? parentId = null)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, ProductPeriod, 1, _given.GetHostingAttributes(), id, parentId);
        }

        protected override void When()
        {
            var cmd = new ChangeBasketItemPeriodCommand(_basket, _basketItemToUpdateBefore.BasketItemId, UpdatedProductPeriod, 0, 88);
            _handler.Handle(cmd);
        }

        [Test]
        public void ShouldChangePeriodOnChilds()
        {
            _commandBus.AssertWasCalled(x => x.Send(Arg<ChangeBasketItemPeriodCommand>.Matches(arg => arg.BasketItemId == 2)));
        }

        [Test]
        public void ShouldNotChangePeriodOnNonChilds()
        {
            _commandBus.AssertWasNotCalled(x => x.Send(Arg<ChangeBasketItemPeriodCommand>.Matches(arg => arg.BasketItemId == 1)));
            _commandBus.AssertWasNotCalled(x => x.Send(Arg<ChangeBasketItemPeriodCommand>.Matches(arg => arg.BasketItemId == 3)));
        }

        [Test]
        public void TheUpdateBasketItemCommandIsSentForTheCorrectBasketItemId()
        {
            _mockedSqlCommandHandler.AssertWasCalled(handler => handler.Execute(Arg<UpdateBasketItem>.Matches(c => c.BasketItem.BasketItemId == _basketItemToUpdateAfter.BasketItemId)));
        }

        [Test]
        public void TheUpdateBasketItemCommandIsSentWithNewPeriodAndPrice()
        {
            _mockedSqlCommandHandler.AssertWasCalled(handler => handler.Execute(Arg<UpdateBasketItem>.Matches(c => c.BasketItem.BasketItemId == _basketItemToUpdateAfter.BasketItemId)));
            _basketItemPricingService.AssertWasCalled(x => x.SetProductInfoAndPrice(_basketItemToUpdateBefore, _basket.CustomerIdentifier, _basket.Provider, Language.English, _basket.VatRatePercentage));
        }        

        [Test]
        public void ThenCompensationAreReappliedForTheUpdatedBasketItem()
        {
            _commandBus.AssertWasCalled(cb => cb.Send(Arg<AddHostingUpgradeCompensationCommand>.Matches(arg => arg.Basket == _basket && arg.BasketItem == _basketItemToUpdateBefore)));
            _commandBus.AssertWasCalled(cb => cb.Send(Arg<AddAddonUpgradeCompensationCommand>.Matches(arg => arg.Basket == _basket && arg.BasketItem == _basketItemToUpdateBefore)));
        }
    }
}