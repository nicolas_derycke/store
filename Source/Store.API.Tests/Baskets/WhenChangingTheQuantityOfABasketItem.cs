﻿using System.Linq;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.CommandHandlers.BasketItems;
using Store.Commands.BasketItems;
using Store.Model;
using Store.Model.Baskets;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.API.Tests.Baskets
{
    [TestFixture]
    public class WhenChangingTheQuantityOfABasketItem : GivenWhenThen
    {
        private ChangeBasketItemQuantityCommandHandler _changeBasketItemQuantityCommandHandler;

        private ISqlCommandHandler _sqlCommandHandler;
        private ChangeBasketItemQuantityCommand _command;
        private int _newQuantity;

        private BasketItem _originalBasketItem;
        private BasketItem _updatedBasketItem;

        protected override void Given()
        {
            var given = new GenericGiven();

            var basicEmailProduct = given.AProduct("basic-e-mail-25-gb", ProductTypes.BasicEmail);
            _originalBasketItem = given.ABasketItemForProduct(basicEmailProduct, 12, 1, null, 1);
            _updatedBasketItem = given.ABasketItemForProduct(basicEmailProduct, 12, 1, null, 1);
            var basket = given.ABasket(new[] { _updatedBasketItem }.ToList());

            _newQuantity = 2;

            _command = new ChangeBasketItemQuantityCommand(basket, basket.Items.First(), _newQuantity, 88);

            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _sqlCommandHandler.Expect(bus => bus.Execute(Arg<UpdateBasketItem>.Matches(c => c.BasketItem.BasketItemId == _updatedBasketItem.BasketItemId)));

            _changeBasketItemQuantityCommandHandler = new ChangeBasketItemQuantityCommandHandler(_sqlCommandHandler);
        }

        protected override void When()
        {
            _changeBasketItemQuantityCommandHandler.Handle(_command);
        }

        [Test]
        public void TheUpdateBasketItemCommandIsSentForTheCorrectBasketItemId()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }

        [Test]
        public void TheTotalPriceIsMultiplied()
        {
            _updatedBasketItem.TotalPrice.ExclVat.Should().Be(_originalBasketItem.TotalPrice.ExclVat * _newQuantity);
            _updatedBasketItem.TotalPrice.InclVat.Should().Be(_originalBasketItem.TotalPrice.InclVat * _newQuantity);
            _updatedBasketItem.TotalPrice.ReductionExclVat.Should().Be(_originalBasketItem.TotalPrice.ReductionExclVat * _newQuantity);
            _updatedBasketItem.TotalPrice.ReductionInclVat.Should().Be(_originalBasketItem.TotalPrice.ReductionInclVat * _newQuantity);
        }
    }
}