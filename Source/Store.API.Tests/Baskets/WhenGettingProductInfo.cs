﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.Data;
using Store.Model;
using Store.Providers;
using Store.Services;
using Store.SqlQueries;

namespace Store.API.Tests.Baskets
{
    public class WhenGettingProductInfo : GivenWhenThen
    {
        protected IQueryHandler _queryHandler;
        protected IEnumerable<ProductInfo> _productWithoutPromo;
        protected IEnumerable<ProductInfo> _productWithPromo;
        protected IProductInfoProvider _productDetailsProvider;
        private IEnumerable<ProductDetailsData> _linuxExpress;
        private IEnumerable<ProductDetailsData> _linuxExpressPromo;
        private IEnumerable<ProductDetailsData> _linuxExpressCustomerDiscount;
        private IEnumerable<ProductDetailsData> _linuxExpressPromoCustomerDiscount;
        private IEnumerable<ProductInfo> _productWithoutPromoCustomerDiscount;
        private IEnumerable<ProductInfo> _productWithPromoCustomerDiscount;
        private IProductGroupService _productGroupService;

        protected override void Given()
        {
            const int reduction = 10;
            _queryHandler = MockRepository.GenerateMock<IQueryHandler>();
            _productGroupService = MockRepository.GenerateMock<IProductGroupService>();

            _linuxExpress = ProductDetailFactory.GetList("linux-express");
            _queryHandler.Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "linux-express" && qry.CustomerId == 0)))
                .Return(_linuxExpress);

            _linuxExpressPromo = ProductDetailFactory.GetList("linux-express-promo");
            _queryHandler.Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "linux-express-promo" && qry.CustomerId == 0)))
                .Return(_linuxExpressPromo);

            _linuxExpressCustomerDiscount = ProductDetailFactory.GetList("linux-express", reduction);
            _queryHandler.Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "linux-express" && qry.CustomerId != 0)))
                .Return(_linuxExpressCustomerDiscount);

            _linuxExpressPromoCustomerDiscount = ProductDetailFactory.GetList("linux-express-promo", reduction);
            _queryHandler.Stub(x => x.Execute(Arg<GetProductDetailsQuery>.Matches(qry => qry.ProductCode == "linux-express-promo" && qry.CustomerId != 0)))
                .Return(_linuxExpressPromoCustomerDiscount);

            _productDetailsProvider = new ProductInfoProvider(_queryHandler, _productGroupService);
        }

        protected override void When()
        {
            _productWithoutPromo = _productDetailsProvider.GetProductInfo("linux-express","", 12, null, 0, Language.Dutch, null);
            _productWithPromo = _productDetailsProvider.GetProductInfo("linux-express", "linux-express-promo", 12, null, 0, Language.Dutch, null);
            _productWithoutPromoCustomerDiscount = _productDetailsProvider.GetProductInfo("linux-express", "", 12, CustomerIdentifier.FromCustomerId(5), 0, Language.Dutch, null);
            _productWithPromoCustomerDiscount = _productDetailsProvider.GetProductInfo("linux-express", "linux-express-promo", 12, CustomerIdentifier.FromCustomerId(5), 0, Language.Dutch, null);
        }

        [Test]
        public void ProductDetailsDataWithoutPromoShouldContainCorrectPriceForPeriod()
        {
            var expectedLinuxPrice = _linuxExpress.Where(p => p.Period == 12).Select(p => p.PeriodPrice).First();
            _productWithoutPromo.First().PeriodPrice.Should().Be(expectedLinuxPrice);
        }

        [Test]
        public void ProductDetailsDataWithPromoShouldContainCorrectPriceForPeriod()
        {
            var expectedPrice = _linuxExpressPromo.Where(p => p.Period == 12).Select(p => p.PeriodPrice).First();
            _productWithPromo.First().PeriodPrice.Should().Be(expectedPrice);
        }

        [Test]
        public void ProductDetailsDataWithCustomerPriceExceptionShouldContainCorrectPriceForPeriod()
        {
            var expectedLinuxPrice = _linuxExpress.Where(p => p.Period == 12).Select(p => p.PeriodPrice).First();
            _productWithoutPromo.First().PeriodPrice.Should().Be(expectedLinuxPrice);

            var expectedPrice = _linuxExpressCustomerDiscount.Where(p => p.Period == 12).Select(p => p.PeriodPrice).First();

            _productWithoutPromoCustomerDiscount.First().PeriodPrice.Should().Be(expectedPrice);
            _productWithoutPromoCustomerDiscount.First().Reduction.Should().Be(expectedLinuxPrice - expectedPrice);
        }

        [Test]
        public void ProductInfoWithCustomerPriceExceptionAndPromoShouldCalculatePromo()
        {
            var expectedLinuxPrice = _linuxExpressCustomerDiscount.Where(p => p.Period == 12).Select(p => p.PeriodPrice).First();
            var expectedPromoPrice = _linuxExpressPromoCustomerDiscount.Where(p => p.Period == 12).Select(p => p.PeriodPrice).First();

            _productWithPromoCustomerDiscount.First().PeriodPrice.Should().Be(expectedPromoPrice);
            _productWithPromoCustomerDiscount.First().Reduction.Should().Be(expectedLinuxPrice - expectedPromoPrice);
        }
    }
}
