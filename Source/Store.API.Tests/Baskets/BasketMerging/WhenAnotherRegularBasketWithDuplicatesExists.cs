﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.CommandHandlers.Baskets;
using Store.Commands.Baskets;
using Store.Data;
using Store.Model;
using Store.Model.Baskets;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;
using Store.Validation;

namespace Store.API.Tests.Baskets.BasketMerging
{
    [TestFixture]
    public class WhenAnotherRegularBasketWithDuplicatesExists : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aLinuxPromo;

        private IQueryHandler _queryHandler;
        private IBasketService _basketService;
        private ISqlCommandHandler _sqlCommandHandler;
        private ILiveBasketItemValidator _liveValidator;

        private MergeBasketsCommandHandler _mergeBasketsCommandHandler;

        private MergeBasketsCommand _mergeBasketsCommand;
        private int _lastUpdateById;
        private ICommandBus _commandBus;
        private Basket _newBasket;
        private ILogger _logger;

        protected override void Given()
        {
            _queryHandler = MockRepository.GenerateMock<IQueryHandler>();
            _basketService = MockRepository.GenerateMock<IBasketService>();
            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _liveValidator = MockRepository.GenerateMock<ILiveBasketItemValidator>();
            _commandBus = MockRepository.GenerateMock<ICommandBus>();
            _logger = MockRepository.GenerateMock<ILogger>();
            var liveValidators = new List<ILiveBasketItemValidator>() { _liveValidator };

            _mergeBasketsCommandHandler = new MergeBasketsCommandHandler(
                _queryHandler,
                _basketService,
                _sqlCommandHandler,
                liveValidators,
                _commandBus,
                _logger);

            var customerIdentifier = CustomerIdentifier.FromCustomerNumber(129829);

            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);

            var basketItems = new List<BasketItem>
            {
                CreateABasketItemWithId(1, _given.GetHostingAttributes("zeno1.be")),
                CreateABasketItemWithId(4, _given.GetHostingAttributes("zeno2.be"))
            };
            _newBasket = _given.ABasket(basketItems, customerIdentifier, "COM-20151130-100");

            _lastUpdateById = 88;
            _mergeBasketsCommand = new MergeBasketsCommand(_newBasket, _lastUpdateById);

            var newBasketItem = CreateABasketItemWithId(5, _given.GetHostingAttributes("zeno3.be"));
            var duplicateBasketItem = CreateABasketItemWithId(6, _given.GetHostingAttributes("zeno1.be"));
            basketItems = new List<BasketItem>
            {
                newBasketItem,
                duplicateBasketItem
            };
            var orginalBasket = _given.ABasket(basketItems, customerIdentifier, "COM-20151130-101");

            _queryHandler.
                Expect(x => x.Execute(Arg<GetBasketsByCustomerQuery>.Matches(arg => arg.CustomerIdentifier == _newBasket.CustomerIdentifier))).
                Return(new[] { 
                     GetBasketReference(orginalBasket),
                     GetBasketReference(_newBasket)
                }).
                Repeat.Once();

            _basketService.
                Expect(x => x.GetBasketByOrderCode(orginalBasket.OrderCode)).
                Return(Task.FromResult(orginalBasket)).
                Repeat.Once();

            _sqlCommandHandler.
                Expect(x => x.Execute(Arg<CreateBasketItem>.Matches(arg => arg.Basket == _newBasket && arg.BasketItem == newBasketItem && arg.CreationById == _lastUpdateById))).
                Repeat.Once();

            _sqlCommandHandler.
                Expect(x => x.Execute(Arg<CreateBasketItem>.Matches(arg => arg.Basket == _newBasket && arg.BasketItem == duplicateBasketItem && arg.CreationById == _lastUpdateById))).
                Repeat.Never();

            _sqlCommandHandler.
                Expect(x => x.Execute(Arg<DeleteBasket>.Matches(arg => arg.BasketId == orginalBasket.BasketId && arg.DeletionById == _lastUpdateById))).
                Repeat.Once();

            _liveValidator.
                Expect(x => x.Validate(newBasketItem, _newBasket)).
                Return(new List<BasketItemValidationError>() { BasketItemValidationError.None }).
                Repeat.Once();

            _liveValidator.
                Expect(x => x.Validate(duplicateBasketItem, _newBasket)).
                Return(new List<BasketItemValidationError>() { BasketItemValidationError.None }).
                Repeat.Never();
        }

        private BasketItem CreateABasketItemWithId(int id, ItemAttributeDictionary attributes)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, attributes, id);
        }

        private BasketReferenceData GetBasketReference(Basket basket)
        {
            return new BasketReferenceData(basket.OrderCode, basket.CustomerIdentifier.CustomerId, basket.CreationDate, basket.ItemCount, basket.BasketType.ToString());
        }

        protected override void When()
        {
            _mergeBasketsCommandHandler.Handle(_mergeBasketsCommand);
        }

        [Test]
        public void ShouldGetCustomersBaskets()
        {
            _queryHandler.VerifyAllExpectations();
            _basketService.VerifyAllExpectations();
        }

        [Test]
        public void ShouldCreateNonDuplicateBasketItemsOnNewBasket()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }

        [Test]
        public void ShouldExecuteLiveValidationOnNonDuplicateBasketItems()
        {
            _liveValidator.VerifyAllExpectations();
        }


        [Test]
        public void VatPercentageIsEvaluated()
        {
            _commandBus.AssertWasCalled(x => x.Send(Arg<EvaluateTaxPercentageCommand>.Matches(c => c.Basket == _newBasket)));
        }
    }
}