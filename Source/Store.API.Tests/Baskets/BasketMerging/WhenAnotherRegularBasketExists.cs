﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.CommandHandlers.Baskets;
using Store.Commands.Baskets;
using Store.Data;
using Store.Model;
using Store.Model.Baskets;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;
using Store.Validation;

namespace Store.API.Tests.Baskets.BasketMerging
{
    [TestFixture]
    public class WhenAnotherRegularBasketExists : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aLinuxPromo;

        private IQueryHandler _queryHandler;
        private IBasketService _basketService;
        private ISqlCommandHandler _sqlCommandHandler;
        private ILiveBasketItemValidator _liveValidator;

        private MergeBasketsCommandHandler _mergeBasketsCommandHandler;

        private MergeBasketsCommand _mergeBasketsCommand;
        private int _lastUpdateById;
        private ICommandBus _commandBus;
        private ILogger _logger;

        protected override void Given()
        {
            _queryHandler = MockRepository.GenerateMock<IQueryHandler>();
            _basketService = MockRepository.GenerateMock<IBasketService>();
            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _liveValidator = MockRepository.GenerateMock<ILiveBasketItemValidator>();
            _commandBus = MockRepository.GenerateMock<ICommandBus>();
            _logger = MockRepository.GenerateMock<ILogger>();
            var liveValidators = new List<ILiveBasketItemValidator>() { _liveValidator };

            _mergeBasketsCommandHandler = new MergeBasketsCommandHandler(
                _queryHandler,
                _basketService,
                _sqlCommandHandler,
                liveValidators,
                _commandBus, 
                _logger);

            var customerIdentifier = CustomerIdentifier.FromCustomerNumber(129829);

            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);

            var basketItems = new List<BasketItem>
            {
                CreateABasketItemWithId(1, _given.GetHostingAttributes("zeno1.be")),
                CreateABasketItemWithId(4, _given.GetHostingAttributes("zeno2.be"))
            };
            var newBasket = _given.ABasket(basketItems, customerIdentifier, "COM-20151130-100");

            _lastUpdateById = 88;
            _mergeBasketsCommand = new MergeBasketsCommand(newBasket, _lastUpdateById);

            basketItems = new List<BasketItem>
            {
                CreateABasketItemWithId(5, _given.GetHostingAttributes("zeno3.be")),
                CreateABasketItemWithId(6, _given.GetHostingAttributes("zeno4.be"), 5)
            };
            var orginalBasket = _given.ABasket(basketItems, customerIdentifier, "COM-20151130-101");

            _queryHandler.
                Expect(x => x.Execute(Arg<GetBasketsByCustomerQuery>.Matches(arg => arg.CustomerIdentifier == newBasket.CustomerIdentifier))).
                Return(new[] { 
                     GetBasketReference(orginalBasket),
                     GetBasketReference(newBasket)
                }).
                Repeat.Once();

            _basketService.
                Expect(x => x.GetBasketByOrderCode(orginalBasket.OrderCode)).
                Return(Task.FromResult(orginalBasket)).
                Repeat.Once();

            foreach (var basketItem in orginalBasket.Items)
            {
                _sqlCommandHandler.
                    Expect(x => x.Execute(Arg<CreateBasketItem>.Matches(arg => arg.Basket == newBasket && arg.BasketItem == basketItem && arg.CreationById == _lastUpdateById && !IsParentIdReferringToPreviousBasketItem(arg.BasketItem.ParentId)))).
                    WhenCalled(invk =>
                    {
                        ((CreateBasketItem) invk.Arguments.First()).BasketItem.BasketItemId *= 10;
                    }).
                    Repeat.Once();

                foreach (var attribute in basketItem.Attributes)
                {
                    _sqlCommandHandler.
                        Expect(x => x.Execute(Arg<CreateBasketItemAttribute>.Matches(arg => arg.AttributeKey == attribute.Key && arg.BasketItemId == basketItem.BasketItemId && arg.CreationById == _lastUpdateById))).
                        Repeat.Once();
                }
            }

            _sqlCommandHandler.
                Expect(x => x.Execute(Arg<DeleteBasket>.Matches(arg => arg.BasketId == orginalBasket.BasketId && arg.DeletionById == _lastUpdateById))).
                Repeat.Once();

            foreach (var basketItem in orginalBasket.Items)
                _liveValidator.
                    Expect(x => x.Validate(basketItem, newBasket)).
                    Return(new List<BasketItemValidationError>() { BasketItemValidationError.None }).
                    Repeat.Once();
        }

        private bool IsParentIdReferringToPreviousBasketItem(int? parentId)
        {
            if (!parentId.HasValue)
                return false; // no parent, so okay

            return parentId.Value == 5;
        }

        private BasketItem CreateABasketItemWithId(int id, ItemAttributeDictionary attributes, int? parentId = null)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, attributes, id, parentId);
        }

        private BasketReferenceData GetBasketReference(Basket basket)
        {
            return new BasketReferenceData(basket.OrderCode, basket.CustomerIdentifier.CustomerId, basket.CreationDate, basket.ItemCount, basket.BasketType.ToString());
        }

        protected override void When()
        {
            _mergeBasketsCommandHandler.Handle(_mergeBasketsCommand);
        }

        [Test]
        public void ShouldGetCustomersBaskets()
        {
            _queryHandler.VerifyAllExpectations();
            _basketService.VerifyAllExpectations();
        }

        [Test]
        public void ShouldCreateBasketItemsOnNewBasket()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }

        [Test]
        public void ShouldExecuteLiveValidationOnCopiedBasketItems()
        {
            _liveValidator.VerifyAllExpectations();
        }
    }
}