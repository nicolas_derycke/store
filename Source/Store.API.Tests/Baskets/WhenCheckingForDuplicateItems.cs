﻿using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model.Baskets;

namespace Store.API.Tests.Baskets
{
    [TestFixture]
    public class WhenCheckingForDuplicateItems : GivenWhenThen
    {
        private Basket _basket;

        private BasketItem _duplicateItem;
        private BasketItem _nonDuplicateItem;
        private BasketItem _originalItem;

        private bool _containsDuplicate;
        private bool _containsNonDuplicate;
        private bool _containsOriginalItem;

        protected override void Given()
        {
            var factory = new GenericGiven();

            var linuxExpress = factory.ABasketItemForProduct(factory.ALinuxExpressProduct(12), 12, 1, new ItemAttributeDictionary() { { AttributeKey.Domain, new DomainItemAttributes("zeno.be") } });
            var linuxExpressDuplicate = factory.ABasketItemForProduct(factory.ALinuxExpressProduct(12), 12, 1, new ItemAttributeDictionary() { { AttributeKey.Domain, new DomainItemAttributes("zeno.be") } });
            var linuxExpressPromo = factory.ABasketItemForProduct(factory.APerformanceHosting(12), 12, 1, new ItemAttributeDictionary() { { AttributeKey.Domain, new DomainItemAttributes("zeno.be") } });
            _basket = factory.ABasket(new List<BasketItem>() { linuxExpress });

            _duplicateItem = linuxExpressDuplicate;
            _nonDuplicateItem = linuxExpressPromo;
            _originalItem = linuxExpress;
        }

        protected override void When()
        {
            _containsDuplicate = _basket.ContainsDuplicateBasketItem(_duplicateItem);
            _containsNonDuplicate = _basket.ContainsDuplicateBasketItem(_nonDuplicateItem);
            _containsOriginalItem = _basket.ContainsDuplicateBasketItem(_originalItem);
        }

        [Test]
        public void ShouldNotContainOriginal()
        {
            _containsOriginalItem.Should().BeFalse();
        }

        [Test]
        public void ShouldContainDuplicate()
        {
            _containsDuplicate.Should().BeTrue();
        }

        [Test]
        public void ShouldNotContainDuplicate()
        {
            _containsNonDuplicate.Should().BeFalse();
        }
    }
}