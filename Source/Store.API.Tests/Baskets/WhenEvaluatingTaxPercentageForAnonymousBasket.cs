﻿using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Serilog;
using Store.API.Contracts;
using Store.CommandHandlers.BasketItems;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using Store.SqlCommands;

namespace Store.API.Tests.Baskets
{
    [TestFixture]
    public class WhenEvaluatingTaxPercentageForAnonymousBasket : GivenWhenThen
    {
        private EvaluateTaxPercentageCommandHandler _handler;
        private ICustomerInvoiceCriteriaProxy _customerInvoiceCriteriaProxy;
        private ILogger _logger;
        private ISqlCommandHandler _commandHandler;
        private ICustomerProxy _customerProxy;
        private BasketItem _basketItem;
        private Basket _basket;

        protected override void Given()
        {
            _customerInvoiceCriteriaProxy = MockRepository.GenerateMock<ICustomerInvoiceCriteriaProxy>();
            _logger = MockRepository.GenerateMock<ILogger>();
            _commandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _customerProxy = MockRepository.GenerateMock<ICustomerProxy>();
            _handler = new EvaluateTaxPercentageCommandHandler(_customerInvoiceCriteriaProxy, _logger, _commandHandler, _customerProxy);

            var given = new GenericGiven();
            _basketItem = given.ABasketItemForProduct(
                new ProductInfo(111, "aproduct", "groupcode", string.Empty, "A product", "groupcode", 222.5m, 333, 0, 12, ProductAction.Renewal, true), 
                12, 
                1, 
                new ItemAttributeDictionary());
            _basket = given.ABasket(new [] { _basketItem});
        }

        protected override void When()
        {
            _handler.Handle(new EvaluateTaxPercentageCommand(_basket));
        }

        [Test]
        public void TaxPercentageIsNotUpdated()
        {
            _commandHandler.AssertWasNotCalled(x => x.Execute(Arg<ICommand>.Is.Anything));
        }

    }
}