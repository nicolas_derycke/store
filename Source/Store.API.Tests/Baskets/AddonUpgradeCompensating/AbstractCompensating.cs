﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Billing.API.Contracts;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.CommandHandlers.BasketItems;
using Store.Commands.BasketItems;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using Store.Services;

namespace Store.API.Tests.Baskets.AddonUpgradeCompensating
{
    [TestFixture]
    abstract public class AbstractCompensating : GivenWhenThen
    {
        protected GenericGiven _given;
        protected ProductInfo _aLinuxPromo;

        private AddAddonUpgradeCompensationCommandHandler _addAddonUpgradeCompensationCommandHandler;

        private IInvoiceCorrectionProxy _invoiceCorrectionProxy;
        private IInvoiceCorrectionService _invoiceCorrectionService;

        private Basket _basket;
        private BasketItem _basketItem;

        protected abstract int CompensationCount { get; }

        protected override void Given()
        {
            _invoiceCorrectionProxy = MockRepository.GenerateMock<IInvoiceCorrectionProxy>();
            _invoiceCorrectionService = MockRepository.GenerateMock<IInvoiceCorrectionService>();

            _addAddonUpgradeCompensationCommandHandler = new AddAddonUpgradeCompensationCommandHandler(_invoiceCorrectionProxy, _invoiceCorrectionService);

            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            _basketItem = CreateABasketItemWithId(1);
            var basketItems = new List<BasketItem>
            {
                _basketItem
            };
            _basket = CreateBasket(basketItems);

            _invoiceCorrectionProxy.Expect(x => x.GetCachingAddonInvoiceCorrection(
                Arg<CustomerIdentifier>.Is.Anything,
                Arg<string>.Is.Anything,
                Arg<int>.Is.Anything,
                Arg<int>.Is.Anything,
                Arg<decimal>.Is.Anything)).
                Return(Task.FromResult<InvoiceCorrectionContract>(null)).
                Repeat.Times(CompensationCount);

            _invoiceCorrectionService.Expect(x => x.CreateBasketItems(
                Arg<InvoiceCorrectionContract>.Is.Anything,
                Arg<Basket>.Is.Anything,
                Arg<string>.Is.Anything,
                Arg<BasketItem>.Is.Anything)).
                Repeat.Times(CompensationCount);
        }

        abstract protected Basket CreateBasket(List<BasketItem> basketItems);

        abstract protected BasketItem CreateABasketItemWithId(int id);

        protected override void When()
        {
            _addAddonUpgradeCompensationCommandHandler.Handle(new AddAddonUpgradeCompensationCommand(_basketItem, _basket));
        }

        [Test]
        public void ShouldMeetAllExpectations()
        {
            _invoiceCorrectionProxy.VerifyAllExpectations();
            _invoiceCorrectionService.VerifyAllExpectations();
        }
    }
}