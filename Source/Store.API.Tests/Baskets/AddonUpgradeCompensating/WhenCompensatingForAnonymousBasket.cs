﻿using System.Collections.Generic;
using Store.Model.Baskets;

namespace Store.API.Tests.Baskets.AddonUpgradeCompensating
{
    public class WhenCompensatingForAnonymousBasket : AbstractCompensating
    {
        protected override Basket CreateBasket(List<BasketItem> basketItems)
        {
            return _given.ABasket(basketItems);
        }

        protected override BasketItem CreateABasketItemWithId(int id)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, _given.GetHostingAttributes(), id, null);
        }

        protected override int CompensationCount
        {
            get { return 0; }
        }
    }
}
