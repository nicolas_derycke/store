﻿using System.Collections.Generic;
using Intelligent.Core.Models;
using Store.Model.Baskets;

namespace Store.API.Tests.Baskets.AddonUpgradeCompensating
{
    public class WhenCompensatingForAddonBasket : AbstractCompensating
    {
        protected override Basket CreateBasket(List<BasketItem> basketItems)
        {
            return _given.ABasket(basketItems, CustomerIdentifier.FromCustomerNumber(129829));
        }

        protected override BasketItem CreateABasketItemWithId(int id)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, _given.AddonAttributes, id, null);
        }

        protected override int CompensationCount
        {
            get { return 0; }
        }
    }
}
