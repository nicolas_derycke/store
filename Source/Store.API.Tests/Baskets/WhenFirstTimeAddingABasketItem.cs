﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.Model;
using Store.Model.Baskets;

namespace Store.API.Tests.Baskets
{
    [TestFixture]
    public class WhenFirstTimeAddingABasketItem : GivenWhenThen
    {
        private Basket _basket;
        private ProductInfo _aLinuxProduct;
        private ProductInfo _aLinuxPromo;
        private BasketItem _basketItem;
        private decimal _vatMultiplier;
        private decimal _vatRate;

        protected override void Given()
        {
            var given = new GenericGiven();
            const int productPeriod = 3;
            _aLinuxProduct = given.ALinuxExpressProduct(productPeriod);
            _aLinuxPromo = given.ALinuxExpressPromoProduct(productPeriod);

            _basketItem = given.ABasketItemForProduct(_aLinuxPromo, productPeriod, 1, given.GetHostingAttributes());
            _basket = given.ABasket(new List<BasketItem> {_basketItem});

            _vatMultiplier = given.VatRate + 1;
            _vatRate = given.VatRate;
        }

        protected override void When()
        {
         
        }

        [Test]
        public void TotalPriceExclVatOfBasketShouldBeCalculated()
        {
            var expectedPrice = _aLinuxPromo.PeriodPrice;
            _basket.TotalPrice.ExclVat.Should().Be(expectedPrice);
        }

        [Test]
        public void TotalPriceInclVatOfBasketShouldBeCalculated()
        {
            var expectedPrice = _aLinuxPromo.PeriodPrice * _vatMultiplier;
            _basket.TotalPrice.InclVat.Should().Be(expectedPrice);
        }

        [Test]
        public void TotalPriceReductionExclVatOfBasketShouldBeCalculated()
        {
            var expectedResult = _aLinuxProduct.PeriodPrice - _aLinuxPromo.PeriodPrice;
            _basket.TotalPrice.ReductionExclVat.Should().Be(expectedResult);
        }

        [Test]
        public void TotalPriceReductionInclVatOfBasketShouldBeCalculated()
        {
            var expectedResult = (_aLinuxProduct.PeriodPrice - _aLinuxPromo.PeriodPrice) * _vatMultiplier;
            _basket.TotalPrice.ReductionInclVat.Should().Be(expectedResult);
        }

        [Test]
        public void VatRateShouldBeCalculated()
        {
            _basket.VatRate.Should().Be(_vatRate);
        }

        [Test]
        public void VatAmountShouldBeCalculated()
        {
            var expectedResult = _basket.TotalPrice.ExclVat * _vatRate;
            _basket.VatAmount.Should().Be(expectedResult);
        }

        [Test]
        public void ShouldHaveCorrectProductName()
        {
            _basket.Items.First().ProductName.Should().Be(_aLinuxProduct.ProductName);
        }

        [Test]
        public void ShouldHaveCorrectProductType()
        {
            _basket.Items.First().ProductType.Should().Be(_aLinuxProduct.ProductGroupCode);
        }

        [Test]
        public void TotalPriceInclVatOfBasketItemShouldBeCalculated()
        {
            var expectedResult = _aLinuxPromo.PeriodPrice * _vatMultiplier;
            _basket.Items.First().TotalPrice.InclVat.Should().Be(expectedResult);
        }

        [Test]
        public void TotalPriceExclVatOfBasketItemShouldBeCalculated()
        {
            _basket.Items.First().TotalPrice.ExclVat.Should().Be(_aLinuxPromo.PeriodPrice);
        }

        [Test]
        public void TotalPriceReductionExclVatOfBasketItemShouldBeCalculated()
        {
            var expectedResult = _aLinuxProduct.PeriodPrice - _aLinuxPromo.PeriodPrice;
            _basket.Items.First().TotalPrice.ReductionExclVat.Should().Be(expectedResult);
        }

        [Test]
        public void TotalPriceReductionInclVatOfBasketItemShouldBeCalculated()
        {
            var expectedResult = (_aLinuxProduct.PeriodPrice - _aLinuxPromo.PeriodPrice) * _vatMultiplier;
            _basket.Items.First().TotalPrice.ReductionInclVat.Should().Be(expectedResult);
        }

        [Test]
        public void PriceReductionInclVatOfBasketItemShouldBeCalculated()
        {
            var expectedResult = (_aLinuxProduct.PeriodPrice - _aLinuxPromo.PeriodPrice) * _vatMultiplier;
            _basket.Items.First().Price.ReductionInclVat.Should().Be(expectedResult);
        }

        [Test]
        public void PriceInclVatOfBasketItemShouldBeCalculated()
        {
            var expectedResult = (_aLinuxPromo.PeriodPrice) * _vatMultiplier;
            _basket.Items.First().Price.InclVat.Should().Be(expectedResult);
        }

        [Test]
        public void ShouldHaveCorrectItemCount()
        {
            _basket.ItemCount.Should().Be(1);
        }        
    }
}
