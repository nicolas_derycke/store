﻿using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.API.Tests.Baskets.BasketCreation
{
    [TestFixture]
    public class WhenCreatingALinuxExpressBasket : AbstractWhenCreatingAnLinuxBasket
    {
        protected override ItemAttributeDictionary GetAttributes()
        {
            return new ItemAttributeDictionary() 
                {
                    { AttributeKey.Domain, new DomainItemAttributes("zeno.be") }
                };
        }

        protected override BasketType ExpectedBasketType
        {
            get { return BasketType.Regular; }
        }
    }
}