﻿using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.API.Tests.Baskets.BasketCreation
{
    [TestFixture]
    public class WhenCreatingALinuxExpressUpgradeBasket : AbstractWhenCreatingAnLinuxBasket
    {
        public WhenCreatingALinuxExpressUpgradeBasket() : base(BasketType.Standalone)
        {}

        protected override ItemAttributeDictionary GetAttributes()
        {
            return new ItemAttributeDictionary() 
                {
                    { AttributeKey.Domain, new DomainItemAttributes("zeno.be") },
                    { AttributeKey.Hosting, new HostingAttributes(985) }
                };
        }        

        protected override BasketType ExpectedBasketType
        {
            get { return BasketType.Standalone; }
        }
    }
}