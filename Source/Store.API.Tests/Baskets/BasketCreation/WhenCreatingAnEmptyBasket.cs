﻿using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts.Baskets;
using Store.CommandHandlers.Baskets;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Data;
using Store.Providers;
using Store.Proxies;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;

namespace Store.API.Tests.Baskets.BasketCreation
{
    [TestFixture]
    public class WhenCreatingAnEmptyBasket : GivenWhenThen
    {
        private CreateBasketCommand _createBasketCommand;

        private ICustomerProxy _customerProxy;
        private IOrderCodeProvider _orderCodeProvider;
        private ISqlCommandHandler _sqlCommandHandler;
        private ICommandBus _commandBus;
        private IQueryHandler _queryHandler;
        private IBasketPricingService _basketPricingService;
        private IBasketService _basketService;

        private CreateBasketCommandHandler _createBasketCommandHandler;

        private string _orderCode;
        private int _basketId;

        protected override void Given()
        {
            _customerProxy = MockRepository.GenerateMock<ICustomerProxy>();
            _orderCodeProvider = MockRepository.GenerateMock<IOrderCodeProvider>();
            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _commandBus = MockRepository.GenerateMock<ICommandBus>();
            _queryHandler = MockRepository.GenerateMock<IQueryHandler>();
            _basketPricingService = MockRepository.GenerateMock<IBasketPricingService>();
            _basketService = MockRepository.GenerateMock<IBasketService>();

            var source = "Combell store";
            _createBasketCommand = new CreateBasketCommand(
                BasketType.Regular,
                CustomerIdentifier.FromCustomerNumber(129829),
                Provider.RegisterBe,
                88,
                Language.German,
                Enumerable.Empty<BasketItemContract>(),
                source);

            _orderCode = "ordercode";
            _orderCodeProvider.Expect(x => x.GenerateOrderCode(_createBasketCommand.Provider.Triplet)).Return(_orderCode).Repeat.Once();

            var vatRatePercentage = 0.99m;
            _customerProxy.Expect(x => x.GetVatRatePercentage(_createBasketCommand.CustomerIdentifier)).Return(Task.FromResult(vatRatePercentage)).Repeat.Once();

            _basketId = 951;
            _sqlCommandHandler.Expect(
                x => x.Execute(Arg<CreateBasket>.Matches(arg => 
                    arg.BasketType == BasketType.Regular && 
                    arg.CreationById == _createBasketCommand.CreationById && 
                    arg.CustomerIdentifier == _createBasketCommand.CustomerIdentifier && 
                    arg.OrderCode == _orderCode && 
                    arg.Provider == _createBasketCommand.Provider && 
                    arg.VatRatePercentage == vatRatePercentage &&
                    arg.Source == source))).
                WhenCalled(invocation => ((CreateBasket)invocation.Arguments.First()).BasketId = _basketId).
                Repeat.Once();

            _commandBus.Expect(x => x.Send(Arg<CreateBasketItemCommand>.Is.Anything)).Repeat.Never();

            _queryHandler.Expect(x => x.Execute(Arg<GetBasketsByCustomerQuery>.Is.Anything)).Return(Enumerable.Empty<BasketReferenceData>());

            _createBasketCommandHandler = new CreateBasketCommandHandler(_customerProxy, _orderCodeProvider, _sqlCommandHandler, _commandBus, _queryHandler, _basketPricingService, _basketService);
        }

        protected override void When()
        {
            _createBasketCommandHandler.Handle(_createBasketCommand);
        }

        [Test]
        public void ShouldCreateBasket()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }

        [Test]
        public void ShouldCreateZeroBasketItems()
        {
            _commandBus.VerifyAllExpectations();
        }

        [Test]
        public void ShouldCommandResultContainBasketItemIdAndOrderCode()
        {
            _createBasketCommand.Result.Should().NotBeNull();
            _createBasketCommand.Result.BasketId.Should().Be(_basketId);
            _createBasketCommand.Result.OrderCode.Should().Be(_orderCode);
        }
    }
}