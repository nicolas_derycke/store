﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.CommandHandlers.Baskets;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Data;
using Store.Model.Baskets;
using Store.Providers;
using Store.Proxies;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;

namespace Store.API.Tests.Baskets.BasketCreation
{
    [TestFixture]
    public class WhenCreatingADuplicateEmptyBasket : GivenWhenThen
    {
        private CreateBasketCommand _createBasketCommand;

        private GenericGiven _given;

        private ICustomerProxy _customerProxy;
        private IOrderCodeProvider _orderCodeProvider;
        private ISqlCommandHandler _sqlCommandHandler;
        private ICommandBus _commandBus;
        private IQueryHandler _queryHandler;
        private IBasketPricingService _basketPricingService;
        private IBasketService _basketService;

        private CreateBasketCommandHandler _createBasketCommandHandler;

        private string _orderCode;

        protected override void Given()
        {
            _given = new GenericGiven();

            _customerProxy = MockRepository.GenerateMock<ICustomerProxy>();
            _orderCodeProvider = MockRepository.GenerateMock<IOrderCodeProvider>();
            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _commandBus = MockRepository.GenerateMock<ICommandBus>();
            _queryHandler = MockRepository.GenerateMock<IQueryHandler>();
            _basketPricingService = MockRepository.GenerateMock<IBasketPricingService>();
            _basketService = MockRepository.GenerateMock<IBasketService>();

            var linuxExpressBasketItemContract = new BasketItemContract(
                0,
                "linux-express",
                "hosting",
                string.Empty,
                "Linux express",
                24,
                1,
                new ItemAttributeDictionary() 
                {
                    { AttributeKey.Domain, new DomainItemAttributes("zeno.be") }
                },
                true,
                null,
                null);

            _createBasketCommand = new CreateBasketCommand(
                BasketType.Regular,
                CustomerIdentifier.FromCustomerNumber(129829),
                Provider.RegisterBe,
                88,
                Language.German,
                new List<BasketItemContract>() { linuxExpressBasketItemContract },
                "Easyhost controlpanel");

            _orderCode = "ordercode";
            _orderCodeProvider.Expect(x => x.GenerateOrderCode(_createBasketCommand.Provider.Triplet)).Return(_orderCode).Repeat.Never();

            var vatRatePercentage = 0.99m;
            _customerProxy.Expect(x => x.GetVatRatePercentage(_createBasketCommand.CustomerIdentifier)).Return(Task.FromResult(vatRatePercentage)).Repeat.Once();

            _sqlCommandHandler.Expect(
                x => x.Execute(Arg<CreateBasket>.Is.Anything)).
                Repeat.Never();

            _queryHandler.
                Expect(x => x.Execute(Arg<GetBasketsByCustomerQuery>.Is.Anything)).
                Return(new[] { new BasketReferenceData("existingbasketordercode", _createBasketCommand.CustomerIdentifier.CustomerId, DateTime.Now, 1, "regular") }).
                Repeat.Once();

            var existingBasket = _given.ABasket(Enumerable.Empty<BasketItem>().ToList(), CustomerIdentifier.FromCustomerNumber(129829), "existingbasketordercode");

            _basketService.
                Expect(x => x.GetBasketByOrderCode("existingbasketordercode")).
                Return(Task.FromResult(existingBasket)).
                Repeat.Once();

            _commandBus.
                Expect(x => x.Send(Arg<CreateBasketItemCommand>.Matches(arg =>
                    arg.Basket.OrderCode == "existingbasketordercode" &&
                    arg.BasketItem.ProductCode == "linux-express"))).
                Repeat.Once();

            _createBasketCommandHandler = new CreateBasketCommandHandler(_customerProxy, _orderCodeProvider, _sqlCommandHandler, _commandBus, _queryHandler, _basketPricingService, _basketService);
        }

        protected override void When()
        {
            _createBasketCommandHandler.Handle(_createBasketCommand);
        }

        [Test]
        public void ShouldNotCreateBasketButUseExistingBasket()
        {
            _sqlCommandHandler.VerifyAllExpectations();
            _commandBus.VerifyAllExpectations();
        }

        [Test]
        public void ShouldRetrievePreviousBaskets()
        {
            _orderCodeProvider.VerifyAllExpectations();
            _queryHandler.VerifyAllExpectations();
            _basketService.VerifyAllExpectations();
            _createBasketCommand.Result.OrderCode.Should().Be("existingbasketordercode");
        }
    }
}