﻿using System.Collections.Generic;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.CommandHandlers.BasketItems;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.Validation;
using Store.Warnings;

namespace Store.API.Tests.Baskets.BasketItemRemoving
{
    [TestFixture]
    public class WhenDeletingAStandaloneBasketItem : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aLinuxPromo;

        private Basket _basket;
        private BasketItem _basketItem;
        private int _deletionById;

        private ISqlCommandHandler _sqlCommandHandler;

        private RemoveBasketItemCommandHandler _removeBasketItemCommandHandler;
      

        protected override void Given()
        {
            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            _basketItem = CreateABasketItemWithId(2);
            var basketItems = new List<BasketItem>
            {
                CreateABasketItemWithId(1),
                _basketItem,
                CreateABasketItemWithId(3)
            };

            _basket = _given.ABasket(basketItems);
            _deletionById = 88;

            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _sqlCommandHandler.Expect(x => x.Execute(Arg<RemoveBasketItem>.Matches(arg => arg.BasketItemId == _basketItem.BasketItemId && arg.DeletionById == _deletionById))).Repeat.Once();
            _sqlCommandHandler.Expect(x => x.Execute(Arg<RemoveBasketItem>.Matches(arg => arg.BasketItemId != _basketItem.BasketItemId))).Repeat.Never();
            _sqlCommandHandler.Expect(x => x.Execute(Arg<UpdateBasketItem>.Matches(arg => arg is UpdateBasketItem))).Repeat.Never();

           _removeBasketItemCommandHandler = new RemoveBasketItemCommandHandler(
               _sqlCommandHandler);
        }

        private BasketItem CreateABasketItemWithId(int id, int? parentId = null)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, _given.GetHostingAttributes(), id, parentId);
        }

        protected override void When()
        {
            _removeBasketItemCommandHandler.Handle(new RemoveBasketItemCommand(_basket, _basketItem, _deletionById));
        }

        [Test]
        public void ShouldDeleteTheRequestedItem()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }

    }
}
