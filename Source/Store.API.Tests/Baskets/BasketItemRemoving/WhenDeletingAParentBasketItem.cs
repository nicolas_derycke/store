﻿using System.Collections.Generic;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.CommandHandlers.BasketItems;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.Validation;
using Store.Warnings;

namespace Store.API.Tests.Baskets.BasketItemRemoving
{
    [TestFixture]
    public class WhenDeletingAParentBasketItem : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aLinuxPromo;

        private Basket _basket;
        private BasketItem _basketItem;
        private int _deletionById;

        private ISqlCommandHandler _sqlCommandHandler;

        private RemoveBasketItemCommandHandler _removeBasketItemCommandHandler;
      

        protected override void Given()
        {
            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            _basketItem = CreateABasketItemWithId(1);
            var basketItems = new List<BasketItem>
            {
                _basketItem,
                CreateABasketItemWithId(2, 1),
                CreateABasketItemWithId(3, 1)
            };

            _basket = _given.ABasket(basketItems);
            _deletionById = 88;

            var productInfo = new ProductInfo(111, "aproduct", "groupcode", string.Empty, "A product", "groupcode", 222.5m, 333, 0, 12, ProductAction.Renewal, true);

            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _sqlCommandHandler.Expect(x => x.Execute(Arg<RemoveBasketItem>.Matches(arg => arg.BasketItemId == _basketItem.BasketItemId && arg.DeletionById == _deletionById))).Repeat.Once();
            _sqlCommandHandler.Expect(x => x.Execute(Arg<RemoveBasketItem>.Matches(arg => arg.BasketItemId == 2 && arg.DeletionById == _deletionById))).Repeat.Once();
            _sqlCommandHandler.Expect(x => x.Execute(Arg<RemoveBasketItem>.Matches(arg => arg.BasketItemId == 3 && arg.DeletionById == _deletionById))).Repeat.Once();

          

            _removeBasketItemCommandHandler = new RemoveBasketItemCommandHandler(
                _sqlCommandHandler);
        }

        private BasketItem CreateABasketItemWithId(int id, int? parentId = null)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, _given.GetHostingAttributes(), id, parentId);
        }

        protected override void When()
        {
            _removeBasketItemCommandHandler.Handle(new RemoveBasketItemCommand(_basket, _basketItem, _deletionById));
        }

        [Test]
        public void ShouldDeleteTheRequestedItemAndChilds()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }
    }
}