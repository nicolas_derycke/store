﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.CommandHandlers.BasketItems;
using Store.Commands.BasketItems;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.SqlCommands;
using Store.Validation;
using Store.Warnings;

namespace Store.API.Tests.Baskets.BasketItemRemoving
{
    [TestFixture]
    public class WhenDeletingAChildBasketItem : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aLinuxPromo;

        private Basket _basket;
        private BasketItem _basketItem;
        private int _deletionById;

        private ISqlCommandHandler _sqlCommandHandler;

        private RemoveBasketItemCommandHandler _removeBasketItemCommandHandler;

        private Action deleteItemAction;

        protected override void Given()
        {
            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            _basketItem = CreateABasketItemWithId(2, 1);
            var basketItems = new List<BasketItem>
            {
                CreateABasketItemWithId(1),
                _basketItem,
                CreateABasketItemWithId(3, 1),
                CreateABasketItemWithId(4)
            };

            _basket = _given.ABasket(basketItems);
            _deletionById = 88;

            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            
            _removeBasketItemCommandHandler = new RemoveBasketItemCommandHandler(
                _sqlCommandHandler);
        }

        private BasketItem CreateABasketItemWithId(int id, int? parentId = null)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, _given.GetHostingAttributes(), id, parentId);
        }

        protected override void When()
        {
            deleteItemAction = () => _removeBasketItemCommandHandler.Handle(new RemoveBasketItemCommand(_basket, _basketItem, _deletionById)); ;
        }

        [Test]
        public void ShouldThrowException()
        {
            deleteItemAction.ShouldThrow<ChildBasketItemDeletionException>();
        }
    }
}
