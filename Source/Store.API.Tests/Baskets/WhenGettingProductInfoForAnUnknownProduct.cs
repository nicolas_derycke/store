﻿using System;
using System.Linq;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.Providers;

namespace Store.API.Tests.Baskets
{
    public class WhenGettingProductInfoForAnUnknownProduct : GivenWhenThen
    {
        private BasketItem _basketItem;
        private Action _productInfo;
        private IProductInfoProvider _productProvider;

        protected override void Given()
        {
            var given = new GenericGiven();
            const int productPeriod = 3;

            _basketItem = new BasketItem(
                0,
                1,
                "nullProduct",
                "hosting",
                "linux-express-promo",
                "Linux express",
                productPeriod,
                123,
                1,
                new Price(100, 0, 21),
                given.GetHostingAttributes(),
                0.21m,
                BasketItemValidationError.None,
                BasketItemWarning.None,
                true,
                false);

            _productProvider = given.AProductProvider();
        }

        protected override void When()
        {
            _productInfo = () => _productProvider.GetProductInfo(_basketItem, CustomerIdentifier.FromCustomerNumber(0), (int)ProviderValue.Combell, Language.Dutch).ToList();
        }

        [Test]
        public void ShouldThrowProductNotFoundException()
        {
            _productInfo.ShouldThrow<ProductNotFoundException>();
        }
    }
}