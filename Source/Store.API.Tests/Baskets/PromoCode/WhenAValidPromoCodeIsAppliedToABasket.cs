﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.CommandHandlers.Baskets;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Providers;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.API.Tests.Baskets
{
    [TestFixture]
    public class WhenAValidPromoCodeIsAppliedToABasket : GivenWhenThen
    {
        private ApplyPromoCodeToBasketCommandHandler _applyPromoCodeToBasketCommandHandler;

        private ISqlCommandHandler _sqlCommandHandler;
        private ApplyPromoCodeToBasketCommand _command;

        private BasketItem _originalBasketItem;
        private BasketItem _updatedBasketItem;

        private string _promoCode;

        private IPromoCodeInfoProvider _promoCodeInfoProvider;
        private IProductInfoProvider _productInfoProvider;


        protected override void Given()
        {
            var given = new GenericGiven();

            _promoCodeInfoProvider = MockRepository.GenerateMock<IPromoCodeInfoProvider>();
            _productInfoProvider = MockRepository.GenerateMock<IProductInfoProvider>();

            var basicEmailProduct = given.AProduct("basic-e-mail-25-gb", ProductTypes.BasicEmail);
            _originalBasketItem = given.ABasketItemForProduct(basicEmailProduct, 12, 1, new ItemAttributeDictionary(), 1);
            _updatedBasketItem = given.ABasketItemForProduct(basicEmailProduct, 12, 1, new ItemAttributeDictionary(), 1);
            given.AProductProvider();
            var basket = given.ABasket(new[] { _updatedBasketItem }.ToList(), language: Language.English);

            _promoCode = "awesomepromo";
            _command = new ApplyPromoCodeToBasketCommand(_promoCode, basket, 88);

            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _sqlCommandHandler.Expect(bus => bus.Execute(Arg<UpdateBasketItem>.Matches(c => c.BasketItem.BasketItemId == _updatedBasketItem.BasketItemId)));

            _promoCodeInfoProvider.
                Expect(x => x.GetPromoCodeInfo(_promoCode, basket.Provider.Id)).
                Return(new PromoCodeInfo(_promoCode, new List<PromoProductInfo>()
                {
                    new PromoProductInfo(_updatedBasketItem.PeriodId, _updatedBasketItem.Period, _updatedBasketItem.ProductName, _updatedBasketItem.ProductId, 2, 0)
                })).
                Repeat.Once();

            _productInfoProvider.
                Expect(
                x => x.GetProductInfo(
                    _originalBasketItem.ProductCode,
                    _originalBasketItem.PromoProductCode,
                    _originalBasketItem.Period,
                    basket.CustomerIdentifier,
                    basket.Provider.Id,
                    Language.English,
                    "zeno.be")).
                Return(new List<ProductInfo>() { basicEmailProduct }).
                Repeat.Once();

            _productInfoProvider.
                Expect(
                x => x.GetProductInfo(_updatedBasketItem,
                    basket.CustomerIdentifier,
                    basket.Provider.Id,
                    Language.English)).
                Return(new List<ProductInfo>() { basicEmailProduct }).
                Repeat.Once();

            IBasketItemPricingService pricingService = new BasketItemPricingService(_productInfoProvider);

            _applyPromoCodeToBasketCommandHandler = new ApplyPromoCodeToBasketCommandHandler(_sqlCommandHandler, _promoCodeInfoProvider, pricingService);
        }

        protected override void When()
        {
            _applyPromoCodeToBasketCommandHandler.Handle(_command);
        }

        [Test]
        public void TheUpdateBasketItemCommandIsSentForTheCorrectBasketItemId()
        {
            _sqlCommandHandler.VerifyAllExpectations();
        }

        [Test]
        public void ThePromoCodeIsUsedToRetrieveInfo()
        {
            _promoCodeInfoProvider.VerifyAllExpectations();
        }

        [Test]
        public void TheItemPriceIsUpdated()
        {
            _updatedBasketItem.Price.ExclVat.Should().Be(0);
            _updatedBasketItem.Price.ReductionExclVat.Should().Be(_originalBasketItem.TotalPrice.ExclVat);
        }
    }
}