﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.Model.Baskets;

namespace Store.API.Tests.Baskets.Revalidation
{
    [TestFixture]
    public class WhenCheckingForRevalidation : GivenWhenThen
    {
        private Basket _basket1;
        private Basket _basket2;
        private Basket _basket3;
        private Basket _basket4;

        private int _revalidationOffsetInDays;

        private bool _isRevalidationRequiredForBasket1;
        private bool _isRevalidationRequiredForBasket2;
        private bool _isRevalidationRequiredForBasket3;
        private bool _isRevalidationRequiredForBasket4;

        protected override void Given()
        {
            _revalidationOffsetInDays = 1;

            _basket1 = GetBasket(DateTime.Now);
            _basket2 = GetBasket(DateTime.Now.AddDays(-1).AddMinutes(1));
            _basket3 = GetBasket(DateTime.Now.AddDays(-2));
            _basket4 = GetBasket(DateTime.Now.AddDays(1));
        }

        protected override void When()
        {
            _isRevalidationRequiredForBasket1 = _basket1.IsRevalidationRequired(_revalidationOffsetInDays);
            _isRevalidationRequiredForBasket2 = _basket2.IsRevalidationRequired(_revalidationOffsetInDays);
            _isRevalidationRequiredForBasket3 = _basket3.IsRevalidationRequired(_revalidationOffsetInDays);
            _isRevalidationRequiredForBasket4 = _basket4.IsRevalidationRequired(_revalidationOffsetInDays);
        }

        private Basket GetBasket(DateTime lastValidationDate)
        {
            var given = new GenericGiven();
            var aLinuxProduct = given.ALinuxExpressProduct(3);
            var basketItem = given.ABasketItemForProduct(aLinuxProduct, 3, 1, given.GetHostingAttributes());
            return given.ABasket(new List<BasketItem> { basketItem }, lastValidationDate);
        }

        [Test]
        public void ShouldRevalidate()
        {
            _isRevalidationRequiredForBasket3.Should().BeTrue();
        }

        [Test]
        public void ShouldNotRevalidate()
        {
            _isRevalidationRequiredForBasket1.Should().BeFalse();
            _isRevalidationRequiredForBasket2.Should().BeFalse();
            _isRevalidationRequiredForBasket4.Should().BeFalse();
        }
    }
}
