using Billing.API.Common.Interface.Contracts.Data;
using Intelligent.Core.Models;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.CommandHandlers.BasketItems;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using ILogger = Serilog.ILogger;

namespace Store.API.Tests.Baskets
{
    [TestFixture]
    public class WhenEvaluatingTaxPercentageForBasketForEuropeanInstitutionWithoutVatNeeded : GivenWhenThen
    {
        private EvaluateTaxPercentageCommandHandler _handler;
        private ICustomerInvoiceCriteriaProxy _customerInvoiceCriteriaProxy;
        private ILogger _logger;
        private ISqlCommandHandler _commandHandler;
        private ICustomerProxy _customerProxy;
        private BasketItem _basketItem;
        private Basket _basket;
        private string _orderCode;

        protected override void Given()
        {
            _customerInvoiceCriteriaProxy = MockRepository.GenerateMock<ICustomerInvoiceCriteriaProxy>();
            _logger = MockRepository.GenerateMock<ILogger>();
            _commandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();
            _customerProxy = MockRepository.GenerateMock<ICustomerProxy>();
            _handler = new EvaluateTaxPercentageCommandHandler(_customerInvoiceCriteriaProxy, _logger, _commandHandler, _customerProxy);

            var given = new GenericGiven();
            _basketItem = given.ABasketItemForProduct(
                new ProductInfo(111, "aproduct", "groupcode", string.Empty, "A product", "groupcode", 200, 333, 0, 12, ProductAction.Renewal, true),
                12,
                1,
                new ItemAttributeDictionary());
            var customerIdentifer = CustomerIdentifier.FromCustomerNumber(45454);
            _customerInvoiceCriteriaProxy
                .Stub(x => x.GetByCustomer(customerIdentifer))
                .Return(new CustomerInvoiceCriteriaDataContract
                {
                    IsEuropeanInstitution = true
                });
            _orderCode = "COM-45445454";
            _basket = given.ABasket(new[] { _basketItem }, customerIdentifer, _orderCode);
        }

        protected override void When()
        {
            _handler.Handle(new EvaluateTaxPercentageCommand(_basket));
        }

        [Test]
        public void TaxPercentageIsSetToZero()
        {
            Assert.That(_basket.VatRatePercentage, Is.EqualTo(0));
            Assert.That(_basketItem.Price.VatRate, Is.EqualTo(0));
            Assert.That(_basketItem.Price.ExclVat, Is.EqualTo(_basketItem.Price.InclVat));
        }

        [Test]
        public void BasketVatRatePercentageIsUpdated()
        {
            _commandHandler.AssertWasCalled(x => x.Execute(Arg<UpdateBasketVatPercentageCommand>.Matches(c => c.VatRatePercentage == 0 && c.OrderCode == _orderCode)));
        }

        [Test]
        public void BasketItemsAreUpdated()
        {
            _commandHandler.AssertWasCalled(x => x.Execute(Arg<UpdateBasketItem>.Matches(c => c.BasketItem == _basketItem)));
        }
    }
}