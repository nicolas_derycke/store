using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Data;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.CommandHandlers.Baskets;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Providers.Attributes;
using Store.Proxies;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.SqlCommands.Baskets;
using Store.Validation;
using Store.Warnings;
using Serilog;

namespace Store.API.Tests.Baskets.BasketCustomerChanging
{
    [TestFixture]
    public class WhenChangingTheCustomerOnABasket : GivenWhenThen
    {
        private ProductInfo _aLinuxProduct;
        private ProductInfo _aLinuxPromo;
        private BasketItem _basketItem;
        private Basket _basket;
        private CustomerIdentifier _customerIdentifier;
        private Language _language;

        private ChangeBasketCustomerCommandHandler _changeCustomerCommandHandler;
        private ISqlCommandHandler _sqlCommandHandler;
        private const int customerReductionPercentage = 5;
        private const decimal customerVatRatePercentage = 17;
        private const int customerId = 48056;
        private IAttributeProvider _attributeProvider;
        private IWarningProvider _warningProvider;
        private ILiveBasketItemValidator _liveValidator;
        private ICommandBus _commandBus;
        private IBasketItemPricingService _basketItemPricingService;
        private ILogger _logger;

        private BasketItemWarning _basketItemWarning;
        private BasketItemValidationError _basketItemValidationError;

        protected override void Given()
        {
            var given = new GenericGiven();
            const int productPeriod = 3;
            _aLinuxProduct = given.ALinuxExpressProduct(productPeriod);
            _aLinuxPromo = given.ALinuxExpressPromoProduct(productPeriod);

            _customerIdentifier = CustomerIdentifier.FromCustomerId(customerId);
            _language = Language.English;

            _basketItem = given.ABasketItemForProduct(_aLinuxProduct, productPeriod, 1, given.GetHostingAttributes());
            _basket = given.ABasket(new List<BasketItem> { _basketItem }, language: _language);
            _sqlCommandHandler = MockRepository.GenerateMock<ISqlCommandHandler>();

            var sqlConnectionProvider = MockRepository.GenerateStub<ISqlConnectionProvider>();

            var customerProxy = MockRepository.GenerateMock<ICustomerProxy>();
            customerProxy
                .Stub(x => x.GetVatRatePercentage(Arg<CustomerIdentifier>.Is.Anything))
                .Return(Task.FromResult(customerVatRatePercentage));

            _attributeProvider = MockRepository.GenerateMock<IAttributeProvider>();
            _warningProvider = MockRepository.GenerateMock<IWarningProvider>();
            _liveValidator = MockRepository.GenerateMock<ILiveBasketItemValidator>();
            _commandBus = MockRepository.GenerateMock<ICommandBus>();

            _attributeProvider.
                Expect(x => x.Get(_basket, _basketItem)).
                Return(new ItemAttributeKeyValuePair(AttributeKey.Registrant, RegistrantAttributes.Empty)).
                Repeat.Once();

            _basketItemWarning = BasketItemWarning.BasicEmailAccountExists;
            _warningProvider.
                Expect(x => x.Validate(_basketItem, _basket)).
                Return(new[] { _basketItemWarning }).
                Repeat.Once();

            _basketItemValidationError = BasketItemValidationError.TrademarkChanged;
            _liveValidator.
                Expect(x => x.Validate(_basketItem, _basket)).
                Return(new[] { _basketItemValidationError }).
                Repeat.Once();

            _commandBus.
                Expect(x => x.Send(Arg<MergeBasketsCommand>.Matches(arg => arg.Basket == _basket))).
                Repeat.Once();

            _basketItemPricingService = MockRepository.GenerateMock<IBasketItemPricingService>();
            _basketItemPricingService.
                Expect(x => x.SetProductInfoAndPrice(_basketItem, _customerIdentifier, _basket.Provider, _language, customerVatRatePercentage)).
                Repeat.Once();

            _logger = MockRepository.GenerateMock<ILogger>();

            _changeCustomerCommandHandler =
                new ChangeBasketCustomerCommandHandler(
                    customerProxy,
                    _basketItemPricingService,
                    new[] { _attributeProvider },
                    _sqlCommandHandler,
                    new[] { _warningProvider },
                    new[] { _liveValidator },
                    _commandBus,
                    _logger);
        }

        protected override void When()
        {
            _changeCustomerCommandHandler.Handle(new ChangeBasketCustomerCommand(_basket, _customerIdentifier, 88));
        }

        [Test]
        public void ShouldAddWarnings()
        {
            _warningProvider.VerifyAllExpectations();
            _basketItem.Warning.Should().Be(_basketItemWarning);
        }

        [Test]
        public void ShouldAddLiveValidationErrors()
        {
            _liveValidator.VerifyAllExpectations();
            _basketItem.ValidationError.Should().Be(_basketItemValidationError);
        }

        [Test]
        public void ShouldProvisionAttributes()
        {
            _attributeProvider.VerifyAllExpectations();
            _basketItem.Attributes.ContainsKey(AttributeKey.Registrant).Should().BeTrue();
        }

        [Test]
        public void TheBasketItemIsUpdatedWithTheNewPriceForTheSamePeriod()
        {
            _sqlCommandHandler.AssertWasCalled(bus => bus.Execute(Arg<UpdateBasketItem>.Matches(c => c.BasketItem.Period == _basketItem.Period)));
        }

        [Test]
        public void TheBasketItemIsUpdatedWithTheNewVatRate()
        {
            _basketItemPricingService.VerifyAllExpectations();
        }

        [Test]
        public void TheCustomerIsUpdatedOnTheBasket()
        {
            _sqlCommandHandler.AssertWasCalled(bus => bus.Execute(Arg<UpdateBasketCustomer>.Matches(c => c.CustomerIdentifier.CustomerId == customerId)));
        }

        [Test]
        public void ShouldMergePreviousBaskets()
        {
            _commandBus.VerifyAllExpectations();
        }
    }
}