﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Rhino.Mocks;
using Store.CommandHandlers.Baskets;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Providers.Attributes;
using Store.Proxies;
using Store.Services;
using Store.SqlCommands;
using Store.Validation;
using Store.Warnings;
using Serilog;

namespace Store.API.Tests.Baskets.BasketCustomerChanging
{
    [TestFixture]
    public class WhenChangingTheCustomerOnAnIdentifiedBasket : GivenWhenThen
    {
        private GenericGiven _given;
        private ProductInfo _aLinuxPromo;

        private Basket _basket;
        private CustomerIdentifier _newCustomerIdentifier;

        private ChangeBasketCustomerCommandHandler _changeBasketCustomerCommandHandler;
        private ILogger _logger;

        private Action _executeCommandHandler;

        protected override void Given()
        {
            _given = new GenericGiven();
            _aLinuxPromo = _given.ALinuxExpressPromoProduct(12);
            var basketItems = new List<BasketItem>
            {
                CreateABasketItemWithId(1),
                CreateABasketItemWithId(3),
                CreateABasketItemWithId(4)
            };

            _basket = _given.ABasket(basketItems, CustomerIdentifier.FromCustomerNumber(123456));
            _newCustomerIdentifier = CustomerIdentifier.FromCustomerNumber(129829);

            _logger = MockRepository.GenerateMock<ILogger>();

            _changeBasketCustomerCommandHandler = 
                new ChangeBasketCustomerCommandHandler(                     
                     MockRepository.GenerateMock<ICustomerProxy>(),
                     MockRepository.GenerateMock<IBasketItemPricingService>(),
                     new [] { MockRepository.GenerateMock<IAttributeProvider>() },
                     MockRepository.GenerateMock<ISqlCommandHandler>(),
                     new [] { MockRepository.GenerateMock<IWarningProvider>() },
                     new [] { MockRepository.GenerateMock<ILiveBasketItemValidator>() },
                     MockRepository.GenerateMock<ICommandBus>(),
                     _logger);
        }

        private BasketItem CreateABasketItemWithId(int id)
        {
            return _given.ABasketItemForProduct(_aLinuxPromo, _aLinuxPromo.Period, 1, _given.GetHostingAttributes(), id);
        }

        protected override void When()
        {
            _executeCommandHandler = () => _changeBasketCustomerCommandHandler.Handle(new ChangeBasketCustomerCommand(_basket, _newCustomerIdentifier, 8));
        }

        [Test]
        public void ThenShouldThrowException()
        {
            _executeCommandHandler.ShouldThrow<NotSupportedException>();
        }
    }
}