﻿using System;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts.Orders;
using Store.Data;
using Store.Mapping;

namespace Store.API.Tests.Mapping
{
    [TestFixture]
    public class WhenMappingOrderStateDataToContract : GivenWhenThen
    {
        private OrderStateData _source;
        private OrderStateContract _result;

        protected override void Given()
        {
            _source = new OrderStateData
            {
                ActivationMethod = "Manual",
                OrderCode = "orderCode",
                ProvisioningStatusLastUpdateDate = DateTime.Now,
                InvoicingStatusLastUpdateDate = DateTime.Now,
                ActivationStatusLastUpdateDate = DateTime.Now,
                InvoicingStatus = "Invoiced",
                PaymentStatusLastUpdateDate = DateTime.Now,
                InitiatedBy = "Jef",
                ActivationBy = "Jos",
                PaymentStatus = "Paid",
                ActivationReason = "Because I can",
                ProvisioningStatus = "NotProvisioned",
                ActivationStatus = "Activated",
                InvoiceType = "Regular invoice",
                InvoiceId = 4545454,
                IsInitiatedByEmployee = true
            };
        }

        protected override void When()
        {
            _result = OrderStateDataToOrderStateContractMapper.Map(_source);
        }

        [Test]
        public void EveryPropertyIsMappedCorrectly()
        {
            Assert.That(_result.OrderCode, Is.EqualTo("orderCode"));
            Assert.That(_result.InitiatedBy, Is.EqualTo("Jef"));
            Assert.That(_result.IsInitiatedByEmployee, Is.True);

            Assert.That(_result.ActivationState, Is.Not.Null);
            Assert.That(_result.ActivationState.ActivationMethod, Is.EqualTo(ActivationMethodContract.Manual));
            Assert.That(_result.ActivationState.ActivationBy, Is.EqualTo("Jos"));
            Assert.That(_result.ActivationState.ActivationReason, Is.EqualTo("Because I can"));
            Assert.That(_result.ActivationState.LastUpdateDate, Is.EqualTo(_source.ActivationStatusLastUpdateDate));
            Assert.That(_result.ActivationState.Status, Is.EqualTo(OrderActivationStatusContract.Activated));

            Assert.That(_result.InvoiceState, Is.Not.Null);
            Assert.That(_result.InvoiceState.LastUpdateDate, Is.EqualTo(_source.InvoicingStatusLastUpdateDate));
            Assert.That(_result.InvoiceState.InvoiceId, Is.EqualTo(_source.InvoiceId));
            Assert.That(_result.InvoiceState.InvoiceType, Is.EqualTo(_source.InvoiceType));
            Assert.That(_result.InvoiceState.Status, Is.EqualTo(OrderInvoicingStatusContract.Invoiced));

            Assert.That(_result.PaymentState, Is.Not.Null);
            Assert.That(_result.PaymentState.LastUpdateDate, Is.EqualTo(_source.PaymentStatusLastUpdateDate));
            Assert.That(_result.PaymentState.Status, Is.EqualTo(OrderPaymentStatusContract.Paid));

            Assert.That(_result.ProvisioningState, Is.Not.Null);
            Assert.That(_result.ProvisioningState.LastUpdateDate, Is.EqualTo(_source.ProvisioningStatusLastUpdateDate));
            Assert.That(_result.ProvisioningState.Status, Is.EqualTo(OrderProvisioningStatusContract.NotProvisioned));
        }
    }
}