﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Intelligent.Core.Models;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Mapping;
using Store.Model;
using Store.Model.Baskets;

namespace Store.API.Tests.Mapping
{
    [TestFixture]
    public class WhenMappingBasketToBasketContract : GivenWhenThen
    {
        private Basket _basket;
        private BasketItem _basketItem;
        private KeyValuePair<AttributeKey, IBasketItemAttribute> _attribute;

        private BasketContract _basketContract;

        protected override void Given()
        {
            _attribute = new KeyValuePair<AttributeKey, IBasketItemAttribute>(AttributeKey.Domain, new DomainItemAttributes("zeno.be"));
            var attributes = new ItemAttributeDictionary();
            attributes.Add(_attribute.Key, _attribute.Value);

            _basketItem = new BasketItem(
                98,
                9871236,
                "domeinnaam-be",
                "be",
                "domeinnaam-be-promo",
                "Domeinnaam .be",
                24,
                1234,
                1,
                new Price(1.2m, 3.4m, 5.6m),
                attributes,
                21.0m,
                BasketItemValidationError.InactiveProduct,
                BasketItemWarning.TransferTradeNotSupported,
                true,
                false);

            var basketItems = new List<BasketItem>() { _basketItem };

            _basket = new Basket(
                87,
                "COM9d5edsrgf",
                CustomerIdentifier.FromCustomerNumber(129829),
                basketItems,
                21.0m,
                new DateTime(2011, 2, 3),
                true,
                new DateTime(2011, 2, 4),
                Provider.EasyHost,
                BasketType.Regular,
                false,
                "combell.store",
                Language.English,
                false,
                "");

            MappingConfiguration.ConfigureMappings();
        }

        protected override void When()
        {
            _basketContract = _basket.MapTo<BasketContract>();
        }

        [Test]
        public void ShouldContainAllData()
        {
            _basketContract.Should().NotBeNull();

            var basketItemContract = _basketContract.Items.First();
            var attributesContract = basketItemContract.Attributes.First();

            _basketContract.CreationDate.Should().Be(_basket.CreationDate);
            _basketContract.CustomerNumber.Should().Be(_basket.CustomerIdentifier.CustomerNumber);
            _basketContract.ItemCount.Should().Be(_basket.ItemCount);
            _basketContract.LastValidationDate.Should().Be(_basket.LastValidationDate);
            _basketContract.OrderCode.Should().Be(_basket.OrderCode);
            _basketContract.TotalPrice.ExclVat.Should().Be(_basket.TotalPrice.ExclVat);
            _basketContract.TotalPrice.InclVat.Should().Be(_basket.TotalPrice.InclVat);
            _basketContract.TotalPrice.ReductionExclVat.Should().Be(_basket.TotalPrice.ReductionExclVat);
            _basketContract.TotalPrice.ReductionInclVat.Should().Be(_basket.TotalPrice.ReductionInclVat);
            _basketContract.VatAmount.Should().Be(_basket.VatAmount);
            _basketContract.VatRatePercentage.Should().Be(_basket.VatRatePercentage);

            basketItemContract.BasketItemId.Should().Be(_basketItem.BasketItemId);
            basketItemContract.Period.Should().Be(_basketItem.Period);
            basketItemContract.PeriodUnitType.ToString().Should().Be(_basketItem.PeriodUnitType.ToString());
            basketItemContract.Price.ExclVat.Should().Be(_basketItem.Price.ExclVat);
            basketItemContract.Price.InclVat.Should().Be(_basketItem.Price.InclVat);
            basketItemContract.Price.ReductionExclVat.Should().Be(_basketItem.Price.ReductionExclVat);
            basketItemContract.Price.ReductionInclVat.Should().Be(_basketItem.Price.ReductionInclVat);
            basketItemContract.ProductCode.Should().Be(_basketItem.ProductCode);
            basketItemContract.ProductType.Should().Be(_basketItem.ProductType);
            basketItemContract.ProductName.Should().Be(_basketItem.ProductName);
            basketItemContract.PromoProductCode.Should().Be(_basketItem.PromoProductCode);
            basketItemContract.Quantity.Should().Be(_basketItem.Quantity);
            basketItemContract.TotalPrice.ExclVat.Should().Be(_basketItem.TotalPrice.ExclVat);
            basketItemContract.TotalPrice.InclVat.Should().Be(_basketItem.TotalPrice.InclVat);
            basketItemContract.TotalPrice.ReductionExclVat.Should().Be(_basketItem.TotalPrice.ReductionExclVat);
            basketItemContract.TotalPrice.ReductionInclVat.Should().Be(_basketItem.TotalPrice.ReductionInclVat);
            basketItemContract.ValidationError.Should().Be(_basketItem.ValidationError);
            basketItemContract.Warning.Should().Be(_basketItem.Warning);

            attributesContract.Key.Should().Be(_attribute.Key);
            attributesContract.Value.Should().Be(_attribute.Value);
        }
    }
}
