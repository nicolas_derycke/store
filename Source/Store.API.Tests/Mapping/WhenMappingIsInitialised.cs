﻿using System;
using AutoMapper;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.Mapping;

namespace Store.API.Tests.Mapping
{
    public class WhenMappingIsInitialised : GivenWhenThen
    {
        private Action _mapping;
        protected override void Given()
        {
        }

        protected override void When()
        {
            _mapping = () => MappingConfiguration.ConfigureMappings();
        }


        [Test]
        public void MappingShouldNotThrowInvalidMappingException()
        {
            _mapping.ShouldNotThrow<AutoMapperConfigurationException>();
        }
    }
}
