﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Intelligent.Shared.Testing.NUnit;
using NUnit.Framework;
using Store.API.Contracts;
using Store.Mapping;
using Store.Model.Exceptions;

namespace Store.API.Tests.Mapping
{
    public class WhenMappingAttributeKeyStringToAttributeKeyEnum : GivenWhenThen
    {
        private string _attributeKey;
        private AttributeKey _attributeKeyEnum;
        private Action _mapAttribute;
        protected override void Given()
        {
            _attributeKey = "domain";
        }

        protected override void When()
        {
            _mapAttribute = () =>
            {
                _attributeKeyEnum = AttributeKeyMapper.Map(_attributeKey);
            };
        }

        [Test]
        public void ShouldNotThrowException()
        {
            _mapAttribute.ShouldNotThrow<InvalidBasketItemAttributeException>();
        }
    }
}
