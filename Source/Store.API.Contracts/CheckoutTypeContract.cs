﻿namespace Store.API.Contracts
{
    public enum CheckoutTypeContract
    {
        Free,
        BankTransfer,
        CreditCardPayment,
        DebitCardPayment,
        DirectActivation
    }
}