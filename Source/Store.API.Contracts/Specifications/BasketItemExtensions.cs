﻿using System;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.API.Contracts.Specifications
{
    public static class BasketItemExtensions
    {
        public static bool IsUniqueIp(this BasketItemContract basketItem)
        {
            return basketItem.ProductCode == "uniek-ip-adres-voor-linux-hosting" || basketItem.ProductCode == "uniek-ip-adres-voor-windows-hosting";
        }

        public static bool IsPreRegistration(this BasketItemContract basketItem)
        {
            return basketItem.ProductCode == "pre-registration";
        }

        public static bool IsEap(this BasketItemContract basketItem)
        {
            var isGoLive = false;
            var isLandRush = false;
            if (basketItem.Attributes.ContainsKey(AttributeKey.Preregistration))
            {
                var preregistrationAttribute = (PreregistrationAttributes)basketItem.Attributes[AttributeKey.Preregistration];
                if (preregistrationAttribute.IsGoLive || preregistrationAttribute.IsLandRush)
                {
                    isGoLive = preregistrationAttribute.IsGoLive;
                    isLandRush = preregistrationAttribute.IsLandRush;
                }
            }

            return (isGoLive || isLandRush);
        }
    }
}