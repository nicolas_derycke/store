﻿using Store.API.Contracts.Orders;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.API.Contracts.Specifications
{
    public static class OrderItemExtensions
    {
        public static bool IsUniqueIp(this OrderItemContract orderItem)
        {
            return orderItem.ProductCode == "uniek-ip-adres-voor-linux-hosting" || orderItem.ProductCode == "uniek-ip-adres-voor-windows-hosting";
        }

        public static bool IsPreRegistration(this OrderItemContract orderItem)
        {
            return orderItem.ProductCode == "pre-registration";
        }

        public static bool IsEap(this OrderItemContract orderItem)
        {
            var isGoLive = false;
            var isLandRush = false;
            if (orderItem.Attributes.ContainsKey(AttributeKey.Preregistration))
            {
                var preregistrationAttribute = (PreregistrationAttributes)orderItem.Attributes[AttributeKey.Preregistration];
                if (preregistrationAttribute.IsGoLive || preregistrationAttribute.IsLandRush)
                {
                    isGoLive = preregistrationAttribute.IsGoLive;
                    isLandRush = preregistrationAttribute.IsLandRush;
                }
            }

            return (isGoLive || isLandRush);
        }
    }
}