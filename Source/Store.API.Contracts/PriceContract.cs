﻿namespace Store.API.Contracts
{
    public class PriceContract
    {
        public decimal InclVat { get; set; }
        public decimal ExclVat { get; set; }
        public decimal ReductionInclVat { get; set; }
        public decimal ReductionExclVat { get; set; }        
    }
}
