﻿namespace Store.API.Contracts
{
    public class ItemAttributeKeyValuePair
    {
        public AttributeKey Key { get; private set; }

        public IBasketItemAttribute Value { get; private set; }
        
        public ItemAttributeKeyValuePair(AttributeKey key, IBasketItemAttribute value)
        {
            Key = key;
            Value = value;
        }
    }
}