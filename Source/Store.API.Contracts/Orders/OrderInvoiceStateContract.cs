namespace Store.API.Contracts.Orders
{
    public class OrderInvoiceStateContract : OrderSubStateContract<OrderInvoicingStatusContract>
    {
        public int? InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceType { get; set; }
    }
}