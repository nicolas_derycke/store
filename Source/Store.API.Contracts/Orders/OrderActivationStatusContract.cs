namespace Store.API.Contracts.Orders
{
    public enum OrderActivationStatusContract
    {
        NotActivated,
        Activated,
        Failed
    }
}