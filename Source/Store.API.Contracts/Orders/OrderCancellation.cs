﻿namespace Store.API.Contracts.Orders
{
    public class OrderCancellation
    {
        public string OrderCode { get; private set; }
        public int CancelledById { get; private set; }
        public string CancelledBy { get; private set; }
        public string Reason { get; private set; }

        public OrderCancellation(string orderCode, string reason, int cancelledById, string cancelledBy)
        {
            OrderCode = orderCode;
            Reason = reason;
            CancelledById = cancelledById;
            CancelledBy = cancelledBy;
        }
    }
}