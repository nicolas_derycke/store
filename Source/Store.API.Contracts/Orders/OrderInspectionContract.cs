﻿using System.Collections.Generic;

namespace Store.API.Contracts.Orders
{
    public class OrderInspectionContract
    {
        public OrderStateContract OrderState { get; set; }
        public IEnumerable<OrderVerificationContract> OrderVerifications { get; set; }
    }
}