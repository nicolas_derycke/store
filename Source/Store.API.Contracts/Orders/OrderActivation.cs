﻿namespace Store.API.Contracts.Orders
{
    public class OrderActivation
    {
        public string OrderCode { get; private set; }
        public string ActivationReason { get; private set; }
        public int ActivationById { get; private set; }
        public string ActivationBy { get; private set; }
        
        public OrderActivation(string orderCode, string activationReason, int activationById, string activationBy)
        {
            OrderCode = orderCode;
            ActivationReason = activationReason;
            ActivationById = activationById;
            ActivationBy = activationBy;
        }
    }
}