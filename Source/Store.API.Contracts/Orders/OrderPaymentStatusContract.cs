    namespace Store.API.Contracts.Orders
{
    public enum OrderPaymentStatusContract
    {
        NotPaid,
        Paid
    }
}