using System;

namespace Store.API.Contracts.Orders
{
    public class OrderSubStateContract<TStatus>
        where TStatus : struct
    {
        public TStatus Status { get; set; }
        public DateTime LastUpdateDate { get; set; }
    }
}