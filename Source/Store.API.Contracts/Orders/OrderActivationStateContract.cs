namespace Store.API.Contracts.Orders
{
    public class OrderActivationStateContract : OrderSubStateContract<OrderActivationStatusContract>
    {
        public string ActivationReason { get; set; }
        public string ActivationBy { get; set; }
        public ActivationMethodContract? ActivationMethod { get; set; }
    }
}