﻿using System;

namespace Store.API.Contracts.Orders
{
    public class OrderVerificationContract
    {
        public DateTime OccuredOn { get; set; }
        public string Message { get; set; }
    }
}