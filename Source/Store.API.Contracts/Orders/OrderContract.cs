﻿using System;
using System.Collections.Generic;

namespace Store.API.Contracts.Orders
{
    public class OrderContract
    {
        public string OrderCode { get; set; }
        public int CustomerNumber { get; set; }
        public IEnumerable<OrderItemContract> Items { get; set; }
        public PriceContract TotalPrice { get; set; }
        public int VatRatePercentage { get; set; }
        public decimal VatAmount { get; set; }
        public CheckoutTypeContract CheckoutType { get; set; }
        public CheckoutDataContract CheckoutData { get; set; }
        public string CustomerReference { get; set; }
        public string Source { get; set; }
        public OrderStatus Status { get; set; }
        public int ProviderId { get; private set; }
        public int TicketId { get; set; }
        public string TicketNumber { get; set; }
        public DateTime CreationDate { get; set; }
        public string EpayCode { get; set; }
        public bool NeedsPayment { get; set; }
        public string Language { get; set; }
    }
}