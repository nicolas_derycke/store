﻿namespace Store.API.Contracts.Orders
{
    public class OrderStateContract
    {
        public string OrderCode { get; set; }
        public string OrderStatus { get; set; }
        public string InitiatedBy { get; set; }
        public bool IsInitiatedByEmployee { get; set; }
        public OrderActivationStateContract ActivationState { get; set; }
        public OrderInvoiceStateContract InvoiceState { get; set; }
        public OrderSubStateContract<OrderProvisioningStatusContract> ProvisioningState { get; set; }
        public OrderSubStateContract<OrderPaymentStatusContract> PaymentState { get; set; }
    }
}