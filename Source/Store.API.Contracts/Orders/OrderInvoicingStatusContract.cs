namespace Store.API.Contracts.Orders
{
    public enum OrderInvoicingStatusContract
    {
        NotInvoiced,
        Invoiced
    }
}