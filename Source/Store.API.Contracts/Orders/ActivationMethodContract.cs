namespace Store.API.Contracts.Orders
{
    public enum ActivationMethodContract
    {
        Direct,
        Free,
        Manual,
        Payment,
    }
}