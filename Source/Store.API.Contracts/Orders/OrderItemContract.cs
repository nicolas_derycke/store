﻿
namespace Store.API.Contracts.Orders
{
    public class OrderItemContract
    {
        public int OrderItemId { get; set; }
        public string ProductCode { get; set; }
        public string ProductType { get; set; }
        public string PromoProductCode { get; set; }
        public string ProductName { get; set; }
        public int Period { get; set; }
        public int Quantity { get; set; }
        public PriceContract TotalPrice { get; set; }
        public PriceContract Price { get; set; }
        public PeriodUnitTypeContract PeriodUnitType { get; set; }
        public ItemAttributeDictionary Attributes { get; set; }
        public int PeriodId { get; set; }
        public bool IsRecurring { get; set; }
        public int? ParentId { get; set; }
        public int ProductId { get; set; }
        public string PromoCode { get; set; }
    }
}