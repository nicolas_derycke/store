﻿using System;

namespace Store.API.Contracts.Orders
{
    public class OrderSummaryContract
    {
        public int CustomerNumber { get; private set; }
        public string CompanyName { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public int ProviderId { get; private set; }
        public string OrderCode { get; private set; }
        public decimal TotalPriceExclVat { get; private set; }
        public DateTime CreationDate { get; private set; }
        public OrderStatus Status { get; private set; }
        public string EpayCode { get; set; }
        public bool NeedsPayment { get; set; }

        public OrderSummaryContract(int customerNumber, string companyName, string firstName, string lastName, int providerId, string orderCode, decimal totalPriceExclVat, DateTime creationDate, OrderStatus status, string epayCode, bool needsPayment)
        {
            CustomerNumber = customerNumber;
            CompanyName = companyName;
            FirstName = firstName;
            LastName = lastName;
            ProviderId = providerId;
            OrderCode = orderCode;
            TotalPriceExclVat = totalPriceExclVat;
            CreationDate = creationDate;
            Status = status;
            EpayCode = epayCode;
            NeedsPayment = needsPayment;
        }
    }
}