namespace Store.API.Contracts.Orders
{
    public enum OrderProvisioningStatusContract
    {
        NotProvisioned,
        Processing,
        Pending,
        Errors,
        Finished,
        Cancelled,
        FailedValidation
    }
}