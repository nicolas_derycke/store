﻿using System;

namespace Store.API.Contracts.Orders
{
    public class OrderActivityContract
    {
        public DateTime OccuredOn { get; set; }
        public string Message { get; set; }
    }
}