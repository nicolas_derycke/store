﻿namespace Store.API.Contracts
{
    public enum PeriodUnitTypeContract
    {
        OneMonth = 1,
        ThreeMonth = 3,
        SixMonth = 6,
        OneYear = 12,
        TwoYear = 24,
        ThreeYear = 36
    }
}
