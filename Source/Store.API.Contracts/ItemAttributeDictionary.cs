﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Store.API.Contracts.Converters;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.API.Contracts
{
    [JsonConverter(typeof(ItemAttributeDictionaryConverter))]
    public class ItemAttributeDictionary : Dictionary<AttributeKey, IBasketItemAttribute>
    {
        public ItemAttributeDictionary()
        {
        }

        public ItemAttributeDictionary(Dictionary<AttributeKey, IBasketItemAttribute> itemAttributes)
            : base(itemAttributes)
        {
        }

        public void Add(ItemAttributeKeyValuePair keyValuePair)
        {
            this.Add(keyValuePair.Key, keyValuePair.Value);
        }

        public Tuple<AttributeKey, IHaveAnIdentifierAttribute> IdentifierItemAttributeKeyValuePair
        {
            get
            {
                if (!this.Any(a => a.Value is IHaveAnIdentifierAttribute))
                    return null;

                var attribute = this.FirstOrDefault(a => a.Value is IHaveAnIdentifierAttribute);

                return new Tuple<AttributeKey, IHaveAnIdentifierAttribute>(attribute.Key, (IHaveAnIdentifierAttribute)attribute.Value);
            }
        }

        public string Identifier
        {
            get
            {
                var itemAttributeKeyValuePair = IdentifierItemAttributeKeyValuePair;

                if (itemAttributeKeyValuePair == null)
                    return null;

                return itemAttributeKeyValuePair.Item2.Identifier;
            }
        }
    }
}