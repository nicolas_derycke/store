﻿namespace Store.API.Contracts
{
    public class CheckoutDataContract
    {
        public decimal PaidAmount { get; private set; }

        public string PaymentProviderReference { get; private set; }
        
        public CheckoutDataContract(decimal paidAmount, string paymentProviderReference)
        {
            PaidAmount = paidAmount;
            PaymentProviderReference = paymentProviderReference;
        }
    }
}