﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class TrademarkAttributes : IBasketItemAttribute
    {
        public bool IsAcceptedByCustomer { get; private set; }
        
        public TrademarkAttributes(bool isAcceptedByCustomer)
        {
            IsAcceptedByCustomer = isAcceptedByCustomer;
        }
    }
}