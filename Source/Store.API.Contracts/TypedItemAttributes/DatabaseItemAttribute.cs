﻿
namespace Store.API.Contracts.TypedItemAttributes
{
    public class DatabaseItemAttribute : IBasketItemAttribute
    {
        public bool IsStandalone { get; private set; }
        public string UpgradeFromDatabaseName { get; private set; }

        public DatabaseItemAttribute(bool isStandalone, string upgradeFromDatabaseName)
        {
            IsStandalone = isStandalone;
            UpgradeFromDatabaseName = upgradeFromDatabaseName;
        }
    }
}
