﻿using System.Collections.Generic;

namespace Store.API.Contracts.TypedItemAttributes
{
    public class ExtraFieldAttributes
    {
        public IEnumerable<string> RequiredFields { get; set; }

        public string CompanyNumber { get; set; }

        public string CodiceFiscal { get; set; }

        public string PassportNumber { get; set; }

        public string TrademarkNumber { get; set; }

        public string TrademarkName { get; set; }

        public string TrademarkCountry { get; set; }

        public ExtraFieldAttributes(IEnumerable<string> requiredFields, string companyNumber, string codiceFiscal, string passportNumber, string trademarkNumber, string trademarkName, string trademarkCountry)
        {
            RequiredFields = requiredFields;
            CompanyNumber = companyNumber;
            CodiceFiscal = codiceFiscal;
            PassportNumber = passportNumber;
            TrademarkNumber = trademarkNumber;
            TrademarkName = trademarkName;
            TrademarkCountry = trademarkCountry;
        }        

        public static ExtraFieldAttributes Create(IEnumerable<string> requiredFields, RegistrantAttributes registrant)
        {
            if (registrant.ExtraFields == null)
                return new ExtraFieldAttributes(requiredFields, null, null, null, null, null, null);
            else
                return new ExtraFieldAttributes(
                    requiredFields,
                    registrant.ExtraFields.CompanyNumber,
                    registrant.ExtraFields.CodiceFiscal,
                    registrant.ExtraFields.PassportNumber,
                    registrant.ExtraFields.TrademarkNumber,
                    registrant.ExtraFields.TrademarkName,
                    registrant.ExtraFields.TrademarkCountry);
        }

        public bool ContainsExtraField()
        {
            return
                !string.IsNullOrWhiteSpace(CompanyNumber) &&
                !string.IsNullOrWhiteSpace(CodiceFiscal) &&
                !string.IsNullOrWhiteSpace(PassportNumber) &&
                !string.IsNullOrWhiteSpace(TrademarkNumber) &&
                !string.IsNullOrWhiteSpace(TrademarkName) &&
                !string.IsNullOrWhiteSpace(TrademarkCountry);
        }
    }
}