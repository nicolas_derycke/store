namespace Store.API.Contracts.TypedItemAttributes
{
    public interface ICanChangeIdentifier : IHaveAnIdentifierAttribute
    {
        void ChangeIdentifier(string newIdentifier);
    }
}