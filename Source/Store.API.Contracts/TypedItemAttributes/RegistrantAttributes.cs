﻿using System;

namespace Store.API.Contracts.TypedItemAttributes
{
    public class RegistrantAttributes : IBasketItemAttribute, IEquatable<RegistrantAttributes>
    {
        public string City { get; private set; }

        public string Company { get; private set; }

        public string Country { get; private set; }

        public string Email { get; private set; }

        public string Fax { get; private set; }

        public string FirstName { get; private set; }

        public string Mobile { get; private set; }

        public string Language { get; private set; }

        public string LastName { get; private set; }

        public string PostalCode { get; private set; }

        public string Street { get; private set; }

        public string Telephone { get; private set; }

        public string HouseNumber { get; private set; }

        public string VatNumber { get; private set; }

        public ExtraFieldAttributes ExtraFields { get; set; }

        public RegistrantAttributes(string city, string company, string country, string email, string fax, string firstName, string mobile, string language, string lastName, string postalCode, string street, string telephone, string houseNumber, string vatNumber)
        {
            City = city;
            Company = company;
            Country = country;
            Email = email;
            Fax = fax;
            FirstName = firstName;
            Mobile = mobile;
            Language = language;
            LastName = lastName;
            PostalCode = postalCode;
            Street = street;
            Telephone = telephone;
            HouseNumber = houseNumber;
            VatNumber = vatNumber;
        }

        public static RegistrantAttributes Empty
        {
            get
            {
                return new RegistrantAttributes(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
            }
        }

        public bool Equals(RegistrantAttributes other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(City, other.City)
                   && string.Equals(Company, other.Company)
                   && string.Equals(Country, other.Country)
                   && string.Equals(Email, other.Email)
                   && string.Equals(Fax, other.Fax)
                   && string.Equals(FirstName, other.FirstName)
                   && string.Equals(Mobile, other.Mobile)
                   && string.Equals(Language, other.Language)
                   && string.Equals(LastName, other.LastName)
                   && string.Equals(PostalCode, other.PostalCode)
                   && string.Equals(Street, other.Street)
                   && string.Equals(Telephone, other.Telephone)
                   && string.Equals(HouseNumber, other.HouseNumber)
                   && string.Equals(VatNumber, other.VatNumber)
                   && Equals(ExtraFields, other.ExtraFields);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RegistrantAttributes)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (City != null ? City.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Company != null ? Company.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Country != null ? Country.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Email != null ? Email.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Fax != null ? Fax.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Mobile != null ? Mobile.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Language != null ? Language.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PostalCode != null ? PostalCode.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Street != null ? Street.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Telephone != null ? Telephone.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (HouseNumber != null ? HouseNumber.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (VatNumber != null ? VatNumber.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ExtraFields != null ? ExtraFields.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(RegistrantAttributes left, RegistrantAttributes right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(RegistrantAttributes left, RegistrantAttributes right)
        {
            return !Equals(left, right);
        }
    }
}