﻿using System.Collections;
using System.Collections.Generic;

namespace Store.API.Contracts.TypedItemAttributes
{
    public class ManualActivationAttributes : IBasketItemAttribute, IEnumerable<ManualActivationAttribute>
    {
        private readonly IEnumerable<ManualActivationAttribute> _manualActivationAttributes;

        public ManualActivationAttributes(IEnumerable<ManualActivationAttribute> manualActivationAttributes)
        {
            _manualActivationAttributes = manualActivationAttributes;
        }

        public IEnumerator<ManualActivationAttribute> GetEnumerator()
        {
            return _manualActivationAttributes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}