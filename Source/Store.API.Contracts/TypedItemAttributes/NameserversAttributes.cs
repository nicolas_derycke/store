﻿using System.Collections.Generic;

namespace Store.API.Contracts.TypedItemAttributes
{
    public class NameserversAttributes : List<NameserverAttributes>, IBasketItemAttribute
    {
    }
}
