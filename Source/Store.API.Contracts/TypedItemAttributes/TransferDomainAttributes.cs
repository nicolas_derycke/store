﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class TransferDomainAttributes : IBasketItemAttribute
    {
        public string AuthorizationCode { get; private set; }

        public bool UseExistingNameservers { get; private set; }

        public bool UseExistingRegistrant { get; private set; }

        public TransferDomainAttributes(string authorizationCode, bool useExistingNameservers, bool useExistingRegistrant)
        {
            AuthorizationCode = authorizationCode;
            UseExistingNameservers = useExistingNameservers;
            UseExistingRegistrant = useExistingRegistrant;
        }
    }
}