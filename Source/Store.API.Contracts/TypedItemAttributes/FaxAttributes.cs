﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class FaxAttributes : ICanChangeIdentifier
    {
        public string Zone { get; private set; }
        public string Number { get; private set; }
        public bool IsNewFaxNumber { get; private set; }
        public string Email { get; private set; }
        public string Identifier { get; private set; }

        public FaxAttributes(string zone, string number, bool isNewFaxNumber, string email, string identifier)
        {
            Zone = zone;
            Number = number;
            IsNewFaxNumber = isNewFaxNumber;
            Email = email;
            Identifier = string.IsNullOrWhiteSpace(identifier) ? Zone + (IsNewFaxNumber ? "xxxxxxxx" : Number) : identifier;
        }

        public void ChangeIdentifier(string newIdentifier)
        {
            if (IsNewFaxNumber)
                Identifier = newIdentifier;
        }
    }
}