﻿using System.Collections.Generic;

namespace Store.API.Contracts.TypedItemAttributes
{
    public interface IHaveMultipleIdentifiersAttribute : IBasketItemAttribute
    {
        IEnumerable<string> Identifiers { get; }
    }
}
