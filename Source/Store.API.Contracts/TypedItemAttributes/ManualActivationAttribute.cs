﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class ManualActivationAttribute
    {
        public ManualActivationAttribute(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; private set; }
        public string Value { get; private set; }
    }
}