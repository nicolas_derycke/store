﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.API.Contracts.TypedItemAttributes
{
    public class EmailAttributes : IBasketItemAttribute
    {
        public int? UpgradeFromMailboxId { get; private set; }
        public EmailAttributes(int? upgradeFromMailboxId)
        {
            UpgradeFromMailboxId = upgradeFromMailboxId;
        }
    }
}
