﻿using System;

namespace Store.API.Contracts.TypedItemAttributes
{
    public class PreregistrationAttributes : IBasketItemAttribute
    {
        public bool IsLandRush { get; private set; }
        public bool IsGoLive { get; private set; }
        public DateTime StageDate { get; private set; }

        public PreregistrationAttributes(bool isLandRush, bool isGoLive, DateTime stageDate)
        {
            StageDate = stageDate;
            IsLandRush = isLandRush;
            IsGoLive = isGoLive;
        }
        
    }
}