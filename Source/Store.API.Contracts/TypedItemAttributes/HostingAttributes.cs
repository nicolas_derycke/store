﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class HostingAttributes : IBasketItemAttribute
    {
        public int? UpgradeFromAccountId { get; private set; }
        
        public HostingAttributes(int? upgradeFromAccountId)
        {
            UpgradeFromAccountId = upgradeFromAccountId;
        }
    }
}