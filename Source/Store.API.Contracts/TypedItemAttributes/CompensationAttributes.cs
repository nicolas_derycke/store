﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class CompensationAttributes : IBasketItemAttribute
    {
        public int InvoiceLineId { get; private set; }
        
        public CompensationAttributes(int invoiceLineId)
        {
            InvoiceLineId = invoiceLineId;
        }
    }
}