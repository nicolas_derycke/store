﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Store.API.Contracts.TypedItemAttributes
{
    public class CertificateAttributes : IHaveMultipleIdentifiersAttribute, IHaveAnIdentifierAttribute
    {
        public CertificateAttributes(IEnumerable<string> hostnames)
        {
            this.Hostnames = hostnames;
        }

        public string Identifier
        {
            get
            {
                if (Hostnames == null)
                    return null;

                return Hostnames.FirstOrDefault();
            }
        }

        public IEnumerable<string> Hostnames { get; private set; }

        [JsonIgnore]
        public IEnumerable<string> Identifiers
        {
            get { return Hostnames; }
        }
    }
}