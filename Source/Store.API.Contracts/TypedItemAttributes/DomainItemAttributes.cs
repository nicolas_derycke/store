﻿
namespace Store.API.Contracts.TypedItemAttributes
{
    public class DomainItemAttributes : IBasketItemAttribute, IHaveAnIdentifierAttribute
    {
        public string DomainName { get; private set; }

        public DomainItemAttributes(string domainName)
        {
            DomainName = domainName;
        }

        public string Identifier { get { return DomainName; } }
    }
}