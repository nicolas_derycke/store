﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public interface IHaveAnIdentifierAttribute : IBasketItemAttribute
    {
        string Identifier { get; }
    }
}