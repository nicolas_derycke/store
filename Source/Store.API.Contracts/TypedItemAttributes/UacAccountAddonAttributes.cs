﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class UacAccountAddonAttributes : IBasketItemAttribute
    {
        public int AccountId { get; private set; }
        public int? UpgradeFromAddonId { get; private set; }
        
        public UacAccountAddonAttributes(int accountId, int? upgradeFromAddonId)
        {
            AccountId = accountId;
            UpgradeFromAddonId = upgradeFromAddonId;
        }
    }
}