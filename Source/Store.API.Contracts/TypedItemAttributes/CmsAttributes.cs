﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class CmsAttributes : IBasketItemAttribute
    {
        public string Version { get; private set; }
        public string Language { get; private set; }
        
        public CmsAttributes(string version, string language)
        {
            Version = version;
            Language = language;
        }
    }
}