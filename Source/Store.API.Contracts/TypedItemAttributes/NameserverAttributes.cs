﻿namespace Store.API.Contracts.TypedItemAttributes
{
    public class NameserverAttributes : IBasketItemAttribute
    {
        public string Name { get; private set; }

        public string Ip { get; private set; }
        
        public NameserverAttributes(string name, string ip)
        {
            Name = name;
            Ip = ip;
        }
    }
}