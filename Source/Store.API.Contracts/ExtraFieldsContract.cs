﻿using System.Collections.Generic;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.API.Contracts
{
    public class ExtraFieldsContract
    {
        public IEnumerable<int> BasketItemIds { get; private set; }

        public IEnumerable<string> DomainNames { get; private set; }

        public ExtraFieldAttributes ExtraFields { get; private set; }

        public ExtraFieldsRegistrantContract Registrant { get; private set; }

        public ExtraFieldsContract(IEnumerable<int> basketItemIds, IEnumerable<string> domainNames, ExtraFieldAttributes extraFields, ExtraFieldsRegistrantContract registrant)
        {
            BasketItemIds = basketItemIds;
            DomainNames = domainNames;
            ExtraFields = extraFields;
            Registrant = registrant;
        }
    }
}