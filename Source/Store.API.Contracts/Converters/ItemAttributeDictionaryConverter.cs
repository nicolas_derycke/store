﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Store.API.Contracts.Converters
{
    public class ItemAttributeDictionaryConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(ItemAttributeDictionary).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var attributes = serializer.Deserialize<Dictionary<AttributeKey, dynamic>>(reader);
            var itemAttributeDictionary = new ItemAttributeDictionary();

            if (attributes != null)
                foreach (var attribute in attributes)
                    itemAttributeDictionary.Add(attribute.Key, AttributeValueFactory.Create(attribute.Key, attribute.Value.ToString()));

            return itemAttributeDictionary;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var itemAttributeDictionary = value as ItemAttributeDictionary;
            var json = new JObject();

            foreach (var itemAttribute in itemAttributeDictionary)
                json.Add(JToken.FromObject(itemAttribute.Key, serializer).Value<string>(), JToken.FromObject(itemAttribute.Value, serializer));

            serializer.Serialize(writer, json);
        }
    }
}