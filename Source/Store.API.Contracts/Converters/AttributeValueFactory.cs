﻿using System;
using Newtonsoft.Json;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.API.Contracts.Converters
{
    public static class AttributeValueFactory
    {
        public static IBasketItemAttribute Create(AttributeKey attributeKey, string attributeValueJson)
        {
            switch (attributeKey)
            {
                case AttributeKey.Registrant:
                    return JsonConvert.DeserializeObject<RegistrantAttributes>(attributeValueJson);
                case AttributeKey.Domain:
                    return JsonConvert.DeserializeObject<DomainItemAttributes>(attributeValueJson);
                case AttributeKey.Nameservers:
                    return JsonConvert.DeserializeObject<NameserversAttributes>(attributeValueJson);
                case AttributeKey.TransferDomain:
                    return JsonConvert.DeserializeObject<TransferDomainAttributes>(attributeValueJson);
                case AttributeKey.Trademark:
                    return JsonConvert.DeserializeObject<TrademarkAttributes>(attributeValueJson);
                case AttributeKey.Hosting:
                    return JsonConvert.DeserializeObject<HostingAttributes>(attributeValueJson);
                case AttributeKey.Compensation:
                    return JsonConvert.DeserializeObject<CompensationAttributes>(attributeValueJson);
                case AttributeKey.UacAccountAddon:
                    return JsonConvert.DeserializeObject<UacAccountAddonAttributes>(attributeValueJson);
                case AttributeKey.Cms:
                    return JsonConvert.DeserializeObject<CmsAttributes>(attributeValueJson);
                case AttributeKey.Certificate:
                    return JsonConvert.DeserializeObject<CertificateAttributes>(attributeValueJson);
                case AttributeKey.PremiumDomain:
                    return JsonConvert.DeserializeObject<PremiumDomainAttribute>(attributeValueJson);
                case AttributeKey.Database:
                    return JsonConvert.DeserializeObject<DatabaseItemAttribute>(attributeValueJson);
                case AttributeKey.ManualActivation:
                    return JsonConvert.DeserializeObject<ManualActivationAttributes>(attributeValueJson);
                case AttributeKey.Fax:
                    return JsonConvert.DeserializeObject<FaxAttributes>(attributeValueJson);
                case AttributeKey.Email:
                    return JsonConvert.DeserializeObject<EmailAttributes>(attributeValueJson);
                case AttributeKey.Preregistration:
                    return JsonConvert.DeserializeObject<PreregistrationAttributes>(attributeValueJson);
                default:
                    throw new Exception(string.Format("No value can be provided for key '{0}'", attributeKey));
            }
        }
    }
}