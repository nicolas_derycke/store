﻿namespace Store.API.Contracts
{
    public enum OrderStatus
    {
        Created,
        CheckOutCompleted,
        Processing,
        Failed,
        Finished,
        Cancelled
    }
}