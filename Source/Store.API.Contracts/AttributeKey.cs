﻿namespace Store.API.Contracts
{
    public enum AttributeKey
    {
        Domain,
        Registrant,
        Nameservers,
        TransferDomain,
        Trademark,
        Hosting,
        Compensation,
        UacAccountAddon,
        Cms,
        Certificate,
        PremiumDomain,
        Database,
        ManualActivation,
        Fax,
        Email,
        Preregistration,
    }
}