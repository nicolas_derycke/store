﻿namespace Store.API.Contracts.Baskets
{
    public enum BasketType
    {
        Regular,
        Standalone
    }
}