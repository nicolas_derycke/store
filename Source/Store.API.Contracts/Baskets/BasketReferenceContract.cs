﻿using System;

namespace Store.API.Contracts.Baskets
{
    public class BasketReferenceContract
    {
        public string OrderCode { get; set; }
        public int CustomerNumber { get; set; }
        public int ItemCount { get; set; }
        public DateTime CreationDate { get; set; }
        public BasketType BasketType { get; set; }
        
        public BasketReferenceContract(string orderCode, int customerNumber, int itemCount, DateTime creationDate, BasketType basketType)
        {
            this.OrderCode = orderCode;
            this.CustomerNumber = customerNumber;
            this.ItemCount = itemCount;
            this.CreationDate = creationDate;
            this.BasketType = basketType;
        }
    }
}