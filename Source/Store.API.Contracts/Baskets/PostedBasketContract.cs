﻿using System.Collections.Generic;

namespace Store.API.Contracts.Baskets
{
    public class PostedBasketContract
    {
        public BasketType? BastketType { get; set; }
        public IEnumerable<BasketItemContract> Items { get; set; }
    }
}
