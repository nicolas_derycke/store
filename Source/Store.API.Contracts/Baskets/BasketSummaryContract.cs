﻿using System;

namespace Store.API.Contracts.Baskets
{
    public class BasketSummaryContract
    {
        public string OrderCode { get; set; }
        public int CustomerNumber { get; set; }
        public PriceContract TotalPrice { get; set; }
        public decimal VatRatePercentage { get; set; }
        public decimal VatAmount { get; set; }
        public int ItemCount { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastValidationDate { get; set; }
        public bool IsRecalculating { get; set; }
        public bool HasError { get; set; }
        public BasketType BasketType { get; set; }
        public string CustomerReference { get; set; }
    }
}
