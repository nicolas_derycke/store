﻿
using System;

namespace Store.API.Contracts.Baskets
{
    public class BasketItemContract
    {
        public BasketItemContract(
            int basketItemId,
            string productCode,
            string productType,
            string promoProductCode,
            string productName,
            int period,
            int quantity,
            ItemAttributeDictionary attributes,
            bool isRecurring,
            int? parentId,
            string promoCode)
        {
            BasketItemId = basketItemId;
            ProductCode = productCode;
            ProductType = productType;
            PromoProductCode = promoProductCode;
            ProductName = productName;
            Period = period;
            Quantity = quantity;
            Attributes = attributes;
            IsRecurring = isRecurring;
            ParentId = parentId;
            PromoCode = promoCode;
        }

        public int BasketItemId { get; set; }
        public string ProductCode { get; set; }
        public string ProductType { get; set; }
        public string PromoProductCode { get; set; }
        public string ProductName { get; set; }
        public int Period { get; set; }
        public int Quantity { get; set; }
        public PriceContract TotalPrice { get; set; }
        public PriceContract Price { get; set; }
        public PriceContract PeriodUnitPrice
        {
            get
            {
                if (Price == null)
                    return null;

                var safePeriod = Period == 0 ? 1 : Period;
                return new PriceContract()
                {
                    ExclVat = Price.ExclVat != 0 ? ((Price.ExclVat / safePeriod) * (int)PeriodUnitType) : 0,
                    InclVat = Price.InclVat != 0 ? ((Price.InclVat / safePeriod) * (int)PeriodUnitType) : 0,
                    ReductionExclVat = Price.ReductionExclVat != 0 ? ((Price.ReductionExclVat / safePeriod) * (int)PeriodUnitType) : 0,
                    ReductionInclVat = Price.ReductionInclVat != 0 ? ((Price.ReductionInclVat / safePeriod) * (int)PeriodUnitType) : 0
                };
            }
        }
        public PeriodUnitTypeContract PeriodUnitType { get; set; }
        public ItemAttributeDictionary Attributes { get; set; }
        public BasketItemValidationError ValidationError { get; private set; }
        public BasketItemWarning Warning { get; private set; }
        public bool IsRecurring { get; private set; }
        public int? ParentId { get; private set; }
        public bool IsPromo
        {
            get { return !String.IsNullOrEmpty(PromoCode); }
        }
        public string PromoCode { get; private set; }
    }
}
