﻿namespace Store.API.Contracts.Baskets
{
    public enum BasketItemWarning
    {
        None = 0,
        TransferTradeNotSupported,
        BasicEmailAccountExists,
        BusinessEmailAccountExists,
        LinuxAccountExists,
        WindowsAccountExists,
        ExtraFieldsRequired
    }
}