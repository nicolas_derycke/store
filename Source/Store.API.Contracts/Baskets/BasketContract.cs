﻿using System.Collections.Generic;

namespace Store.API.Contracts.Baskets
{
    public class BasketContract : BasketSummaryContract
    {
        public IEnumerable<BasketItemContract> Items { get; set; }
    }
}
