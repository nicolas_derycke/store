﻿namespace Store.API.Contracts.Baskets
{
    public enum BasketItemValidationError
    {
        None = 0,
        InactiveProduct,
        InactiveProductPeriod,
        DomainAvailabilityChanged,
        TrademarkChanged,
        NegativeBasketItemPrice,
        UacAccountAlreadyExists,
        ResellerProfileAlreadyExists,
        AccountBelongsToOtherCustomer,
        DuplicateProductCombination,
        UnacceptedTrademark,
        DuplicatePreregistration
    }
}