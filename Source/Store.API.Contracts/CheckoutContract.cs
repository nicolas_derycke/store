﻿namespace Store.API.Contracts
{
    public class CheckoutContract
    {
        public string OrderCode { get; private set; }

        public CheckoutTypeContract CheckoutType { get; private set; }

        public CheckoutDataContract PaymentData { get; private set; }

        public string CustomerReference { get; private set; }

        public CheckoutContract(string orderCode, CheckoutTypeContract checkoutType, CheckoutDataContract paymentData, string customerReference)
        {
            OrderCode = orderCode;
            CheckoutType = checkoutType;
            PaymentData = paymentData;
            CustomerReference = customerReference;
        }
    }
}