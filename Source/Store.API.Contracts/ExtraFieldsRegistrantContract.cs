﻿namespace Store.API.Contracts
{
    public class ExtraFieldsRegistrantContract
    {
        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Company { get; private set; }
        
        public ExtraFieldsRegistrantContract(string firstName, string lastName, string company)
        {
            FirstName = firstName;
            LastName = lastName;
            Company = company;
        }
    }
}