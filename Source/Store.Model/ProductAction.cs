﻿namespace Store.Model
{
    public enum ProductAction
    {
        Renewal = 1,
        Register = 2,
        Transfer = 3,
        Trade = 4
    }
}