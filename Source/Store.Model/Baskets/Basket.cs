﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Core.Models;
using Intelligent.Shared.Reflection;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model.Exceptions;

namespace Store.Model.Baskets
{
    public class Basket
    {
        public int BasketId { get; set; }
        public string OrderCode { get; private set; }
        public CustomerIdentifier CustomerIdentifier { get; private set; }
        public List<BasketItem> Items { get; private set; }
        public bool CheckedOut { get; set; }
        public DateTime CreationDate { get; private set; }
        public DateTime LastValidationDate { get; set; }
        public Provider Provider { get; set; }
        public BasketType BasketType { get; private set; }
        public bool IsRecalculating { get; private set; }
        public string Source { get; private set; }
        public Language Language { get; set; }
        public bool Deleted { get; set; }
        public string CustomerReference { get; set; }

        public Price TotalPrice
        {
            get
            {
                return new Price(
                    Items.Sum(i => i.TotalPrice.ExclVat),
                    Items.Sum(i => i.TotalPrice.ReductionExclVat),
                    VatRatePercentage);
            }
        }

        public decimal VatRate
        {
            get { return VatRatePercentage/100; }
        }

        public decimal VatRatePercentage { get; private set; }

        public decimal VatAmount
        {
            get { return TotalPrice.InclVat - TotalPrice.ExclVat; }
        }

        public bool HasItems
        {
            get { return Items.Any(); }
        }

        public int ItemCount
        {
            get { return Items.Count(); }
        }

        public bool IsAnonymousBasket
        {
            get { return CustomerIdentifier == null; }
        }

        public Basket(int basketId,
            string orderCode,
            CustomerIdentifier customerIdentifier,
            IEnumerable<BasketItem> items,
            decimal vatRatePercentage,
            DateTime creationDate,
            bool checkedOut,
            DateTime lastValidationDate,
            Provider provider,
            BasketType basketType,
            bool isRecalculating,
            string source,
            Language language,
            bool deleted,
            string customerReference)
        {
            BasketId = basketId;
            OrderCode = orderCode;
            CustomerIdentifier = customerIdentifier;
            Items = items.ToList();
            VatRatePercentage = vatRatePercentage;
            CheckedOut = checkedOut;
            CreationDate = creationDate;
            LastValidationDate = lastValidationDate;
            Provider = provider;
            BasketType = basketType;
            IsRecalculating = isRecalculating;
            Source = source;
            Language = language;
            Deleted = deleted;
            CustomerReference = customerReference;
        }
         
        public bool IsRevalidationRequired(int revalidationOffsetInDays)
        {
            return LastValidationDate < DateTime.Now.AddDays(-revalidationOffsetInDays);
        }

        public BasketItem GetBasketItem(int basketItemId)
        {
            var basketItem = Items.FirstOrDefault(i => i.BasketItemId == basketItemId);

            if (basketItem == null)
                throw new BasketItemNotFoundException(basketItemId, OrderCode);

            return basketItem;
        }

        public IEnumerable<BasketItem> GetChildBasketItems(int parentId)
        {
            return Items.Where(i => i.ParentId == parentId);
        }

        public bool ContainsDuplicateBasketItem(BasketItem basketItem)
        {
            var identifier = basketItem.Attributes.Identifier;

            // basket item without an identifier can't be a duplicate of another item
            if (identifier == null)
                return false;

            return Items.Any(i =>
                i != basketItem &&
                i.ProductCode == basketItem.ProductCode &&
                i.Attributes.Identifier == identifier);
        }

        public void ChangeCustomer(CustomerIdentifier customerIdentifier)
        {
            CustomerIdentifier = customerIdentifier;
        }

        public void SetPeriodUnitTypeForBasketItems(PeriodUnitType periodUnitType)
        {
            foreach (var basketItem in Items)
            {
                basketItem.PeriodUnitType = periodUnitType;
            }
        }

        public void RemoveVat()
        {
            UpdateVatRatePercentage(0);
        }

        public void UpdateVatRatePercentage(decimal vatRatePercentage)
        {
            foreach (var basketItem in Items)
            {
                basketItem.UpdateVatRatePercentage(vatRatePercentage);
            }

            VatRatePercentage = vatRatePercentage;
        }

        public void MakeIdentifierUnique(BasketItem basketItem)
        {
            var identifierChangers = basketItem.Attributes
                .Select(x => x.Value)
                .Where(x => x.GetType().ImplementsInterface(typeof(ICanChangeIdentifier))).Cast<ICanChangeIdentifier>();

            foreach (var identifierChanger in identifierChangers)
            {
                var currentIdentifier = identifierChanger.Identifier;
                var isIdentifierUnique = false;
                var identifierCounter = 0;
                while (!isIdentifierUnique)
                {
                    isIdentifierUnique = !Items.Any(x => x != basketItem && x.ProductCode == basketItem.ProductCode && x.Attributes.Identifier == currentIdentifier);
                    if (isIdentifierUnique) continue;
                    currentIdentifier = identifierChanger.Identifier + ++identifierCounter;
                }

                if (currentIdentifier != identifierChanger.Identifier)
                    identifierChanger.ChangeIdentifier(currentIdentifier);
            }
        }
    }
}
