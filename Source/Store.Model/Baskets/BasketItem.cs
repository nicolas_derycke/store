﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using Intelligent.Shared.Core;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.Model.Baskets
{
    public class BasketItem
    {
        private static readonly List<BasketItemValidationError> BasketItemValidationErrorPriorities = new List<BasketItemValidationError>()
            {
                BasketItemValidationError.InactiveProduct,
                BasketItemValidationError.InactiveProductPeriod,
                BasketItemValidationError.DomainAvailabilityChanged,
                BasketItemValidationError.TrademarkChanged
            };

        private static readonly List<BasketItemWarning> BasketItemWarningPriorities = new List<BasketItemWarning>()
            {
                BasketItemWarning.TransferTradeNotSupported,
                BasketItemWarning.LinuxAccountExists,
                BasketItemWarning.WindowsAccountExists,
                BasketItemWarning.BasicEmailAccountExists,
                BasketItemWarning.BusinessEmailAccountExists
            };

        public int BasketItemId { get; set; }
        public int ProductId { get; private set; }
        public string ProductCode { get; private set; }
        public string ProductType { get; private set; }
        public string PromoProductCode { get; private set; }
        public string ProductName { get; private set; }
        public int Period { get; set; }
        public int PeriodId { get; private set; }
        public int Quantity { get; private set; }
        public PeriodUnitType PeriodUnitType { get; set; }
        public Price TotalPrice
        {
            get
            {
                return new Price(Price.ExclVat * Quantity, Price.ReductionExclVat * Quantity, _vatRatePercentage);
            }
        }

        public string ActualProductCode
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(PromoProductCode))
                    return PromoProductCode;

                return ProductCode;
            }
        }

        public bool IsFixedPrice { get; private set; }

        public bool IsPromo
        {
            get { return !String.IsNullOrEmpty(PromoCode); }
        }

        public Price Price { get; private set; }
        public ItemAttributeDictionary Attributes { get; private set; }
        public BasketItemValidationError ValidationError { get; set; }
        public BasketItemWarning Warning { get; private set; }
        private readonly decimal _vatRatePercentage;
        public bool IsRecurring { get; private set; }
        public int? ParentId { get; set; }
        public string PromoCode { get; private set; }

        public bool IsUpgrade
        {
            get
            {
                var hostingAttributes = Attributes.GetOptionalBasketItemAttribute<HostingAttributes>(AttributeKey.Hosting);
                var uacAccountAddonAttributes = Attributes.GetOptionalBasketItemAttribute<UacAccountAddonAttributes>(AttributeKey.UacAccountAddon);
                return
                    (hostingAttributes.HasValue && hostingAttributes.Value.UpgradeFromAccountId.HasValue) ||
                    (uacAccountAddonAttributes.HasValue && uacAccountAddonAttributes.Value.UpgradeFromAddonId.HasValue);
            }
        }

        public bool IsCompensation
        {
            get
            {
                return !IsRecurring && Quantity < 0;
            }
        }

        public BasketItem(
            string productCode,
            string promoProductCode,
            int period,
            int quantity,
            ItemAttributeDictionary attributes,
            decimal vatRatePercentage,
            bool isRecurring,
            int? parentId = null)
        {
            ProductCode = productCode;
            PromoProductCode = promoProductCode;
            Period = period;
            Quantity = quantity;
            Attributes = attributes;
            if (Attributes == null)
                Attributes = new ItemAttributeDictionary();
            _vatRatePercentage = vatRatePercentage;
            ValidationError = BasketItemValidationError.None;
            Warning = BasketItemWarning.None;
            PeriodUnitType = PeriodUnitType.OneYear;
            IsRecurring = isRecurring;
            ParentId = parentId;
            Price = new Price();
            ProductName = productCode.Replace('-', ' ').ToUpper();
            ProductType = string.Empty;
        }

        public BasketItem(
            int basketItemId,
            int productId,
            string productCode,
            string productType,
            string promoProductCode,
            string productName,
            int period,
            int periodId,
            int quantity,
            Price price,
            ItemAttributeDictionary attributes,
            decimal vatRatePercentage,
            BasketItemValidationError validationError,
            BasketItemWarning warning,
            bool isRecurring,
            bool isFixedPrice,
            int? parentId = null,
            string promoCode = null)
        {
            BasketItemId = basketItemId;
            ProductId = productId;
            ProductCode = productCode;
            ProductType = productType;
            PromoProductCode = promoProductCode;
            ProductName = productName;
            Period = period;
            PeriodId = periodId;
            Quantity = quantity;
            Price = price;
            Attributes = attributes;
            if (Attributes == null)
                Attributes = new ItemAttributeDictionary();
            _vatRatePercentage = vatRatePercentage;
            ValidationError = validationError;
            Warning = warning;
            PeriodUnitType = PeriodUnitType.OneYear;
            IsRecurring = isRecurring;
            IsFixedPrice = isFixedPrice;
            ParentId = parentId;
            PromoCode = promoCode;
        }

        public void SetFixedPrice(decimal exclVat)
        {
            IsFixedPrice = true;
            Price = new Price(exclVat, 0, _vatRatePercentage);
        }

        public void SetFreeBasketItem()
        {
            Price = new Price(0, Price.ExclVat + Price.ReductionExclVat, _vatRatePercentage);
        }

        public void ChangeQuantity(int quantity)
        {
            Quantity = quantity;
        }

        public void ChangePeriod(int period)
        {
            Period = period;
        }

        public void SetProductInfoAndPrice(ProductInfo productInfo, Price price)
        {
            ProductId = productInfo.ProductId;
            ProductName = productInfo.ProductName;
            ProductType = productInfo.ProductType;

            PeriodId = productInfo.PeriodId;

            Price = price;

            // if the basket item was created as non-recurring we don't want to override this based on product mgmt
            if (IsRecurring)
                IsRecurring = productInfo.IsRecurring;
            // there are too many customerexceptions, hardcoded for now in the productinfoprovider
            //if ((Price.ExclVat == 0 && Price.ReductionExclVat == 0))
            //    IsRecurring = false;

            // reset inactive product error as we just set a new one.
            if (ValidationError == BasketItemValidationError.InactiveProduct || ValidationError == BasketItemValidationError.InactiveProductPeriod)
                ValidationError = BasketItemValidationError.None;

            // reset promocode
            PromoCode = null;
        }

        public void SetPromoPrice(decimal promoPriceExclVat, string promoCode)
        {
            if (promoPriceExclVat > Price.ExclVat)
                return;

            var extraReduction = Price.ExclVat - promoPriceExclVat;

            Price = new Price(promoPriceExclVat, Price.ReductionExclVat + extraReduction, _vatRatePercentage);
            PromoCode = promoCode;
        }

        public void SetBasketItemValidationErrors(BasketItemValidationError validationError)
        {
            SetBasketItemValidationErrors(new List<BasketItemValidationError>() { validationError });
        }

        public void UpdateBasketItemValidationErrors(IEnumerable<BasketItemValidationError> validationErrors)
        {
            SetBasketItemValidationErrors(validationErrors.Concat(new[] { ValidationError }));
        }

        public void SetBasketItemValidationErrors(IEnumerable<BasketItemValidationError> validationErrors)
        {
            GetFirstPriority(validationErrors, BasketItemValidationErrorPriorities).
                WhenHasValue(v => this.ValidationError = v).
                WhenEmpty(() => this.ValidationError = BasketItemValidationError.None);
        }

        public void SetBasketItemWarnings(IEnumerable<BasketItemWarning> warnings)
        {
            GetFirstPriority(warnings, BasketItemWarningPriorities).
                WhenHasValue(w => this.Warning = w).
                WhenEmpty(() => this.Warning = BasketItemWarning.None);
        }

        private static Optional<T> GetFirstPriority<T>(IEnumerable<T> unprioritizedItems, IEnumerable<T> priorities)
        {
            if (!unprioritizedItems.Any())
                return Optional<T>.Empty;

            if (unprioritizedItems.Any(w => priorities.Contains(w)))
                return unprioritizedItems.First(w => priorities.Contains(w));
            else
                return unprioritizedItems.FirstOrDefault();
        }

        public void UpdateVatRatePercentage(decimal vatRatePercentage)
        {
            Price = Price.WithVatRatePercentage(vatRatePercentage);
        }
    }
}