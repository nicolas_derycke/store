using System.Collections.Generic;

namespace Store.Model
{
    public class PromoCodeInfo
    {
        public string PromoCode { get; private set; }
        public IEnumerable<PromoProductInfo> PromoProducts { get; private set; }

        public PromoCodeInfo(string promoCode, IEnumerable<PromoProductInfo> promoProducts)
        {
            PromoCode = promoCode;
            PromoProducts = promoProducts;
        }
    }
}