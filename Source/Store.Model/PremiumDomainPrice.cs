﻿namespace Store.Model
{
    public class PremiumDomainPrice
    {
        public decimal SetupPrice { get; private set; }
        public decimal PeriodPrice { get; private set; }
        public int Period { get; private set; }

        public PremiumDomainPrice(decimal setupPrice, decimal periodPrice, int period)
        {
            SetupPrice = setupPrice;
            PeriodPrice = periodPrice;
            Period = period;
        }
    }
}