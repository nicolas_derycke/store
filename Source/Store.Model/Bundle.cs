﻿using System.Collections.Generic;
using Store.Model.Baskets;

namespace Store.Model
{
    public class Bundle
    {
        public BasketItem Parent { get; private set; }
        public IEnumerable<BasketItem> Childs { get; private set; }
        public BasketItem FreeChild { get; private set; }
        
        public Bundle(BasketItem parent, IEnumerable<BasketItem> childs, BasketItem freeChild)
        {
            Parent = parent;
            Childs = childs;
            FreeChild = freeChild;
        }
    }
}