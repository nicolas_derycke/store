﻿namespace Store.Model
{
    public static class ProductTypes
    {
        public const string DomainName = "domainname";

        public const string LinuxSharedHosting = "linux-shared-hosting";
        public const string WindowsSharedHosting = "windows-shared-hosting";

        public const string BasicEmail = "basic-e-mail";
        public const string BusinessEmail = "business-e-mail";

        public const string Caching = "caching";

        public const string Drupal = "drupal";
        public const string Joomla = "joomla";
        public const string Magento = "magento";
        public const string Wordpress = "wordpress";

        public const string Sitebuilder = "sitebuilder";

        public const string StandardDomainValidation = "standard-domain-validation";
        public const string MultiDomainDomainValidation = "multi-domain-domain-validation";
        public const string WildcardDomainValidation = "wildcard-domain-validation";
        public const string StandardOrganisation = "standard-organisation";
        public const string MultiDomainOrganisation = "multi-domain-organisation";
        public const string WildcardOrganisation = "wildcard-organisation";
        public const string StandardEv = "standard-ev";
        public const string MultiDomainEv = "multi-domain-ev";

        public const string MsSqlDb = "ms-sql-db";
        public const string MysqlDb = "mysql-db";

        public const string Faxdiensten = "faxdiensten";
        public const string VeamCloudConnect = "veeam-cloud-connect";

        public const string ResellerPlatform = "dual-platform";

        public const string UniqueIp = "uniek-ip-adres";

    }
}
