﻿using System;

namespace Store.Model
{
    public class Price
    {
        public decimal VatRate { get; private set; }

        public Price() : this(0,0,0)
        {
        }

        public Price(decimal exclVat, decimal reductionExclVat, decimal vatRatePercentage)
        {
            VatRate = Round(vatRatePercentage) / 100;
            ExclVat = Round(exclVat);
            ReductionExclVat = Round(reductionExclVat);
        }

        public decimal ExclVat { get; private set; }

        public decimal InclVat
        {
            get
            {
                return Round(ExclVat + (ExclVat * VatRate));
            }
        }

        public decimal ReductionExclVat { get; private set; }

        public decimal ReductionInclVat
        {
            get
            {
                return Round(ReductionExclVat + (ReductionExclVat * VatRate));
            }
        }

        private Price Multiply(int quantity)
        {
            return new Price(ExclVat * quantity, ReductionExclVat * quantity, VatRate * 100);
        }

        public static Price operator *(Price price, int quantity)
        {
            return price.Multiply(quantity);
        }

        public Price WithoutVat()
        {
            return WithVatRatePercentage(0);
        }

        public Price WithVatRatePercentage(decimal vatRatePercentage)
        {
            return new Price(ExclVat, ReductionExclVat, vatRatePercentage);
        }

        protected bool Equals(Price other)
        {
            return VatRate == other.VatRate && ExclVat == other.ExclVat && ReductionExclVat == other.ReductionExclVat;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Price) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = VatRate.GetHashCode();
                hashCode = (hashCode*397) ^ ExclVat.GetHashCode();
                hashCode = (hashCode*397) ^ ReductionExclVat.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Price left, Price right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Price left, Price right)
        {
            return !Equals(left, right);
        }

        private decimal Round(decimal valueToRound)
        {
            return Math.Round(valueToRound, 2, MidpointRounding.AwayFromZero);
        }
    }
}
