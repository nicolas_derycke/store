﻿using System;

namespace Store.Model
{
    public class ProductInfo
    {
        public int ProductId { get; private set; }
        public string ProductCode { get; private set; }
        public string ProductGroupCode { get; private set; }
        public string PromoProductCode { get; private set; }
        public string ProductName { get; private set; }
        public string ProductType { get; private set; }
        public decimal PeriodPrice { get; private set; }
        public int PeriodId { get; private set; }
        public decimal Reduction { get; private set; }
        public int Period { get; private set; }
        public ProductAction ProductAction { get; private set; }

        public bool IsRecurring { get; private set; }

        public ProductInfo(
            int productId, 
            string productCode, 
            string productGroupCode, 
            string promoProductCode, 
            string productName, 
            string productType,
            decimal periodPrice, 
            int periodId, 
            decimal reduction, 
            int period, 
            ProductAction productAction,
            bool isRecurring)
        {
            if (productCode == null) throw new ArgumentNullException("productCode");
            if (productGroupCode == null) throw new ArgumentNullException("productGroupCode");
            if (productName == null) throw new ArgumentNullException("productName");

            if (promoProductCode == null) promoProductCode = string.Empty;

            ProductId = productId;
            ProductCode = productCode;
            ProductGroupCode = productGroupCode;
            PromoProductCode = promoProductCode;
            ProductName = productName;
            ProductType = productType;
            PeriodPrice = periodPrice;
            PeriodId = periodId;
            Reduction = reduction;
            Period = period;
            ProductAction = productAction;
            IsRecurring = isRecurring;
        }
    }
}