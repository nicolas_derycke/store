﻿using System;

namespace Store.Model.Exceptions
{
    public class UnauthorizedProviderException : Exception
    {
        public UnauthorizedProviderException()
            : base("Resource does not belong to the provider")
        {
        }
    }
}