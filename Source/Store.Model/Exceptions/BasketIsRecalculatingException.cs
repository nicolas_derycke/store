﻿using System;

namespace Store.Model.Exceptions
{
    public class BasketIsRecalculatingException : Exception
    {
        public BasketIsRecalculatingException(string orderCode)
            : base(string.Format("Basket with ordercode '{0}' is recalculating", orderCode))
        {
        }
    }
}