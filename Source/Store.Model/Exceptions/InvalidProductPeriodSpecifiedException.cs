﻿using System;

namespace Store.Model.Exceptions
{
    public class InvalidProductPeriodSpecifiedException : Exception
    {
        public InvalidProductPeriodSpecifiedException(int period)
            : base(string.Format("The period specified is invalid. Period: '{0}'", period))
        {
        }
    }
}