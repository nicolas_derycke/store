﻿using System;
using Store.API.Contracts;

namespace Store.Model.Exceptions
{
    public class AttributeNotFoundException : Exception
    {
        public AttributeNotFoundException(AttributeKey key)
            : base(string.Format("Attribute with key '{0}' was not found", key.ToString()))
        {
        }
    }
}