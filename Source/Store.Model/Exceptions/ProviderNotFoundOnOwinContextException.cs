﻿using System;

namespace Store.Model.Exceptions
{
    public class ProviderNotFoundOnOwinContextException : Exception
    {
        public ProviderNotFoundOnOwinContextException()
            : base("Provider is not found on the owin request scope context")
        {
        }
    }
}
