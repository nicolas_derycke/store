﻿using System;

namespace Store.Model.Exceptions
{
    public class OrderAlreadyActivatedException : Exception
    {
        public OrderAlreadyActivatedException(string ordercode)
            : base(string.Format("Order '{0}' is already activated.", ordercode))
        {
        }
    }
}
