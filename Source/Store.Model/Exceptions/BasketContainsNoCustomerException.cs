﻿using System;

namespace Store.Model.Exceptions
{
    public class BasketContainsNoCustomerException : Exception
    {
        public BasketContainsNoCustomerException(string ordercode)
            : base(string.Format("Basket with ordercode '{0}' has no customer linked", ordercode))
        {
        }
    }
}