﻿using System;

namespace Store.Model.Exceptions
{
    public class DuplicateBasketItemAttributeException : Exception
    {
        public DuplicateBasketItemAttributeException(string attributeKey)
            : base(string.Format("The basket item allready has an attribute with key: '{0}'", attributeKey))
        {
        }
    }
}
