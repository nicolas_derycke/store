using System;
using Store.API.Contracts;

namespace Store.Model.Exceptions
{
    public class CheckoutTypeNotAllowedException : Exception
    {
        public CheckoutTypeNotAllowedException(string ordercode, CheckoutTypeContract checkoutType)
            : base(string.Format("Checkout type '{0}' not allowed for basket with ordercode '{1}'", checkoutType, ordercode))
        {
        }
    }
}