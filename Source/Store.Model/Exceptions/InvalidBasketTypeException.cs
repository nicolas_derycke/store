﻿using System;

namespace Store.Model.Exceptions
{
    public class InvalidBasketTypeException : Exception
    {
        public InvalidBasketTypeException(string message)
            : base(message)
        {
        }
    }
}