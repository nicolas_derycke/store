﻿using System;

namespace Store.Model.Exceptions
{
    public class ProductNotFoundException : Exception
    {
        public ProductNotFoundException(string productCode)
            : base(string.Format("Product with code '{0}' is not found", productCode))
        {
        }
    }
}
