﻿using System;

namespace Store.Model.Exceptions
{
    public class InvalidPaymentDataExpection : Exception
    {
        public InvalidPaymentDataExpection(string message)
            : base(message)
        {
        }
    }
}