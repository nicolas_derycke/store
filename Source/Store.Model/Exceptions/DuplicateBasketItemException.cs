﻿using System;

namespace Store.Model.Exceptions
{
    public class DuplicateBasketItemException : Exception
    {
        public DuplicateBasketItemException(string productCode)
            : base(string.Format("Basket item with product code '{0}' is a duplicate", productCode))
        {
        }
    }
}
