﻿using System;

namespace Store.Model.Exceptions
{
    public class BasketItemNotFoundException : Exception
    {
        public BasketItemNotFoundException(int basketItemId, string ordercode)
            : base(string.Format("Basket item with ID: {0} is not found in order with ordercode '{1}'", basketItemId, ordercode))
        {
        }
    }
}
