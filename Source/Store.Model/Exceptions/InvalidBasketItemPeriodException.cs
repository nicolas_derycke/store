﻿using System;

namespace Store.Model.Exceptions
{
    public class InvalidBasketItemPeriodException : Exception
    {
        public InvalidBasketItemPeriodException(int period, string productCode)
            : base(string.Format("Period '{0}' is not allowed for the basket item '{1}'", period, productCode))
        {
        }
    }
}