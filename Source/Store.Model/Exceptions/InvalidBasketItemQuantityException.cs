﻿using System;

namespace Store.Model.Exceptions
{
    public class InvalidBasketItemQuantityException : Exception
    {
        public InvalidBasketItemQuantityException(int basketItemId, int quantity)
            : base(string.Format("Quantity '{0}' is not valid for basket item '{1}'", quantity, basketItemId))
        {
        }
    }
}