﻿using System;

namespace Store.Model.Exceptions
{
    public class BasketFullException : Exception
    {
        public BasketFullException()
            : base("Basket contains maximum number of items.")
        {
        }

        public BasketFullException(int maxNumber)
            : base(string.Format("Basket contains maximum number of {0} items.", maxNumber))
        {
        }
    }
}
