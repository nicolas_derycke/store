﻿using System;

namespace Store.Model.Exceptions
{
    public class BasketEmptyException : Exception
    {
        public BasketEmptyException()
            : base("An empty basket is not allowed.")
        {
        }
    }
}