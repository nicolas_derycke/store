﻿using System;
using Store.API.Contracts;

namespace Store.Model.Exceptions
{
    public class InvalidBasketItemAttributeException : Exception
    {
        public InvalidBasketItemAttributeException(string key, string error)
            : base(string.Format("Attribute with key: '{0}' is invalid. {1}", key, error))
        {
        }

        public InvalidBasketItemAttributeException(AttributeKey key, string error)
            : this(key.ToString(), error)
        {
        }
    }
}
