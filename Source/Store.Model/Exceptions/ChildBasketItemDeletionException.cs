﻿using System;

namespace Store.Model.Exceptions
{
    public class ChildBasketItemDeletionException : Exception
    {
        public ChildBasketItemDeletionException(int basketItemId, int parentBasketItemId)
            : base(string.Format("Basket item '{0}' can't be deleted because it has a parent basket item '{1}'", basketItemId, parentBasketItemId))
        {
        }
    }
}