﻿using System;

namespace Store.Model.Exceptions
{
    public class OrderNotFoundException : Exception
    {
        public OrderNotFoundException(string ordercode)
            : base(string.Format("Order for ordercode '{0}' is not found", ordercode))
        {
        }
    }
}
