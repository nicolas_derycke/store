﻿using System;
using Store.API.Contracts;

namespace Store.Model.Exceptions
{
    public class InvalidOrderStatusException : Exception
    {
        public InvalidOrderStatusException(OrderStatus status)
            : base(string.Format("Invalid order status: {0}", status))
        {
        }
    }
}
