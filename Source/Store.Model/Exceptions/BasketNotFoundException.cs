﻿using System;

namespace Store.Model.Exceptions
{
    public class BasketNotFoundException : Exception
    {
        public BasketNotFoundException(string ordercode)
            : base(string.Format("Basket for ordercode '{0}' is not found", ordercode))
        {
        }
    }
}
