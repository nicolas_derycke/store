﻿using System;

namespace Store.Model.Exceptions
{
    public class BasketContainsValidationErrorsException : Exception
    {
        public BasketContainsValidationErrorsException(string ordercode)
            : base(string.Format("Basket with ordercode '{0}' contains validation errors", ordercode))
        {
        }
    }
}