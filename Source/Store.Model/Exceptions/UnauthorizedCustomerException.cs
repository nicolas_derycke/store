﻿using System;

namespace Store.Model.Exceptions
{
    public class UnauthorizedCustomerException : Exception
    {
        public UnauthorizedCustomerException()
            : base("Resource does not belong to the customer")
        {
        }
    }
}