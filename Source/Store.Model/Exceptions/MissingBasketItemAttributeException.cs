﻿using System;
using Store.API.Contracts;

namespace Store.Model.Exceptions
{
    public class MissingBasketItemAttributeException : Exception
    {
        public MissingBasketItemAttributeException(AttributeKey key, int basketItemId)
            : base(string.Format("Missing attribute key '{0}' in basket item '{1}'", key, basketItemId))
        {
        }
    }
}