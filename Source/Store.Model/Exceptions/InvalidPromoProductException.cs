﻿using System;

namespace Store.Model.Exceptions
{
    public class InvalidPromoProductException : Exception
    {
        public InvalidPromoProductException(string productCode, string promoProductCode)
            : base(string.Format("Product with code '{0}' does not have the same productgroup as promo product with code '{1}'", productCode, promoProductCode))
        {
        }
    }
}
