﻿using System;

namespace Store.Model.Exceptions
{
    public class BasketAlreadyCheckedOutException : Exception
    {
        public BasketAlreadyCheckedOutException(string orderCode)
            : base(string.Format("Basket with ordercode '{0}' is already checkedout", orderCode))
        {
        }
    }
}