﻿using System;

namespace Store.Model.Exceptions
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException()
            : base("Resource does not belong to the applicant")
        {
        }
    }
}