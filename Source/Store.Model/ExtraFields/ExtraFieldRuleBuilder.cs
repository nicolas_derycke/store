﻿using System;
using Intelligent.Core.Models;

namespace Store.Model.ExtraFields
{
    public class ExtraFieldRuleBuilder
    {
        private readonly ExtraFieldRule _extraFieldRule;

        public ExtraFieldRuleBuilder()
        {
            _extraFieldRule = new ExtraFieldRule();
        }

        public ExtraFieldRuleBuilder ForExtension(string extension)
        {
            _extraFieldRule.Extension = new Extension(extension);
            return this;
        }

        public ExtraFieldRuleBuilder ForCompanies()
        {
            _extraFieldRule.RegistrantType = RegistrantType.Company;
            return this;
        }

        public ExtraFieldRuleBuilder ForIndividuals()
        {
            _extraFieldRule.RegistrantType = RegistrantType.Individual;
            return this;
        }

        public ExtraFieldRuleBuilder ForAnyone()
        {
            _extraFieldRule.RegistrantType = null;
            return this;
        }

        public ExtraFieldRuleBuilder Inside(string countryCode)
        {
            _extraFieldRule.CountryCode = countryCode;
            _extraFieldRule.OutsideCountryCode = false;
            return this;
        }

        public ExtraFieldRuleBuilder Outside(string countryCode)
        {
            _extraFieldRule.CountryCode = countryCode;
            _extraFieldRule.OutsideCountryCode = true;
            return this;
        }
        
        public ExtraFieldRule Build()
        {
            if (_extraFieldRule.Extension == null)
                throw new Exception("An extension is required to create an extra field rule.");

            return _extraFieldRule;
        }
    }
}