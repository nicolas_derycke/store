﻿using System;
using Intelligent.Core.Models;
using M = Store.Model.ExtraFields;

namespace Store.Model.ExtraFields
{
    public class ExtraFieldRule
    {
        internal ExtraFieldRule()
        {
        }

        public Extension Extension { get; set; }

        public RegistrantType? RegistrantType { get; set; }

        public string CountryCode { get; set; }

        public bool OutsideCountryCode { get; set; }

        public bool IsMatch(DomainName domainName, string company, string countryCode)
        {
            if (domainName.Extension != Extension)
                return false;

            var registrantType = string.IsNullOrWhiteSpace(company) ? M.RegistrantType.Individual : M.RegistrantType.Company;
            if (RegistrantType.HasValue && RegistrantType != registrantType)
                return false;

            if (!string.IsNullOrWhiteSpace(CountryCode) && !OutsideCountryCode && !string.Equals(countryCode, CountryCode, StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.IsNullOrWhiteSpace(CountryCode) && OutsideCountryCode && string.Equals(countryCode, CountryCode, StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
    }
}