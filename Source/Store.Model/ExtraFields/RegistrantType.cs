﻿namespace Store.Model.ExtraFields
{
    public enum RegistrantType
    {
        Individual,
        Company,
    }
}