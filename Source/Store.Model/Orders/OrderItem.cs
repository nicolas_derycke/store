﻿using System.Collections.Generic;
using Store.API.Contracts;

namespace Store.Model.Orders
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductType { get; set; }
        public string PromoProductCode { get; set; }
        public string ProductName { get; set; }
        public int Period { get; set; }
        public int Quantity { get; set; }
        public PeriodUnitType PeriodUnitType { get; set; }
        public Price TotalPrice { get; set; }
        public Price Price { get; set; }
        public Dictionary<AttributeKey, string> Attributes { get; set; }
        public int PeriodId { get; set; }
        public bool IsRecurring { get; set; }
        public int? ParentId { get; set; }
        public string Identifier { get; set; }
        public string PromoCode { get; set; }
    }
}
