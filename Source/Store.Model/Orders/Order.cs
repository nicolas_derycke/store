﻿using System;
using System.Collections.Generic;
using Intelligent.Core.Models;
using Store.API.Contracts;

namespace Store.Model.Orders
{
    public class Order
    {
        public Order(
            int orderId,
            string orderCode,
            CustomerIdentifier customerIdentifier,
            IEnumerable<OrderItem> items,
            Price totalPrice,
            decimal vatRatePercentage,
            decimal vatAmount,
            CheckoutType checkoutType,
            CheckoutData checkoutData,
            string customerReference,
            string source,
            Provider provider,
            OrderStatus status,
            int ticketId,
            string ticketNumber,
            DateTime creationDate,
            string epayCode,
            bool needsPayment,
            Language language)
        {
            OrderId = orderId;
            OrderCode = orderCode;
            CustomerIdentifier = customerIdentifier;
            Items = items;
            TotalPrice = totalPrice;
            VatRatePercentage = vatRatePercentage;
            VatAmount = vatAmount;
            CheckoutType = checkoutType;
            CheckoutData = checkoutData;
            CustomerReference = customerReference;
            Source = source;
            Provider = provider;
            Status = status;
            TicketId = ticketId;
            TicketNumber = ticketNumber;
            CreationDate = creationDate;
            EpayCode = epayCode;
            NeedsPayment = needsPayment;
            Language = language;
        }

        public int OrderId { get; private set; }
        public string OrderCode { get; private set; }
        public CustomerIdentifier CustomerIdentifier { get; private set; }
        public IEnumerable<OrderItem> Items { get; private set; }
        public Price TotalPrice { get; private set; }
        public decimal VatRatePercentage { get; private set; }
        public decimal VatRate { get; private set; }
        public decimal VatAmount { get; private set; }
        public CheckoutType CheckoutType { get; set; }
        public CheckoutData CheckoutData { get; set; }
        public string CustomerReference { get; set; }
        public string Source { get; private set; }
        public Provider Provider { get; private set; }
        public OrderStatus Status { get; private set; }
        public int TicketId { get; private set; }
        public string TicketNumber { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string EpayCode { get; set; }
        public bool NeedsPayment { get; set; }
        public Language Language { get; private set; }

        public void AssignTicket(int ticketId, string ticketNumber)
        {
            TicketId = ticketId;
            TicketNumber = ticketNumber;
        }
    }
}
