﻿namespace Store.Model.Orders
{
    public enum CheckoutType
    {
        None,
        Free,
        BankTransfer,
        CreditCardPayment,
        DebitCardPayment,
        DirectActivation,
		PayPal
    }
}