﻿namespace Store.Model.Orders
{
    public class CheckoutData
    {
        public decimal PaidAmount { get; private set; }

        public string PaymentProviderReference { get; private set; }

        public CheckoutData(decimal paidAmount, string paymentProviderReference)
        {
            PaidAmount = paidAmount;
            PaymentProviderReference = paymentProviderReference;
        }
    }
}