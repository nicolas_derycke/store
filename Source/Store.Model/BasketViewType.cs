﻿namespace Store.Model
{
    public enum BasketViewType
    {
        Detailed = 0,
        Summary = 1
    }
}
