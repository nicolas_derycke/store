﻿namespace Store.Model
{
    public class PromoProductInfo
    {
        public int PeriodId { get; private set; }
        public int Period { get; private set; }
        public string ProductName { get; private set; }
        public int ProductId { get; private set; }
        public int ProductGroupId { get; private set; }
        public decimal PeriodPrice { get; private set; }

        public PromoProductInfo(int periodId, int period, string productName, int productId, int productGroupId, decimal periodPrice)
        {
            PeriodId = periodId;
            Period = period;
            ProductName = productName;
            ProductId = productId;
            ProductGroupId = productGroupId;
            PeriodPrice = periodPrice;
        }
    }
}