﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Core.Owin;
using Core.Owin.Security.OAuth;
using Core.Security;
using IdentityServer3.AccessTokenValidation;
using Intelligent.Shared.Logging.Serilog;
using Intelligent.Shared.Messaging.MassTransit3;
using Intelligent.Shared.Owin;
using Intelligent.Shared.Owin.ExceptionLogging;
using Intelligent.Shared.WebApi;
using Intelligent.Shared.WebApi.ErrorHandling;
using Microsoft.Owin;
using Microsoft.Practices.Unity;
using Owin;
using Serilog;
using Store.API.Controllers;
using Store.API.Swashbuckle;
using Store.Mapping;
using Store.Model.Exceptions;
using Swashbuckle.Application;
using Unity.WebApi;
using MassTransit;

[assembly: OwinStartup(typeof(Store.API.Startup))]
namespace Store.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            LogConfiguration.Initialize(c => c.Enrich.WithProperties(() => OwinRequestScopeContext.Current.GetIntelliProperties()));

            app.UseExceptionLogging(ex => Log.Error(ex, "Exception in store API"));

            var container = UnityHelper.GetConfiguredContainer();

            MappingConfiguration.ConfigureMappings();

            var webApiConfig = ConfigurationBuilder.Build(new UnityDependencyResolver(container));

            app.MapWhen(
                context => context.Request.Path.Value.Contains(Routes.DocumentationPrefix) || context.Request.Path.Value.Contains(Routes.InspectionPrefix),
                appBuilder =>
                {
                    appBuilder.UseWebApi(webApiConfig);
                });

            //app.MapWhen(
            //    context => context.Request.Path.Value.Contains(Routes.BasketsRoutePrefix),
            //    appBuilder =>
            //    {
            //        webApiConfig.Filters.Add(new RequireAdminUserAuthorizationAttribute());
            //    });
            
            webApiConfig.HandleExceptions()
               .WithExceptionLogging(ex => Log.Error(ex, "Webapi Exception in store API"))
               .WithStatusAndErrorCode<BasketNotFoundException>(HttpStatusCode.NotFound, "basket_not_found")
               .WithStatusAndErrorCode<DuplicateBasketItemAttributeException>(HttpStatusCode.BadRequest, "basket_duplicate_item_attribute")
               .WithStatusAndErrorCode<BasketItemNotFoundException>(HttpStatusCode.NotFound, "basket_item_not_found")
               .WithStatusAndErrorCode<AttributeNotFoundException>(HttpStatusCode.NotFound, "basket_attribute_not_found")
               .WithStatusAndErrorCode<InvalidProductPeriodSpecifiedException>(HttpStatusCode.BadRequest, "product_invalid_period")
               .WithStatusAndErrorCode<ProductNotFoundException>(HttpStatusCode.NotFound, "product_not_found")
               .WithStatusAndErrorCode<BasketContainsValidationErrorsException>(HttpStatusCode.BadRequest, "basket_contains_validation_errors")
               .WithStatusAndErrorCode<BasketAlreadyCheckedOutException>(HttpStatusCode.BadRequest, "basket_already_checkedout")
               .WithStatusAndErrorCode<InvalidBasketTypeException>(HttpStatusCode.BadRequest, "basket_type_invalid")
               .WithStatusAndErrorCode<ChildBasketItemDeletionException>(HttpStatusCode.Forbidden, "basket_child_item_not_deletable")
               .WithStatusAndErrorCode<InvalidPaymentDataExpection>(HttpStatusCode.BadRequest, "checkout_invalid_payment_data")
               .WithStatusAndErrorCode<MissingBasketItemAttributeException>(HttpStatusCode.BadRequest, "basket_missing_item_attribute")
               .WithStatusAndErrorCode<InvalidBasketItemQuantityException>(HttpStatusCode.BadRequest, "basket_invalid_item_quantity")
               .WithStatusAndErrorCode<InvalidBasketItemPeriodException>(HttpStatusCode.BadRequest, "basket_invalid_item_period")
               .WithStatusAndErrorCode<BasketEmptyException>(HttpStatusCode.Forbidden, "basket_empty")
               .WithStatusAndErrorCode<BasketFullException>(HttpStatusCode.Forbidden, "basket_full")
               .WithStatusAndErrorCode<BasketIsRecalculatingException>(HttpStatusCode.Forbidden, "basket_is_recalculating")
               .WithStatusCode<ProviderNotFoundOnOwinContextException>(HttpStatusCode.BadRequest)
               .WithStatusCode<NotSupportedException>(HttpStatusCode.BadRequest)
               .WithStatusCode<InvalidOrderStatusException>(HttpStatusCode.BadRequest)
               .WithStatusCode<OrderNotFoundException>(HttpStatusCode.NotFound)
               .WithStatusCode<UnauthorizedCustomerException>(HttpStatusCode.Unauthorized)
               .WithStatusCode<UnauthorizedException>(HttpStatusCode.Unauthorized)
               .WithStatusCode<UnauthorizedProviderException>(HttpStatusCode.Unauthorized);

            var busControl = BusControlBuilder.New(
                    ConfigurationManager.AppSettings["MassTransit_HostAddress"],
                    ConfigurationManager.AppSettings["MassTransit_UserName"],
                    ConfigurationManager.AppSettings["MassTransit_Password"]
                )
                .WithAddedBusConfiguration(cfg =>
                {
                    cfg.ConfigurePublish(publishCfg => publishCfg.UseSendExecute(context =>
                    {
                        var intelliProperties = OwinRequestScopeContext.Current.GetIntelliProperties();
                        foreach (var intelliProperty in intelliProperties)
                        {
                            context.Headers.Set(intelliProperty.Key, intelliProperty.Value);
                        }
                    }));
                })
                .Build();

            container.RegisterBusControl(busControl);

            EnableSwagger(webApiConfig);

            webApiConfig.Formatters.Remove(webApiConfig.Formatters.XmlFormatter);
            webApiConfig.Formatters.JsonFormatter.SerializerSettings.Error = (sender, args) =>
            {
                Log.Error(string.Format("{0} - {1}", args.ErrorContext.Error.Message, args.ErrorContext.Error.StackTrace));
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    System.Diagnostics.Debugger.Break();
                }
            };



            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = ConfigurationManager.AppSettings["IdentityAuthority"],
                ValidationMode = ValidationMode.ValidationEndpoint,
                RequiredScopes = new[] { Scopes.Customer }
            });

            app.MapWhen(
                context => (!context.Request.Path.Value.Contains(Routes.DocumentationPrefix) && !context.Request.Path.Value.Contains(Routes.InspectionPrefix)),
                appBuilder =>
                {
                    appBuilder.
                        UseRequestScopeContext().
                        UseIntelliHeadersMiddleware().
                        Use<AuthenticatedUserMiddleware>().
                        Use<AuthenticatedEmployeeMiddleware>(container.Resolve<IEmployeeRightsProvider>()).
                        Use<AuthenticatedApplicationMiddleware>().
                        Use<LanguageMiddleware>().
                        Use<ProviderMiddleware>().
                        Use<ClientIdMiddleware>().
                        Use<CustomerIdentifierMiddleware>().
                        Use<BearerTokenMiddleware>().
                        UseWebApi(webApiConfig);
                });

            
        }

        private static void EnableSwagger(HttpConfiguration webApiConfig)
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var authority = ConfigurationManager.AppSettings["IdentityAuthority"];
            webApiConfig.
                EnableSwagger(Routes.DocumentationPrefix + "/swagger/{apiVersion}", c =>
                {
                    c.OAuth2("idsrv")
                        .Flow("implicit")
                        .Scopes(s => s.Add(Scopes.Customer, "Customer scope"))
                        .Description("OAuth2 Implicit Grant")
                        .TokenUrl(authority + "/connect/token")
                        .AuthorizationUrl(authority + "/connect/authorize");
                    c.SingleApiVersion("v1", "Basket API").
                        Contact(contact =>
                        {
                            contact.Email("dev@combellgroup.com");
                            contact.Name("Development");
                        }).
                        Description("The store API allows you to create, update & get baskets. OAUTH is used as security mechanism.");
                    c.IncludeXmlComments(Path.Combine(baseDirectory, "bin", @"Store.API.Contracts.XML"));
                    c.IncludeXmlComments(Path.Combine(baseDirectory, "bin", @"Store.API.XML"));
                    c.OperationFilter<AddAuthorizationHeaderOperationFilter>();
                }).
                EnableSwaggerUi(Routes.DocumentationPrefix + "/{*assetPath}", c =>
                {
                    c.DisableValidator();
                    c.EnableOAuth2Support("intelligent.swash", "swash", "intelligent-realm", "Swagger UI");
                });
        }
    }
}