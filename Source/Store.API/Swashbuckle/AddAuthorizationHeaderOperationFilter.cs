﻿using System.Collections.Generic;
using System.Web.Http.Description;
using Core.Security;
using Swashbuckle.Swagger;

namespace Store.API.Swashbuckle
{
    internal class AddAuthorizationHeaderOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation == null)
                return;

            if (operation.security == null)
                operation.security = new List<IDictionary<string, IEnumerable<string>>>();

            var oAuthRequirements = new Dictionary<string, IEnumerable<string>> { { "idsrv", new List<string> { Scopes.Customer } } };
            operation.security.Add(oAuthRequirements);
        }
    }
}