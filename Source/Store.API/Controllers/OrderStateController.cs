﻿using System;
using System.Linq;
using System.Web.Http;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Web.ErrorHandling.Exceptions;
using Store.API.Contracts.Orders;
using Store.Mapping;
using Store.SqlQueries;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.OrderStatesPrefix)]
    public class OrderStateController : ApiController
    {
        private readonly IQueryHandler _queryHandler;

        public OrderStateController(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            _queryHandler = queryHandler;
        }

        /// <summary>
        /// Get order state
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <response code="200">The order state.</response>
        /// <response code="404">When order state not found.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}")]
        public OrderStateContract GetOrderState(string orderCode)
        {
            return _queryHandler.Execute(new GetOrderStateQuery(orderCode))
                .WhenEmpty(() => { throw new ResourceNotFoundException("orderstate", orderCode); })
                .Select(OrderStateDataToOrderStateContractMapper.Map)
                .FirstOrDefault();
        }
    }
}