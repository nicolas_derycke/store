﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Core.Owin;
using Core.Security;
using Intelligent.Core.Mappers;
using Intelligent.Core.Models;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Owin;
using MassTransit;
using Serilog;
using Store.API.Authorization;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Commands.Baskets;
using Store.Messages.Events;
using Store.Messages.Payment;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.Proxies;
using Store.Services;
using Store.SqlQueries;
using Store.SqlCommands.Baskets;
using Store.SqlCommands;
using Store.Helpers;
using Intelligent.Shared.Threading;

namespace Store.API.Controllers
{
    [RequireAdminUserAuthorization]
    public class CheckoutController : ApiController
    {
        private readonly IBus _bus;
        private readonly ICommandBus _commandBus;
        private readonly IBasketService _basketService;
        private readonly ICustomerInvoiceCriteriaProxy _customerInvoiceCriteriaProxy;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly ILogger _logger;

        private static readonly Dictionary<string, AttributeKey[]> ProductTypeRequiredAttributes = new Dictionary<string, AttributeKey[]>
        {
            {ProductTypes.DomainName, new[] {AttributeKey.Domain}},
            {ProductTypes.LinuxSharedHosting, new[] {AttributeKey.Domain}},
            {ProductTypes.WindowsSharedHosting, new[] {AttributeKey.Domain}},
            {ProductTypes.BasicEmail, new[] {AttributeKey.Domain}},
            {ProductTypes.BusinessEmail, new[] {AttributeKey.Domain}},
            {ProductTypes.Caching, new[] {AttributeKey.Domain}},
            {ProductTypes.Drupal, new[] {AttributeKey.Domain}},
            {ProductTypes.Joomla, new[] {AttributeKey.Domain}},
            {ProductTypes.Magento, new[] {AttributeKey.Domain}},
            {ProductTypes.Wordpress, new[] {AttributeKey.Domain}},
            {ProductTypes.StandardDomainValidation, new[] {AttributeKey.Certificate}},
            {ProductTypes.MultiDomainDomainValidation, new[] {AttributeKey.Certificate}},
            {ProductTypes.WildcardDomainValidation, new[] {AttributeKey.Certificate}},
            {ProductTypes.StandardOrganisation, new[] {AttributeKey.Certificate}},
            {ProductTypes.MultiDomainOrganisation, new[] {AttributeKey.Certificate}},
            {ProductTypes.WildcardOrganisation, new[] {AttributeKey.Certificate}},
            {ProductTypes.StandardEv, new[] {AttributeKey.Certificate}},
            {ProductTypes.MultiDomainEv, new[] {AttributeKey.Certificate}},
            {ProductTypes.MsSqlDb, new[] {AttributeKey.Database}},
            {ProductTypes.MysqlDb, new[] {AttributeKey.Database}},
            {ProductTypes.Faxdiensten, new[] {AttributeKey.Fax}},
            {ProductTypes.UniqueIp, new[] {AttributeKey.Domain}}
        };

        public CheckoutController(IBus bus, ICommandBus commandBus, IBasketService basketService, ICustomerInvoiceCriteriaProxy customerInvoiceCriteriaProxy, ISqlCommandHandler sqlCommandHandler, ILogger logger)
        {
            if (bus == null) throw new ArgumentNullException("bus");
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (customerInvoiceCriteriaProxy == null) throw new ArgumentNullException("customerInvoiceCriteriaProxy");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (logger == null) throw new ArgumentNullException("logger");

            _bus = bus;
            _commandBus = commandBus;
            _basketService = basketService;
            _customerInvoiceCriteriaProxy = customerInvoiceCriteriaProxy;
            _sqlCommandHandler = sqlCommandHandler;
            _logger = logger;
        }

        /// <summary>
        /// Get allowed checkout types on a basket.
        /// </summary>
        /// <param name="orderCode">Order code</param>
        /// <response code="200">Checkout types.</response>
        /// <response code="404">Basket or order not found</response>
        /// <returns>CheckoutTypes: free, bankTransfer, creditCardPayment, debitCardPayment, directActivation</returns>
        [HttpGet]
        [Route("baskets/{orderCode}/allowedcheckouttypes")]
        public async Task<IEnumerable<CheckoutTypeContract>> GetAllowedCheckoutTypes(string orderCode)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoReadAccess(basket);

            if (basket.CheckedOut)
                throw new BasketAlreadyCheckedOutException(orderCode);
            
            // refresh cache to avoid faulty checkouts
            _customerInvoiceCriteriaProxy.ClearCache(basket.CustomerIdentifier);

            return GetAllowedCheckoutTypes(basket);
        }

        [HttpGet]
        [Route("checkedoutbaskets/{orderCode}")]
        public async Task<BasketContract> GetCheckedoutBasket(string orderCode)
        {
            var isRequestByApplication = ((ClaimsIdentity)User.Identity).HasClaim(c => c.Type == IntelligentClaimTypes.Application);

            if (!isRequestByApplication)
                throw new UnauthorizedException();

            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            return basket.MapTo<BasketContract>();
        }

        /// <summary>
        /// Checkout a basket. Converts basket to an order
        /// </summary>
        /// <param name="checkoutContract">Checkout contract with CheckoutTypes: free, bankTransfer, creditCardPayment, debitCardPayment, directActivation</param>
        /// <response code="404">Basket or order not found</response>
        [HttpPost]
        [Route("checkedoutbaskets")]
        public async Task<HttpResponseMessage> Checkout([FromBody]CheckoutContract checkoutContract)
        {
            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();
            _logger.Information("Checkout was started for the order {orderCode} by user {userId}", checkoutContract.OrderCode, userId);
            var user = new GetUacUserById(userId, OwinRequestScopeContext.Current.GetProvider().Value).Execute();
            var isCheckedoutByEmployee = EmployeeHelper.IsEmployee(User);

            var orderCode = checkoutContract.OrderCode;

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                var basket = await _basketService.GetBasketByOrderCode(orderCode);

                AuthorizationHelper.ThrowIfNoWriteAccess(basket);

                if (!basket.Items.Any())
                    throw new BasketEmptyException();

                if (basket.IsAnonymousBasket)
                    throw new BasketContainsNoCustomerException(orderCode);

                if (basket.CheckedOut)
                {
                    _logger.Information("Basket with ordercode {OrderCode} already checked out, nothing to check out", basket.OrderCode);
                    transaction.Complete();
                    return Request.CreateResponse(HttpStatusCode.Accepted);
                }

                // WARNING: very dirty fix for sales to be able to order a transfer for a domain name that the customer already has
                // so they can update the licensee info
                new DirtyFixForEmployeeHelper(User).RemoveUacAccountAlreadyExistsErrorForDomainNameProducts(basket);

                if (basket.Items.Any(i => i.ValidationError != BasketItemValidationError.None))
                    throw new BasketContainsValidationErrorsException(orderCode);

                if (!GetAllowedCheckoutTypes(basket).Contains(checkoutContract.CheckoutType))
                    throw new CheckoutTypeNotAllowedException(orderCode, checkoutContract.CheckoutType);

                var isPaid = IsPaid(checkoutContract);
                if (!isPaid && checkoutContract.PaymentData != null)
                    throw new InvalidPaymentDataExpection(string.Format("No payment data expected when the payment method is '{0}'", checkoutContract.CheckoutType));

                if (isPaid && checkoutContract.PaymentData == null)
                    throw new InvalidPaymentDataExpection(string.Format("Payment data is required when the payment method is '{0}'", checkoutContract.CheckoutType));

                var checkoutType = checkoutContract.CheckoutType.MapTo<CheckoutType>();
                CheckoutData checkoutData = null;
                if (isPaid)
                    checkoutData = checkoutContract.PaymentData.MapTo<CheckoutData>();

                foreach (var basketItem in basket.Items)
                {
                    var registrantAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<RegistrantAttributes>(AttributeKey.Registrant);

                    Language language;
                    if (registrantAttributes.HasValue && !LanguageMapper.TryMapLanguage(registrantAttributes.Value.Language, out language))
                        throw new InvalidBasketItemAttributeException(AttributeKey.Registrant, "Invalid format for language.");

                    var certificateAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<CertificateAttributes>(AttributeKey.Certificate);
                    if (certificateAttributes.HasValue && basketItem.ParentId.HasValue && certificateAttributes.Value.Hostnames.Count() > 1)
                        throw new InvalidBasketItemAttributeException(AttributeKey.Certificate, "Child certificate basket items can only contain 1 hostname.");

                    var faxAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<FaxAttributes>(AttributeKey.Fax);
                    if (faxAttributes.HasValue && !faxAttributes.Value.IsNewFaxNumber && string.IsNullOrWhiteSpace(faxAttributes.Value.Number))
                        throw new InvalidBasketItemAttributeException(AttributeKey.Fax, "Number can not be empty when transferring a fax number");
                    if (faxAttributes.HasValue && faxAttributes.Value.IsNewFaxNumber && !string.IsNullOrWhiteSpace(faxAttributes.Value.Number))
                        throw new InvalidBasketItemAttributeException(AttributeKey.Fax, "Number can not be filled in when ordering a new fax number");

                    if (ProductTypeRequiredAttributes.ContainsKey(basketItem.ProductType))
                        foreach (var requiredAttribute in ProductTypeRequiredAttributes[basketItem.ProductType])
                            if (!basketItem.Attributes.ContainsKey(requiredAttribute))
                                throw new MissingBasketItemAttributeException(requiredAttribute, basketItem.BasketItemId);
                }

                _commandBus.Send(new CheckoutBasketCommand(basket, checkoutType, checkoutData, checkoutContract.CustomerReference, userId, user.Name, isCheckedoutByEmployee));
                _customerInvoiceCriteriaProxy.ClearCache(basket.CustomerIdentifier);

                transaction.Complete();
                _logger.Information("Checked out basket with ordercode {OrderCode}", basket.OrderCode);
            }

            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        private bool IsPaid(CheckoutContract checkoutContract)
        {
            return checkoutContract.CheckoutType == CheckoutTypeContract.CreditCardPayment ||
                   checkoutContract.CheckoutType == CheckoutTypeContract.DebitCardPayment;
        }

        private IEnumerable<CheckoutTypeContract> GetAllowedCheckoutTypes(Basket basket)
        {
            if (basket.ItemCount == 0) // no checkouttypes on empty basket
                yield break;

            if (!basket.IsAnonymousBasket &&
                _customerInvoiceCriteriaProxy.GetByCustomer(basket.CustomerIdentifier).Value.DirectActivation)
                yield return CheckoutTypeContract.DirectActivation;
            else if (basket.TotalPrice.ExclVat == 0)
                yield return CheckoutTypeContract.Free;
            else
            {
                yield return CheckoutTypeContract.CreditCardPayment;
                yield return CheckoutTypeContract.DebitCardPayment;
                yield return CheckoutTypeContract.BankTransfer;
            }
        }
    }
}