﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Core.Owin.Security.OAuth.WebApi;
using Core.Security;
using Intelligent.Shared.Core.Queries;
using Store.API.Contracts.Orders;
using Store.SqlQueries;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.OrderActivityPrefix)]
    public class OrderActivityController : ApiController
    {
        private readonly IQueryHandler _queryHandler;

        public OrderActivityController(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            _queryHandler = queryHandler;
        }

        /// <summary>
        /// Gets all activities for an order
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <response code="200">The order activties.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}")]
        [RequiresAnyClaimFromListAuthorization(IntelligentClaimTypes.EmployeeId)]
        public IEnumerable<OrderActivityContract> GetOrderActivities(string orderCode)
        {
            return _queryHandler.Execute(new GetOrderActivitiesQuery(orderCode))
                .Select(x => new OrderActivityContract { Message = x.Message, OccuredOn = x.OccuredOn });
        }
    }
}