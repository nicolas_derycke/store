﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ValueProviders;
using Core.Owin;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Owin;
using Store.API.Authorization;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.API.ValueProviders;
using Store.Commands.BasketItemAttributes;
using Store.Helpers;
using Store.Mapping;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.Providers;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.BasketsRoutePrefix)]
    [RequireAdminUserAuthorization]
    public class BasketItemAttributeController : ApiController
    {
        private readonly ICommandBus _commandBus;
        private readonly IBasketService _basketService;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IRequiredExtraFieldsProvider _requiredExtraFieldsProvider;
        private readonly IBasketPricingService _basketPricingService;

        public BasketItemAttributeController(
            ICommandBus commandBus,
            IBasketService basketService,
            ISqlCommandHandler sqlCommandHandler,
            IRequiredExtraFieldsProvider requiredExtraFieldsProvider,
            IBasketPricingService basketPricingService)
        {
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (requiredExtraFieldsProvider == null) throw new ArgumentNullException("requiredExtraFieldsProvider");
            if (basketPricingService == null) throw new ArgumentNullException("basketPricingService");

            _commandBus = commandBus;
            _basketService = basketService;
            _sqlCommandHandler = sqlCommandHandler;
            _requiredExtraFieldsProvider = requiredExtraFieldsProvider;
            _basketPricingService = basketPricingService;
        }

        /// <summary>
        /// Get an item attribute from a basket item
        /// </summary>
        /// <param name="orderCode"></param>
        /// <param name="basketItemId"></param>
        /// <param name="attribute"></param>
        /// <response code="200">The basket attribute.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}/items/{basketItemId:int}/attributes/{attribute:alpha}")]
        public async Task<object> GetBasketItemAttribute(string orderCode, int basketItemId, string attribute)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoReadAccess(basket);

            var attributeKey = AttributeKeyMapper.Map(attribute);

            var basketItem = basket.GetBasketItem(basketItemId);
            var basketItemContract = basketItem.MapTo<BasketItemContract>();

            var basketItemAttribute = basketItemContract.Attributes.GetOptionalBasketItemAttribute<IBasketItemAttribute>(attributeKey);

            if (!basketItemAttribute.HasValue)
                throw new AttributeNotFoundException(attributeKey);

            if (attributeKey == AttributeKey.Registrant)
            {
                var registrant = (RegistrantAttributes)basketItemAttribute.Value;

                ProvisionRequiredExtraFields(basketItem, registrant);
            }

            return basketItemAttribute.Value;
        }

        private void ProvisionRequiredExtraFields(BasketItem basketItem, RegistrantAttributes registrant)
        {
            var requiredExtraFields = _requiredExtraFieldsProvider.Get(basketItem.Attributes.Identifier, registrant);
            registrant.ExtraFields = ExtraFieldAttributes.Create(requiredExtraFields, registrant);
        }

        /// <summary>
        /// Add an attribute to a basket item
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="basketItemId">Basket item ID</param>
        /// <param name="attributes">Attribute value to add</param>
        /// <response code="200">Attribute is added</response>
        /// <response code="500">Attribute already exists on the basket item</response>
        /// <returns></returns>
        [HttpPost]
        [Route("{ordercode}/items/{basketItemId:int}/attributes")]
        public async Task<HttpResponseMessage> CreateBasketItemAttributes(string orderCode, int basketItemId, [FromBody]ItemAttributeDictionary attributes)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            var basketItem = basket.GetBasketItem(basketItemId);

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                foreach (var postedAttribute in attributes)
                {
                    if (basketItem.Attributes.ContainsKey(postedAttribute.Key))
                        throw new DuplicateBasketItemAttributeException(postedAttribute.Key.ToString());

                    _sqlCommandHandler.Execute(new CreateBasketItemAttribute(basketItemId, postedAttribute.Key, postedAttribute.Value, userId));
                }

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Update an attribute of a basket item
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="basketItemId">Basket item ID to update</param>
        /// <param name="attribute">Attribute key to update</param>
        /// <param name="attributeValue">Attribute value to update</param>
        /// <response code="200">Attribute is updated</response>
        /// <returns></returns>
        [HttpPut]
        [Route("{ordercode}/items/{basketItemId:int}/attributes/{attribute:alpha}")]
        public async Task<HttpResponseMessage> UpdateBasketItemAttribute(string orderCode, int basketItemId, string attribute, [ValueProvider(typeof(AttributeValueValueProviderFactory))]IBasketItemAttribute attributeValue)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);
            var attributeKey = AttributeKeyMapper.Map(attribute);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var updatedByUserId = OwinRequestScopeContext.Current.GetIntelliUserId();

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(
                    new UpdateBasketItemAttributeCommand(
                        basket,
                        basket.GetBasketItem(basketItemId),
                        attributeKey,
                        attributeValue,
                        updatedByUserId));

                _basketPricingService.ApplyBundles(basket, updatedByUserId);

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Get all attributes for a given attribute key on a basket.
        /// </summary>
        /// <param name="orderCode"></param>
        /// <param name="attribute"></param>
        /// <response code="200">The basket attribute.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}/items/attributes/{attribute:alpha}")]
        public async Task<IEnumerable<IBasketItemAttribute>> GetBasketAttributes(string orderCode, string attribute)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoReadAccess(basket);

            var attributeKey = AttributeKeyMapper.Map(attribute);

            return basket.Items.SelectMany(i => i.Attributes).Where(a => a.Key == attributeKey).Select(a => a.Value).Distinct();
        }

        /// <summary>
        /// Update an attribute on every basket item which has a matching attribute (key)
        /// </summary>
        /// <param name="orderCode"></param>
        /// <param name="attribute"></param>
        /// <param name="attributeValue"></param>
        /// <response code="200">Attribute is updated</response>
        /// <returns></returns>
        [HttpPut]
        [Route("{ordercode}/items/attributes/{attribute:alpha}")]
        public async Task<HttpResponseMessage> UpdateBasketAttribute(string orderCode, string attribute, [ValueProvider(typeof(AttributeValueValueProviderFactory))]IBasketItemAttribute attributeValue)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);
            var attributeKey = AttributeKeyMapper.Map(attribute);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var updatedByUserId = OwinRequestScopeContext.Current.GetIntelliUserId();

            var basketItemToUpdate = basket.Items.Where(i => i.Attributes.Any(a => a.Key == attributeKey));
            if (attributeKey == AttributeKey.Registrant)
                // bulk registrant updates are only desired for new registrations
                basketItemToUpdate = basketItemToUpdate.Where(i => !i.Attributes.ContainsKey(AttributeKey.TransferDomain));

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                foreach (var basketItem in basketItemToUpdate)
                    _commandBus.Send(
                        new UpdateBasketItemAttributeCommand(basket, basketItem, attributeKey, attributeValue, updatedByUserId));

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}