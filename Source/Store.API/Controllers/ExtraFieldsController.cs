﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Core.Owin;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Owin;
using Store.API.Authorization;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Commands.BasketItemAttributes;
using Store.Helpers;
using Store.Providers;
using Store.Services;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.BasketsRoutePrefix)]
    [RequireAdminUserAuthorization]
    public class ExtraFieldsController : ApiController
    {
        private readonly IBasketService _basketService;
        private readonly IRequiredExtraFieldsProvider _requiredExtraFieldsProvider;
        private readonly ICommandBus _commandBus;

        public ExtraFieldsController(IBasketService basketService, IRequiredExtraFieldsProvider requiredExtraFieldsProvider, ICommandBus commandBus)
        {
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (requiredExtraFieldsProvider == null) throw new ArgumentNullException("requiredExtraFieldsProvider");
            if (commandBus == null) throw new ArgumentNullException("commandBus");

            this._basketService = basketService;
            this._requiredExtraFieldsProvider = requiredExtraFieldsProvider;
            this._commandBus = commandBus;
        }

        /// <summary>
        /// Get all the required extra fields for a basket.
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <response code="200">Required extra fields</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}/registrants/extrafields")]
        public async Task<IEnumerable<ExtraFieldsContract>> GetRegistrantExtraFields(string orderCode)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoReadAccess(basket);

            var uniqueRegistrants =
                basket.Items.
                    Where(i => i.Attributes.ContainsKey(AttributeKey.Registrant)).
                    Select(i =>
                        new
                        {
                            BasketItemId = i.BasketItemId,
                            Registrant = i.Attributes.GetBasketItemAttribute<RegistrantAttributes>(AttributeKey.Registrant),
                            DomainName = i.Attributes.Identifier,
                            ExtraFields = _requiredExtraFieldsProvider.Get(
                                i.Attributes.Identifier,
                                i.Attributes.GetBasketItemAttribute<RegistrantAttributes>(AttributeKey.Registrant))
                        }).
                    GroupBy(ef => new { ef.Registrant.Company, ef.Registrant.FirstName, ef.Registrant.LastName });

            var extraFields = new List<ExtraFieldsContract>();

            foreach (var uniqueRegistrant in uniqueRegistrants)
                extraFields.Add(
                    new ExtraFieldsContract(
                        uniqueRegistrant.Select(r => r.BasketItemId),
                        uniqueRegistrant.Where(r => !string.IsNullOrWhiteSpace(r.DomainName) && r.ExtraFields.Any()).Select(r => r.DomainName),
                        ExtraFieldAttributes.Create(uniqueRegistrant.SelectMany(r => r.ExtraFields).Distinct(), uniqueRegistrant.First().Registrant),
                        new ExtraFieldsRegistrantContract(
                            uniqueRegistrant.Key.FirstName,
                            uniqueRegistrant.Key.LastName,
                            uniqueRegistrant.Key.Company)));

            return extraFields.Where(ef => ef.ExtraFields.RequiredFields.Any());
        }

        /// <summary>
        /// Update the extra fields for an entire basket.
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="extraFields">Extra fields to be updated</param>
        /// <response code="200">Extra fields are updated</response>
        /// <returns></returns>
        [HttpPut]
        [Route("{ordercode}/registrants/extrafields")]
        public async Task<HttpResponseMessage> UpdateRegistrantExtraFields(string orderCode, IEnumerable<ExtraFieldsContract> extraFields)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);
            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            using(var transaction = TransactionHelper.GetNewTransactionScope())
            { 
                _commandBus.Send(new UpdateRegistrantExtraFieldsCommand(basket, extraFields, userId));

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}