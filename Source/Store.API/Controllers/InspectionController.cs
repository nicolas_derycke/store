﻿using System;
using System.Linq;
using System.Web.Http;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Web.ErrorHandling.Exceptions;
using Store.API.Contracts.Orders;
using Store.Mapping;
using Store.Model.Exceptions;
using Store.SqlQueries;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.InspectionPrefix)]
    public class InspectionController : ApiController
    {
        private readonly IQueryHandler _queryHandler;

        public InspectionController(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            _queryHandler = queryHandler;
        }

        /// <summary>
        /// Gets all verifications for an order
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <response code="200">The order verifications.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}")]
        public OrderInspectionContract GetOrderVerification(string orderCode)
        {
            var orderState =
                _queryHandler.Execute(new GetOrderStateQuery(orderCode))
                    .WhenEmpty(() => { throw new ResourceNotFoundException("orderstate", orderCode); })
                    .Select(OrderStateDataToOrderStateContractMapper.Map)
                    .FirstOrDefault();

            if (orderState == null) throw new OrderNotFoundException("orderCode");

            var orderVerifications =
                _queryHandler.Execute(new GetOrderVerificationQuery(orderCode))
                .Select(x => new OrderVerificationContract { Message = x.Result, OccuredOn = x.CreationDate });

            return new OrderInspectionContract()
            {
                OrderState = orderState,
                OrderVerifications = orderVerifications
            };
        }
    }
}