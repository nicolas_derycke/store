﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Core.Queries;
using Store.API.Contracts.Orders;
using Store.Helpers;
using Store.Model.Exceptions;
using Store.SqlQueries;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.OrdersRoutePrefix)]
    public class OrderController : ApiController
    {
        private readonly IQueryHandler _queryHandler;

        public OrderController(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            _queryHandler = queryHandler;
        }

        /// <summary>
        /// Get an order by ordercode
        /// </summary>
        /// <param name="ordercode">OrderCode</param>
        /// <response code="200">The order.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}")]
        public async Task<OrderContract> GetOrder(string ordercode)
        {
            var order = await _queryHandler.ExecuteAsync(new GetOrderByOrderCodeQuery(ordercode));
            if (!order.HasValue)
                throw new OrderNotFoundException(ordercode);

            order.Value.NeedsPayment = NeedsPaymentHelper.CalculateNeedsPayment(order.Value.NeedsPayment, order.Value.CheckoutType, order.Value.CreationDate);

            return order.Value.MapTo<OrderContract>();
        }
    }
}