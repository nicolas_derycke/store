﻿using System;
using System.Collections.Generic;
using System.IdentityModel;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Core.Owin;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Owin;
using Intelligent.Shared.WebApi;
using Serilog;
using Store.API.Authorization;
using Store.API.Contracts.Baskets;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Helpers;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.Services;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.BasketsRoutePrefix)]
    [RequireAdminUserAuthorization]
    public class BasketItemController : ApiController
    {
        private readonly ICommandBus _commandBus;
        private readonly IBasketService _basketService;
        private readonly IBasketPricingService _basketPricingService;

        public BasketItemController(
            ICommandBus commandBus,
            IBasketService basketService,
            IBasketPricingService basketPricingService)
        {
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (basketPricingService == null) throw new ArgumentNullException("basketPricingService");

            _commandBus = commandBus;
            _basketService = basketService;
            _basketPricingService = basketPricingService;
        }

        /// <summary>
        /// Get a basket item by id
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="basketItemId">BasketItemId</param>
        /// <param name="periodUnitType">PeriodUnitType querystring. Default: OneYear</param>
        /// <response code="200">The basket item.</response>
        /// <response code="404">Basket not found or basketitem not found.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}/items/{basketItemId:int}")]
        public async Task<BasketItemContract> GetBasketItemById(string orderCode, int basketItemId, PeriodUnitType periodUnitType = PeriodUnitType.OneYear)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoReadAccess(basket);

            basket.SetPeriodUnitTypeForBasketItems(periodUnitType);

            var basketItem = basket.GetBasketItem(basketItemId);

            var basketItemContract = basketItem.MapTo<BasketItemContract>();

            return basketItemContract;
        }

        /// <summary>
        /// Clear the basket: remove all items
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <response code="200">Basket cleared</response>
        /// <response code="404">Basket not found</response>
        /// <returns></returns>
        [HttpDelete]
        [Route("{ordercode}/items")]
        public async Task<HttpResponseMessage> ClearBasketItems(string orderCode)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();
            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(new ClearBasketCommand(basket, userId));

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Add a basketitem to a basket.
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="basketItemContract">Basket item to create</param>
        /// <response code="200">Basket item created</response>
        /// <response code="404">Basket not found</response>
        /// <returns></returns>
        [HttpPost]
        [Route("{ordercode}/items")]
        public async Task<HttpResponseMessage> CreateBasketItem(string orderCode, [FromBody]BasketItemContract basketItemContract)
        {
            if (basketItemContract == null)
            {
                Log.Information(await Request.Content.ReadAsStringAsync());
                throw new BadRequestException("invalid basketitem format");
            }

            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();
            var customerIdentifier = OwinRequestScopeContext.Current.GetCustomerIdentifier();

            // limit number of items for anonymous baskets
            if (customerIdentifier == null && basket.ItemCount > 50)
                throw new BasketFullException(basket.ItemCount);

            var basketItem = new BasketItem(
                basketItemContract.ProductCode,
                basketItemContract.PromoProductCode,
                basketItemContract.Period,
                basketItemContract.Quantity,
                basketItemContract.Attributes,
                basket.VatRatePercentage,
                true);

            basket.Items.Add(basketItem);

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(new CreateBasketItemCommand(basket, basketItem, userId));
                _basketPricingService.ApplyBundles(basket, userId);
                _commandBus.Send(new EvaluateTaxPercentageCommand(basket));

                transaction.Complete();
            }

            return Request.CreateNewResourceResponse(basketItem.BasketItemId);
        }

        /// <summary>
        /// Add one or more basket items to a basket for a parent basket item.
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="basketItemId">Parent basket item id</param>
        /// <param name="basketItemContracts">Basket items to create</param>
        /// <response code="200">Basket item created</response>
        /// <response code="404">Basket not found</response>
        /// <returns></returns>
        [HttpPost]
        [Route("{ordercode}/items/{basketItemId:int}/childs")]
        public async Task<HttpResponseMessage> CreateChildBasketItems(string orderCode, int basketItemId, [FromBody]IEnumerable<BasketItemContract> basketItemContracts)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            var parentBasketItem = basket.GetBasketItem(basketItemId);

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                foreach (var basketItemContract in basketItemContracts)
                {
                    var childItem = new BasketItem(basketItemContract.ProductCode,
                                                    basketItemContract.PromoProductCode,
                                                    basketItemContract.Period,
                                                    basketItemContract.Quantity,
                                                    basketItemContract.Attributes,
                                                    basket.VatRatePercentage,
                                                    true,
                                                    parentBasketItem.BasketItemId);

                    basket.Items.Add(childItem);

                    var createBasketItemCommand = new CreateBasketItemCommand(basket, childItem, userId);
                    _commandBus.Send(createBasketItemCommand);
                }

                _basketPricingService.ApplyBundles(basket, userId);
                _commandBus.Send(new EvaluateTaxPercentageCommand(basket));

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        /// <summary>
        /// Remove a basket item from a basket
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="basketItemId">Basket item to remove</param>
        /// <response code="200">Basket item is removed from the basket</response>
        /// <response code="404">Basket is not found</response>
        /// <returns></returns>
        [HttpDelete]
        [Route("{ordercode}/items/{basketItemId:int}")]
        public async Task<HttpResponseMessage> RemoveBasketItem(string orderCode, int basketItemId)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var basketItem = basket.GetBasketItem(basketItemId);

            var deletionById = OwinRequestScopeContext.Current.GetIntelliUserId();

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(new RemoveBasketItemCommand(basket, basketItem, deletionById));
                _basketPricingService.ApplyBundles(basket, deletionById);
                _commandBus.Send(new EvaluateTaxPercentageCommand(basket));

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Update the period of a basket item
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="basketItemId">Basket item to update</param>
        /// <param name="newPeriod">New period (in months) to apply</param>
        /// <response code="200">Basket item period updated</response>
        /// <response code="404">Basket item not found in the basket</response>
        /// <returns></returns>
        [HttpPut]
        [Route("{ordercode}/items/{basketItemId:int}/period")]
        public async Task<HttpResponseMessage> ChangePeriodOnBasketItem(string orderCode, int basketItemId, [FromBody]int newPeriod)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var providerId = OwinRequestScopeContext.Current.GetProvider().Value;

            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            basket.GetBasketItem(basketItemId);

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(new ChangeBasketItemPeriodCommand(basket, basketItemId, newPeriod, providerId, userId));

                _basketPricingService.ApplyBundles(basket, userId);
                _commandBus.Send(new EvaluateTaxPercentageCommand(basket));

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Update the quantity of a basket item
        /// </summary>
        /// <param name="orderCode"></param>
        /// <param name="basketItemId"></param>
        /// <param name="newQuantity"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{ordercode}/items/{basketItemId:int}/quantity")]
        public async Task<HttpResponseMessage> ChangeQuantityOnBasketItem(string orderCode, int basketItemId, [FromBody]int newQuantity)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            var basketItem = basket.GetBasketItem(basketItemId);
            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(new ChangeBasketItemQuantityCommand(basket, basketItem, newQuantity, userId));

                _basketPricingService.ApplyBundles(basket, userId);
                _commandBus.Send(new EvaluateTaxPercentageCommand(basket));

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}