﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Core.Owin;
using Intelligent.Core.Models;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Collections;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Owin;
using Intelligent.Shared.WebApi;
using MassTransit;
using Store.API.Authorization;
using Store.API.Contracts.Baskets;
using Store.Commands.Baskets;
using Store.Helpers;
using Store.Messages.Commands;
using Store.Model;
using Store.Model.Exceptions;
using Store.Services;
using Store.SqlCommands.BasketItems;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;
using Intelligent.Shared.Threading;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.BasketsRoutePrefix)]
    [RequireAdminUserAuthorization]
    public class BasketController : ApiController
    {
        private readonly ICommandBus _commandBus;
        private readonly IBasketService _basketService;
        private readonly IBus _bus;
        private readonly IQueryHandler _queryHandler;
        private readonly IBasketPricingService _basketPricingService;
        private readonly IBasketItemPricingService _basketItemPricingService;

        public BasketController(
            ICommandBus commandBus,
            IBasketService basketService,
            IBus bus,
            IQueryHandler queryHandler,
            IBasketPricingService basketPricingService,
            IBasketItemPricingService basketItemPricingService)
        {
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (bus == null) throw new ArgumentNullException("bus");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (basketPricingService == null) throw new ArgumentNullException("basketPricingService");
            if (basketItemPricingService == null) throw new ArgumentNullException("basketItemPricingService");

            _commandBus = commandBus;
            _basketService = basketService;
            _bus = bus;
            _queryHandler = queryHandler;
            _basketPricingService = basketPricingService;
            _basketItemPricingService = basketItemPricingService;
        }

        /// <summary>
        /// Get all open baskets
        /// </summary>        
        /// <response code="200">The baskets.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public async Task<IEnumerable<BasketReferenceContract>> GetBaskets(BasketViewType view = BasketViewType.Summary)
        {
            var customer = OwinRequestScopeContext.Current.GetCustomerIdentifier();
            if (customer == null)
                return Enumerable.Empty<BasketReferenceContract>();

            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            if (view != BasketViewType.Summary)
                throw new NotSupportedException(string.Format("Unsupported basket view type: '{0}'", view));

            var basketReferences = _queryHandler.Execute(new GetBasketsByCustomerQuery(customer)).Select(b => b.MapTo<BasketReferenceContract>());

            if (basketReferences.Count() > 1)
            {
                var mostRecentBasket = await _basketService.GetBasketByOrderCode(basketReferences.OrderByDescending(b => b.CreationDate).First().OrderCode);
                _commandBus.Send(new MergeBasketsCommand(mostRecentBasket, userId));

                await _bus.Publish(new ValidateBasket(mostRecentBasket.OrderCode, false));

                basketReferences = _queryHandler.Execute(new GetBasketsByCustomerQuery(customer)).Select(b => b.MapTo<BasketReferenceContract>());
            }

            return basketReferences;
        }

        /// <summary>
        /// Get a basket 
        /// </summary>
        /// <param name="orderCode">OrderCode</param>
        /// <param name="view">View querystring</param>
        /// <param name="periodUnitType">The unittype of the period in months</param>
        /// <response code="200">The basket.</response>
        /// <returns></returns>
        [HttpGet]
        [Route("{ordercode}")]
        public async Task<BasketSummaryContract> GetBasket(string orderCode, BasketViewType view = BasketViewType.Summary, PeriodUnitType periodUnitType = PeriodUnitType.OneYear)
        {
            var basket = AsyncPump.Await(async () => await _basketService.GetBasketByOrderCode(orderCode));

            AuthorizationHelper.ThrowIfNoReadAccess(basket);

            var customerIdentifier = OwinRequestScopeContext.Current.GetCustomerIdentifier();
            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();
            var language = OwinRequestScopeContext.Current.GetLanguage();

            if (customerIdentifier != null && basket.CustomerIdentifier != customerIdentifier)
            {
                using (var transaction = TransactionHelper.GetNewTransactionScope())
                {
                    _commandBus.Send(new ChangeBasketCustomerCommand(basket, customerIdentifier, userId));
                    _basketPricingService.ApplyBundles(basket, userId);

                    transaction.Complete();
                }
                basket = await _basketService.GetBasketByOrderCode(orderCode);
            }

            if (basket.Language != language && !basket.CheckedOut)
            {
                using (var transaction = TransactionHelper.GetNewTransactionScope())
                {
                    // get applied promocodes to re-apply them after the re-calculation
                    var appliedPromoCodes = basket.Items.Where(i => i.IsPromo).Select(i => i.PromoCode).ToList();

                    basket.Language = language;
                    new UpdateBasketLanguage(basket.BasketId, basket.Language, userId).Execute();
                    foreach (var basketItem in basket.Items)
                    {
                        // recalc (and trigger translation in the process)
                        _basketItemPricingService.SetProductInfoAndPrice(basketItem, basket.CustomerIdentifier, basket.Provider, basket.Language, basket.VatRatePercentage);
                        new UpdateBasketItem(basketItem, userId).Execute();
                    }

                    _basketPricingService.ApplyBundles(basket, userId);

                    // re-apply promocodes to basket
                    foreach (var promoCode in appliedPromoCodes)
                        _commandBus.Send(new ApplyPromoCodeToBasketCommand(promoCode, basket, userId));

                    transaction.Complete();
                }
            }

            basket.SetPeriodUnitTypeForBasketItems(periodUnitType);

            var revalidationOffsetInDays = int.Parse(ConfigurationManager.AppSettings["RevalidationOffsetInDays"]);
            if (basket.IsRevalidationRequired(revalidationOffsetInDays))
                await _bus.Publish(new ValidateBasket(basket.OrderCode, false));

            // WARNING: very dirty fix for sales to be able to order a transfer for a domain name that the customer already has
            // so they can update the licensee info
            new DirtyFixForEmployeeHelper(User).RemoveUacAccountAlreadyExistsErrorForDomainNameProducts(basket);            

            switch (view)
            {
                case BasketViewType.Summary:
                    return basket.MapTo<BasketSummaryContract>();
                case BasketViewType.Detailed:
                    return basket.MapTo<BasketContract>();
                default:
                    throw new NotSupportedException(string.Format("Unsupported basket view type: '{0}'", view));
            }
        }

        /// <summary>
        /// Logicaly deletes the last validationdate so it triggers a new validation.
        /// </summary>
        /// <param name="orderCode">OrderCode</param>        
        /// <returns></returns>
        [HttpDelete]
        [Route("{ordercode}/lastvalidationdate")]
        public async Task<IHttpActionResult> ValidateBasket(string orderCode)
        {
            var basket = await _basketService.GetBasketByOrderCode(orderCode);

            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            if (!basket.IsRecalculating)
                await _bus.Publish(new ValidateBasket(basket.OrderCode, true));

            return Ok();
        }

        /// <summary>
        /// Create a basket and create a basket item.
        /// </summary>
        /// <param name="postedBasket">Basket to create</param>
        /// <response code="200">Customer created</response>
        /// <response code="303">A basket already exists</response>
        /// <response code="400"></response>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public HttpResponseMessage CreateBasket([FromBody]PostedBasketContract postedBasket)
        {
            var providerId = OwinRequestScopeContext.Current.GetProvider();
            if (!providerId.HasValue)
                throw new ProviderNotFoundOnOwinContextException();
            var provider = new Provider(providerId.Value);

            var customerIdentifier = OwinRequestScopeContext.Current.GetCustomerIdentifier();
            var language = OwinRequestScopeContext.Current.GetLanguage();
            var creationById = OwinRequestScopeContext.Current.GetIntelliUserId();
            var clientId = OwinRequestScopeContext.Current.GetClientId();

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                BasketType basketType = BasketType.Regular;
                if (postedBasket != null && postedBasket.BastketType.HasValue)
                    basketType = postedBasket.BastketType.Value;

                var createBasketCommand =
                    new CreateBasketCommand(
                        basketType,
                        customerIdentifier,
                        provider,
                        creationById,
                        language,
                        postedBasket == null ? Enumerable.Empty<BasketItemContract>() : postedBasket.Items.ToSafeEnumerable(),
                        clientId);
                _commandBus.Send(createBasketCommand);

                transaction.Complete();

                return Request.CreateNewResourceResponse(createBasketCommand.Result.OrderCode);
            }
        }

        [HttpPut]
        [Route("{orderCode}/customerreference")]
        public HttpResponseMessage UpdatePoReferenceOnBasket(string orderCode, [FromBody] string customerReference)
        {
            var basket = AsyncPump.Await(async () => await _basketService.GetBasketByOrderCode(orderCode));
            AuthorizationHelper.ThrowIfNoReadAccess(basket);
            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(new ChangeBasketCustomerReferenceCommand(basket, customerReference, userId));
                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Change customer on a basket
        /// </summary>
        /// <param name="orderCode"></param>
        /// <param name="customerNumber"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{orderCode}/customer")]
        public HttpResponseMessage UpdateCustomerOnBasket(string orderCode, [FromBody] int customerNumber)
        {
            var basket = AsyncPump.Await(async () => await _basketService.GetBasketByOrderCode(orderCode));

            AuthorizationHelper.ThrowIfNoReadAccess(basket);

            var customerIdentifier = CustomerIdentifier.FromCustomerNumber(customerNumber);
            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _commandBus.Send(new ChangeBasketCustomerCommand(basket, customerIdentifier, userId, true));
                _basketPricingService.ApplyBundles(basket, userId);

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Apply promocode to all items in the basket.
        /// </summary>        
        /// <response code="200">Promo code applied.</response>
        /// <returns>TotalCount as header</returns>
        [HttpPut]
        [Route("{ordercode}/promocode")]
        public async Task<HttpResponseMessage> ApplyPromoCode(string orderCode, [FromBody]string promoCode)
        {
            var customer = OwinRequestScopeContext.Current.GetCustomerIdentifier();
            if (customer == null)
                throw new BasketContainsNoCustomerException(orderCode);

            var providerid = OwinRequestScopeContext.Current.GetProvider();
            if (!providerid.HasValue)
                throw new ProviderNotFoundOnOwinContextException();

            var userId = OwinRequestScopeContext.Current.GetIntelliUserId();

            var basket = await _basketService.GetBasketByOrderCode(orderCode);
            AuthorizationHelper.ThrowIfNoWriteAccess(basket);

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                var applyCommand = new ApplyPromoCodeToBasketCommand(promoCode, basket, userId);
                _commandBus.Send(applyCommand);

                _commandBus.Send(new EvaluateTaxPercentageCommand(basket));

                var httpResult = new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
                httpResult.Headers.Add("TotalCount", applyCommand.Result.AffectedItems.ToString());

                transaction.Complete();

                return httpResult;
            }
        }
    }
}
