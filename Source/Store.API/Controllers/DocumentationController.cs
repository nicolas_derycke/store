using System;
using System.Web.Http;
using Store.API.Contracts.Baskets;

namespace Store.API.Controllers
{
    /// <summary>
    /// Controller dedicated to provide documentation info
    /// </summary>
    [RoutePrefix(Routes.DocumentationPrefix)]
    public class DocumentationController : ApiController
    {
        /// <summary>
        /// Gets the possible warnings on a basket item.
        /// </summary>
        /// <returns>An array of possible warnings on a basket item.</returns>
        [HttpGet]
        [Route("basketitem/warnings")]
        public Array GetWarnings()
        {
            return Enum.GetValues(typeof(BasketItemWarning));
        }

        /// <summary>
        /// Gets the possible validation errors on a basket item.
        /// </summary>
        /// <returns>An array of possible validation errors on a basket item.</returns>
        [HttpGet]
        [Route("basketitem/validationerrors")]
        public Array GetValidationErrors()
        {
            return Enum.GetValues(typeof(BasketItemValidationError));
        }
    }
}