﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Core.Owin;
using Intelligent.Core.Models;
using Intelligent.Shared.AutoMapper;
using Intelligent.Shared.Core.Collections;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Owin;
using Intelligent.Shared.WebApi.Paging;
using Store.API.Contracts.Orders;
using Store.Data;
using Store.Helpers;
using Store.SqlQueries;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.OrdersRoutePrefix)]
    public class SearchOrderController : ApiController
    {
        private readonly IQueryHandler _queryHandler;

        public SearchOrderController(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._queryHandler = queryHandler;
        }

        [HttpGet]
        [Route("")]
        public HttpResponseMessage SearchOrders(
            [FromUri] PagingParameters paging,
            [FromUri] IEnumerable<int> providerIds = null,
            [FromUri] int? customerNumber = null,
            [FromUri] string identifier = null,
            [FromUri] DateTime? creationDateFrom = null,
            [FromUri] DateTime? creationDateUntil = null,
            [FromUri] string status = null,
            [FromUri] string orderCode = null)
        {
            IEnumerable<OrderSummaryData> orderReferences = new List<OrderSummaryData>();

            if (paging == null)
                paging = new PagingParameters() { Skip = 0, Take = 50 };

            var customer = OwinRequestScopeContext.Current.GetCustomerIdentifier();
            if (customer != null)
                customerNumber = customer.CustomerNumber;

            var totalCount = 0;
            if (string.IsNullOrEmpty(identifier) && !string.IsNullOrEmpty(orderCode))
            {
                orderReferences = SearchOrdersByOrderCode(
                       creationDateFrom,
                       creationDateUntil,
                       providerIds,
                       customerNumber,
                       orderCode,
                       status);



                if (orderReferences.Any())
                {
                    totalCount = orderReferences.Count();
                    if (paging.Skip.HasValue && paging.Take.HasValue)
                    {
                        orderReferences = orderReferences.Skip(paging.Skip.Value)
                                                        .Take(paging.Take.Value);
                    }
                }

            }
            else if (!string.IsNullOrEmpty(identifier))
            {
                orderReferences = SearchOrdersByIdentifierOrOrderCode(
                    identifier,
                    creationDateFrom,
                    creationDateUntil,
                    providerIds,
                    customerNumber,
                    orderCode,
                    status);



                if (orderReferences.Any())
                {
                    totalCount = orderReferences.Count();
                    if (paging.Skip.HasValue && paging.Take.HasValue)
                    {
                        orderReferences = orderReferences.Skip(paging.Skip.Value)
                                                        .Take(paging.Take.Value);
                    }
                }
            }
            else if (customerNumber.HasValue)
            {
                orderReferences = SearchOrdersByCustomerNumber(
                    customerNumber,
                    creationDateFrom,
                    creationDateUntil,
                    providerIds, status);

                if (orderReferences.Any())
                {
                    totalCount = orderReferences.Count();
                    if (paging.Skip.HasValue && paging.Take.HasValue)
                    {
                        orderReferences = orderReferences.Skip(paging.Skip.Value)
                                                        .Take(paging.Take.Value);
                    }
                }
            }
            else if (providerIds != null && (providerIds.Any() || !string.IsNullOrEmpty(status)))
            {
                orderReferences = _queryHandler.Execute(
                    new SearchOrdersByProviderAndStatusQuery(
                        paging.Skip.Value,
                        paging.Take.Value,
                        providerIds,
                        status,
                        creationDateFrom,
                        creationDateUntil
                        )
                    );
                if (orderReferences.Any())
                    totalCount = orderReferences.First().TotalResults;
            }

            foreach(var orderReference in orderReferences)
                orderReference.NeedsPayment = NeedsPaymentHelper.CalculateNeedsPayment(orderReference.NeedsPayment, orderReference.CheckoutType, orderReference.CreationDate);

            return Request.CreatePagedHttpResponseMessage(
                new Subset<OrderSummaryContract>(
                    orderReferences.Select(or => or.MapTo<OrderSummaryContract>()),
                    totalCount
                )
            );
        }


        private IEnumerable<OrderSummaryData> SearchOrdersByIdentifierOrOrderCode(
            string identifier,
            DateTime? creationDateFrom,
            DateTime? creationDateUntil,
            IEnumerable<int> providerIds,
            int? customerNumber,
            string orderCode,
            string status)
        {
            var orderReferences = _queryHandler.Execute(
                    new SearchOrdersByIdentifierQuery(
                        identifier,
                        creationDateFrom,
                        creationDateUntil
                        )
                    );

            if (providerIds.Any())
                orderReferences = orderReferences.Where(o => providerIds.Contains(o.ProviderId));

            if (customerNumber.HasValue)
                orderReferences =
                    orderReferences.Where(
                        o => o.CustomerId == CustomerIdentifier.FromCustomerNumber(customerNumber.Value).CustomerId);

            if (!string.IsNullOrEmpty(orderCode))
                orderReferences = orderReferences.Where(o => o.OrderCode == orderCode);

            if (!string.IsNullOrEmpty(status))
                orderReferences = orderReferences.Where(o => o.Status == status);

            return orderReferences;
        }


        private IEnumerable<OrderSummaryData> SearchOrdersByOrderCode(
            DateTime? creationDateFrom,
            DateTime? creationDateUntil,
            IEnumerable<int> providerIds,
            int? customerNumber,
            string orderCode,
            string status)
        {
            var orderReferences = _queryHandler.Execute(
                    new SearchOrdersByOrderCodeQuery(
                        orderCode
                        )
                    );

            if (providerIds.Any())
                orderReferences = orderReferences.Where(o => providerIds.Contains(o.ProviderId));

            if (customerNumber.HasValue)
                orderReferences =
                    orderReferences.Where(
                        o => o.CustomerId == CustomerIdentifier.FromCustomerNumber(customerNumber.Value).CustomerId);

            if (!string.IsNullOrEmpty(status))
                orderReferences = orderReferences.Where(o => o.Status == status);

            if (creationDateFrom.HasValue && creationDateUntil.HasValue)
                orderReferences =
                    orderReferences.Where(
                        o => o.CreationDate > creationDateFrom.Value && o.CreationDate < creationDateUntil.Value);

            return orderReferences;
        }

        private IEnumerable<OrderSummaryData> SearchOrdersByCustomerNumber(
            int? customerNumber,
            DateTime? creationDateFrom,
            DateTime? creationDateUntil,
            IEnumerable<int> providerIds,
            string status)
        {
            var orderReferences = _queryHandler.Execute(
                    new SearchOrdersByCustomerIdQuery(
                       customerNumber.Value,
                       creationDateFrom,
                       creationDateUntil
                    )
                );

            if (providerIds.Any())
                orderReferences = orderReferences.Where(o => providerIds.Contains(o.ProviderId));

            if (!string.IsNullOrEmpty(status))
                orderReferences = orderReferences.Where(o => o.Status == status);

            return orderReferences;
        }
    }
}
