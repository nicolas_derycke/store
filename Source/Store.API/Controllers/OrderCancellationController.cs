﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Intelligent.Core.Users;
using Intelligent.Shared.Core.Queries;
using MassTransit;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.Data;
using Store.Helpers;
using Store.Messages.Events;
using Store.Model.Exceptions;
using Store.SqlCommands;
using Store.SqlCommands.Orders;
using Store.SqlQueries;

namespace Store.API.Controllers
{
    [RoutePrefix(Routes.OrdercancellationPrefix)]
    public class OrderCancellationController : ApiController
    {
        private readonly IQueryHandler _queryHandler;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IBus _bus;

        public OrderCancellationController(IQueryHandler queryHandler, ISqlCommandHandler sqlCommandHandler, IBus bus)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");

            _queryHandler = queryHandler;
            _sqlCommandHandler = sqlCommandHandler;
            _bus = bus;
        }

        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> CancelOrder([FromBody] OrderCancellation orderCancellation)
        {
            var result = await _queryHandler.ExecuteAsync(new GetOrderByOrderCodeQuery(orderCancellation.OrderCode));
            if (!result.HasValue)
                throw new OrderNotFoundException(orderCancellation.OrderCode);

            var order = result.Value;

            if (order.Status != OrderStatus.Created)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            using (var transaction = TransactionHelper.GetNewTransactionScope())
            {
                _sqlCommandHandler.Execute(new UpdateOrderStatus(order.OrderId, OrderStatus.Cancelled, orderCancellation.CancelledById));
                _sqlCommandHandler.Execute(new UpdateOrderPaymentData(order.OrderId,
                   new OrderPaymentData(order.EpayCode, false), SystemUser.Id));

                await _bus.Publish(new OrderCancelled(orderCancellation.OrderCode, orderCancellation.Reason, DateTime.Now, orderCancellation.CancelledById, orderCancellation.CancelledBy));

                transaction.Complete();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
} 