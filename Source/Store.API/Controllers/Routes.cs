﻿namespace Store.API.Controllers
{
    public class Routes
    {
        public const string BasketsRoutePrefix = "baskets";
        public const string OrdersRoutePrefix = "orders";
        public const string OrderStatesPrefix = "orderstates";
        public const string OrderActivityPrefix = "orderactivities";
        public const string OrdercancellationPrefix = "ordercancellations";

        public const string DocumentationPrefix = "documentation";

        public const string InspectionPrefix = "orderinspections";
    }
}