﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Activator.Messages;
using Activator.Messages.Commands;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Web.ErrorHandling.Exceptions;
using MassTransit;
using Store.API.Contracts;
using Store.API.Contracts.Orders;
using Store.Mapping;
using Store.Model.Exceptions;
using Store.SqlQueries;

namespace Store.API.Controllers
{
    public class ActivationController : ApiController
    {
        private readonly IBus _bus;
        private readonly IQueryHandler _queryHandler;

        public ActivationController(IBus bus, IQueryHandler queryHandler)
        {
            if (bus == null) throw new ArgumentNullException("bus");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._bus = bus;
            this._queryHandler = queryHandler;
        }

        /// <summary>
        /// Activate an order
        /// </summary>
        /// <param name="orderActivation">Data to activate an order (order code and reason of activation)</param>
        [HttpPost]
        [Route("orderactivations")]
        public async Task<HttpResponseMessage> ActivateOrder(OrderActivation orderActivation)
        {
            var orderCode = orderActivation.OrderCode;

            var order = await _queryHandler.ExecuteAsync(new GetOrderByOrderCodeQuery(orderCode));
            if (!order.HasValue)
                throw new OrderNotFoundException(orderCode);

            if (order.Value.Status == OrderStatus.Cancelled)
                throw new InvalidOrderStatusException(OrderStatus.Cancelled);
            
            var orderState = _queryHandler.Execute(new GetOrderStateQuery(orderCode))
                .WhenEmpty(() => { throw new ResourceNotFoundException("orderstate", orderCode); })
                .Select(OrderStateDataToOrderStateContractMapper.Map)
                .First();

            if (orderState.ActivationState.Status == OrderActivationStatusContract.Activated)
                throw new OrderAlreadyActivatedException(orderCode);

            await _bus.Publish(new ActivateOrder(orderCode, ActivationMethod.Manual, orderActivation.ActivationReason, orderActivation.ActivationById, orderActivation.ActivationBy));

            return Request.CreateResponse(HttpStatusCode.Accepted);
        }
    }
}