﻿using System.Security.Claims;
using System.Security.Principal;
using Core.Security;

namespace Store.API
{
    internal static class EmployeeHelper
    {
        internal static bool IsEmployee(IPrincipal user)
        {
            return ((ClaimsIdentity)user.Identity).HasClaim(c => c.Type == IntelligentClaimTypes.EmployeeId);
        }
    }
}