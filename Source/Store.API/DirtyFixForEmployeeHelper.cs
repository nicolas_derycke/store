using System;
using System.Linq;
using System.Security.Principal;
using Store.API.Contracts.Baskets;
using Store.Model;
using Store.Model.Baskets;

namespace Store.API
{
    // Remove this class if http://jira.intelligent.local:8080/browse/EXP-759 is done
    internal class DirtyFixForEmployeeHelper
    {
        private IPrincipal User { get; set; }

        public DirtyFixForEmployeeHelper(IPrincipal user)
        {
            if (user == null) throw new ArgumentNullException("user");
            User = user;
        }

        internal void RemoveUacAccountAlreadyExistsErrorForDomainNameProducts(Basket basket)
        {
            // WARNING: very dirty fix for sales to be able to order a transfer for a domain name that the customer already has
            // so they can update the licensee info
            if (EmployeeHelper.IsEmployee(User))
            {
                // basketItem.ProductType == ProductTypes.DomainName && !basketItem.ParentId.HasValue
                var itemsToDirtyFix = basket.Items
                    .Where(i => i.ProductType == ProductTypes.DomainName)
                    .Where(i => !i.ParentId.HasValue)
                    .Where(i => i.ValidationError == BasketItemValidationError.UacAccountAlreadyExists);
                foreach (var item in itemsToDirtyFix)
                    item.SetBasketItemValidationErrors(BasketItemValidationError.None);
            }
        }
    }
}