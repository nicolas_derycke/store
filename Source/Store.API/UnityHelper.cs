﻿using System;
using Core.Owin.Security.OAuth;
using Intelligent.Shared.Messaging.CommandBus;
using Microsoft.Practices.Unity;
using Serilog;
using Store.Helpers;
using Store.Providers;
using Store.Providers.Attributes;
using Store.Proxies;
using Store.Repositories;
using Store.Services;
using Store.Validation;

namespace Store.API
{
    class UnityHelper
    {
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<ILogger>(new ContainerControlledLifetimeManager(), new InjectionFactory(x => Log.Logger));
            container.RegisterType<IEmployeeRightsProvider, EmployeeRightsProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<IInvoiceCorrectionService, InvoiceCorrectionService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IOrderCodeProvider, SequenceBasedOrderCodeProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<IRegistrantTemplateProxy, RegistrantTemplateProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<INameserverTemplateProxy, NameserverTemplateProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<IAppSettingsConfiguration, AppSettingsConfiguration>(new ContainerControlledLifetimeManager());
            container.RegisterType<IInvoiceCorrectionProxy, InvoiceCorrectionProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<IPromoCodeInfoProvider, PromoCodeInfoProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBundleService, BundleService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IHttpClientFactory, ApiHttpClientFactory>(new PerResolveLifetimeManager());
            container.RegisterType<IRrpProxy, RrpProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<IIntelliPropertiesProvider, IntelliPropertiesProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICommandBus, CommandBus>(new PerThreadLifetimeManager());

            container.RegisterTypesWhichImplement<IAttributeProvider>();
            container.RegisterTypesWhichImplement<ILiveBasketItemValidator>();

            Store.UnityHelper.ConfigureContainer(container);

            return container;
        });

        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
    }
}