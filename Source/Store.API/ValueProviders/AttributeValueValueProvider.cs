﻿using System;
using System.Globalization;
using System.Web.Http.Controllers;
using System.Web.Http.ValueProviders;
using Store.API.Contracts;
using Store.API.Contracts.Converters;
using Store.Mapping;

namespace Store.API.ValueProviders
{
    public class AttributeValueValueProvider : IValueProvider
    {
        private const string AttributeKey = "attribute";

        private readonly IBasketItemAttribute _attribute;
        private readonly string _body;

        public AttributeValueValueProvider(HttpActionContext actionContext)
        {
            if (actionContext == null)
                throw new ArgumentNullException("actionContext");

            if (!actionContext.ActionArguments.ContainsKey(AttributeKey))
                throw new Exception(string.Format("Action argument with key '{0}' was not found!", AttributeKey));

            var attributeKey = AttributeKeyMapper.Map(actionContext.ActionArguments[AttributeKey].ToString());

            _body = actionContext.Request.Content.ReadAsStringAsync().Result;
            _attribute = AttributeValueFactory.Create(attributeKey, _body);
        }

        public bool ContainsPrefix(string prefix)
        {
            return _body.Contains("\"" + prefix + "\"");
        }

        public ValueProviderResult GetValue(string key)
        {
            return new ValueProviderResult(_attribute, _body, CultureInfo.InvariantCulture);
        }
    }
}