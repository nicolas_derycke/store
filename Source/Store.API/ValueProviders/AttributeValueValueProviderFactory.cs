﻿using System.Web.Http.Controllers;
using System.Web.Http.ValueProviders;

namespace Store.API.ValueProviders
{
    public class AttributeValueValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(HttpActionContext actionContext)
        {
            return new AttributeValueValueProvider(actionContext);
        }
    }
}