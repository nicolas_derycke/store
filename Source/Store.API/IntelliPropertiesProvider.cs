using System;
using System.Collections.Generic;
using Core.Owin;
using Intelligent.Shared.Owin;

namespace Store.API
{
    public class IntelliPropertiesProvider : IIntelliPropertiesProvider
    {
        public Func<IEnumerable<KeyValuePair<string, string>>> IntelliPropertiesAccessor
        {
            get
            {
                return () => OwinRequestScopeContext.Current.GetIntelliProperties();
            }
        }
    }
}