﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Core.Owin.Security.OAuth;

namespace Store.API.Authorization
{
    public class RequireAdminUserAuthorizationAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var claimsIdentity = actionContext.RequestContext.Principal.Identity as ClaimsIdentity;
            var containsUserClaim = claimsIdentity.HasClaim(c => c.Type == ClaimTypes.Role && c.Value == UserIdentity.UserRole);

            if (containsUserClaim)
            {
                var isAdmin = claimsIdentity.HasClaim(c => c.Type == ClaimTypes.Role && c.Value == UserIdentity.UserAdminRole);

                if (!isAdmin)
                    throw new Intelligent.Shared.Web.ErrorHandling.Exceptions.UnauthorizedException(
                        "User is not allowed to perform this action.");
            }
            base.OnAuthorization(actionContext);
        }
    }
}