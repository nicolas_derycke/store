﻿using Core.Owin;
using Intelligent.Shared.Owin;
using Store.Model.Baskets;
using Store.Model.Exceptions;

namespace Store.API.Authorization
{
    public class AuthorizationHelper
    {
        private static void ThrowIfWrongCustomerNumberForOwinContext(Basket basket)
        {
            var customerIdentifier = OwinRequestScopeContext.Current.GetCustomerIdentifier();

            if (basket.IsAnonymousBasket)
                return;

            if (customerIdentifier != basket.CustomerIdentifier)
                throw new UnauthorizedCustomerException();
        }

        private static void ThrowIfWrongProviderForOwinContext(Basket basket)
        {
            var provider = OwinRequestScopeContext.Current.GetProvider();
            if (!provider.HasValue)
                throw new ProviderNotFoundOnOwinContextException();

            if (provider.Value != basket.Provider.Id)
                throw new UnauthorizedProviderException();
        }

        public static void ThrowIfNoReadAccess(Basket basket)
        {
            ThrowIfWrongProviderForOwinContext(basket);
            ThrowIfWrongCustomerNumberForOwinContext(basket);
        }

        public static void ThrowIfNoWriteAccess(Basket basket)
        {
            ThrowIfNoReadAccess(basket);
            if (basket.CheckedOut)
                throw new BasketAlreadyCheckedOutException(basket.OrderCode);
            if (basket.IsRecalculating)
                throw new BasketIsRecalculatingException(basket.OrderCode);
            
        }
    }
}