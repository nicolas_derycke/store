using Intelligent.Shared.Net.Http;

namespace Store
{
    public interface IHttpClientFactory
    {
        StronglyTypedHttpClient BuildStoreClient();
    }
}