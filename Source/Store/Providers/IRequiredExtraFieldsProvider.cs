using System.Collections.Generic;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.Providers
{
    public interface IRequiredExtraFieldsProvider
    {
        IEnumerable<string> Get(string domainName, RegistrantAttributes registrant);
    }
}