using Store.Model;

namespace Store.Providers
{
    public interface IPromoCodeInfoProvider
    {
        PromoCodeInfo GetPromoCodeInfo(string promoCode, int providerId);
    }
}