﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.Threading;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Mapping;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;

namespace Store.Providers.Attributes
{
    public class RegistrantAttributeProvider : IAttributeProvider
    {
        private readonly IRegistrantTemplateProxy _registrantTemplateProxy;

        public RegistrantAttributeProvider(
            IRegistrantTemplateProxy registrantTemplateProxy)
        {
            if (registrantTemplateProxy == null) throw new ArgumentNullException("registrantTemplateProxy");

            _registrantTemplateProxy = registrantTemplateProxy;
        }

        public Optional<ItemAttributeKeyValuePair> Get(Basket basket, BasketItem basketItem)
        {
            if (!basket.IsAnonymousBasket &&
                basketItem.ProductType == ProductTypes.DomainName)
            {
                if (!basketItem.Attributes.ContainsKey(AttributeKey.Registrant))
                {
                    var attribute = AsyncPump.Await(async () => await GetRegistrantTemplate(basket.CustomerIdentifier));

                    if (!attribute.HasValue)
                        attribute = RegistrantAttributes.Empty;

                    return new ItemAttributeKeyValuePair(AttributeKey.Registrant, attribute.Value);
                }
            }
            return Optional<ItemAttributeKeyValuePair>.Empty;
        }

        private async Task<Optional<IBasketItemAttribute>> GetRegistrantTemplate(CustomerIdentifier customerIdentifier)
        {
            var registrantTemplate = await _registrantTemplateProxy.GetDefault(customerIdentifier);

            if (!registrantTemplate.Any())
                return Optional<IBasketItemAttribute>.Empty;

            return ContactTemplateContractToRegistrantAttributesMapper.Map(registrantTemplate.First());
        }
    }
}