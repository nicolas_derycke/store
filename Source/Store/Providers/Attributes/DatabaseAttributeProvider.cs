﻿using Intelligent.Shared.Core;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Providers.Attributes
{
    public class DatabaseAttributeProvider : IAttributeProvider
    {
        public Optional<ItemAttributeKeyValuePair> Get(Basket basket, BasketItem basketItem)
        {
            if ((basketItem.ProductType == ProductTypes.MsSqlDb || basketItem.ProductType == ProductTypes.MysqlDb))
            {
                if (!basketItem.Attributes.ContainsKey(AttributeKey.Database))
                {
                    return new ItemAttributeKeyValuePair(AttributeKey.Database,
                        new DatabaseItemAttribute(!basketItem.Attributes.ContainsKey(AttributeKey.Domain), null));
                }
            }
            return Optional<ItemAttributeKeyValuePair>.Empty;
        }
    }
}
