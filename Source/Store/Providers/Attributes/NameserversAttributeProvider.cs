﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.Threading;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Mapping;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;

namespace Store.Providers.Attributes
{
    public class NameserversAttributeProvider : IAttributeProvider
    {
        private readonly INameserverTemplateProxy _nameserverTemplateProxy;

        public NameserversAttributeProvider(
            INameserverTemplateProxy nameserverTemplateProxy)
        {
            if (nameserverTemplateProxy == null) throw new ArgumentNullException("nameserverTemplateProxy");

            _nameserverTemplateProxy = nameserverTemplateProxy;
        }

        public Optional<ItemAttributeKeyValuePair> Get(Basket basket, BasketItem basketItem)
        {
            if (!basket.IsAnonymousBasket &&
                basketItem.ProductType == ProductTypes.DomainName)
            {
                if (!basketItem.Attributes.ContainsKey(AttributeKey.Nameservers))
                {
                    var attribute = AsyncPump.Await(async () => await GetNameserversTemplate(basket.CustomerIdentifier));

                    if (!attribute.HasValue)
                        attribute = new NameserversAttributes();

                    return new ItemAttributeKeyValuePair(AttributeKey.Nameservers, attribute.Value);
                }
            }
            return Optional<ItemAttributeKeyValuePair>.Empty;
        }

        private async Task<Optional<IBasketItemAttribute>> GetNameserversTemplate(CustomerIdentifier customerIdentifier)
        {
            var nameserverTemplates = await _nameserverTemplateProxy.GetDefault(customerIdentifier);

            if (!nameserverTemplates.Any())
                return Optional<IBasketItemAttribute>.Empty;

            var nameserversAttributes = new NameserversAttributes();
            nameserversAttributes.AddRange(nameserverTemplates.First().Nameservers.Select(ns => NameserverDataContractToNameserverAttributesMapper.Map(ns)));
            return nameserversAttributes;
        }
    }
}