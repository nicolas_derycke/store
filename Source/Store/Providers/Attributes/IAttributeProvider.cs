﻿using Intelligent.Shared.Core;
using Store.API.Contracts;
using Store.Model.Baskets;

namespace Store.Providers.Attributes
{
    public interface IAttributeProvider
    {
        Optional<ItemAttributeKeyValuePair> Get(Basket basket, BasketItem basketItem);
    }
}