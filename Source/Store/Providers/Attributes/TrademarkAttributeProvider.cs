﻿using System;
using Intelligent.Shared.Core;
using Intelligent.Shared.Threading;
using Serilog;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using Trademarks.API.Contract;

namespace Store.Providers.Attributes
{
    public class TrademarkAttributeProvider : IAttributeProvider
    {
        private readonly ITrademarkExistanceProxy _trademarkExistanceProxy;
        private readonly ILogger _logger;

        public TrademarkAttributeProvider(ITrademarkExistanceProxy trademarkExistanceProxy, ILogger logger)
        {
            if (logger == null) throw new ArgumentNullException("logger");
            if (trademarkExistanceProxy == null) throw new ArgumentNullException("trademarkExistanceProxy");

            _logger = logger;
            _trademarkExistanceProxy = trademarkExistanceProxy;
        }

        public Optional<ItemAttributeKeyValuePair> Get(Basket basket, BasketItem basketItem)
        {
            if (basketItem.Attributes.ContainsKey(AttributeKey.Trademark))
                return Optional<ItemAttributeKeyValuePair>.Empty;

            if (basketItem.ProductType == ProductTypes.DomainName)
            {
                var domainAttributes = basketItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
                try
                {
                    var trademarkCheck = AsyncPump.Await(async () => await _trademarkExistanceProxy.Get(domainAttributes.DomainName));
                    if (trademarkCheck.ClaimExistanceValue == ClaimExistance.Exists)
                        return new ItemAttributeKeyValuePair(AttributeKey.Trademark, new TrademarkAttributes(false));
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Could not check trademark existence for {domainName}", domainAttributes.DomainName);
                }
            }
            return Optional<ItemAttributeKeyValuePair>.Empty;
        }
    }
}
