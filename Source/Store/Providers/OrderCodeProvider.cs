﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Dapper;
using Intelligent.Shared.Data;

namespace Store.Providers
{
    public class OrderCodeProvider : IOrderCodeProvider
    {
        public string GenerateOrderCode(string provider)
        {
            string orderCode;

            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                orderCode = connection.ExecuteScalar<string>(
                           "[store].[GenerateBasketOrderCode]",
                           new
                           {
                               Provider = provider
                           },
                           commandType: CommandType.StoredProcedure);
            }

            return orderCode;
        }
    }

    public class SequenceBasedOrderCodeProvider : IOrderCodeProvider
    {
        private readonly int NumberOfSequences = 2;

        private static ConcurrentDictionary<string, SequenceState> SequenceStates; 
        private object _lockObject = new object();
        private DateTime SequenceDate;
        private string DatePart;

        public SequenceBasedOrderCodeProvider()
        {
            SequenceDate = DateTime.Now.Date.AddDays(-1); // intialize 1 day before so it starts fresh
            if(!string.IsNullOrEmpty(ConfigurationManager.AppSettings["OrderCodeSquences"]))
                NumberOfSequences = Convert.ToInt32(ConfigurationManager.AppSettings["OrderCodeSquences"]);
        }        

        public string GenerateOrderCode(string provider)
        {
            if (DateTime.Now.Date > SequenceDate)
                ResetDate();

            lock (_lockObject)
            {
                var currentSequenceState = SequenceStates.GetOrAdd(provider, key => new SequenceState(NumberOfSequences));

                if (currentSequenceState.CurrentSequenceItem == NumberOfSequences)
                {
                    currentSequenceState.CurrentSequenceItem = 0;
                    using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                    {
                        currentSequenceState.StartSequence = connection.ExecuteScalar<int>(
                            "[store].[GetNextOrderCodeSequence]",
                            new
                            {
                                Provider = provider,
                                NumberOfSequences
                            },
                            commandType: CommandType.StoredProcedure);
                    }
                }

                string orderCode = string.Format("{0}-{1}-{2}", provider, DatePart, currentSequenceState.StartSequence + currentSequenceState.CurrentSequenceItem);

                currentSequenceState.CurrentSequenceItem++;
                return orderCode;
            }
        }

        private void ResetDate()
        {
            SequenceDate = DateTime.Now.Date;
            DatePart = DateTime.Now.ToString("yyyyMMdd");
            SequenceStates = new ConcurrentDictionary<string, SequenceState>();
        }
    }

    internal class SequenceState
    {
        public SequenceState(int initialSequence)
        {
            CurrentSequenceItem = initialSequence;
        }
        internal int StartSequence { get; set; }
        internal int CurrentSequenceItem { get; set; }
    }
}
