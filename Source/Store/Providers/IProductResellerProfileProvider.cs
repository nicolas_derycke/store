namespace Store.Providers
{
    public interface IProductResellerProfileProvider
    {
        int GetResellerProfileIdByProductId(int productId);
    }
}