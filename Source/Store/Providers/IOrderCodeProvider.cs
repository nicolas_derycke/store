﻿namespace Store.Providers
{
    public interface IOrderCodeProvider
    {
        string GenerateOrderCode(string provider);
    }
}
