﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Transactions;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Store.Data;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.Services;
using Store.SqlQueries;

namespace Store.Providers
{
    public class ProductInfoProvider : IProductInfoProvider
    {
        private readonly IQueryHandler _queryHandler;
        private readonly IProductGroupService _productGroupService;

        public ProductInfoProvider(IQueryHandler queryHandler, IProductGroupService productGroupService)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (productGroupService == null) throw new ArgumentNullException("productGroupService");

            _queryHandler = queryHandler;
            _productGroupService = productGroupService;
        }

        public IEnumerable<ProductInfo> GetProductInfo(BasketItem basketItem, CustomerIdentifier customerIdentifier, int providerId, Language language)
        {
            return GetProductInfo(
                basketItem.ProductCode,
                basketItem.PromoProductCode,
                basketItem.Period,
                customerIdentifier,
                providerId,
                language,
                basketItem.Attributes.Identifier);
        }

        public IEnumerable<ProductInfo> GetProductInfo(string productCode, string promoProductCode, int period, CustomerIdentifier customerIdentifier, int providerId, Language language, string identifier)
        {
            var cachekey = GetCacheKey(productCode, promoProductCode, period, customerIdentifier, providerId, language, identifier);
            IEnumerable<ProductInfo> result;
            if (TryGetFromCache(cachekey, out result))
                return result;

            using (var transaction = new TransactionScope(TransactionScopeOption.Suppress))
            {
                var productInfos = new List<ProductInfo>();
                var productDetails = GetProductDetails(productCode, customerIdentifier, providerId, language, period, identifier);

                var isPromo = !string.IsNullOrWhiteSpace(promoProductCode);
                var isCustomerIdentified = customerIdentifier != null && customerIdentifier.CustomerId != 0;

                if (isPromo)
                {
                    var promoProductDetails = GetProductDetails(promoProductCode, customerIdentifier, providerId, language, period, identifier);

                    foreach (var productDetail in productDetails)
                    {
                        var promoProductDetail = promoProductDetails.FirstOrDefault(p => p.Action == productDetail.Action);

                        if (promoProductDetail != null)
                        {
                            if (promoProductDetail.ProductGroupCode != productDetail.ProductGroupCode)
                                throw new InvalidPromoProductException(productCode, promoProductCode);

                            var reduction = CalculateReduction(productDetail.PeriodPrice, promoProductDetail.PeriodPrice);

                            productInfos.Add(MapProductInfo(productDetail, promoProductDetail.PeriodPrice, productCode, promoProductCode, reduction));
                        }
                        else
                            productInfos.Add(MapProductInfo(productDetail, productDetail.PeriodPrice, productCode, promoProductCode, 0));
                    }
                }
                else if (isCustomerIdentified && !isPromo)
                {
                    var customerIndependentProductDetails = GetProductDetails(productCode, null, providerId, language, period, identifier);

                    foreach (var productDetail in productDetails)
                    {
                        var customerIndependentProductDetail = customerIndependentProductDetails.First(p => p.Action == productDetail.Action);

                        var reduction = CalculateReduction(customerIndependentProductDetail.PeriodPrice, productDetail.PeriodPrice);

                        productInfos.Add(MapProductInfo(productDetail, productDetail.PeriodPrice, productCode, promoProductCode, reduction));
                    }
                }
                else
                {
                    foreach (var productDetail in productDetails)
                    {
                        productInfos.Add(MapProductInfo(productDetail, productDetail.PeriodPrice, productCode, promoProductCode, 0));
                    }
                }

                AddToCache(cachekey, productInfos, customerIdentifier);

                transaction.Complete();

                return productInfos;
            }
        }

        private static decimal CalculateReduction(decimal price, decimal promoPrice)
        {
            var reduction = price - promoPrice;

            if (reduction < 0)
                return 0;
            else
                return reduction;
        }

        private ProductInfo MapProductInfo(ProductDetailsData productDetail, decimal periodPrice, string productCode, string promoProductCode, decimal reduction)
        {
            var productType = productDetail.ProductGroupCode;
            if (_productGroupService.IsDomain(productType))
                productType = ProductTypes.DomainName;

            return new ProductInfo(
                productDetail.ProductId,
                productCode,
                productDetail.ProductGroupCode,
                promoProductCode,
                productDetail.ProductName,
                productType,
                periodPrice,
                productDetail.PeriodId,
                reduction,
                productDetail.Period,
                productDetail.Action,
                productDetail.IsRecurring);
        }

        private IEnumerable<ProductDetailsData> GetProductDetails(string productCode, CustomerIdentifier customerIdentifier, int providerId, Language language, int period, string identifier)
        {
            var customerId = 0;
            if (customerIdentifier != null)
                customerId = customerIdentifier.CustomerId;

            var productDetails = _queryHandler.Execute(new GetProductDetailsQuery(language, productCode, customerId, providerId, identifier));

            if (!productDetails.Any())
                throw new ProductNotFoundException(productCode);

            if (!productDetails.Any(l => l.Period == period))
                throw new InvalidProductPeriodSpecifiedException(period);

            return productDetails
                .Where(l => l.Period == period)
                .Select(productDetail =>
                {
                    productDetail.IsRecurring = GetIsProductRecurring(productCode, productDetail.ProductGroupCode);
                    return productDetail;
                });
        }

        private bool GetIsProductRecurring(string productCode, string productGroupCode)
        {
            var notRecurringProducts = new List<string> { "reseller-platform", "combell-mover", "pre-registration" };
            return notRecurringProducts.Contains(productCode) || productGroupCode == "setup-activatiekosten-600" ? false : true;
        }

        private string GetCacheKey(string productCode, string promoProductCode, int period, CustomerIdentifier customerIdentifier, int providerId, Language language, string identifier)
        {
            var key = new StringBuilder();
            key.Append(productCode)
                .Append(promoProductCode)
                .Append(period.ToString())
                .Append(providerId)
                .Append(language);
            if (customerIdentifier != null) // don't include identifier for anonymous customers
                key.Append(customerIdentifier.CustomerNumber).Append(identifier);
            return key.ToString();
        }

        private void AddToCache(string key, IEnumerable<ProductInfo> productInfos, CustomerIdentifier customerIdentifier)
        {
            var expirationInMinutes = customerIdentifier == null ? 10 : 2; // lower the expiration value for identified customers
            MemoryCache.Default.Add(key,
                productInfos,
                new CacheItemPolicy()
                {
                    AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(expirationInMinutes)
                    // SlidingExpiration = TimeSpan.FromMinutes(expirationInMinutes)
                });
        }

        private bool TryGetFromCache(string key, out IEnumerable<ProductInfo> productInfos)
        {
            productInfos = null;
            if (!MemoryCache.Default.Contains(key))
                return false;

            var cacheItem = MemoryCache.Default.GetCacheItem(key);
            if (cacheItem != null && cacheItem.Value is IEnumerable<ProductInfo>)
            {
                productInfos = (IEnumerable<ProductInfo>)cacheItem.Value;
                return true;
            }

            return false;
        }
    }
}