﻿using System;
using Intelligent.Shared.Core.Queries;
using Store.SqlQueries;

namespace Store.Providers
{
    public class ProductResellerProfileProvider : IProductResellerProfileProvider
    {
        private readonly IQueryHandler _queryHandler;

        public ProductResellerProfileProvider(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._queryHandler = queryHandler;
        }

        public int GetResellerProfileIdByProductId(int productId)
        {
            return _queryHandler.Execute(new GetResellerProfileIdByProductIdQuery(productId));
        }
    }
}