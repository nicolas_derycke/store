﻿using System.Collections.Generic;
using Intelligent.Core.Models;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Providers
{
    public interface IProductInfoProvider
    {
        IEnumerable<ProductInfo> GetProductInfo(BasketItem basketItem, CustomerIdentifier customerIdentifier, int providerId, Language language);
        IEnumerable<ProductInfo> GetProductInfo(string productCode, string promoProductCode, int period, CustomerIdentifier customerIdentifier, int providerId, Language language, string identifier);
    }
}