﻿using System;
using System.Linq;
using Intelligent.Shared.Core.Queries;
using Store.Data;
using Store.Model;
using Store.SqlQueries;

namespace Store.Providers
{
    public class PromoCodeInfoProvider : IPromoCodeInfoProvider
    {
        private readonly IQueryHandler _queryHandler;

        public PromoCodeInfoProvider(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            _queryHandler = queryHandler;
        }

        public PromoCodeInfo GetPromoCodeInfo(string promoCode, int providerId)
        {            
            var promoCodeDetails = _queryHandler.Execute(new GetPromoCodeDetails(promoCode, providerId));

            return new PromoCodeInfo(promoCode, promoCodeDetails.Select(MapPromoProductInfo));
        }

        private PromoProductInfo MapPromoProductInfo(PromoCodeDetailData promoCodeDetail)
        {
            return new PromoProductInfo(
                promoCodeDetail.PeriodId,
                promoCodeDetail.Period,
                promoCodeDetail.ProductName,
                promoCodeDetail.ProductId,
                promoCodeDetail.ProductGroupId,
                promoCodeDetail.Price);
        }
    }
}