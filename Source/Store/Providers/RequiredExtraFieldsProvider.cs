﻿using System.Collections.Generic;
using System.Linq;
using Intelligent.Core.Models;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model.ExtraFields;

namespace Store.Providers
{
    public class RequiredExtraFieldsProvider : IRequiredExtraFieldsProvider
    {
        private static string CompanyNumber { get { return "companyNumber"; } }
        private static string CodiceFiscal { get { return "codiceFiscal"; } }
        private static string PassportNumber { get { return "passportNumber"; } }
        private static string TrademarkNumber { get { return "trademarkNumber"; } }
        private static string TrademarkName { get { return "trademarkName"; } }
        private static string TrademarkCountry { get { return "trademarkCountry"; } }

        private static readonly Dictionary<ExtraFieldRule, IEnumerable<string>> ExtraFieldsRules =
            new Dictionary<ExtraFieldRule, IEnumerable<string>>()
            {
                { new ExtraFieldRuleBuilder().ForExtension("fr").ForCompanies().Build(), new [] { CompanyNumber } },

                { new ExtraFieldRuleBuilder().ForExtension("it").ForIndividuals().Inside("it").Build(), new [] { CodiceFiscal } },
                { new ExtraFieldRuleBuilder().ForExtension("it").ForIndividuals().Outside("it").Build(), new [] { PassportNumber } },

                { new ExtraFieldRuleBuilder().ForExtension("ca").ForAnyone().Outside("ca").Build(), new [] { TrademarkNumber, TrademarkName, TrademarkCountry } },

                { new ExtraFieldRuleBuilder().ForExtension("dk").ForCompanies().Build(), new [] { CompanyNumber } },

                { new ExtraFieldRuleBuilder().ForExtension("nu").ForCompanies().Build(), new [] { CompanyNumber } },
                { new ExtraFieldRuleBuilder().ForExtension("nu").ForIndividuals().Build(), new [] { PassportNumber } },
                
                { new ExtraFieldRuleBuilder().ForExtension("se").ForCompanies().Build(), new [] { CompanyNumber } },
                { new ExtraFieldRuleBuilder().ForExtension("se").ForIndividuals().Build(), new [] { PassportNumber } },
            };

        public IEnumerable<string> Get(string domainName, RegistrantAttributes registrant)
        {
            if (string.IsNullOrWhiteSpace(domainName))
                return Enumerable.Empty<string>();

            var key = ExtraFieldsRules.Keys.FirstOrDefault(k => k.IsMatch(new DomainName(domainName), registrant.Company, registrant.Country));

            if (key == null)
                return Enumerable.Empty<string>();

            return ExtraFieldsRules[key];
        }
    }
}
