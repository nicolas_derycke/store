﻿using System.Data;
using System.Linq;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;
using Store.Model;

namespace Store.SqlQueries
{
    public class IsProductPeriodVowQuery : IQuery<Optional<ProductPeriodVisibility>>
    {
        public string ProductCode { get; private set; }
        public int Period { get; private set; }
        public ProductAction Action { get; private set; }
        public Provider Provider { get; private set; }

        public IsProductPeriodVowQuery(string productCode, int period, ProductAction action, Provider provider)
        {
            ProductCode = productCode;
            Period = period;
            Action = action;
            Provider = provider;
        }

        public Optional<ProductPeriodVisibility> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<ProductPeriodVisibility>(
                    "[store].[IsProductPeriodVow]",
                    new
                    {
                        ProductCode,
                        Period,
                        ActionId = (int)Action,
                        ProviderId = Provider.Id
                    },
                    commandType: CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}