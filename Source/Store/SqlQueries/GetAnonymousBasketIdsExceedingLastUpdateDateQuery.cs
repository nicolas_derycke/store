using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;

namespace Store.SqlQueries
{
    public class GetAnonymousBasketIdsExceedingLastUpdateDateQuery : IAsyncQuery<IEnumerable<int>>
    {
        public DateTime UpdateDate { get; private set; }

        public GetAnonymousBasketIdsExceedingLastUpdateDateQuery(DateTime updateDate)
        {
            UpdateDate = updateDate;
        }

        public async Task<IEnumerable<int>> ExecuteAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return await connection.QueryAsync<int>(
                    "[store].[GetAnonymousBasketIdsExceedingLastUpdateDate]",
                    new
                    {
                        UpdateDate
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}