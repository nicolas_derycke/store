﻿using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;
using System.Collections.Generic;
using System.Data;

namespace Store.SqlQueries
{
    public class GetPreregistrationsByNameQuery : IQuery<IEnumerable<Preregistration>>
    {
        public DomainName DomainName { get; private set; }

        public GetPreregistrationsByNameQuery(DomainName domainName)
        {
            this.DomainName = domainName;
        }

        public IEnumerable<Preregistration> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<Preregistration>(
                    "[dbo].[GTLD_GetDomainsByName]",
                    new
                    {
                        DomainName = DomainName.FullName
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}