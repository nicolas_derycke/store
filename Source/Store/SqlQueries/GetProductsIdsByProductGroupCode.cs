﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;

namespace Store.SqlQueries
{
    public class GetProductsIdsByProductGroupCode : IQuery<IEnumerable<int>>
    {
        public string ProductGroupCode { get; private set; }
        public Provider Provider { get; private set; }

        public GetProductsIdsByProductGroupCode(string productGroupCode, Provider provider)
        {
            ProductGroupCode = productGroupCode;
            Provider = provider;
        }

        public IEnumerable<int> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<int>(
                    "[dbo].[PRM_GetProductsByProductGroup]",
                    new
                    {
                        ProductGroupId = 0,
                        CompanyId = Provider.Id,
                        ProductGroupCode
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}