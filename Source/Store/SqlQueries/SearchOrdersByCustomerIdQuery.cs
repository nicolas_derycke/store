﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class SearchOrdersByCustomerIdQuery : IQuery<IEnumerable<OrderSummaryData>>
    {
        public int CustomerNumber { get; private set; }
        public DateTime? CreationDateFrom { get; private set; }
        public DateTime? CreationDateUntill { get; private set; }

        public SearchOrdersByCustomerIdQuery(int customerNumber, DateTime? creationDateFrom, DateTime? creationDateUntill)
        {
            CustomerNumber = customerNumber;
            CreationDateFrom = creationDateFrom.HasValue ? creationDateFrom : (DateTime)SqlDateTime.MinValue;
            CreationDateUntill = creationDateUntill.HasValue ? creationDateUntill : (DateTime)SqlDateTime.MaxValue;
        }

        public IEnumerable<OrderSummaryData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return connection.Query<OrderSummaryData>(
                    "[store].[SearchOrdersByCustomerId]",
                    new
                    {
                        CustomerIdentifier.FromCustomerNumber(CustomerNumber).CustomerId,
                        CreationDateFrom,
                        CreationDateUntill
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
