﻿using System.Data;
using System.Linq;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class GetUacUserById : IQuery<UacUser>
    {
        public int UserId { get; private set; }
        public int ProviderId { get; private set; }

        public GetUacUserById(int userId, int providerId)
        {
            UserId = userId;
            ProviderId = providerId;
        }

        public UacUser Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<UacUser>(
                    "[uac].[GetUserIdentityByUserId]",
                    new
                    {
                        UserId,
                        ProviderId
                    },
                    commandType: CommandType.StoredProcedure).First();
        }
    }
}