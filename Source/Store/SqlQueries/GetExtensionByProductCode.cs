﻿using Dapper;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;
using System.Data;
using System.Linq;

namespace Store.SqlQueries
{
    public class GetExtensionByProductCode : IQuery<Optional<DomainProductExtension>>
    {
        public GetExtensionByProductCode(string productCode)
        {
            this.ProductCode = productCode;
        }

        public string ProductCode { get; private set; }

        public Optional<DomainProductExtension> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<DomainProductExtension>(
                    "[dbo].[PRM_GetExtensionByProductCode]",
                    new
                    {
                        ProductCode
                    },
                    commandType: CommandType.StoredProcedure).FirstOrDefault();

        }
    }
}
