﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class GetOrderVerificationQuery : IQuery<IEnumerable<OrderVerificationData>>
    {
        public string OrderCode { get; private set; }

        public GetOrderVerificationQuery(string orderCode)
        {
            OrderCode = orderCode;
        }

        public IEnumerable<OrderVerificationData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<OrderVerificationData>(
                    "ordertracking.GetOrderVerifications",
                    new
                    {
                        OrderCode
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}