﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class GetBasketsByCustomerQuery : IQuery<IEnumerable<BasketReferenceData>>
    {
        public CustomerIdentifier CustomerIdentifier { get; private set; }

        public GetBasketsByCustomerQuery(CustomerIdentifier customerIdentifier)
        {
            CustomerIdentifier = customerIdentifier;
        }

        public IEnumerable<BasketReferenceData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<BasketReferenceData>(
                    "[store].[GetBasketsByCustomerId]",
                    new
                    {
                        CustomerIdentifier.CustomerId
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}