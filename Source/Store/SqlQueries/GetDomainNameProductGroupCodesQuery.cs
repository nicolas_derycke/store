﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;

namespace Store.SqlQueries
{
    public class GetDomainNameProductGroupCodesQuery : IQuery<IEnumerable<string>>
    {
        public IEnumerable<string> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<string>(
                    "[dbo].[PM_GetDomainNameProductGroupCodes]",
                    commandType: CommandType.StoredProcedure);
        }
    }
}
