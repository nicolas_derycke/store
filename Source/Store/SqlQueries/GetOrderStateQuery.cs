﻿using System.Data;
using System.Linq;
using Dapper;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class GetOrderStateQuery : IQuery<Optional<OrderStateData>>
    {
        public string OrderCode { get; private set; }

        public GetOrderStateQuery(string orderCode)
        {
            OrderCode = orderCode;
        }

        public Optional<OrderStateData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<OrderStateData>(
                    "ordertracking.GetOrderState",
                    new
                    {
                        OrderCode
                    },
                    commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
        }
    }
}