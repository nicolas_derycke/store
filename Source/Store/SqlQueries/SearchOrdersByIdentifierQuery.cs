﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class SearchOrdersByIdentifierQuery : IQuery<IEnumerable<OrderSummaryData>>
    {
        public string Identifier { get; private set; }
        public DateTime? CreationDateFrom { get; private set; }
        public DateTime? CreationDateUntill { get; private set; }

        public SearchOrdersByIdentifierQuery(string identifier, DateTime? creationDateFrom, DateTime? creationDateUntill)
        {
            Identifier = identifier;
            CreationDateFrom = creationDateFrom.HasValue ? creationDateFrom : (DateTime)SqlDateTime.MinValue;
            CreationDateUntill = creationDateUntill.HasValue ? creationDateUntill : (DateTime)SqlDateTime.MaxValue;
        }

        public IEnumerable<OrderSummaryData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return connection.Query<OrderSummaryData>(
                    "[store].[SearchOrdersByIdentifier]",
                    new
                    {
                        Identifier,
                        CreationDateFrom,
                        CreationDateUntill
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
