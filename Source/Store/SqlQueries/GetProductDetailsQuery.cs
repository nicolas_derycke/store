﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class GetProductDetailsQuery : IQuery<IEnumerable<ProductDetailsData>>
    {
        private readonly string _productCode;
        private readonly int _customerId;
        private readonly int _languageId;
        private readonly int _provider;
        private readonly string _identifier;

        public string ProductCode
        {
            get { return _productCode; }
        }

        public int CustomerId
        {
            get { return _customerId; }
        }

        public GetProductDetailsQuery(Language language, string productCode, int customerId, int providerId, string identifier)
        {
            _productCode = productCode;
            _customerId = customerId;
            _languageId = (int)language;
            _provider = providerId;
            _identifier = identifier;
        }

        public IEnumerable<ProductDetailsData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                using (var multi = connection.QueryMultiple(
                    "[dbo].[ORF_GetProduct]",
                    new
                    {
                        taalID = _languageId,
                        productCode = _productCode,
                        customerID = _customerId,
                        promotionCode = string.Empty,
                        productID = 0,
                        levFirmaID = _provider,
                        Identifier = _identifier
                    },
                    commandType: CommandType.StoredProcedure))
                {
                    var productDetails = multi.Read<dynamic>();

                    return productDetails.Select(
                            bid => 
                            {
                                return new ProductDetailsData(
                                    bid.ProductID,
                                    bid.ProductName, 
                                    bid.ProductGroupCode, 
                                    bid.ProductPeriodPrice, 
                                    bid.ProductPeriodStandardPrice - bid.ProductPeriodPrice, 
                                    bid.Periode, 
                                    bid.ProductPrijsID,
                                    bid.ActionId);
                            });
                }
            }
        }
    }
}