﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class GetOrderActivitiesQuery : IQuery<IEnumerable<OrderActivityData>>
    {
        public string OrderCode { get; private set; }

        public GetOrderActivitiesQuery(string orderCode)
        {
            OrderCode = orderCode;
        }

        public IEnumerable<OrderActivityData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<OrderActivityData>(
                    "ordertracking.GetOrderActivities",
                    new
                    {
                        OrderCode
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}