﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class GetBasketOrderCodesExceedingLastValidationDateQuery : IAsyncQuery<IEnumerable<IsRecalculationRequestedBasket>>
    {
        public DateTime ValidationDate { get; private set; }
        public DateTime StartDate { get; private set; }

        public GetBasketOrderCodesExceedingLastValidationDateQuery(int offsetInDays, int periodInDays)
        {
            ValidationDate = DateTime.Now.AddDays(-offsetInDays);
            StartDate = DateTime.Now.AddDays(-periodInDays);
        }

        public async Task<IEnumerable<IsRecalculationRequestedBasket>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return await connection.QueryAsync<IsRecalculationRequestedBasket>(
                    "[store].[GetBasketOrderCodesExceedingLastValidationDate]",
                    new
                    {
                        ValidationDate,
                        StartDate
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}