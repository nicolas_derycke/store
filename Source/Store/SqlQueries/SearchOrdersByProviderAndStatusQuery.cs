﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using Dapper;
using Intelligent.Shared.Collections;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Intelligent.Shared.Data.Collections;
using Store.Data;

namespace Store.SqlQueries
{
    public class SearchOrdersByProviderAndStatusQuery : IQuery<IEnumerable<OrderSummaryData>>
    {
        public IEnumerable<int> ProviderIds { get; private set; }
        public string Status { get; private set; }
        public int Skip { get; private set; }
        public int Take { get; private set; }
        public DateTime? CreationDateFrom { get; private set; }
        public DateTime? CreationDateUntill { get; private set; }

        public SearchOrdersByProviderAndStatusQuery(
            int skip,
            int take,
            IEnumerable<int> providerIds,
            string status,
            DateTime? creationDateFrom,
            DateTime? creationDateUntill)
        {
            ProviderIds = providerIds;
            Status = status;
            Skip = skip;
            Take = take;
            CreationDateFrom = creationDateFrom.HasValue ? creationDateFrom : (DateTime)SqlDateTime.MinValue;
            CreationDateUntill = creationDateUntill.HasValue ? creationDateUntill : (DateTime)SqlDateTime.MaxValue;
        }

        public IEnumerable<OrderSummaryData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return connection.Query<OrderSummaryData>(
                    "[store].[SearchOrdersByProviderAndStatus]",
                    new
                    {
                        Skip,
                        Take,
                        ProviderIds =
                            ProviderIds.ToSafeEnumerable()
                                .ToSingleColumnDataTable("GEN_IDListTableType")
                                .AsTableValuedParameter(),
                        Status,
                        CreationDateFrom,
                        CreationDateUntill
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
