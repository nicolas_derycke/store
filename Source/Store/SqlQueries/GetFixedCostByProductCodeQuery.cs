﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;

namespace Store.SqlQueries
{
    public class GetFixedCostByProductCodeQuery : IQuery<IEnumerable<string>>
    {
        public string ProductCode { get; private set; }

        public GetFixedCostByProductCodeQuery(string productCode)
        {
            ProductCode = productCode;
        }

        public IEnumerable<string> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.Query<string>(
                    "[store].[GetFixedCostByProductCode]",
                    new
                    {
                        ProductCode
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}