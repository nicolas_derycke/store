﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Collections;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Intelligent.Shared.Data.Collections;
using Store.Data;

namespace Store.SqlQueries
{
    public class SearchOrdersQuery : IQuery<IEnumerable<OrderSummaryData>>
    {
        public int? Skip { get; private set; }
        public int? Take { get; private set; }
        public IEnumerable<int> ProviderIds { get; private set; }
        public int? CustomerNumber { get; private set; }
        public string Identifier { get; private set; }
        public DateTime? CreationDateFrom { get; private set; }
        public DateTime? CreationDateUntill { get; private set; }
        public string Status { get; private set; }
        public string OrderCode { get; private set; }

        public SearchOrdersQuery(
            int? skip,
            int? take,
            IEnumerable<int> providerIds,
            int? customerNumber,
            string identifier,
            DateTime? creationDateFrom,
            DateTime? creationDateUntill,
            string status,
            string orderCode)
        {
            Skip = skip;
            Take = take;
            ProviderIds = providerIds;
            CustomerNumber = customerNumber;
            Identifier = identifier;
            CreationDateFrom = creationDateFrom;
            CreationDateUntill = creationDateUntill;
            Status = status;
            OrderCode = orderCode;
        }

        public IEnumerable<OrderSummaryData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return connection.Query<OrderSummaryData>(
                    "[store].[SearchOrders]",
                    new
                    {
                        Skip,
                        Take,
                        ProviderIds =
                            ProviderIds.ToSafeEnumerable()
                                .ToSingleColumnDataTable("GEN_IDListTableType")
                                .AsTableValuedParameter(),
                        CustomerId =
                            CustomerNumber.HasValue
                                ? CustomerIdentifier.FromCustomerNumber(CustomerNumber.Value).CustomerId
                                : (int?)null,
                        Identifier,
                        CreationDateFrom,
                        CreationDateUntill,
                        Status,
                        OrderCode
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}