﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class SearchOrdersByOrderCodeQuery : IQuery<IEnumerable<OrderSummaryData>>
    {
        public string OrderCode { get; private set; }

        public SearchOrdersByOrderCodeQuery(string orderCode)
        {
            OrderCode = orderCode;
        }

        public IEnumerable<OrderSummaryData> Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                return connection.Query<OrderSummaryData>(
                    "[store].[SearchOrdersByOrderCode]",
                    new
                    {
                        OrderCode
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
