﻿using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;
using Store.Mapping;
using Store.Model.Baskets;
using System.Collections.Generic;

namespace Store.SqlQueries
{
    public class GetBasketByOrderCodeQuery : IAsyncQuery<Optional<Basket>>
    {
        private readonly string _orderCode;
        private readonly BasketDataToModelMapper _basketMapper;

        public GetBasketByOrderCodeQuery(string orderCode)
        {
            _orderCode = orderCode;
            _basketMapper = new BasketDataToModelMapper();
        }

        public async Task<Optional<Basket>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                using (var multi = await connection.QueryMultipleAsync(
                    "[store].[GetBasketByOrderCode]",
                    new
                    {
                        OrderCode = _orderCode
                    },
                    commandType: CommandType.StoredProcedure))
                {
                    var basket = multi.Read<BasketData>().FirstOrDefault();

                    if (basket == null)
                        return Optional<Basket>.Empty;

                    var unsortedBasketItems = multi.Read<BasketItemData>().ToList();
                    var basketItems = unsortedBasketItems.Where(bi => !bi.ParentId.HasValue).ToList();
                    foreach (var child in unsortedBasketItems.Where(bi => bi.ParentId.HasValue).OrderByDescending(bi => bi.BasketItemId))
                        basketItems.Insert(basketItems.FindIndex(bi => bi.BasketItemId == child.ParentId) + 1, child);

                    foreach (var attribute in multi.Read<BasketAttributeData>())
                    {
                        var basketItem = basketItems.FirstOrDefault(b => b.BasketItemId == attribute.BasketItemId);
                        basketItem.Attributes.Add(attribute.Key, attribute.Value);
                    }

                    basket.Items = basketItems;

                    return _basketMapper.Map(basket);
                }
            }
        }
    }
}