﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.API.Contracts.Baskets;

namespace Store.SqlQueries
{
    public class GetBasketIdsExceedingLastUpdateDateQuery : IAsyncQuery<IEnumerable<int>>
    {
        public DateTime UpdateDate { get; private set; }
        public BasketType BasketType { get; private set; }

        public GetBasketIdsExceedingLastUpdateDateQuery(DateTime updateDate, BasketType basketType)
        {
            this.UpdateDate = updateDate;
            this.BasketType = basketType;
        }

        public async Task<IEnumerable<int>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return await connection.QueryAsync<int>(
                    "[store].[GetBasketIdsExceedingLastUpdateDate]",
                    new
                    {
                        UpdateDate,
                        BasketType = BasketType.ToString()
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}