﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;

namespace Store.SqlQueries
{
    public class GetResellerProfileIdByProductIdQuery : IQuery<int>
    {
        public int ProductId { get; private set; }

        public GetResellerProfileIdByProductIdQuery(int productId)
        {
            ProductId = productId;
        }

        public int Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                return connection.ExecuteScalar<int>(
                    "[dbo].[UAC_GetResellerProfileByProductID]",
                    new
                    {
                        ProductID = ProductId
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}