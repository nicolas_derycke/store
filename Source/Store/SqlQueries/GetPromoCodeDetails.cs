﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlQueries
{
    public class GetPromoCodeDetails : IQuery<IEnumerable<PromoCodeDetailData>>
    {
        public string PromoCode { get; private set; }
        public int ProviderId { get; private set; }

        public GetPromoCodeDetails(string promoCode, int providerId)
        {
            PromoCode = promoCode;
            ProviderId = providerId;
        }

        public IEnumerable<PromoCodeDetailData> Execute()
        {
               using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                   return connection.Query<PromoCodeDetailData>(
                    "[dbo].[ORF_CheckPromoCode]",
                    new
                    {
                        PromoCode,
                        taalID = 0,
                        levFirmaID = ProviderId
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}