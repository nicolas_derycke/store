﻿using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Store.Data;
using Store.Mapping;
using Store.Model.Orders;

namespace Store.SqlQueries
{
    public class GetOrderByOrderCodeQuery : IAsyncQuery<Optional<Order>>
    {
        private readonly string _orderCode;
        private readonly OrderDataToModelMapper _orderMapper;

        public GetOrderByOrderCodeQuery(string orderCode)
        {
            _orderCode = orderCode;
            _orderMapper = new OrderDataToModelMapper();
        }

        public async Task<Optional<Order>> ExecuteAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                using (var multi = await connection.QueryMultipleAsync(
                    "[store].[GetOrderByOrderCode]",
                    new
                    {
                        OrderCode = _orderCode
                    },
                    commandType: CommandType.StoredProcedure))
                {
                    var order = multi.Read<OrderData>().FirstOrDefault();

                    if (order == null)
                        return Optional<Order>.Empty;

                    var orderItems = multi.Read<OrderItemData>().ToList();

                    foreach (var attribute in multi.Read<OrderAttributeData>())
                    {
                        var orderItem = orderItems.FirstOrDefault(b => b.OrderItemID == attribute.OrderItemId);
                        orderItem.Attributes.Add(attribute.Key, attribute.Value);
                    }

                    var checkoutData = multi.Read<Data.CheckoutData>().FirstOrDefault();

                    order.Items = orderItems;
                    order.CheckoutData = checkoutData;

                    return _orderMapper.Map(order);
                }
            }
        }
    }
}