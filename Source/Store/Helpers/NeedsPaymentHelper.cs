﻿using System;
using Store.Model.Orders;

namespace Store.Helpers
{
    public static class NeedsPaymentHelper
    {
        public static bool CalculateNeedsPayment(bool needsPayment, string checkoutType, DateTime creationDate)
        {
            return CalculateNeedsPayment(needsPayment, (CheckoutType)Enum.Parse(typeof(CheckoutType), checkoutType, true), creationDate);
        }

        public static bool CalculateNeedsPayment(bool needsPayment, CheckoutType checkoutType, DateTime creationDate)
        {
            if (!needsPayment)
                return false;

            // ogone callback triggers the needsPayment to become false, this isn't instant, we change true to false within the first 24 hours to give the callback time
            if ((checkoutType == CheckoutType.CreditCardPayment || checkoutType == CheckoutType.DebitCardPayment) && DateTime.Now < creationDate.AddDays(1))
                return false;

            return true;
        }
    }
}
