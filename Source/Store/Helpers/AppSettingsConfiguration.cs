﻿using System.Configuration;

namespace Store.Helpers
{
    public class AppSettingsConfiguration : IAppSettingsConfiguration
    {
        public string BillingApiUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["BillingAPI"];
            }
        }

        public string UserAgent
        {
            get
            {
                return ConfigurationManager.AppSettings["UserAgent"];
            }
        }
    }
}
