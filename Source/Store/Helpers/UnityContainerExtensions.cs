﻿using System.Collections.Generic;
using Intelligent.Shared.Reflection;
using Microsoft.Practices.Unity;

namespace Store.Helpers
{
    public static class UnityContainerExtensions
    {
        public static void RegisterTypesWhichImplement<T>(this IUnityContainer container)
        {
            var validators = TypeFinder.FindTypesWhichImplement(typeof(T).Assembly, typeof(T));
            foreach (var validator in validators)
                container.RegisterType(typeof(T), validator, validator.Name, new ContainerControlledLifetimeManager());
            container.RegisterType<IEnumerable<T>, T[]>(new ContainerControlledLifetimeManager());
        }
    }
}
