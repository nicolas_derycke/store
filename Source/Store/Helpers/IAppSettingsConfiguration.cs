namespace Store.Helpers
{
    public interface IAppSettingsConfiguration
    {
        string BillingApiUrl { get; }
        string UserAgent { get;  }
    }
}