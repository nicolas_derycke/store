﻿using System.Transactions;

namespace Store.Helpers
{
    public static class TransactionHelper
    {
        public static TransactionScope GetNewTransactionScope()
        {
            var options = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            };

            var newTransaction = new TransactionScope(TransactionScopeOption.Required, options, TransactionScopeAsyncFlowOption.Enabled);

            return newTransaction;
        }
    }
}