﻿using System;
using Core.Owin;
using Intelligent.Shared.Net.Http;
using Intelligent.Shared.Owin;

namespace Store
{
    public class ApiHttpClientFactory : IHttpClientFactory
    {
        private readonly IIntelliPropertiesProvider _propertiesProvider;

        public ApiHttpClientFactory(IIntelliPropertiesProvider propertiesProvider)
        {
            if (propertiesProvider == null) throw new ArgumentNullException("propertiesProvider");
            _propertiesProvider = propertiesProvider;
        }

        public StronglyTypedHttpClient BuildStoreClient()
        {
            var token = OwinRequestScopeContext.Current.GetBearerToken();
            return StronglyTypedHttpClient
                .Create()
                .WithDynamicHeaders(_propertiesProvider.IntelliPropertiesAccessor)
                .WithBearerToken(token)
                .Build();
        }
    }
}