﻿using Intelligent.Shared.Core.Commands;

namespace Store.SqlCommands
{
    public class SqlCommandHandler : CommandHandler, ISqlCommandHandler
    {
        public new void Execute(ICommand command)
        {
            base.Execute(command);
        }
    }
}