﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Newtonsoft.Json;
using Store.API.Contracts;

namespace Store.SqlCommands.BasketItems
{
    public class UpdateBasketItemAttribute : ICommand
    {
        public int BasketItemId { get; private set; }
        public AttributeKey AttributeKey { get; private set; }
        public IBasketItemAttribute AttributeValue { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketItemAttribute(int basketItemId, AttributeKey attributeKey, IBasketItemAttribute attributeValue, int lastUpdateById)
        {
            if (attributeValue == null) throw new ArgumentNullException("attributeValue");

            BasketItemId = basketItemId;
            AttributeKey = attributeKey;
            AttributeValue = attributeValue;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            var attributeValue = JsonConvert.SerializeObject(AttributeValue);
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketItemAttribute]",
                    new
                    {
                        BasketItemID = BasketItemId,
                        AttributeValue = attributeValue,
                        AttributeKey = AttributeKey.ToString(),
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}
