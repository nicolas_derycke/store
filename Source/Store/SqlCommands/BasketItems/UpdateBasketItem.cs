﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.Model.Baskets;

namespace Store.SqlCommands.BasketItems
{
    public class UpdateBasketItem : ICommand
    {
        public BasketItem BasketItem { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketItem(BasketItem basketItem, int lastUpdateById)
        {
            BasketItem = basketItem;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketItem]",
                    new
                    {
                        BasketItemID = BasketItem.BasketItemId,
                        OrderCode = "",
                        BasketItem.ProductCode,
                        ProductType = BasketItem.ProductType,
                        BasketItem.ProductName,
                        BasketItem.PromoProductCode,
                        BasketItem.PeriodId,
                        BasketItem.Period,
                        BasketItem.Quantity,
                        TotalPriceInclVat = BasketItem.TotalPrice.InclVat,
                        TotalPriceExclVat = BasketItem.TotalPrice.ExclVat,
                        TotalPriceReductionInclVat = BasketItem.TotalPrice.ReductionInclVat,
                        TotalPriceReductionExclVat = BasketItem.TotalPrice.ReductionExclVat,
                        PriceInclVat = BasketItem.Price.InclVat,
                        PriceExclVat = BasketItem.Price.ExclVat,
                        PriceReductionInclVat = BasketItem.Price.ReductionInclVat,
                        PriceReductionExclVat = BasketItem.Price.ReductionExclVat,
                        LastUpdateById,
                        ValidationError = BasketItem.ValidationError.ToString(),
                        Warning = BasketItem.Warning.ToString(),
                        BasketItem.ParentId,
                        BasketItem.IsRecurring,
                        BasketItem.PromoCode
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}
