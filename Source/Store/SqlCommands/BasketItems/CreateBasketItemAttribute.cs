﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Newtonsoft.Json;
using Store.API.Contracts;

namespace Store.SqlCommands.BasketItems
{
    public class CreateBasketItemAttribute : ICommand
    {
        public int BasketItemId { get; private set; }
        public AttributeKey AttributeKey { get; private set; }
        public string AttributeValueJson { get; private set; }
        public int CreationById { get; private set; }

        public string AttributeKeyString
        {
            get { return Enum.GetName(typeof(AttributeKey), AttributeKey); }
        }

        public CreateBasketItemAttribute(int basketItemId, AttributeKey attributeKey, string attributeValueJson, int creationById)
        {
            BasketItemId = basketItemId;
            AttributeKey = attributeKey;
            AttributeValueJson = attributeValueJson;
            CreationById = creationById;
        }

        public CreateBasketItemAttribute(int basketItemId, AttributeKey attributeKey, IBasketItemAttribute attributeValue, int creationById)
        {
            BasketItemId = basketItemId;
            AttributeKey = attributeKey;
            AttributeValueJson = JsonConvert.SerializeObject(attributeValue);
            CreationById = creationById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[CreateBasketItemAttribute]",
                    new
                    {
                        BasketItemID = BasketItemId,
                        AttributeKey = AttributeKeyString,
                        AttributeValue = AttributeValueJson,
                        CreationById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}
