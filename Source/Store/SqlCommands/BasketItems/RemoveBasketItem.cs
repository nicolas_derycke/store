﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.BasketItems
{
    public class RemoveBasketItem : ICommand
    {
        public int BasketItemId { get; private set; }
        public int DeletionById { get; private set; }

        public RemoveBasketItem(int basketItemId, int deletionById)
        {
            BasketItemId = basketItemId;
            DeletionById = deletionById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[RemoveBasketItem]",
                    new
                    {
                        BasketItemID = BasketItemId,
                        DeletionById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}
