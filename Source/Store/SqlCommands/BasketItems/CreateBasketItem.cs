﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.Model.Baskets;

namespace Store.SqlCommands.BasketItems
{
    public class CreateBasketItem : ICommand
    {
        public Basket Basket { get; private set; }
        public BasketItem BasketItem { get; private set; }
        public int CreationById { get; private set; }

        public CreateBasketItem(Basket basket, BasketItem basketItem, int creationById)
        {
            Basket = basket;
            BasketItem = basketItem;
            CreationById = creationById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                BasketItem.BasketItemId = connection.ExecuteScalar<int>(
                    "[store].[CreateBasketItem]",
                    new
                    {
                        Basket.BasketId,
                        BasketItem.ProductId,
                        BasketItem.ProductCode,
                        ProductType = BasketItem.ProductType,
                        BasketItem.ProductName,
                        PromoProductCode = string.IsNullOrWhiteSpace(BasketItem.PromoProductCode) ? null : BasketItem.PromoProductCode.Trim(),
                        BasketItem.Period,
                        BasketItem.Quantity,
                        TotalPriceInclVat = BasketItem.TotalPrice.InclVat,
                        TotalPriceExclVat = BasketItem.TotalPrice.ExclVat,
                        TotalPriceReductionInclVat = BasketItem.TotalPrice.ReductionInclVat,
                        TotalPriceReductionExclVat = BasketItem.TotalPrice.ReductionExclVat,
                        PriceInclVat = BasketItem.Price.InclVat,
                        PriceExclVat = BasketItem.Price.ExclVat,
                        PriceReductionInclVat = BasketItem.Price.ReductionInclVat,
                        PriceReductionExclVat = BasketItem.Price.ReductionExclVat,
                        CreationById,
                        ValidationError = BasketItem.ValidationError.ToString(),
                        Warning = BasketItem.Warning.ToString(),
                        BasketItem.PeriodId,
                        BasketItem.ParentId,
                        BasketItem.IsRecurring,
                        BasketItem.IsFixedPrice
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}
