﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.Baskets
{
    public class UpdateBasketIsRecalculating : ICommand
    {
        public int BasketId { get; private set; }
        public bool IsRecalculating { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketIsRecalculating(int basketId, bool isRecalculating, int lastUpdateById)
        {
            BasketId = basketId;
            IsRecalculating = isRecalculating;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketIsRecalculating]",
                    new
                    {
                        BasketId,
                        IsRecalculating,
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}