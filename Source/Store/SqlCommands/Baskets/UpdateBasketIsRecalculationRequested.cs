﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.Baskets
{
    public class UpdateBasketIsRecalculationRequested : ICommand
    {
        public string OrderCode { get; private set; }
        public bool IsRecalculationRequested { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketIsRecalculationRequested(string orderCode, bool isRecalculationRequested, int lastUpdateById)
        {
            OrderCode = orderCode;
            IsRecalculationRequested = isRecalculationRequested;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketIsRecalculationRequested]",
                    new
                    {
                        OrderCode,
                        IsRecalculationRequested,
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}