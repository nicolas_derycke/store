﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.Baskets
{
    public class UpdateBasketLastValidationDate : ICommand
    {
        public int BasketId { get; private set; }
        public DateTime LastValidationDate { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketLastValidationDate(int basketId, DateTime lastValidationDate, int lastUpdateById)
        {
            BasketId = basketId;
            LastValidationDate = lastValidationDate;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketLastValidationDate]",
                    new
                    {
                        BasketId,
                        LastValidationDate,
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}
