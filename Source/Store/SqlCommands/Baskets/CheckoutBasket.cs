﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.Baskets
{
    public class CheckoutBasket : ICommand
    {
        public string OrderCode { get; private set; }
        public int LastUpdateById { get; private set; }

        public CheckoutBasket(string orderCode, int lastUpdateById)
        {
            if (orderCode == null) throw new ArgumentNullException("orderCode");
            OrderCode = orderCode;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[SetBasketCheckedOut]",
                    new
                    {
                        OrderCode,
                        LastUpdateById,
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}