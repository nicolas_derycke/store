﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.Baskets
{
    public class UpdateBasketCustomerReference : ICommand
    {
        public int BasketId { get; private set; }
        public string CustomerReference { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketCustomerReference(
            int basketId, 
            string customerReference, 
            int lastUpdateById)
        {
            BasketId = basketId;
            CustomerReference = customerReference;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketCustomerReference]",
                    new
                    {
                        BasketId,
                        CustomerReference,
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}
