﻿using System.Data;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.Baskets
{
    public class UpdateBasketLanguage : ICommand
    {
        public int BasketId { get; private set; }
        public Language Language { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketLanguage(int basketId, Language language, int lastUpdateById)
        {
            BasketId = basketId;
            Language = language;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketLanguage]",
                    new
                    {
                        BasketId,
                        Language = Language.ToString(),
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}