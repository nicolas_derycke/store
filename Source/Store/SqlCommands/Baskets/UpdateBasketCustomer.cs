﻿using System;
using System.Data;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.Baskets
{
    public class UpdateBasketCustomer : ICommand
    {
        public string OrderCode { get; private set; }
        public CustomerIdentifier CustomerIdentifier { get; private set; }
        public decimal VatRatePercentage { get; private set; }
        public int LastUpdatedById { get; private set; }

        public UpdateBasketCustomer(string orderCode, CustomerIdentifier customerIdentifier, decimal vatRatePercentage, int lastUpdatedById)
        {
            if (orderCode == null) throw new ArgumentNullException("orderCode");
            if (customerIdentifier == null) throw new ArgumentNullException("customerIdentifier");
            OrderCode = orderCode;
            CustomerIdentifier = customerIdentifier;
            LastUpdatedById = lastUpdatedById;
            VatRatePercentage = vatRatePercentage;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketCustomer]",
                    new
                    {
                        OrderCode,
                        CustomerID = CustomerIdentifier.CustomerId,
                        VatRatePercentage,
                        LastUpdateByID = LastUpdatedById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}