﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.SqlCommands.Baskets
{
    public class DeleteBasket : ICommand
    {
        public int BasketId { get; private set; }

        public int DeletionById { get; private set; }

        public DeleteBasket(int basketId, int deletionById)
        {
            BasketId = basketId;
            DeletionById = deletionById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[DeleteBasket]",
                    new
                    {
                        BasketId,
                        DeletionById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}