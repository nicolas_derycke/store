﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.API.Contracts.Baskets;

namespace Store.SqlCommands.Baskets
{
    public class UpdateBasketType : ICommand
    {
        public int BasketId { get; private set; }
        public BasketType BasketType { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketType(int basketId, BasketType basketType, int lastUpdateById)
        {
            BasketId = basketId;
            BasketType = basketType;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "[store].[UpdateBasketType]",
                    new
                    {
                        BasketId,
                        BasketType,
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}