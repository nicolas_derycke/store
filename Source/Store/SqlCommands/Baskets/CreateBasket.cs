﻿using System.Data;
using Dapper;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.API.Contracts.Baskets;

namespace Store.SqlCommands.Baskets
{
    public class CreateBasket : ICommand
    {
        public string OrderCode { get; private set; }
        public CustomerIdentifier CustomerIdentifier { get; private set; }
        public decimal VatRatePercentage { get; private set; }
        public Provider Provider { get; private set; }
        public int CreationById { get; private set; }
        public BasketType BasketType { get; private set; }
        public string Source { get; private set; }
        public int BasketId { get; set; }
        public Language Language { get; private set; }

        public CreateBasket(string orderCode, CustomerIdentifier customerIdentifier, decimal vatRatePercentage, Provider provider, BasketType basketType, string source, Language language, int creationById)
        {
            OrderCode = orderCode;
            CustomerIdentifier = customerIdentifier;
            VatRatePercentage = vatRatePercentage;
            Provider = provider;
            CreationById = creationById;
            BasketType = basketType;
            Source = source;
            Language = language;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                BasketId = connection.ExecuteScalar<int>(
                    "[store].[CreateBasket]",
                    new
                    {
                        OrderCode,
                        CustomerID = CustomerIdentifier == null ? (int?)null : CustomerIdentifier.CustomerId,
                        VatRatePercentage,
                        CreationByID = CreationById,
                        ProviderId = Provider.Id,
                        BasketType = BasketType.ToString(),
                        Source,
                        Language = Language.ToString()
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}