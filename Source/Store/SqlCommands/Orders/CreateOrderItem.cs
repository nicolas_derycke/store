﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.Model.Orders;

namespace Store.SqlCommands.Orders
{
    public class CreateOrderItem : ICommand
    {
        public OrderItem OrderItem { get; private set; }
        public int OrderId { get; private set; }
        public int CreationById { get; private set; }

        public CreateOrderItem(OrderItem orderItem, int orderId, int creationById)
        {
            if (orderItem == null) throw new ArgumentNullException("orderItem");

            OrderItem = orderItem;
            OrderId = orderId;
            CreationById = creationById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                connection.Execute(
                    "[store].[CreateOrderItem]",
                    new
                    {
                        OrderItem.OrderItemId,
                        OrderItem.ProductId,
                        OrderId,
                        OrderItem.ProductCode,
                        ProductType = OrderItem.ProductType,
                        OrderItem.ProductName,
                        OrderItem.PromoProductCode,
                        OrderItem.Period,
                        OrderItem.Quantity,
                        TotalPriceInclVat = OrderItem.TotalPrice.InclVat,
                        TotalPriceExclVat = OrderItem.TotalPrice.ExclVat,
                        TotalPriceReductionInclVat = OrderItem.TotalPrice.ReductionInclVat,
                        TotalPriceReductionExclVat = OrderItem.TotalPrice.ReductionExclVat,
                        PriceInclVat = OrderItem.Price.InclVat,
                        PriceExclVat = OrderItem.Price.ExclVat,
                        PriceReductionInclVat = OrderItem.Price.ReductionInclVat,
                        PriceReductionExclVat = OrderItem.Price.ReductionExclVat,
                        CreationById,
                        OrderItem.PeriodId,
                        OrderItem.ParentId,
                        OrderItem.IsRecurring,
                        OrderItem.Identifier,
                        OrderItem.PromoCode
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}