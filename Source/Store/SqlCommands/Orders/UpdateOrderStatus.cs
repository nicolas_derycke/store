﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.API.Contracts;

namespace Store.SqlCommands.Orders
{
    public class UpdateOrderStatus : ICommand
    {
        public int OrderId { get; private set; }
        public OrderStatus Status { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateOrderStatus(int orderId, OrderStatus status, int lastUpdateById)
        {
            OrderId = orderId;
            Status = status;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                connection.Execute(
                    "[store].[UpdateOrderStatus]",
                    new
                    {
                        OrderId,
                        Status = Status.ToString(),
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
            }

        }
    }
}