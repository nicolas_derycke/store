﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.Model.Orders;

namespace Store.SqlCommands.Orders
{
    public class CreateOrderPayment : ICommand
    {
        public int OrderId { get; private set; }
        public CheckoutData Payment { get; private set; }
        public int CreationById { get; private set; }

        public CreateOrderPayment(int orderId, CheckoutData payment, int creationById)
        {
            if (payment == null) throw new ArgumentNullException("payment");

            OrderId = orderId;
            Payment = payment;
            CreationById = creationById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                connection.Execute(
                    "[store].[CreateOrderPayment]",
                    new
                    {
                        OrderId,
                        Payment.PaidAmount,
                        Payment.PaymentProviderReference,
                        CreationById
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}