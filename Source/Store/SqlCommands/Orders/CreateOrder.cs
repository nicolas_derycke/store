﻿using System;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.Model.Orders;

namespace Store.SqlCommands.Orders
{
    public class CreateOrder : ICommand
    {
        public Order Order { get; private set; }
        public int CreationById { get; private set; }

        public CreateOrder(Order order, int creationById)
        {
            if (order == null) throw new ArgumentNullException("order");

            Order = order;
            CreationById = creationById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                connection.Execute(
                    "[store].[CreateOrder]",
                    new
                    {
                        Order.OrderId,
                        Order.OrderCode,
                        CustomerID = Order.CustomerIdentifier.CustomerId,
                        TotalPriceInclVat = Order.TotalPrice.InclVat,
                        TotalPriceExclVat = Order.TotalPrice.ExclVat,
                        TotalPriceReductionInclVat = Order.TotalPrice.ReductionInclVat,
                        TotalPriceReductionExclVat = Order.TotalPrice.ReductionExclVat,
                        Order.VatRatePercentage,
                        Order.VatAmount,
                        CreationByID = CreationById,
                        CheckoutType = Order.CheckoutType.ToString(),
                        Order.CustomerReference,
                        Order.Source,
                        ProviderId = Order.Provider.Id,
                        Status = Order.Status.ToString(),
                        Order.TicketId,
                        Order.TicketNumber,
                        Language = Order.Language.ToString()
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}