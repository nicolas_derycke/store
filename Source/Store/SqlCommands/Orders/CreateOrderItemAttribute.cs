﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.API.Contracts;

namespace Store.SqlCommands.Orders
{
    public class CreateOrderItemAttribute : ICommand
    {
        public KeyValuePair<AttributeKey, string> BasketItemAttribute { get; set; }
        public int BasketItemId { get; set; }
        public int CreationById { get; set; }

        public CreateOrderItemAttribute(KeyValuePair<AttributeKey, string> basketItemAttribute, int basketItemId, int creationById)
        {
            BasketItemAttribute = basketItemAttribute;
            BasketItemId = basketItemId;
            CreationById = creationById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                var attribute = BasketItemAttribute;
                connection.ExecuteScalar<int>(
                    "[store].[CreateOrderItemAttribute]",
                    new
                    {
                        OrderItemID = BasketItemId,
                        AttributeKey = Enum.GetName(typeof(AttributeKey), attribute.Key),
                        AttributeValue = attribute.Value,
                        CreationById
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}