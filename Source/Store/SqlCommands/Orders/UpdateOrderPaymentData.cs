﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;
using Store.Data;

namespace Store.SqlCommands.Orders
{
    public class UpdateOrderPaymentData : ICommand
    {
        public UpdateOrderPaymentData(int orderId, OrderPaymentData orderPaymentStatus, int lastUpdateById)
        {
            OrderId = orderId;
            OrderPaymentData = orderPaymentStatus;
            LastUpdateById = lastUpdateById;
        }

        public int OrderId { get; private set; }
        public OrderPaymentData OrderPaymentData { get; private set; }
        public int LastUpdateById { get; private set; }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
            {
                connection.Execute(
                    "[store].[UpdateOrderPaymentData]",
                    new
                    {
                        OrderId,
                        EpayCode = string.IsNullOrWhiteSpace(OrderPaymentData.EpayCode) ? null : OrderPaymentData.EpayCode,
                        OrderPaymentData.NeedsPayment,
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}