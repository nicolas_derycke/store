﻿using System;
using System.Collections.Generic;

namespace Store
{
    public interface IIntelliPropertiesProvider
    {
        Func<IEnumerable<KeyValuePair<string, string>>> IntelliPropertiesAccessor { get; }
    }
}