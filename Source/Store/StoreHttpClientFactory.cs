﻿using System;
using Core.Security;
using IdentityModel.Client;
using Intelligent.Shared.Net.Http;

namespace Store
{
    public class StoreHttpClientFactory : IHttpClientFactory
    {
        private readonly ITokenProvider _tokenProvider;
        private readonly IIntelliPropertiesProvider _propertiesProvider;

        public StoreHttpClientFactory(ITokenProvider tokenProvider, IIntelliPropertiesProvider propertiesProvider)
        {
            if (tokenProvider == null) throw new ArgumentNullException("tokenProvider");
            if (propertiesProvider == null) throw new ArgumentNullException("propertiesProvider");

            this._tokenProvider = tokenProvider;
            _propertiesProvider = propertiesProvider;
        }

        public StronglyTypedHttpClient BuildStoreClient()
        {
            var applicationGrantToken = _tokenProvider.GetApplicationGrantToken("combell.store");

            if (applicationGrantToken.IsError)
                throw new Exception(string.Format("Error getting token: {0}", applicationGrantToken.Error));

            var token = applicationGrantToken.AccessToken;
            return StronglyTypedHttpClient
                .Create()
                .WithDynamicHeaders(_propertiesProvider.IntelliPropertiesAccessor)
                .WithBearerToken(token)
                .Build();
        }
    }
}
