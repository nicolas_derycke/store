﻿using System;
using System.Linq;
using Intelligent.Core.Models;
using Store.API.Contracts.Baskets;
using Store.Data;
using Store.Model.Baskets;

namespace Store.Mapping
{
    public class BasketDataToModelMapper
    {
        private readonly BasketItemDataToModelMapper _basketItemMapper;

        public BasketDataToModelMapper()
        {
            _basketItemMapper = new BasketItemDataToModelMapper();
        }

        public Basket Map(BasketData data)
        {
            var vatRatePercentage = data.VatRatePercentage;
            var basketItems = data.Items.Select(c => _basketItemMapper.Map(c, vatRatePercentage)).ToList();
            return new Basket(data.BasketId,
                data.OrderCode,
                data.CustomerId.HasValue ? CustomerIdentifier.FromCustomerId(data.CustomerId.Value) : null,
                basketItems,
                data.VatRatePercentage,
                data.CreationDate,
                data.CheckedOut,
                data.LastValidationDate,
                new Provider(data.ProviderId),
                (BasketType)Enum.Parse(typeof(BasketType), data.BasketType, true),
                data.IsRecalculating,
                data.Source,
                (Language)Enum.Parse(typeof(Language), data.Language, true),
                data.Deleted,
                data.CustomerReference);
        }
    }
}