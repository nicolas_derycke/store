﻿using Store.Model;

namespace Store.Mapping
{
    public class PriceDataToModelMapper
    {

        public Price Map(decimal exclVat, decimal reductionExclVat, decimal vatRatePercentage)
        {
            return new Price(exclVat, reductionExclVat, vatRatePercentage);
        }
    }
}
