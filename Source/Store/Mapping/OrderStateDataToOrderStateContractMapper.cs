﻿using System;
using Store.API.Contracts.Orders;
using Store.Data;

namespace Store.Mapping
{
    public static class OrderStateDataToOrderStateContractMapper
    {
        public static OrderStateContract Map(OrderStateData data)
        {
            return new OrderStateContract
            {
                OrderCode = data.OrderCode,
                OrderStatus = data.OrderStatus,
                ActivationState = new OrderActivationStateContract
                {
                    ActivationBy = data.ActivationBy,
                    ActivationMethod = GetActivationMethod(data.ActivationMethod),
                    ActivationReason = data.ActivationReason,
                    LastUpdateDate = data.ActivationStatusLastUpdateDate,
                    Status = (OrderActivationStatusContract)Enum.Parse(typeof(OrderActivationStatusContract), data.ActivationStatus)
                },
                InitiatedBy = data.InitiatedBy,
                IsInitiatedByEmployee = data.IsInitiatedByEmployee,
                InvoiceState = new OrderInvoiceStateContract
                {
                    LastUpdateDate = data.InvoicingStatusLastUpdateDate,
                    Status = (OrderInvoicingStatusContract)Enum.Parse(typeof(OrderInvoicingStatusContract), data.InvoicingStatus),
                    InvoiceId = data.InvoiceId,
                    InvoiceType = data.InvoiceType,
                    InvoiceNumber = data.InvoiceNumber
                },
                PaymentState = new OrderSubStateContract<OrderPaymentStatusContract>
                {
                    LastUpdateDate = data.PaymentStatusLastUpdateDate,
                    Status = (OrderPaymentStatusContract)Enum.Parse(typeof(OrderPaymentStatusContract), data.PaymentStatus)
                },
                ProvisioningState = new OrderSubStateContract<OrderProvisioningStatusContract>
                {
                    LastUpdateDate = data.ProvisioningStatusLastUpdateDate,
                    Status = (OrderProvisioningStatusContract)Enum.Parse(typeof(OrderProvisioningStatusContract), data.ProvisioningStatus)
                }
            };
        }

        private static ActivationMethodContract? GetActivationMethod(string activationMethodSource)
        {
            ActivationMethodContract activationMethod;
            if (Enum.TryParse(activationMethodSource, out activationMethod))
            {
                return activationMethod;
            }

            return null;
        }
    }
}