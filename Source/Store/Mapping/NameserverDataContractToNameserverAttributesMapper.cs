﻿using DomainManagement.Api.NameserverTemplates.Contracts;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.Mapping
{
    public static class NameserverDataContractToNameserverAttributesMapper
    {
        public static NameserverAttributes Map(NameserverContract template)
        {
            return new NameserverAttributes(template.Hostname, string.Empty);
        }
    }
}
