﻿using System.Linq;
using Store.Data;
using Store.Model.Orders;

namespace Store.Mapping
{
    public class OrderItemDataToModelMapper
    {
        private readonly PriceDataToModelMapper _priceMapper;

        public OrderItemDataToModelMapper()
        {
            _priceMapper = new PriceDataToModelMapper();
        }

        public OrderItem Map(OrderItemData data, decimal vatRatePercentage)
        {
            return new OrderItem()
            {
                OrderItemId = data.OrderItemID,
                Period = data.Period,
                ProductCode = data.ProductCode,
                ProductType = data.ProductType,
                ProductName = data.ProductName,
                PromoProductCode = data.PromoProductCode,
                Quantity = data.Quantity,
                TotalPrice = _priceMapper.Map(data.TotalPriceExclVat, data.TotalPriceReductionExclVat, vatRatePercentage),
                Price = _priceMapper.Map(data.PriceExclVat, data.PriceReductionExclVat, vatRatePercentage),
                Attributes = data.Attributes == null ? null : data.Attributes.ToDictionary(x => AttributeKeyMapper.Map(x.Key), x => x.Value),
                PeriodId = data.PeriodId,
                ParentId = data.ParentId,
                ProductId = data.ProductId,
                IsRecurring = data.IsRecurring,
                PromoCode = data.PromoCode
            };
        }
    }
}