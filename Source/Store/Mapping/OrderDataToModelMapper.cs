﻿using System;
using System.Linq;
using Intelligent.Core.Models;
using Store.API.Contracts;
using Store.Data;
using Store.Model.Orders;

namespace Store.Mapping
{
    public class OrderDataToModelMapper
    {
        private readonly OrderItemDataToModelMapper _orderItemMapper;
        private readonly PriceDataToModelMapper _priceMapper;
        private readonly OrderPaymentDataToModelMapper _paymentMapper;

        public OrderDataToModelMapper()
        {
            _orderItemMapper = new OrderItemDataToModelMapper();
            _priceMapper = new PriceDataToModelMapper();
            _paymentMapper = new OrderPaymentDataToModelMapper();
        }

        public Order Map(OrderData data)
        {
            var orderItems = data.Items.Select(c => _orderItemMapper.Map(c, data.VatRatePercentage));
            return new Order(
                data.OrderId,
                data.OrderCode,
                CustomerIdentifier.FromCustomerId(data.CustomerId),
                orderItems,
                _priceMapper.Map(data.TotalPriceExclVat, data.TotalPriceReductionExclVat, data.VatRatePercentage),
                data.VatRatePercentage,
                data.VatAmount,
                (CheckoutType)Enum.Parse(typeof(CheckoutType), data.CheckoutType, true),
                _paymentMapper.Map(data.CheckoutData),
                data.CustomerReference,
                data.Source,
                new Provider(data.ProviderId),
                (OrderStatus)Enum.Parse(typeof(OrderStatus), data.Status, true),
                data.TicketId,
                data.TicketNumber,
                data.CreationDate,
                data.EpayCode,
                data.NeedsPayment,
                (Language)Enum.Parse(typeof(Language), data.Language, true));
        }
    }
}