﻿namespace Store.Mapping
{
    public class OrderPaymentDataToModelMapper
    {
        public Model.Orders.CheckoutData Map(Data.CheckoutData data)
        {
            if (data == null)
                return null;

            return new Model.Orders.CheckoutData(data.PaidAmount, data.PaymentProviderReference);
        }
    }
}
