﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Intelligent.Core.Mappers;
using Intelligent.Core.Models;
using Intelligent.Shared.AutoMapper;
using Newtonsoft.Json;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.Converters;
using Store.API.Contracts.Orders;
using Store.Data;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Orders;

namespace Store.Mapping
{
    public static class MappingConfiguration
    {
        private static bool _isConfigured;

        public static void ConfigureMappings()
        {
            if (_isConfigured)
                return;
            _isConfigured = true;

            //Basket mappings
            Mapper.CreateMap<BasketReferenceData, BasketReferenceContract>()
                .ConstructUsing(basket => new BasketReferenceContract(
                    basket.OrderCode,
                    basket.CustomerIdentifier.CustomerNumber,
                    basket.ItemCount,
                    basket.CreationDate,
                    basket.BasketType))
                .ForMember(dest => dest.CustomerNumber, opt => opt.Ignore());

            Mapper.CreateMap<BasketContract, Basket>()
                .ConstructUsing(basket => new Basket(0,
                    basket.OrderCode,
                    CustomerIdentifier.FromCustomerNumber(basket.CustomerNumber),
                    null,
                    basket.VatRatePercentage,
                    basket.CreationDate,
                    false,
                    basket.LastValidationDate,
                    Provider.Combell,
                    BasketType.Regular,
                    basket.IsRecalculating,
                    null,
                    Language.Dutch,
                    false,
                    basket.CustomerReference))
                .ForMember(dest => dest.CustomerIdentifier, opt => opt.MapFrom(src => CustomerIdentifier.FromCustomerNumber(src.CustomerNumber)))
                .ForMember(dest => dest.BasketId, opt => opt.Ignore())
                .ForMember(dest => dest.Provider, opt => opt.Ignore())
                .ForMember(dest => dest.BasketType, opt => opt.Ignore())
                .ForMember(dest => dest.Language, opt => opt.Ignore())
                .ForMember(dest => dest.Source, opt => opt.Ignore())
                .ForMember(dest => dest.CheckedOut, opt => opt.Ignore())
                .ForMember(dest => dest.Deleted, opt => opt.Ignore());

            Mapper.CreateMap<Basket, BasketContract>()
                .ForMember(dest => dest.HasError, opt => opt.MapFrom(src => src.Items.Any(i => i.ValidationError != BasketItemValidationError.None)))
                .ForMember(dest => dest.CustomerNumber, opt => opt.MapFrom(src => src.CustomerIdentifier.CustomerNumber)).ReverseMap();

            Mapper.CreateMap<Basket, BasketSummaryContract>()
                .ForMember(dest => dest.HasError, opt => opt.MapFrom(src => src.Items.Any(i => i.ValidationError != BasketItemValidationError.None)))
                .ForMember(dest => dest.CustomerNumber, opt => opt.MapFrom(src => src.CustomerIdentifier.CustomerNumber));

            Mapper.CreateMap<BasketSummaryContract, Basket>()
                .ForMember(dest => dest.CustomerIdentifier, opt => opt.Ignore())
                .ForMember(dest => dest.VatAmount, opt => opt.Ignore())
                .ForMember(dest => dest.Items, opt => opt.Ignore())
                .ForMember(dest => dest.BasketId, opt => opt.Ignore())
                .ForMember(dest => dest.Provider, opt => opt.Ignore())
                .ForMember(dest => dest.CheckedOut, opt => opt.Ignore())
                .ForMember(dest => dest.Source, opt => opt.Ignore())
                .ForMember(dest => dest.BasketType, opt => opt.Ignore())
                .ForMember(dest => dest.Language, opt => opt.Ignore())
                .ForMember(dest => dest.Deleted, opt => opt.Ignore());


            Mapper.CreateMap<BasketItem, BasketItemContract>()
                .ForMember(dest => dest.PeriodUnitPrice, opt => opt.Ignore());

            Mapper.CreateMap<BasketItemContract, BasketItem>()
                .ForMember(dest => dest.TotalPrice, opt => opt.Ignore())
                .ForMember(dest => dest.PeriodId, opt => opt.Ignore())
                .ForMember(dest => dest.ProductId, opt => opt.Ignore())
                .ForMember(dest => dest.IsPromo, opt => opt.Ignore())
                .ForMember(dest => dest.IsFixedPrice, opt => opt.Ignore())
                .ForMember(dest => dest.PromoCode, opt => opt.Ignore());

            //Order mappings
            Mapper.CreateMap<OrderContract, Order>()
                .ForMember(dest => dest.VatRate, opt => opt.Ignore())
                .ForMember(dest => dest.OrderId, opt => opt.Ignore())
                .ForMember(dest => dest.Provider, opt => opt.Ignore())
                .ForMember(dest => dest.CustomerIdentifier, opt => opt.MapFrom(src => CustomerIdentifier.FromCustomerNumber(src.CustomerNumber)));
            Mapper.CreateMap<Order, OrderContract>()
                .ForMember(dest => dest.CustomerNumber, opt => opt.MapFrom(src => src.CustomerIdentifier.CustomerNumber))
                .ForMember(dest => dest.ProviderId, opt => opt.MapFrom(src => src.Provider.Id))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => LanguageToStringMapper.MapLanguage(src.Language)));
            Mapper.CreateMap<OrderItem, OrderItemContract>()
                .ForMember(dest => dest.Attributes, opt => opt.MapFrom(src => src.Attributes.ToDictionary(x => x.Key, x => AttributeValueFactory.Create(x.Key, x.Value)))).ReverseMap();

            //General objects for orders en baskets
            Mapper.CreateMap<OrderSummaryData, OrderSummaryContract>()
                .ForMember(dest => dest.CustomerNumber, opt => opt.Ignore())
                .ConstructUsing(order => new OrderSummaryContract(
                    CustomerIdentifier.FromCustomerId(order.CustomerId).CustomerNumber,
                    order.CompanyName,
                    order.FirstName,
                    order.LastName,
                    order.ProviderId,
                    order.OrderCode,
                    order.TotalPriceExclVat,
                    order.CreationDate,
                    (OrderStatus) Enum.Parse(typeof(OrderStatus), order.Status, true),
                    order.EpayCode,
                    order.NeedsPayment));
            Mapper.CreateMap<Price, PriceContract>();
            Mapper.CreateMap<PriceContract, Price>()
                .ForMember(dest => dest.InclVat, opt => opt.Ignore())
                .ForMember(dest => dest.VatRate, opt => opt.Ignore())
                .ForMember(dest => dest.ReductionInclVat, opt => opt.Ignore());

            Mapper.CreateMap<PeriodUnitType, PeriodUnitTypeContract>().ReverseMap();

            Mapper.CreateMap<Basket, Order>()
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.BasketId))
                .ForMember(dest => dest.CheckoutType, opt => opt.Ignore())
                .ForMember(dest => dest.CheckoutData, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore())
                .ForMember(dest => dest.CustomerReference, opt => opt.Ignore())
                .ForMember(dest => dest.TicketId, opt => opt.Ignore())
                .ForMember(dest => dest.TicketNumber, opt => opt.Ignore())
                .ForMember(dest => dest.EpayCode, opt => opt.Ignore())
                .ForMember(dest => dest.NeedsPayment, opt => opt.Ignore())
                .ConstructUsing(b => new Order(
                    b.BasketId,
                    b.OrderCode,
                    b.CustomerIdentifier,
                    b.Items.MapTo<List<OrderItem>>(),
                    b.TotalPrice,
                    b.VatRatePercentage,
                    b.VatAmount,
                    CheckoutType.None,
                    null,
                    string.Empty,
                    b.Source,
                    b.Provider,
                    OrderStatus.Created,
                    0,
                    string.Empty,
                    b.CreationDate,
                    string.Empty,
                    false,
                    b.Language));
            Mapper.CreateMap<BasketItem, OrderItem>()
                .ForMember(dest => dest.OrderItemId, opt => opt.MapFrom(src => src.BasketItemId))
                .ForMember(dest => dest.Identifier, opt => opt.MapFrom(src => src.Attributes.Identifier))
                .ForMember(dest => dest.Attributes, opt => opt.MapFrom(src => src.Attributes.ToDictionary(x => x.Key, x => JsonConvert.SerializeObject(x.Value))));

            Mapper.CreateMap<CheckoutDataContract, Messages.Payment.CheckoutData>();
            Mapper.CreateMap<CheckoutTypeContract, CheckoutType>()
                .ConvertUsing(input =>
                    (CheckoutType)Enum.Parse(typeof(CheckoutType), input.ToString()));
            Mapper.CreateMap<CheckoutTypeContract, Messages.Payment.CheckoutType>()
                .ConvertUsing(input =>
                    (Messages.Payment.CheckoutType)Enum.Parse(typeof(Messages.Payment.CheckoutType), input.ToString()));

            Mapper.CreateMap<Messages.Payment.CheckoutData, Model.Orders.CheckoutData>();
            Mapper.CreateMap<Messages.Payment.CheckoutType, CheckoutType>()
                .ConvertUsing(input =>
                    (CheckoutType)Enum.Parse(typeof(CheckoutType), input.ToString()));

            Mapper.CreateMap<Model.Orders.CheckoutData, CheckoutDataContract>().ReverseMap();
            Mapper.CreateMap<CheckoutType, CheckoutTypeContract>()
                .ConvertUsing(input =>
                    (CheckoutTypeContract)Enum.Parse(typeof(CheckoutTypeContract), input.ToString()));

            Mapper.AssertConfigurationIsValid();
        }
    }
}