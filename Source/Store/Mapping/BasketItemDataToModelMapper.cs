﻿using System;
using System.Linq;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.Converters;
using Store.Data;
using Store.Model.Baskets;

namespace Store.Mapping
{
    public class BasketItemDataToModelMapper
    {
        private readonly PriceDataToModelMapper _priceMapper;

        public BasketItemDataToModelMapper()
        {
            _priceMapper = new PriceDataToModelMapper();
        }

        public BasketItem Map(BasketItemData data, decimal vatRatePercentage)
        {
            return new BasketItem(
                data.BasketItemId,
                data.ProductId,
                data.ProductCode,
                data.ProductType,
                data.PromoProductCode,
                data.ProductName,
                data.Period,
                data.PeriodId,
                data.Quantity,
                _priceMapper.Map(data.PriceExclVat, data.PriceReductionExclVat, vatRatePercentage),
                data.Attributes == null ? null : new ItemAttributeDictionary(data.Attributes.
                    Select(x => new { Key = AttributeKeyMapper.Map(x.Key), x.Value }).
                    ToDictionary(
                        x => x.Key, 
                        x => AttributeValueFactory.Create(x.Key, x.Value))),
                vatRatePercentage,
                (BasketItemValidationError)Enum.Parse(typeof(BasketItemValidationError), data.ValidationError, true),
                (BasketItemWarning)Enum.Parse(typeof(BasketItemWarning), data.Warning, true),
                data.IsRecurring,
                data.IsFixedPrice,
                data.ParentId,
                data.PromoCode
            );
        }
    }
}
