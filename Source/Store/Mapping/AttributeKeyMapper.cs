﻿using System;
using Store.API.Contracts;
using Store.Model.Exceptions;

namespace Store.Mapping
{
    public class AttributeKeyMapper
    {
        public static AttributeKey Map(string key)
        {
            AttributeKey attributeKey;

            var itemAttribute = Enum.TryParse(key, true, out attributeKey);
            if (!itemAttribute)
                throw new InvalidBasketItemAttributeException(key, "Unknown key.");

            return attributeKey;
        }
    }
}
