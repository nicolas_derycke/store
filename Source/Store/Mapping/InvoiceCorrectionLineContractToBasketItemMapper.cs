﻿using Billing.API.Contracts;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Mapping
{
    public static class InvoiceCorrectionLineContractToBasketItemMapper
    {
        public static BasketItem Map(InvoiceCorrectionLineContract invoiceCorrectionLine, decimal vatRatePercentage, string domainName, int parentId, BasketItemValidationError validationError)
        {
            return new BasketItem(
                0,
                invoiceCorrectionLine.ProductId,
                invoiceCorrectionLine.ProductCode,
                invoiceCorrectionLine.ProductGroupCode,
                null,
                invoiceCorrectionLine.ProductName,
                invoiceCorrectionLine.NumberOfMonths,
                invoiceCorrectionLine.CorrectionProductPeriodId,
                -1,
                new Price(invoiceCorrectionLine.CorrectionPrice, 0, vatRatePercentage),
                new ItemAttributeDictionary() { 
                    { AttributeKey.Domain, new DomainItemAttributes(domainName) },
                    { AttributeKey.Compensation, new CompensationAttributes(invoiceCorrectionLine.CurrentInvoiceLineId) }
                },
                vatRatePercentage,
                validationError,
                BasketItemWarning.None,
                false,
                true,
                parentId);
        }
    }
}