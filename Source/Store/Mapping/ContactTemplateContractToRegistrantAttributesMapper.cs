﻿using System.Linq;
using DomainManagement.Api.ContactTemplates.Contracts;
using Store.API.Contracts.TypedItemAttributes;

namespace Store.Mapping
{
    public static class ContactTemplateContractToRegistrantAttributesMapper
    {
        public static RegistrantAttributes Map(ContactTemplateContract template)
        {
            var registrant = new RegistrantAttributes(
                template.ContactData.City,
                template.ContactData.Company,
                template.ContactData.Country,
                template.ContactData.Email,
                template.ContactData.Fax,
                template.ContactData.FirstName,
                template.ContactData.Mobile,
                template.ContactData.Language,
                template.ContactData.LastName,
                template.ContactData.PostalCode,
                template.ContactData.Address,
                template.ContactData.Phone,
                template.ContactData.HouseNumber,
                template.ContactData.Vat);

            registrant.ExtraFields = MapExtraFields(template);

            return registrant;
        }

        private static ExtraFieldAttributes MapExtraFields(ContactTemplateContract template)
        {
            var extraFields = new ExtraFieldAttributes(
                Enumerable.Empty<string>(),
                template.ContactData.CompanyNumber,
                template.ContactData.CodeFiscal,
                template.ContactData.PassportNumber,
                template.ContactData.TrademarkNumber,
                template.ContactData.TrademarkName,
                template.ContactData.TrademarkCountry);

            if (!extraFields.ContainsExtraField())
                return null;

            return extraFields;
        }
    }
}