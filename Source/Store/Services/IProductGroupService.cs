namespace Store.Services
{
    public interface IProductGroupService
    {
        bool IsDomain(string productGroupCode);
    }
}