﻿using System;
using System.Linq;
using Store.Model.Baskets;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.Services
{
    public class BasketPricingService : IBasketPricingService
    {
        private readonly IBundleService _bundleService;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IBasketItemPricingService _basketItemPricingService;

        public BasketPricingService(IBundleService bundleService, ISqlCommandHandler sqlCommandHandler, IBasketItemPricingService basketItemPricingService)
        {
            if (bundleService == null) throw new ArgumentNullException("bundleService");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (basketItemPricingService == null) throw new ArgumentNullException("basketItemPricingService");

            this._bundleService = bundleService;
            this._sqlCommandHandler = sqlCommandHandler;
            this._basketItemPricingService = basketItemPricingService;
        }

        public void ApplyBundles(Basket basket, int lastUpdateById)
        {
            // remove all free childs which are no longer free
            foreach(var freeChild in basket.Items.Where(i => !i.IsFixedPrice && i.Price.ExclVat == 0 && !i.IsPromo))
            {
                var bundle = _bundleService.GetBundle(basket, freeChild);

                if (!bundle.HasValue ||
                    bundle.Value.FreeChild != freeChild)
                {
                    _basketItemPricingService.SetProductInfoAndPrice(freeChild, basket.CustomerIdentifier, basket.Provider, basket.Language, basket.VatRatePercentage);
                    _sqlCommandHandler.Execute(new UpdateBasketItem(freeChild, lastUpdateById));
                }
            }

            // update all free childs to become free
            foreach (var basketItem in basket.Items)
            {
                var bundle = _bundleService.GetBundle(basket, basketItem);

                if (bundle.HasValue && 
                    bundle.Value.FreeChild == basketItem && 
                    basketItem.Price.ExclVat > 0)
                {
                    _basketItemPricingService.SetFreeBasketItem(basketItem);
                    _sqlCommandHandler.Execute(new UpdateBasketItem(basketItem, lastUpdateById));
                }
            }
        }
    }
}