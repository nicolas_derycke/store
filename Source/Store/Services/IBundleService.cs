using Intelligent.Shared.Core;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Services
{
    public interface IBundleService
    {
        Optional<Bundle> GetBundle(Basket basket, BasketItem child);
    }
}