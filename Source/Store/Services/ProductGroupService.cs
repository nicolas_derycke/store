﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using Intelligent.Shared.Core.Queries;
using Store.SqlQueries;

namespace Store.Services
{
    public class ProductGroupService : IProductGroupService
    {
        private readonly HashSet<string> _domainProductGroupCodes;
        private const string DomainProductGroupCodesKey = "Store.Services.ProductGroupService.DomainProductGroupCodes";
        private const int CacheExpirationInMinutes = 60;

        public ProductGroupService(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            if (MemoryCache.Default.Contains(DomainProductGroupCodesKey))
                _domainProductGroupCodes = (HashSet<string>) MemoryCache.Default.Get(DomainProductGroupCodesKey);
            else { 
                _domainProductGroupCodes = new HashSet<string>(queryHandler.Execute(new GetDomainNameProductGroupCodesQuery()));
                MemoryCache.Default.Add(DomainProductGroupCodesKey, _domainProductGroupCodes, new CacheItemPolicy()
                {
                    AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(CacheExpirationInMinutes)
                });
            }
        }

        public bool IsDomain(string productGroupCode)
        {
            return _domainProductGroupCodes.Contains(productGroupCode);
        }        
    }
}
