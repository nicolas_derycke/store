﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.Core.Queries;
using Serilog;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using Store.Proxies.Data;
using Store.SqlQueries;

namespace Store.Services
{
    public class PremiumDomainPriceService : IPremiumDomainPriceService
    {
        private const decimal ProfitRatio = 1.25m;

        private readonly IRrpProxy _rrpProxy;
        private readonly ILogger _logger;
        private readonly IQueryHandler _queryHandler;

        public PremiumDomainPriceService(
            IRrpProxy rrpProxy, 
            ILogger logger,
            IQueryHandler queryHandler)
        {
            if (rrpProxy == null) throw new ArgumentNullException("rrpProxy");
            if (logger == null) throw new ArgumentNullException("logger");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            _rrpProxy = rrpProxy;
            _logger = logger;
            _queryHandler = queryHandler;
        }

        public async Task<Optional<RrpProxyDomainPrice>> GetRrpProxyDomainPrice(BasketItem basketItem, Provider provider)
        {
            var domainName = basketItem.Attributes.Identifier;

            var premiumProductIds = _queryHandler.Execute(new GetProductsIdsByProductGroupCode("new-gtld-domeinnamen", provider));
            if (!premiumProductIds.Contains(basketItem.ProductId))
                return null;

            try
            {
                return await _rrpProxy.GetRrpProxyDomainPrice(domainName);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Could not get RrpProxy pricing for domain name {domainName}", domainName);
                return null;
            }
        }

        public PremiumDomainPrice CalculatePremiumDomainPrice(RrpProxyDomainPrice rrpProxyDomainPrice, decimal defaultPeriodPrice, int periodInMonths)
        {
            if (!rrpProxyDomainPrice.IsPremium)
                throw new Exception(string.Format("Domain is not premium"));

            var premiumDomainPrice = new PremiumDomainPrice(rrpProxyDomainPrice.SetupCost * ProfitRatio, rrpProxyDomainPrice.AnnualPrice * (periodInMonths / 12) * ProfitRatio, periodInMonths);

            if (defaultPeriodPrice >= premiumDomainPrice.PeriodPrice)
            {
                // premium price is not higher than standard price

                if (premiumDomainPrice.SetupPrice > 0)
                    // but there is a setup cost
                    return new PremiumDomainPrice(premiumDomainPrice.SetupPrice, defaultPeriodPrice, periodInMonths);
                
                return null;
            }

            return premiumDomainPrice;
        }
    }
}