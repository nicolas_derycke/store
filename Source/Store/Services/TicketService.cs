﻿using System;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.ServiceModel.Client;
using Store.Proxies;
using Ticketing.API.OrderQueue.Interface.Contracts.Data;
using Ticketing.API.OrderQueue.Interface.Contracts.Services;

namespace Store.Services
{
    public class TicketService : ITicketService
    {
        private readonly IServiceInvoker<ITicketingService> _ticketingService;
        private readonly ICustomerProxy _customerProxy;

        public TicketService(IServiceInvoker<ITicketingService> ticketingService, ICustomerProxy customerProxy)
        {
            if (ticketingService == null) throw new ArgumentNullException("ticketingService");
            if (customerProxy == null) throw new ArgumentNullException("customerProxy");

            this._ticketingService = ticketingService;
            this._customerProxy = customerProxy;
        }

        public async Task<TicketDataContract> CreateTicketForOrder(string orderCode, CustomerIdentifier customerIdentifier, Provider provider)
        {
            var customer = await _customerProxy.GetMinimalCustomer(customerIdentifier);
            var title =
                string.Format(
                "Order {0} submitted by {1} {2} {3}",
                orderCode,
                customer.FirstName,
                customer.LastName,
                customer.CompanyName);

            var ticket = _ticketingService.Invoke(t => t.CreateTicket(title, provider.Id));
            _ticketingService.Invoke(t => t.SetCustomerInformationForTicket(ticket.TicketId, customerIdentifier.CustomerId));

            return ticket;
        }
    }
}
