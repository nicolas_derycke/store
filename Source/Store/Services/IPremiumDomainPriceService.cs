using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies.Data;

namespace Store.Services
{
    public interface IPremiumDomainPriceService
    {
        Task<Optional<RrpProxyDomainPrice>> GetRrpProxyDomainPrice(BasketItem basketItem, Provider provider);

        PremiumDomainPrice CalculatePremiumDomainPrice(RrpProxyDomainPrice rrpProxyDomainPrice, decimal defaultPeriodPrice, int periodInMonths);
    }
}