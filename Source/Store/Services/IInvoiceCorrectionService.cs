using Billing.API.Contracts;
using Store.Model.Baskets;

namespace Store.Services
{
    public interface IInvoiceCorrectionService
    {
        void CreateBasketItems(InvoiceCorrectionContract invoiceCorrection, Basket basket, string identifier, BasketItem basketItem);
    }
}