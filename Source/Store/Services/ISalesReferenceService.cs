using System.Threading.Tasks;
using Intelligent.Core.Models;

namespace Store.Services
{
    public interface ISalesReferenceService
    {
        Task<string> Generate(CustomerIdentifier customerIdentifier, string source, bool isCheckedoutByEmployee);
    }
}