﻿using System;
using System.Threading.Tasks;
using Intelligent.Shared.Core.Queries;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.SqlQueries;

namespace Store.Services
{
    public class BasketService : IBasketService
    {
        private readonly IQueryHandler _queryHandler;

        public BasketService(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._queryHandler = queryHandler;
        }

        public async Task<Basket> GetBasketByOrderCode(string orderCode)
        {
            var basket = await _queryHandler.ExecuteAsync(new GetBasketByOrderCodeQuery(orderCode));

            if (!basket.HasValue || basket.Value.Deleted)
                throw new BasketNotFoundException(orderCode);

            return basket.Value;
        }

        public async Task<Basket> GetBasketByOrderCodeToValidate(string orderCode)
        {
            var basket = await _queryHandler.ExecuteAsync(new GetBasketByOrderCodeQuery(orderCode));
            if (!basket.HasValue)
                return null;

            return basket.Value;
        }
    }
}
