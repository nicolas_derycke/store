using System.Threading.Tasks;
using Intelligent.Core.Models;
using Ticketing.API.OrderQueue.Interface.Contracts.Data;

namespace Store.Services
{
    public interface ITicketService
    {
        Task<TicketDataContract> CreateTicketForOrder(string orderCode, CustomerIdentifier customerIdentifier, Provider provider);
    }
}