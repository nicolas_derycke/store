using System.Threading.Tasks;
using Store.Model.Baskets;

namespace Store.Services
{
    public interface IBasketService
    {
        Task<Basket> GetBasketByOrderCode(string orderCode);
        Task<Basket> GetBasketByOrderCodeToValidate(string orderCode);
    }
}