﻿using System;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Store.Proxies;

namespace Store.Services
{
    public class SalesReferenceService : ISalesReferenceService
    {
        private readonly ICustomerProxy _customerProxy;
        private readonly ICustomerInvoiceCriteriaProxy _customerInvoiceCriteriaProxy;

        public SalesReferenceService(ICustomerProxy customerProxy, ICustomerInvoiceCriteriaProxy customerInvoiceCriteriaProxy)
        {
            if (customerProxy == null) throw new ArgumentNullException("customerProxy");
            if (customerInvoiceCriteriaProxy == null) throw new ArgumentNullException("customerInvoiceCriteriaProxy");

            _customerProxy = customerProxy;
            _customerInvoiceCriteriaProxy = customerInvoiceCriteriaProxy;
        }

        public async Task<string> Generate(CustomerIdentifier customerIdentifier, string source, bool isCheckedoutByEmployee)
        {
            var salesReference = string.Empty;

            // part A
            if (await _customerProxy.IsNewCustomer(customerIdentifier))
                salesReference += "AA";
            else
                salesReference += "BB";

            var invoiceSettings = _customerInvoiceCriteriaProxy.GetByCustomer(customerIdentifier).Value;
            if (invoiceSettings.OneInvoicePerMonthForNewOrders)
                salesReference += "22";
            else
                salesReference += "11";

            if (isCheckedoutByEmployee)
                salesReference += "99"; // via tussenkomst van een online sales
            else if (source.ToLower().Contains("controlpanel"))
                salesReference += "55"; // via mijn combell
            else
                salesReference += "88"; // via website zonder tussenkomst

            return salesReference;
        }
    }
}