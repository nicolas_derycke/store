using Store.Model.Baskets;

namespace Store.Services
{
    public interface IBasketPricingService
    {
        void ApplyBundles(Basket basket, int lastUpdateById);
    }
}