﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Core.Models;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using Store.Providers;

namespace Store.Services
{
    public class BasketItemPricingService : IBasketItemPricingService
    {
        private readonly IProductInfoProvider _productInfoProvider;

        public BasketItemPricingService(IProductInfoProvider productInfoProvider)
        {
            if (productInfoProvider == null) throw new ArgumentNullException("productInfoProvider");

            _productInfoProvider = productInfoProvider;
        }

        public void SetProductInfoAndPrice(BasketItem basketItem, CustomerIdentifier customerIdentifier, Provider provider, Language language, decimal vatRatePercentage)
        {
            // periods of compensation do not match the periods of the products 
            // (e.g. a compensation can be for 11 months, product mgmt does not know the period 11 months)
            if (basketItem.IsCompensation)
                return;

            ProductInfo productInfo;

            try
            {
                var productInfoCollection = _productInfoProvider.GetProductInfo(basketItem, customerIdentifier, provider.Id, language).ToList();
                productInfo = GetProductInfo(productInfoCollection, basketItem);
            }
            catch (ProductNotFoundException)
            {
                basketItem.ValidationError = BasketItemValidationError.InactiveProduct;
                return;
            }
            catch (InvalidProductPeriodSpecifiedException)
            {
                basketItem.ValidationError = BasketItemValidationError.InactiveProductPeriod;
                return;
            }

            if(productInfo == null) // don't fail if product or period is inactive
            { 
                basketItem.ValidationError = BasketItemValidationError.InactiveProduct;
                return;
            }

            var newPrice = new Price(productInfo.PeriodPrice, productInfo.Reduction, vatRatePercentage);

            if (basketItem.IsFixedPrice)
                newPrice = basketItem.Price;

            basketItem.SetProductInfoAndPrice(productInfo, newPrice);
        }

        public void SetPremiumDomainFixedPrice(BasketItem basketItem, decimal fixedPriceExclVat)
        {
            basketItem.SetFixedPrice(fixedPriceExclVat);
            basketItem.Attributes.Add(AttributeKey.PremiumDomain, new PremiumDomainAttribute());
        }

        public void SetCompensationFixedPrice(BasketItem basketItem, decimal fixedPriceExclVat, int invoiceLineId)
        {
            basketItem.SetFixedPrice(fixedPriceExclVat);
            basketItem.Attributes.Add(AttributeKey.Compensation, new CompensationAttributes(invoiceLineId));
        }

        public void SetFixedPrice(BasketItem basketItem, decimal fixedPriceExclVat)
        {
            basketItem.SetFixedPrice(fixedPriceExclVat);
        }

        public void SetPromoFixedPrice(BasketItem basketItem, decimal promoPriceExclVat, string promoCode)
        {
            if (promoPriceExclVat > basketItem.Price.ExclVat) // do not increase price in case of a promo
                return;

            basketItem.SetPromoPrice(promoPriceExclVat, promoCode);
        }

        public void SetFreeBasketItem(BasketItem basketItem)
        {
            basketItem.SetFreeBasketItem();
        }

        private ProductInfo GetProductInfo(IEnumerable<ProductInfo> productInfos, BasketItem basketItem)
        {
            var renewalProductInfo = productInfos.First(p => p.ProductAction == ProductAction.Renewal);
            if (renewalProductInfo.ProductType != ProductTypes.DomainName)
                return renewalProductInfo;

            var preferedAction = basketItem.Attributes.ContainsKey(AttributeKey.TransferDomain) ? ProductAction.Transfer : ProductAction.Register;
            if (productInfos.Any(p => p.ProductAction == preferedAction))
                return productInfos.First(p => p.ProductAction == preferedAction);

            return renewalProductInfo;
        }
    }
}
