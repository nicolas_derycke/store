using Intelligent.Core.Models;
using Store.Model.Baskets;

namespace Store.Services
{
    public interface IBasketItemPricingService
    {
        void SetFreeBasketItem(BasketItem basketItem);
        void SetProductInfoAndPrice(BasketItem basketItem, CustomerIdentifier customerIdentifier, Provider provider, Language language, decimal vatRatePercentage);
        void SetPremiumDomainFixedPrice(BasketItem basketItem, decimal fixedPriceExclVat);
        void SetFixedPrice(BasketItem basketItem, decimal fixedPriceExclVat);
        void SetCompensationFixedPrice(BasketItem basketItem, decimal fixedPriceExclVat, int invoiceLineId);
        void SetPromoFixedPrice(BasketItem basketItem, decimal fixedPromoPriceExclVat, string promoCode);        
    }
}