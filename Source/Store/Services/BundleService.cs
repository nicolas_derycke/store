﻿using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Core;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Services
{
    public class BundleService : IBundleService
    {
        private static readonly HashSet<string> SharedHostingProductTypes = new HashSet<string>() 
        { 
            ProductTypes.LinuxSharedHosting, 
            ProductTypes.WindowsSharedHosting,
            ProductTypes.Drupal,
            ProductTypes.Joomla,
            ProductTypes.Magento,
            ProductTypes.Wordpress,
            ProductTypes.Sitebuilder
        };

        private static readonly Dictionary<string, HashSet<string>> _productCodeProductTypesMapping = new Dictionary<string, HashSet<string>>()
        {
            { "domeinnaam-be", SharedHostingProductTypes },
            { "domeinnaam-be-promo", SharedHostingProductTypes },
            { "domeinnaam-nl", SharedHostingProductTypes },
            { "domeinnaam-nl-promo", SharedHostingProductTypes },
            { "domeinnaam-com", SharedHostingProductTypes },
            { "domeinnaam-com-promo", SharedHostingProductTypes },
            { "domeinnaam-eu", SharedHostingProductTypes },
            { "domeinnaam-eu-promo", SharedHostingProductTypes },
            { "domeinnaam-net", SharedHostingProductTypes },
            { "domeinnaam-net-promo", SharedHostingProductTypes },
            { "domeinnaam-info", SharedHostingProductTypes },
            { "domeinnaam-info-promo", SharedHostingProductTypes },
            { "domeinnaam-biz", SharedHostingProductTypes },
            { "domeinnaam-biz-promo",SharedHostingProductTypes },
            { "domeinnaam-org",  SharedHostingProductTypes },
            { "domeinnaam-org-promo", SharedHostingProductTypes }   
        
        };

        public Optional<Bundle> GetBundle(Basket basket, BasketItem basketItem)
        {
            if (_productCodeProductTypesMapping.ContainsKey(basketItem.ProductCode))
                return GetByChild(basket, basketItem);

            if (SharedHostingProductTypes.Contains(basketItem.ProductType))
                return GetByParent(basket, basketItem);

            return Optional<Bundle>.Empty;
        }

        private Optional<Bundle> GetByParent(Basket basket, BasketItem parent)
        {
            var childs = GetChilds(basket, parent);

            if (!childs.Any())
                return Optional<Bundle>.Empty;

            var freeChild = GetFreeChild(childs);
            return new Bundle(parent, childs.ToList(), freeChild);
        }

        private Optional<Bundle> GetByChild(Basket basket, BasketItem child)
        {
            var productTypes = _productCodeProductTypesMapping[child.ProductCode];

            var parent = basket.Items.FirstOrDefault(i => productTypes.Any(pt => pt == i.ProductType) && i.Attributes.Identifier == child.Attributes.Identifier);
            if (parent == null)
                return Optional<Bundle>.Empty;

            return GetByParent(basket, parent);
        }

        private BasketItem GetFreeChild(IEnumerable<BasketItem> childs)
        {
            var freeChild = childs.FirstOrDefault(c => c.Price.ExclVat == 0);
            if (freeChild == null)
                freeChild = childs.First();
            return freeChild;
        }

        private IEnumerable<BasketItem> GetChilds(Basket basket, BasketItem parent)
        {
            var childProductCodes = _productCodeProductTypesMapping.Where(kv => kv.Value.Any(pt => pt == parent.ProductType)).Select(kv => kv.Key);
            return basket.Items.Where(i => childProductCodes.Any(pt => pt == i.ProductCode) && parent.Attributes.Identifier == i.Attributes.Identifier && !i.IsFixedPrice && i.Period == 12);
        }
    }
}