﻿using System;
using System.Linq;
using Billing.API.Contracts;
using Intelligent.Core.Users;
using Intelligent.Shared.Messaging.CommandBus;
using Store.API.Contracts.Baskets;
using Store.Commands.BasketItems;
using Store.Mapping;
using Store.Model.Baskets;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.Services
{
    public class InvoiceCorrectionService : IInvoiceCorrectionService
    {
        private readonly ICommandBus _commandBus;
        private readonly ISqlCommandHandler _sqlCommandHandler;

        public InvoiceCorrectionService(
            ICommandBus commandBus,
            ISqlCommandHandler sqlCommandHandler)
        {
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");

            this._commandBus = commandBus;
            this._sqlCommandHandler = sqlCommandHandler;
        }

        public void CreateBasketItems(InvoiceCorrectionContract invoiceCorrection, Basket basket, string identifier, BasketItem basketItem)
        {
            // when re-applying compensation, first remove the current compensation items
            var currentCompensationItems = basket.GetChildBasketItems(basketItem.BasketItemId).ToList();
            basket.Items.RemoveAll(i => currentCompensationItems.Any(c => c.BasketItemId == i.BasketItemId));
            currentCompensationItems.ForEach(c => _sqlCommandHandler.Execute(new RemoveBasketItem(c.BasketItemId, SystemUser.Id)));

            var compensationBasketItems = Enumerable.Empty<BasketItem>();
            switch (invoiceCorrection.Status)
            {
                case InvoiceCorrectionStatusContract.ValidCorrection:
                    compensationBasketItems = 
                        invoiceCorrection.CorrectionLines.Select(cl =>
                            InvoiceCorrectionLineContractToBasketItemMapper.Map(cl, basket.VatRatePercentage, identifier, basketItem.BasketItemId, BasketItemValidationError.None));
                        break;
                case InvoiceCorrectionStatusContract.NegativeCorrection:
                    compensationBasketItems = 
                        invoiceCorrection.CorrectionLines.Select(cl =>
                            InvoiceCorrectionLineContractToBasketItemMapper.Map(cl, basket.VatRatePercentage, identifier, basketItem.BasketItemId, BasketItemValidationError.NegativeBasketItemPrice));
                    break;
                case InvoiceCorrectionStatusContract.CurrentInvoiceLineNotFound:
                case InvoiceCorrectionStatusContract.RenewalInvoiceFound:
                    // we won't apply any compensation, customer has to complain (this is verified by Nick)
                    break;
                default:
                    throw new Exception(string.Format("Unknown invoice correction status '{0}'", invoiceCorrection.Status));
            }

            foreach (var compensationBasketItem in compensationBasketItems)
            {
                basket.Items.Add(compensationBasketItem);
                _commandBus.Send(new CreateBasketItemCommand(basket, compensationBasketItem, SystemUser.Id));
            }
        }
    }
}