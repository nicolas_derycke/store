﻿namespace Store.Data
{
    public class ProductPeriodVisibility
    {
        public int ProductId { get; private set; }
        public bool IsProductActive { get; private set; }
        public bool IsProductVow { get; private set; }
        public bool IsProductPeriodVow { get; private set; }
        public bool IsProductGroupTreeVow { get; private set; }

        public ProductPeriodVisibility(int productId, bool isProductActive, bool isProductVow, bool isProductPeriodVow, bool isProductGroupTreeVow)
        {
            ProductId = productId;
            IsProductActive = isProductActive;
            IsProductVow = isProductVow;
            IsProductPeriodVow = isProductPeriodVow;
            IsProductGroupTreeVow = isProductGroupTreeVow;
        }
    }
}