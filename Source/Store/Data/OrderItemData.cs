﻿using System.Collections.Generic;

namespace Store.Data
{
    public class OrderItemData
    {
        public OrderItemData()
        {
            Attributes = new Dictionary<string, string>();
        }

        public int OrderItemID { get; set; }
        public int OrderId { get; set; }
        public string ProductCode { get; set; }
        public string ProductType { get; set; }
        public string PromoProductCode { get; set; }
        public string ProductName { get; set; }
        public int Period { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPriceInclVat { get; set; }
        public decimal TotalPriceExclVat { get; set; }
        public decimal TotalPriceReductionInclVat { get; set; }
        public decimal TotalPriceReductionExclVat { get; set; }
        public decimal PriceInclVat { get; set; }
        public decimal PriceExclVat { get; set; }
        public decimal PriceReductionInclVat { get; set; }
        public decimal PriceReductionExclVat { get; set; }
        public Dictionary<string, string> Attributes { get; set; }
        public int PeriodId { get; set; }
        public bool IsRecurring { get; set; }
        public int? ParentId { get; set; }
        public int ProductId { get; set; }
        public string PromoCode { get; set; }
    }
}
