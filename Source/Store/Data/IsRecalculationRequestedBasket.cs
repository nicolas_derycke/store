﻿namespace Store.Data
{
    public class IsRecalculationRequestedBasket
    {
        public IsRecalculationRequestedBasket(string orderCode, bool isRecalculationRequested)
        {
            OrderCode = orderCode;
            IsRecalculationRequested = isRecalculationRequested;
        }

        public string OrderCode { get; private set; }
        public bool IsRecalculationRequested { get; private set; }
    }
}
