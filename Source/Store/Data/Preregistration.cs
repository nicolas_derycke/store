﻿using System;

namespace Store.Data
{
    public class Preregistration
    {
        public int DomainId { get; private set; }
        public int CustomerId { get; private set; }
        public string DomainName { get; private set; }
        public DateTime QueuedForLandrushDate { get; private set; }
        public DateTime QueuedForGoliveDate { get; private set; }
        
        public Preregistration(int domainId, int customerId, string domainName, DateTime queuedForLandrushDate, DateTime queuedForGoliveDate)
        {
            this.DomainId = domainId;
            this.CustomerId = customerId;
            this.DomainName = domainName;
            this.QueuedForLandrushDate = queuedForLandrushDate;
            this.QueuedForGoliveDate = queuedForGoliveDate;
        }
    }
}