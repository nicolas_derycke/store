﻿using Store.Model;

namespace Store.Data
{
    public class ProductDetailsData
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductGroupCode { get; set; }
        public decimal PeriodPrice { get; set; }
        public decimal Reduction { get; set; }
        public int Period { get; set; }
        public int PeriodId { get; set; }
        public ProductAction Action { get; set; }
        public bool IsRecurring { get; set; }

        public ProductDetailsData(int productId, string productName, string productGroupCode, decimal periodPrice, decimal reduction, int period, int periodId, int actionId)
        {
            ProductId = productId;
            ProductName = productName;
            ProductGroupCode = productGroupCode;
            PeriodPrice = periodPrice;
            Reduction = reduction;
            Period = period;
            PeriodId = periodId;
            Action = (ProductAction)actionId;
            IsRecurring = true;
        }

        public ProductDetailsData()
        {
            IsRecurring = true;
        }
    }
}