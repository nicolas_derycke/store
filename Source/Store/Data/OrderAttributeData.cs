﻿namespace Store.Data
{
    public class OrderAttributeData
    {
        public int OrderItemId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}