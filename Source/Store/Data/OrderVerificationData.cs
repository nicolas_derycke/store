﻿using System;

namespace Store.Data
{
    public class OrderVerificationData
    {
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public string Rule { get; set; }
        public string Result { get; set; }
        public DateTime CreationDate { get; set; }
    }
}