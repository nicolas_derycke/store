﻿using System;

namespace Store.Data
{
    public class OrderStateData
    {
        public string OrderCode { get; set; }
        public string OrderStatus { get; set; }
        public string InitiatedBy { get; set; }
        public bool IsInitiatedByEmployee { get; set; }
        public string ActivationMethod { get; set; }
        public string ActivationReason { get; set; }
        public string ActivationBy { get; set; }
        public string ActivationStatus { get; set; }
        public DateTime ActivationStatusLastUpdateDate { get; set; }
        public string InvoicingStatus { get; set; }
        public DateTime InvoicingStatusLastUpdateDate { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime PaymentStatusLastUpdateDate { get; set; }
        public string ProvisioningStatus { get; set; }
        public DateTime ProvisioningStatusLastUpdateDate { get; set; }
        public int? InvoiceId { get; set; }
        public string InvoiceType { get; set; }
        public string InvoiceNumber { get; set; }
    }
}