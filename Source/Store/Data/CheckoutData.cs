﻿namespace Store.Data
{
    public class CheckoutData
    {
        public decimal PaidAmount { get; private set; }

        public string PaymentProviderReference { get; private set; }
    }
}