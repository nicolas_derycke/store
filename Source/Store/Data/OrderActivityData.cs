﻿using System;

namespace Store.Data
{
    public class OrderActivityData
    {
        public DateTime OccuredOn { get; set; }
        public string Message { get; set; }
    }
}