﻿using System;
using Intelligent.Core.Models;
using Store.API.Contracts.Baskets;

namespace Store.Data
{
    public class BasketReferenceData
    {
        public string OrderCode { get; set; }
        public CustomerIdentifier CustomerIdentifier { get; set; }
        public int ItemCount { get; set; }
        public DateTime CreationDate { get; set; }
        public BasketType BasketType { get; set; }

        public BasketReferenceData(string orderCode, int customerId, DateTime creationDate, int itemCount, string basketType)
        {
            OrderCode = orderCode;
            CustomerIdentifier = CustomerIdentifier.FromCustomerId(customerId);
            CreationDate = creationDate;
            ItemCount = itemCount;
            BasketType = (BasketType)Enum.Parse(typeof(BasketType), basketType, true);
        }
    }
}