﻿namespace Store.Data
{
    public class DomainProductExtension
    {
        public DomainProductExtension(int extensionId, string extensionWithDot)
        {
            this.ExtensionId = extensionId;
            this.ExtensionWithDot = extensionWithDot;
        }

        public int ExtensionId { get; private set; }
        public string ExtensionWithDot { get; private set; }
    }
}