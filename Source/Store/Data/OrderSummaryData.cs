﻿using System;

namespace Store.Data
{
    public class OrderSummaryData
    {
        public int CustomerId { get; private set; }
        public string CompanyName { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public int ProviderId { get; private set; }
        public string OrderCode { get; private set; }
        public decimal TotalPriceExclVat { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string Status { get; private set; }
        public int TotalResults { get; set; }
        public string EpayCode { get; set; }
        public bool NeedsPayment { get; set; }
        public string CheckoutType { get; private set; }

        public OrderSummaryData(int customerId, string companyName, string firstName, string lastName, int providerId, string orderCode, decimal totalPriceExclVat, DateTime creationDate, string status, string epayCode, bool needsPayment, string checkoutType, int totalResults)
        {
            CustomerId = customerId;
            CompanyName = companyName;
            FirstName = firstName;
            LastName = lastName;
            ProviderId = providerId;
            OrderCode = orderCode;
            TotalPriceExclVat = totalPriceExclVat;
            CreationDate = creationDate;
            Status = status;
            TotalResults = totalResults;
            EpayCode = epayCode;
            NeedsPayment = needsPayment;
            CheckoutType = checkoutType;
        }
    }
}