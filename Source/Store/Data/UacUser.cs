﻿namespace Store.Data
{
    public class UacUser
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string UserName { get; private set; }
        
        public string Name
        {
            get
            {
                if (!string.IsNullOrEmpty(FirstName) || !string.IsNullOrEmpty(LastName))
                    return string.Format("{0} {1}", FirstName, LastName).Trim();
                else
                    return UserName.Trim();
            }
        }
        public UacUser(int userId, int customerId, int providerId, string userName, string firstName, string lastName, string email, bool isAdmin, bool isActive)
        {
            FirstName = firstName;
            LastName = lastName;
            UserName = userName;
        }
    }
}