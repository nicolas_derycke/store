﻿namespace Store.Data
{
    public class OrderPaymentData
    {
        public OrderPaymentData(string epayCode, bool needsPayment)
        {
            EpayCode = epayCode;
            NeedsPayment = needsPayment;
        }

        public string EpayCode { get; private set; }
        public bool NeedsPayment { get; private set; }
    }
}