﻿namespace Store.Data
{
    public class BasketAttributeData
    {
        public int BasketItemId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}