﻿using System;
using System.Collections.Generic;

namespace Store.Data
{
    public class BasketData
    {
        public decimal VatRatePercentage { get; set; }
        public int CreationById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdateById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? CustomerId { get; set; }
        public string OrderCode { get; set; }
        public List<BasketItemData> Items { get; set; }
        public int ItemAmount { get; set; }
        public int BasketId { get; set; }
        public bool CheckedOut { get; set; }
        public DateTime LastValidationDate { get; set; }
        public int ProviderId { get; set; }
        public string BasketType { get; set; }
        public bool IsRecalculating { get; set; }
        public string Source { get; set; }
        public string Language { get; set; }
        public bool Deleted { get; set; }
        public string CustomerReference { get; set; }
    }
}
