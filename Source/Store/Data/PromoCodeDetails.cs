﻿namespace Store.Data
{
    public class PromoCodeDetailData
    {
        public string PromoCode { get; set; }
        public int PeriodId { get; set; }
        public decimal Price { get; set; }
        public decimal StandardPrice { get; set; }
        public decimal DiscountPercentage { get; set; }
        public int Period { get; set; }
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public int ProductGroupId { get; set; }

        public PromoCodeDetailData(string code, int periodeId, decimal price, decimal standardPrice, decimal discountPercentage, int periode, string productName, int productId, int productGroupId)
        {
            PromoCode = code;
            PeriodId = periodeId;
            Price = price;
            StandardPrice = standardPrice;
            DiscountPercentage = discountPercentage;
            Period = periode;
            ProductName = productName;
            ProductId = productId;
            ProductGroupId = productGroupId;
        }
    }
}