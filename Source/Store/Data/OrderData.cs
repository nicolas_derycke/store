﻿using System;
using System.Collections.Generic;

namespace Store.Data
{
    public class OrderData
    {
        public int OrderId { get; set; }
        public decimal TotalPriceInclVat { get; set; }
        public decimal TotalPriceExclVat { get; set; }
        public decimal TotalPriceReductionInclVat { get; set; }
        public decimal TotalPriceReductionExclVat { get; set; }
        public decimal VatRatePercentage { get; set; }
        public decimal VatAmount { get; set; }
        public int CreationById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdateById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int CustomerId { get; set; }
        public string OrderCode { get; set; }
        public string CheckoutType { get; set; }
        public string CustomerReference { get; set; }
        public string Source { get; set; }
        public int ProviderId { get; set; }
        public string Status { get; set; }
        public int TicketId { get; set; }
        public string TicketNumber { get; set; }

        public List<OrderItemData> Items { get; set; }
        public CheckoutData CheckoutData { get; set; }
        public string EpayCode { get; private set; }
        public bool NeedsPayment { get; set; }
        public string Language { get; set; }
    }
}
