﻿using Store.Model.Baskets;

namespace Store.Commands.BasketItems
{
    public class AddFixedCostsCommand
    {
        public BasketItem BasketItem { get; private set; }

        public Basket Basket { get; private set; }

        public AddFixedCostsCommand(BasketItem basketItem, Basket basket)
        {
            BasketItem = basketItem;
            Basket = basket;
        }
    }
}
