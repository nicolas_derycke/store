﻿using System;
using Store.Model.Baskets;

namespace Store.Commands.BasketItems
{
    public class ChangeBasketItemPeriodCommand
    {
        public Basket Basket { get; private set; }
        public int BasketItemId { get; private set; }
        public int NewPeriod { get; private set; }
        public int ProviderId { get; private set; }
        public int LastUpdatedById { get; private set; }

        public ChangeBasketItemPeriodCommand(Basket basket, int basketItemId, int newPeriod, int providerId, int lastUpdatedById)
        {
            if (basket == null) throw new ArgumentNullException("basket");

            Basket = basket;
            BasketItemId = basketItemId;
            NewPeriod = newPeriod;
            ProviderId = providerId;
            LastUpdatedById = lastUpdatedById;
        }
    }
}