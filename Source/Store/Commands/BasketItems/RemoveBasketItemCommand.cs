﻿using Store.Model.Baskets;

namespace Store.Commands.BasketItems
{
    public class RemoveBasketItemCommand
    {
        public Basket Basket { get; private set; }
        public BasketItem BasketItem { get; private set; }
        public int DeletionById { get; private set; }

        public RemoveBasketItemCommand(Basket basket, BasketItem basketItem, int deletionById)
        {
            Basket = basket;
            BasketItem = basketItem;
            DeletionById = deletionById;
        }
    }
}