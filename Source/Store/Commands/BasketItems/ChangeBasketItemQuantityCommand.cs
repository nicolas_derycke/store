﻿using Store.Model.Baskets;

namespace Store.Commands.BasketItems
{
    public class ChangeBasketItemQuantityCommand
    {
        public Basket Basket { get; private set; }
        public BasketItem BasketItem { get; private set; }
        public int NewQuantity { get; private set; }
        public int LastUpdatedById { get; private set; }

        public ChangeBasketItemQuantityCommand(Basket basket, BasketItem basketItem, int newQuantity, int lastUpdatedById)
        {
            Basket = basket;
            BasketItem = basketItem;
            NewQuantity = newQuantity;
            LastUpdatedById = lastUpdatedById;
        }
    }
}