﻿using Store.Model.Baskets;

namespace Store.Commands.BasketItems
{
    public class AddAddonUpgradeCompensationCommand
    {      
        public BasketItem BasketItem { get; private set; }

        public Basket Basket { get; private set; }

        public AddAddonUpgradeCompensationCommand(BasketItem basketItem, Basket basket)
        {
            BasketItem = basketItem;
            Basket = basket;
        }
    }
}