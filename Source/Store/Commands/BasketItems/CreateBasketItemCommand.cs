﻿using System;
using Store.Model.Baskets;

namespace Store.Commands.BasketItems
{
    public class CreateBasketItemCommand
    {
        public BasketItem BasketItem { get; private set; }
        public int CreationById { get; private set; }
        public Basket Basket { get; private set; }

        public CreateBasketItemCommand(
            Basket basket,
            BasketItem basketItem,  
            int creationById)
        {
            if (basketItem == null) throw new ArgumentNullException("basketItem");

            BasketItem = basketItem;
            CreationById = creationById;
            Basket = basket;
        }
    }
}
