﻿using Store.Model.Baskets;

namespace Store.Commands.BasketItems
{
    public class AddHostingUpgradeCompensationCommand
    {
        public BasketItem BasketItem { get; private set; }

        public Basket Basket { get; private set; }

        public AddHostingUpgradeCompensationCommand(BasketItem basketItem, Basket basket)
        {
            BasketItem = basketItem;
            Basket = basket;
        }
    }
}
