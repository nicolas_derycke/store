﻿using Intelligent.Shared.Core;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Commands.BasketItems
{
    public class AddPremiumDomainOneTimeFeeCommand
    {
        public AddPremiumDomainOneTimeFeeCommand(Optional<PremiumDomainPrice> premiumDomainPrice, Basket basket, BasketItem premiumDomainBasketItem, int creationByid)
        {
            this.PremiumDomainPrice = premiumDomainPrice;
            this.Basket = basket;
            this.PremiumDomainBasketItem = premiumDomainBasketItem;
            this.CreationById = creationByid;
        }

        public Optional<PremiumDomainPrice> PremiumDomainPrice { get; private set; }
        public Basket Basket { get; private set; }
        public BasketItem PremiumDomainBasketItem { get; private set; }
        public int CreationById { get; private set; }
    }
}