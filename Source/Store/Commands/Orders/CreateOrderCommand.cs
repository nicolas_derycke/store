﻿using System;
using Store.Model.Orders;

namespace Store.Commands.Orders
{
    public class CreateOrderCommand
    {
        public Order Order { get; private set; }
        public int CreationById { get; private set; }

        public CreateOrderCommand(Order order, int creationById)
        {
            if (order == null) throw new ArgumentNullException("order");

            Order = order;
            CreationById = creationById;
        }        
    }
}
