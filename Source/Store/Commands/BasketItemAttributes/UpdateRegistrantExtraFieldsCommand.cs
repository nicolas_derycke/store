﻿using System.Collections.Generic;
using Store.API.Contracts;
using Store.Model.Baskets;

namespace Store.Commands.BasketItemAttributes
{
    public class UpdateRegistrantExtraFieldsCommand
    {
        public Basket Basket { get; private set; }
        public IEnumerable<ExtraFieldsContract> ExtraFields { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateRegistrantExtraFieldsCommand(Basket basket, IEnumerable<ExtraFieldsContract> extraFields, int lastUpdateById)
        {
            Basket = basket;
            ExtraFields = extraFields;
            LastUpdateById = lastUpdateById;
        }
    }
}