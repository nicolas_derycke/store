﻿using Store.API.Contracts;
using Store.Model.Baskets;

namespace Store.Commands.BasketItemAttributes
{
    public class UpdateBasketItemAttributeCommand
    {
        public Basket Basket { get; private set; }
        public BasketItem BasketItem { get; private set; }
        public AttributeKey AttributeKey { get; private set; }
        public IBasketItemAttribute AttributeValue { get; private set; }
        public int LastUpdateById { get; private set; }
        
        public UpdateBasketItemAttributeCommand(Basket basket, BasketItem basketItem, AttributeKey attributeKey, IBasketItemAttribute attributeValue, int lastUpdateById)
        {
            Basket = basket;
            BasketItem = basketItem;
            AttributeKey = attributeKey;
            AttributeValue = attributeValue;
            LastUpdateById = lastUpdateById;
        }
    }
}