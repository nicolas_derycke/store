﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Store.Model.Baskets;

namespace Store.Commands.Baskets
{
    public class ChangeBasketCustomerReferenceCommand
    {
        public Basket Basket { get; private set; }
        public string CustomerReference { get; private set; }
        public int LastUpdatedById { get; private set; }

        public ChangeBasketCustomerReferenceCommand(Basket basket, string customerReference, int lastUpdatedById)
        {
            if (basket == null) throw new ArgumentNullException("basket");

            Basket = basket;
            CustomerReference = customerReference;
            LastUpdatedById = lastUpdatedById;
        }
    }
}
