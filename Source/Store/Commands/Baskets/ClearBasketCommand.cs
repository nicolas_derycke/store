﻿using Store.Model.Baskets;

namespace Store.Commands.Baskets
{
    public class ClearBasketCommand
    {
        public ClearBasketCommand(Basket basket, int deletionById)
        {
            this.Basket = basket;
            this.DeletionById = deletionById;
        }

        public Basket Basket { get; private set; }

        public int DeletionById { get; private set; }
    }
}