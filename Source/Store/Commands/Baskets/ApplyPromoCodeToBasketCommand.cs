﻿using Store.Model.Baskets;

namespace Store.Commands.Baskets
{
    public class ApplyPromoCodeToBasketCommand
    {
        public Basket Basket { get; private set; }
        public int LastUpdatedById { get; private set; }
        public string PromoCode { get; private set; }
        public CommandResult Result { get; set; }

        public ApplyPromoCodeToBasketCommand(string promoCode, Basket basket, int lastUpdatedById)
        {
            Basket = basket;
            LastUpdatedById = lastUpdatedById;
            PromoCode = promoCode;
            Result = new CommandResult(0);
        }

        public class CommandResult
        {
            public int AffectedItems{ get; private set; }

            public CommandResult(int affectedItems)
            {
                AffectedItems = affectedItems;
            }
        }
    }
}