﻿using System;
using Intelligent.Core.Models;
using Store.Model.Baskets;

namespace Store.Commands.Baskets
{
    public class ChangeBasketCustomerCommand
    {
        public Basket Basket { get; private set; }
        public CustomerIdentifier CustomerIdentifier { get; private set; }
        public int LastUpdatedById { get; private set; }

        public bool Force { get; private set; }

        public ChangeBasketCustomerCommand(Basket basket, CustomerIdentifier newCustomerIdentifier, int lastUpdatedById, bool force = false)
        {
            if (basket == null) throw new ArgumentNullException("basket");
            if (newCustomerIdentifier == null) throw new ArgumentNullException("newCustomerIdentifier");

            Basket = basket;
            CustomerIdentifier = newCustomerIdentifier;
            LastUpdatedById = lastUpdatedById;
            Force = force;
        }
    }
}