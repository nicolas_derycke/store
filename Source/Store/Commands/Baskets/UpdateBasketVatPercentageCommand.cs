﻿using System.Data;
using Dapper;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Data;

namespace Store.Commands.Baskets
{
    public class UpdateBasketVatPercentageCommand : ICommand
    {
        public string OrderCode { get; private set; }
        public decimal VatRatePercentage { get; private set; }
        public int LastUpdateById { get; private set; }

        public UpdateBasketVatPercentageCommand(string orderCode, decimal vatRatePercentage, int lastUpdateById)
        {
            OrderCode = orderCode;
            VatRatePercentage = vatRatePercentage;
            LastUpdateById = lastUpdateById;
        }

        public void Execute()
        {
            using (var connection = SqlConnectionFactory.CreateCombellGroupConnection())
                connection.Execute(
                    "store.UpdateBasketVatPercentage",
                    new
                    {
                        OrderCode,
                        VatRatePercentage,
                        LastUpdateById
                    },
                    commandType: CommandType.StoredProcedure);
        }
    }
}