﻿using System.Collections.Generic;
using Intelligent.Core.Models;
using Store.API.Contracts.Baskets;

namespace Store.Commands.Baskets
{
    public class CreateBasketCommand
    {
        public BasketType BasketType { get; private set; }
        public CustomerIdentifier CustomerIdentifier { get; private set; }
        public Provider Provider { get; private set; }
        public int CreationById { get; private set; }
        public Language Language { get; private set; }
        public IEnumerable<BasketItemContract> BasketItems { get; private set; }
        public string Source { get; private set; }

        public CreateBasketCommand(
            BasketType basketType, 
            CustomerIdentifier customerIdentifier, 
            Provider provider, 
            int creationById, 
            Language language, 
            IEnumerable<BasketItemContract> basketItems, 
            string source)
        {
            BasketType = basketType;
            CustomerIdentifier = customerIdentifier;
            Provider = provider;
            CreationById = creationById;
            Language = language;
            BasketItems = basketItems;
            Source = source;
        }

        public CommandResult Result { get; set; }

        public class CommandResult
        {
            public int BasketId { get; private set; }
            public string OrderCode { get; private set; }

            public CommandResult(int basketId, string orderCode)
            {
                BasketId = basketId;
                OrderCode = orderCode;
            }
        }
    }
}