﻿using Store.Model.Baskets;

namespace Store.Commands.Baskets
{
    public class EvaluateTaxPercentageCommand
    {
        public Basket Basket { get; private set; }

        public EvaluateTaxPercentageCommand(Basket basket)
        {
            Basket = basket;
        }
    }
}