﻿using Store.Model.Baskets;

namespace Store.Commands.Baskets
{
    public class MergeBasketsCommand
    {
        public Basket Basket { get; private set; }
        public int LastUpdateById { get; private set; }

        public MergeBasketsCommand(Basket basket, int lastUpdateById)
        {
            Basket = basket;
            LastUpdateById = lastUpdateById;
        }
    }
}