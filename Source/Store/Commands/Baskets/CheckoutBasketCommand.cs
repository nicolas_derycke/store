﻿using System;
using Store.Messages.Payment;
using Store.Model.Baskets;

namespace Store.Commands.Baskets
{
    public class CheckoutBasketCommand
    {
        public Basket Basket { get; private set; }
        public int UserId { get; private set; }
        public string UserName { get; set; }
        public bool IsByEmployee { get; set; }
        public CheckoutType CheckoutType { get; private set; }
        public CheckoutData CheckoutData { get; private set; }
        public string CustomerReference { get; private set; }

        public CheckoutBasketCommand(Basket basket, CheckoutType checkoutType, CheckoutData checkoutData, string customerReference, int userId, string userName, bool isByEmployee)
        {
            if (basket == null) throw new ArgumentNullException("basket");
            Basket = basket;
            CheckoutType = checkoutType;
            CheckoutData = checkoutData;
            CustomerReference = customerReference;
            UserId = userId;
            UserName = userName;
            IsByEmployee = isByEmployee;
        }
    }
}