﻿using Billing.API.Common.Interface.Contracts.Services;
using Combell.RCS.Webservice.Interface;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Data;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.ServiceModel.Client;
using Microsoft.Practices.Unity;
using Store.Helpers;
using Store.Providers;
using Store.Proxies;
using Store.Services;
using Store.SqlCommands;
using Store.Warnings;

namespace Store
{
    public static class UnityHelper
    {
        /// <summary>
        /// Registers all types which are shared between the API and the service.
        /// </summary>
        /// <param name="container"></param>
        public static void ConfigureContainer(IUnityContainer container)
        {
            container.RegisterType<ISqlCommandHandler, SqlCommandHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IQueryHandler, QueryHandler>(new ContainerControlledLifetimeManager());
            container.RegisterType<IProductGroupService, ProductGroupService>(new ContainerControlledLifetimeManager());
            container.RegisterType<ITrademarkExistanceProxy, TrademarkExistanceProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<IProductResellerProfileProvider, ProductResellerProfileProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBasketService, BasketService>(new ContainerControlledLifetimeManager());
            container.RegisterType<ISqlConnectionProvider, SqlConnectionProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<IUacProxy, UacProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<IProductInfoProvider, ProductInfoProvider>(new ContainerControlledLifetimeManager());
            container.RegisterInstance<IServiceInvoker<IRegistryService>>(new ServiceInvoker<IRegistryService>("BasicHttpBinding_IRegistryService"), new ContainerControlledLifetimeManager());
            container.RegisterType<IRequiredExtraFieldsProvider, RequiredExtraFieldsProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICustomerProxy, CustomerProxy>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICustomerInvoiceCriteriaProxy, CustomerInvoiceCriteriaProxy>(new ContainerControlledLifetimeManager());
            container.RegisterInstance<IServiceInvoker<ICustomerService>>(new ServiceInvoker<ICustomerService>("Billing_Common_CustomerServiceConfiguration"));
            container.RegisterType<IPremiumDomainPriceService, PremiumDomainPriceService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBasketItemPricingService, BasketItemPricingService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBasketPricingService, BasketPricingService>(new ContainerControlledLifetimeManager());

            container.RegisterTypesWhichImplement<IWarningProvider>();

            container.ConfigureCommandBus();
        }
    }
}