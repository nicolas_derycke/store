﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Threading;
using Serilog;
using Store.API.Contracts.Baskets;
using Store.Commands.Baskets;
using Store.Model.Baskets;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;
using Store.Validation;

namespace Store.CommandHandlers.Baskets
{
    public class MergeBasketsCommandHandler : ICommandHandler<MergeBasketsCommand>
    {
        private readonly IQueryHandler _queryHandler;
        private readonly IBasketService _basketService;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IEnumerable<ILiveBasketItemValidator> _liveValidators;
        private readonly ICommandBus _commandBus;
        private readonly ILogger _logger;

        public MergeBasketsCommandHandler(
            IQueryHandler queryHandler,
            IBasketService basketService,
            ISqlCommandHandler sqlCommandHandler,
            IEnumerable<ILiveBasketItemValidator> liveValidators, 
            ICommandBus commandBus, 
            ILogger logger)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (basketService == null) throw new ArgumentNullException("basketService");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (liveValidators == null) throw new ArgumentNullException("liveValidators");
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (logger == null) throw new ArgumentNullException("logger");

            _queryHandler = queryHandler;
            _basketService = basketService;
            _sqlCommandHandler = sqlCommandHandler;
            _liveValidators = liveValidators;
            _commandBus = commandBus;
            _logger = logger;
        }

        public void Handle(MergeBasketsCommand command)
        {
            var basket = command.Basket;

            if (basket.BasketType == BasketType.Standalone)
                return;

            _logger.Information("Start merging basket with ordercode {OrderCode}", basket.OrderCode);
            var customersOtherBaskets = AsyncPump.Await(async () =>
                await Task.WhenAll(_queryHandler.
                    Execute(new GetBasketsByCustomerQuery(basket.CustomerIdentifier)).
                    Where(b => b.BasketType == BasketType.Regular).
                    Select(b => b.OrderCode).
                    Where(oc => oc != basket.OrderCode).
                    Select(oc => _basketService.GetBasketByOrderCode(oc)).
                    ToList()));

            foreach (var otherBasket in customersOtherBaskets)
            {
                _logger.Information("Other basket found with ordercode {OrderCode} to merge.", otherBasket.OrderCode);
                foreach (var basketItem in otherBasket.Items.Where(i => !i.ParentId.HasValue && !basket.ContainsDuplicateBasketItem(i)))
                {
                    var childBasketItems = otherBasket.Items.Where(i => i.ParentId == basketItem.BasketItemId).ToList();
                    MergeBasketItem(basket, basketItem, command.LastUpdateById);
                    // merge childs (childs don't get merged if parent isn't merged)
                    foreach (var childBasketItem in childBasketItems)
                    {
                        childBasketItem.ParentId = basketItem.BasketItemId;
                        MergeBasketItem(basket, childBasketItem, command.LastUpdateById);
                    }
                }

                _sqlCommandHandler.Execute(new DeleteBasket(otherBasket.BasketId, command.LastUpdateById));
                _logger.Information("Other basket with ordercode {OrderCode} deleted after merge.", otherBasket.OrderCode);
            }

            _commandBus.Send(new EvaluateTaxPercentageCommand(basket));
        }

        private void MergeBasketItem(Basket basket, BasketItem basketItem, int lastUpdateById)
        {
            basket.Items.Add(basketItem);

            basketItem.SetBasketItemValidationErrors(_liveValidators.SelectMany(v => v.Validate(basketItem, basket)).ToList());

            _sqlCommandHandler.Execute(new CreateBasketItem(basket, basketItem, lastUpdateById));

            if (basketItem.Attributes != null)
                foreach (var attribute in basketItem.Attributes)
                    _sqlCommandHandler.Execute(new CreateBasketItemAttribute(basketItem.BasketItemId, attribute.Key, attribute.Value, lastUpdateById));
        }
    }
}