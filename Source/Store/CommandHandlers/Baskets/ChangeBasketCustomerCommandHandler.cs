﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Threading;
using Store.API.Contracts.Baskets;
using Store.Commands.Baskets;
using Store.Providers.Attributes;
using Store.Proxies;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.SqlCommands.Baskets;
using Store.Validation;
using Store.Warnings;
using Serilog;

namespace Store.CommandHandlers.Baskets
{
    public class ChangeBasketCustomerCommandHandler : ICommandHandler<ChangeBasketCustomerCommand>
    {
        private readonly ICustomerProxy _customerProxy;
        private readonly IBasketItemPricingService _basketItemPricingService;
        private readonly IEnumerable<IAttributeProvider> _attributeProviders;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IEnumerable<IWarningProvider> _warningProviders;
        private readonly IEnumerable<ILiveBasketItemValidator> _liveValidators;
        private readonly ICommandBus _commandBus;
        private readonly ILogger _logger;

        public ChangeBasketCustomerCommandHandler(
            ICustomerProxy customerProxy,
            IBasketItemPricingService basketItemPricingService,
            IEnumerable<IAttributeProvider> attributeProviders,
            ISqlCommandHandler sqlCommandHandler,
            IEnumerable<IWarningProvider> warningProviders,
            IEnumerable<ILiveBasketItemValidator> liveValidators,
            ICommandBus commandBus,
            ILogger logger)
        {
            if (customerProxy == null) throw new ArgumentNullException("customerProxy");
            if (basketItemPricingService == null) throw new ArgumentNullException("basketItemPricingService");
            if (attributeProviders == null) throw new ArgumentNullException("attributeProviders");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (warningProviders == null) throw new ArgumentNullException("warningProviders");
            if (liveValidators == null) throw new ArgumentNullException("liveValidators");
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (logger == null) throw new ArgumentNullException("logger");

            _customerProxy = customerProxy;
            _basketItemPricingService = basketItemPricingService;
            _attributeProviders = attributeProviders;
            _sqlCommandHandler = sqlCommandHandler;
            _warningProviders = warningProviders;
            _liveValidators = liveValidators;
            _commandBus = commandBus;
            _logger = logger;
        }

        public void Handle(ChangeBasketCustomerCommand command)
        {
            _logger.Information("Setting the customer {customerNumber} on the basket {orderCode}", command.CustomerIdentifier.CustomerNumber, command.Basket.OrderCode);

            var basket = command.Basket;
            var customerIdentifier = command.CustomerIdentifier;

            if (!basket.IsAnonymousBasket)
            {
                if (basket.CustomerIdentifier.CustomerNumber != customerIdentifier.CustomerNumber)
                    throw new NotSupportedException("Basket already belongs to another customer.");
            }

            if (basket.IsAnonymousBasket || command.Force)
            {
                var vatRatePercentage = AsyncPump.Await(async () => await _customerProxy.GetVatRatePercentage(customerIdentifier));

                basket.ChangeCustomer(command.CustomerIdentifier);

                _sqlCommandHandler.Execute(new UpdateBasketCustomer(basket.OrderCode, basket.CustomerIdentifier, vatRatePercentage, command.LastUpdatedById));

                foreach (var basketItem in basket.Items)
                {
                    foreach (var attributeProvider in _attributeProviders)
                        attributeProvider.Get(basket, basketItem).
                            WhenHasValue(attribute =>
                                {
                                    basketItem.Attributes.Add(attribute);
                                    _sqlCommandHandler.Execute(new CreateBasketItemAttribute(basketItem.BasketItemId, attribute.Key, attribute.Value, command.LastUpdatedById));
                                });

                    basketItem.SetBasketItemWarnings(_warningProviders.SelectMany(w => w.Validate(basketItem, basket)).ToList());

                    basketItem.SetBasketItemValidationErrors(_liveValidators.SelectMany(v => v.Validate(basketItem, basket)).ToList());

                    if (basketItem.ValidationError != BasketItemValidationError.InactiveProduct && basketItem.ValidationError != BasketItemValidationError.InactiveProductPeriod)
                        _basketItemPricingService.SetProductInfoAndPrice(basketItem, customerIdentifier, basket.Provider, basket.Language, vatRatePercentage);

                    _sqlCommandHandler.Execute(new UpdateBasketItem(basketItem, command.LastUpdatedById));
                }

                _commandBus.Send(new MergeBasketsCommand(basket, command.LastUpdatedById));
            }
        }
    }
}