﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Intelligent.Shared.Messaging.CommandBus;
using Serilog;
using Store.Commands.Baskets;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;

namespace Store.CommandHandlers.Baskets
{
    public class ChangeBasketCustomerReferenceCommandHandler : ICommandHandler<ChangeBasketCustomerReferenceCommand>
    {
        private ISqlCommandHandler _sqlCommandHandler;
        private ILogger _logger;

        public ChangeBasketCustomerReferenceCommandHandler(
            ISqlCommandHandler sqlCommandHandler,
            ILogger logger)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (logger == null) throw new ArgumentNullException("logger");

            _logger = logger;
            _sqlCommandHandler = sqlCommandHandler;
        }

        public void Handle(ChangeBasketCustomerReferenceCommand command)
        {
            var basket = command.Basket;
            _logger.Information(string.Format("Saving customer reference '{0}' on basket with ordercode '{1}'", command.CustomerReference, command.Basket.OrderCode));
            _sqlCommandHandler.Execute(new UpdateBasketCustomerReference(basket.BasketId, command.CustomerReference, command.LastUpdatedById));
        }
    }
}
