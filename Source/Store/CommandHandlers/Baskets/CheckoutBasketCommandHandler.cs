using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Threading;
using MassTransit;
using Serilog;
using Store.Commands.Baskets;
using Store.Messages.Events;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;
using System;

namespace Store.CommandHandlers.Baskets
{
    public class CheckoutBasketCommandHandler : ICommandHandler<CheckoutBasketCommand>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IBus _bus;
        private readonly ILogger _logger;

        public CheckoutBasketCommandHandler(
            ISqlCommandHandler sqlCommandHandler,
            IBus bus,
            ILogger logger)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (bus == null) throw new ArgumentNullException("bus");
            if (logger == null) throw new ArgumentNullException("logger");

            _sqlCommandHandler = sqlCommandHandler;
            _bus = bus;
            _logger = logger;
        }

        public void Handle(CheckoutBasketCommand command)
        {
            var basket = command.Basket;
            _logger.Information("Checking out basket with ordercode {OrderCode}", basket.OrderCode);
            _sqlCommandHandler.Execute(new CheckoutBasket(basket.OrderCode, command.UserId));

            AsyncPump.Await(async () => await _bus.Publish(
                new CheckoutCompleted(
                    basket.OrderCode,
                    basket.CustomerIdentifier.CustomerId,
                    command.CheckoutType,
                    command.CheckoutData,
                    command.CustomerReference,
                    DateTime.Now,
                    command.UserId,
                    command.UserName,
                    command.IsByEmployee)));
        }
    }
}