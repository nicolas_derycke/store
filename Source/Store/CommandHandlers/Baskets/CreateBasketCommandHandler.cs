﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Threading;
using Store.API.Contracts.Baskets;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Model.Baskets;
using Store.Providers;
using Store.Proxies;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;
using Store.SqlQueries;

namespace Store.CommandHandlers.Baskets
{
    public class CreateBasketCommandHandler : ICommandHandler<CreateBasketCommand>
    {
        private readonly ICustomerProxy _customerProxy;
        private readonly IOrderCodeProvider _orderCodeProvider;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly ICommandBus _commandBus;
        private readonly IQueryHandler _queryHandler;
        private readonly IBasketPricingService _basketPricingService;
        private readonly IBasketService _basketService;

        public CreateBasketCommandHandler(
            ICustomerProxy customerProxy,
            IOrderCodeProvider orderCodeProvider,
            ISqlCommandHandler sqlCommandHandler,
            ICommandBus commandBus,
            IQueryHandler queryHandler,
            IBasketPricingService basketPricingService,
            IBasketService basketService)
        {
            if (customerProxy == null) throw new ArgumentNullException("customerProxy");
            if (orderCodeProvider == null) throw new ArgumentNullException("orderCodeProvider");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (basketPricingService == null) throw new ArgumentNullException("basketPricingService");
            if (basketService == null) throw new ArgumentNullException("basketService");

            _customerProxy = customerProxy;
            _orderCodeProvider = orderCodeProvider;
            _sqlCommandHandler = sqlCommandHandler;
            _commandBus = commandBus;
            _queryHandler = queryHandler;
            _basketPricingService = basketPricingService;
            _basketService = basketService;
        }

        public void Handle(CreateBasketCommand command)
        {
            var customerIdentifier = command.CustomerIdentifier;
            var language = command.Language;
            var provider = command.Provider;

            var vatRatePercentage = AsyncPump.Await(async () => await _customerProxy.GetVatRatePercentage(customerIdentifier));

            Basket basket = null;
            if (customerIdentifier != null && command.BasketType == BasketType.Regular)
            {
                var customerBaskets =
                    _queryHandler.
                        Execute(new GetBasketsByCustomerQuery(customerIdentifier)).
                        Where(b => b.BasketType == BasketType.Regular);

                if (customerBaskets.Any())
                    basket = AsyncPump.Await(async () => await _basketService.GetBasketByOrderCode(customerBaskets.First().OrderCode));
            }

            if (basket == null)
            {
                var newOrderCode = _orderCodeProvider.GenerateOrderCode(provider.Triplet);

                var createBasket = new CreateBasket(newOrderCode, customerIdentifier, vatRatePercentage, provider, command.BasketType, command.Source, language, command.CreationById);
                _sqlCommandHandler.Execute(createBasket);

                basket =
                    new Basket(
                        createBasket.BasketId,
                        newOrderCode,
                        customerIdentifier,
                        new List<BasketItem>(),
                        vatRatePercentage,
                        DateTime.Now,
                        false,
                        DateTime.Now,
                        provider,
                        command.BasketType,
                        false,
                        command.Source,
                        language,
                        false,
                        "");
            }

            foreach (var basketItemContract in command.BasketItems)
            {
                var basketItem = new BasketItem(basketItemContract.ProductCode,
                                                basketItemContract.PromoProductCode,
                                                basketItemContract.Period,
                                                basketItemContract.Quantity,
                                                basketItemContract.Attributes,
                                                vatRatePercentage,
                                                true);

                basket.Items.Add(basketItem);

                _commandBus.Send(new CreateBasketItemCommand(basket, basketItem, command.CreationById));
            }

            _basketPricingService.ApplyBundles(basket, command.CreationById);

            command.Result = new CreateBasketCommand.CommandResult(basket.BasketId, basket.OrderCode);
        }
    }
}