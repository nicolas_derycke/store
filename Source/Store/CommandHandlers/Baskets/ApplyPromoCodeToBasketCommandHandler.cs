using System;
using System.Linq;
using Intelligent.Shared.Messaging.CommandBus;
using Store.API.Contracts.Baskets;
using Store.Commands.Baskets;
using Store.Providers;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.CommandHandlers.Baskets
{
    public class ApplyPromoCodeToBasketCommandHandler : ICommandHandler<ApplyPromoCodeToBasketCommand>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IPromoCodeInfoProvider _promoCodeInfoProvider;
        private readonly IBasketItemPricingService _basketItemPricingService;

        public ApplyPromoCodeToBasketCommandHandler(
            ISqlCommandHandler sqlCommandHandler,
            IPromoCodeInfoProvider promoCodeInfoProvider,
            IBasketItemPricingService basketItemPricingService)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (promoCodeInfoProvider == null) throw new ArgumentNullException("promoCodeInfoProvider");
            if (basketItemPricingService == null) throw new ArgumentNullException("basketItemPricingService");

            _sqlCommandHandler = sqlCommandHandler;
            _promoCodeInfoProvider = promoCodeInfoProvider;
            _basketItemPricingService = basketItemPricingService;
        }

        public void Handle(ApplyPromoCodeToBasketCommand command)
        {
            var basket = command.Basket;
            var affectedItems = 0;

            if (basket.BasketType == BasketType.Standalone)
                return;

            // get promocode details
            var promoInfo = _promoCodeInfoProvider.GetPromoCodeInfo(command.PromoCode, command.Basket.Provider.Id);

            // remove other codes
            foreach (var basketItem in basket.Items.Where(bi => bi.IsPromo))
            {
                _basketItemPricingService.SetProductInfoAndPrice(basketItem, basket.CustomerIdentifier, basket.Provider, basket.Language, basket.VatRatePercentage);
                _sqlCommandHandler.Execute(new UpdateBasketItem(basketItem, command.LastUpdatedById));
            }

            // apply and store new promocode
            foreach (var promoProduct in promoInfo.PromoProducts)
            {
                var promoBasketItem = basket.Items.FirstOrDefault(b => b.PeriodId == promoProduct.PeriodId && !b.IsFixedPrice && b.Price.ExclVat > 0);

                if (promoBasketItem != null)
                {
                    _basketItemPricingService.SetPromoFixedPrice(promoBasketItem, promoProduct.PeriodPrice, command.PromoCode);
                    _sqlCommandHandler.Execute(new UpdateBasketItem(promoBasketItem, command.LastUpdatedById));
                    affectedItems++;
                }
            }

            command.Result = new ApplyPromoCodeToBasketCommand.CommandResult(affectedItems);
        }
    }
}