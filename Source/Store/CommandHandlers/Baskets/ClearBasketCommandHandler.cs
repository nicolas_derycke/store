﻿using Intelligent.Shared.Messaging.CommandBus;
using Store.Commands.Baskets;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using System;

namespace Store.CommandHandlers.Baskets
{
    public class ClearBasketCommandHandler : ICommandHandler<ClearBasketCommand>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;

        public ClearBasketCommandHandler(ISqlCommandHandler sqlCommandHandler)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");

            _sqlCommandHandler = sqlCommandHandler;
        }

        public void Handle(ClearBasketCommand command)
        {
            var basket = command.Basket;

            foreach (var basketItem in basket.Items)
                _sqlCommandHandler.Execute(new RemoveBasketItem(basketItem.BasketItemId, command.DeletionById));

            basket.Items.Clear();
        }
    }
}