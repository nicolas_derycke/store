﻿using System;
using Intelligent.Shared.Collections;
using Intelligent.Shared.Messaging.CommandBus;
using Store.Commands.Orders;
using Store.SqlCommands;
using Store.SqlCommands.Baskets;
using Store.SqlCommands.Orders;

namespace Store.CommandHandlers.Orders
{
    public class CreateOrderCommandHandler : ICommandHandler<CreateOrderCommand>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;

        public CreateOrderCommandHandler(ISqlCommandHandler sqlCommandHandler)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");

            _sqlCommandHandler = sqlCommandHandler;
        }

        public void Handle(CreateOrderCommand command)
        {
            var order = command.Order;
            _sqlCommandHandler.Execute(new CreateOrder(order, command.CreationById));

            foreach (var orderItem in order.Items.ToSafeEnumerable())
            {
                _sqlCommandHandler.Execute(new CreateOrderItem(orderItem, order.OrderId, command.CreationById));

                foreach (var basketItemAttribute in orderItem.Attributes.ToSafeEnumerable())
                    _sqlCommandHandler.Execute(new CreateOrderItemAttribute(basketItemAttribute, orderItem.OrderItemId, command.CreationById));
            }

            if (order.CheckoutData != null)
                _sqlCommandHandler.Execute(new CreateOrderPayment(order.OrderId, order.CheckoutData, command.CreationById));
        }
    }
}