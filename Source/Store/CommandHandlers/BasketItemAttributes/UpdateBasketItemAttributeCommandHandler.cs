﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Core.Commands;
using Intelligent.Shared.Messaging.CommandBus;
using Store.Commands.BasketItemAttributes;
using Store.Model.Exceptions;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.Validation;
using Store.Warnings;

namespace Store.CommandHandlers.BasketItemAttributes
{
    public class UpdateBasketItemAttributeCommandHandler : ICommandHandler<UpdateBasketItemAttributeCommand>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IEnumerable<ILiveBasketItemValidator> _liveValidators;
        private readonly IEnumerable<IWarningProvider> _warningProviders;

        public UpdateBasketItemAttributeCommandHandler(
            ISqlCommandHandler sqlCommandHandler,
            IEnumerable<ILiveBasketItemValidator> liveValidators,
            IEnumerable<IWarningProvider> warningProviders)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (liveValidators == null) throw new ArgumentNullException("liveValidators");
            if (warningProviders == null) throw new ArgumentNullException("warningProviders");

            _sqlCommandHandler = sqlCommandHandler;
            _liveValidators = liveValidators;
            _warningProviders = warningProviders;
        }

        public void Handle(UpdateBasketItemAttributeCommand command)
        {
            var basket = command.Basket;
            var basketItem = command.BasketItem;
            var childBasketItem = basket.Items.FirstOrDefault(i => i.ParentId == basketItem.BasketItemId);

            DuplicateSslHostnameValidator.ThrowIfInvalid(basket, basketItem);

            ICommand sqlCommandBasketItem;
            if (basketItem.Attributes.ContainsKey(command.AttributeKey))
            {
                basketItem.Attributes[command.AttributeKey] = command.AttributeValue;
                sqlCommandBasketItem = new UpdateBasketItemAttribute(basketItem.BasketItemId, command.AttributeKey, command.AttributeValue, command.LastUpdateById);
            }
            else
            {
                basketItem.Attributes.Add(command.AttributeKey, command.AttributeValue);
                sqlCommandBasketItem = new CreateBasketItemAttribute(basketItem.BasketItemId, command.AttributeKey, command.AttributeValue, command.LastUpdateById);
            }

            ICommand sqlCommandChildBasketItem = null;
            if (childBasketItem != null)
            {
                if (childBasketItem.Attributes.ContainsKey(command.AttributeKey))
                {
                    childBasketItem.Attributes[command.AttributeKey] = command.AttributeValue;
                    sqlCommandChildBasketItem = new UpdateBasketItemAttribute(childBasketItem.BasketItemId, command.AttributeKey,
                        command.AttributeValue, command.LastUpdateById);
                }
            }


            // add warnings & validation errors
            basketItem.SetBasketItemWarnings(_warningProviders.SelectMany(w => w.Validate(basketItem, basket)).ToList());
            basketItem.SetBasketItemValidationErrors(_liveValidators.SelectMany(v => v.Validate(basketItem, basket)).ToList());

            if (basket.ContainsDuplicateBasketItem(basketItem))
                throw new DuplicateBasketItemException(basketItem.ProductCode);

            _sqlCommandHandler.Execute(new UpdateBasketItem(basketItem, command.LastUpdateById));
            _sqlCommandHandler.Execute(sqlCommandBasketItem);

            if(sqlCommandChildBasketItem != null)
                _sqlCommandHandler.Execute(sqlCommandChildBasketItem);

        }
    }
}