﻿using System;
using System.Collections.Generic;
using Intelligent.Shared.Messaging.CommandBus;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Commands.BasketItemAttributes;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.CommandHandlers.BasketItemAttributes
{
    public class UpdateRegistrantExtraFieldsCommandHandler : ICommandHandler<UpdateRegistrantExtraFieldsCommand>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;

        public UpdateRegistrantExtraFieldsCommandHandler(
            ISqlCommandHandler sqlCommandHandler)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");

            _sqlCommandHandler = sqlCommandHandler;
        }

        public void Handle(UpdateRegistrantExtraFieldsCommand command)
        {
            var basket = command.Basket;

            foreach (var extraField in command.ExtraFields)
            {
                foreach (var basketItemId in extraField.BasketItemIds)
                {
                    var basketItem = basket.GetBasketItem(basketItemId);

                    var registrant = basketItem.Attributes.GetBasketItemAttribute<RegistrantAttributes>(AttributeKey.Registrant);
                    registrant.ExtraFields = extraField.ExtraFields;

                    basketItem.SetBasketItemWarnings(new List<BasketItemWarning>());

                    _sqlCommandHandler.Execute(new UpdateBasketItemAttribute(basketItem.BasketItemId, AttributeKey.Registrant, registrant, command.LastUpdateById));
                    _sqlCommandHandler.Execute(new UpdateBasketItem(basketItem, command.LastUpdateById));

                }
            }
        }
    }
}