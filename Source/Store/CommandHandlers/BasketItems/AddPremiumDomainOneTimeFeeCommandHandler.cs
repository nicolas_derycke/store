﻿using Intelligent.Shared.Messaging.CommandBus;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Commands.BasketItems;
using Store.Model.Baskets;
using Store.Services;
using System;

namespace Store.CommandHandlers.BasketItems
{
    public class AddPremiumDomainOneTimeFeeCommandHandler : ICommandHandler<AddPremiumDomainOneTimeFeeCommand>
    {
        private readonly IBasketItemPricingService _basketItemPricingService;
        private readonly ICommandBus _commandBus;

        public AddPremiumDomainOneTimeFeeCommandHandler(IBasketItemPricingService basketItemPricingService, ICommandBus commandBus)
        {
            if (basketItemPricingService == null) throw new ArgumentNullException("basketItemPricingService");
            if (commandBus == null) throw new ArgumentNullException("commandBus");

            this._basketItemPricingService = basketItemPricingService;
            this._commandBus = commandBus;
        }

        public void Handle(AddPremiumDomainOneTimeFeeCommand command)
        {
            var premiumDomainPrice = command.PremiumDomainPrice;
            var basket = command.Basket;
            var premiumDomainBasketItem = command.PremiumDomainBasketItem;

            if (premiumDomainPrice.HasValue && premiumDomainPrice.Value.SetupPrice > 0)
            {
                var extraBasketItem = new BasketItem(
                                            "eenmalige-activatie-premium-domeinnaam",
                                            string.Empty,
                                            0,
                                            1,
                                            new ItemAttributeDictionary() {
                                                            { AttributeKey.Domain, new DomainItemAttributes(premiumDomainBasketItem.Attributes.Identifier) }
                                                },
                                            basket.VatRatePercentage,
                                            false,
                                            premiumDomainBasketItem.BasketItemId);

                _basketItemPricingService.SetFixedPrice(extraBasketItem, premiumDomainPrice.Value.SetupPrice);

                _commandBus.Send(new CreateBasketItemCommand(basket, extraBasketItem, command.CreationById));
            }
        }
    }
}