using System;
using Intelligent.Core.Users;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Threading;
using Serilog;
using Store.Commands.Baskets;
using Store.Proxies;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.CommandHandlers.BasketItems
{
    public class EvaluateTaxPercentageCommandHandler : ICommandHandler<EvaluateTaxPercentageCommand>
    {
        private const decimal MaxAmountWithVatForEuropeanInstitution = 123.95m;
        private readonly ICustomerInvoiceCriteriaProxy _customerInvoiceCriteriaProxy;
        private readonly ILogger _logger;
        private readonly ISqlCommandHandler _commandHandler;
        private readonly ICustomerProxy _customerProxy;

        public EvaluateTaxPercentageCommandHandler(ICustomerInvoiceCriteriaProxy customerInvoiceCriteriaProxy, ILogger logger, ISqlCommandHandler commandHandler, ICustomerProxy customerProxy)
        {
            if (customerInvoiceCriteriaProxy == null) throw new ArgumentNullException("customerInvoiceCriteriaProxy");
            if (logger == null) throw new ArgumentNullException("logger");
            if (commandHandler == null) throw new ArgumentNullException("commandHandler");
            if (customerProxy == null) throw new ArgumentNullException("customerProxy");

            _customerInvoiceCriteriaProxy = customerInvoiceCriteriaProxy;
            _logger = logger;
            _commandHandler = commandHandler;
            _customerProxy = customerProxy;
        }

        public void Handle(EvaluateTaxPercentageCommand command)
        {
            _logger.Debug("Evaluating tax percentage for basket with ordercode {orderCode}", command.Basket.OrderCode);
            var basket = command.Basket;
            var customerInvoiceCriteria = _customerInvoiceCriteriaProxy.GetByCustomer(basket.CustomerIdentifier);
            if (!customerInvoiceCriteria.HasValue || !customerInvoiceCriteria.Value.IsEuropeanInstitution)
                return;

            var isTaxPercentageChanged = false;
            if (customerInvoiceCriteria.Value.IsEuropeanInstitution && basket.TotalPrice.ExclVat <= MaxAmountWithVatForEuropeanInstitution && basket.VatRatePercentage == 0)
            {
                var vatRatePercentage = AsyncPump.Await(async () => await _customerProxy.GetVatRatePercentage(basket.CustomerIdentifier));
                basket.UpdateVatRatePercentage(vatRatePercentage);
                _logger.Debug(
                   "VAT set back to {vat} for customer with id {customerId} because it is an European Institution and the basket amount <= {maxAmount}",
                   basket.CustomerIdentifier.CustomerId,
                   MaxAmountWithVatForEuropeanInstitution,
                   vatRatePercentage);
                isTaxPercentageChanged = true;
            }
            else if (customerInvoiceCriteria.Value.IsEuropeanInstitution && basket.TotalPrice.ExclVat > MaxAmountWithVatForEuropeanInstitution && basket.VatRatePercentage > 0)
            {
                basket.RemoveVat();
                _logger.Debug(
                    "VAT removed for customer with id {customerId} because it is an European Institution and the basket amount > {maxAmount}",
                    basket.CustomerIdentifier.CustomerId,
                    MaxAmountWithVatForEuropeanInstitution);
                isTaxPercentageChanged = true;
            }

            if (isTaxPercentageChanged)
            {
                _commandHandler.Execute(new UpdateBasketVatPercentageCommand(basket.OrderCode, basket.VatRatePercentage, SystemUser.Id));
                foreach (var item in basket.Items)
                {
                    _commandHandler.Execute(new UpdateBasketItem(item, SystemUser.Id));
                }
            }
        }
    }
}