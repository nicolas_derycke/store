﻿using System;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Threading;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Commands.BasketItems;
using Store.Proxies;
using Store.Services;

namespace Store.CommandHandlers.BasketItems
{
    public class AddHostingUpgradeCompensationCommandHandler : ICommandHandler<AddHostingUpgradeCompensationCommand>
    {
        private readonly IInvoiceCorrectionProxy _invoiceCorrectionProxy;
        private readonly IInvoiceCorrectionService _invoiceCorrectionService;

        public AddHostingUpgradeCompensationCommandHandler(
            IInvoiceCorrectionProxy invoiceCorrectionProxy,
            IInvoiceCorrectionService invoiceCorrectionService)
        {
            if (invoiceCorrectionProxy == null) throw new ArgumentNullException("invoiceCorrectionProxy");
            if (invoiceCorrectionService == null) throw new ArgumentNullException("invoiceCorrectionService");

            _invoiceCorrectionProxy = invoiceCorrectionProxy;
            _invoiceCorrectionService = invoiceCorrectionService;
        }

        public void Handle(AddHostingUpgradeCompensationCommand command)
        {
            var basket = command.Basket;
            var basketItem = command.BasketItem;

            var hostingAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<HostingAttributes>(AttributeKey.Hosting);
            if (!basket.IsAnonymousBasket &&
                hostingAttributes.HasValue &&
                hostingAttributes.Value.UpgradeFromAccountId.HasValue)
            {
                var domainAttributes = basketItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);

                var invoiceCorrection = AsyncPump.Await(async () => await _invoiceCorrectionProxy.GetInvoiceCorrection(
                        basket.CustomerIdentifier,
                        domainAttributes.DomainName,
                        basketItem.PeriodId,
                        basketItem.Price.ExclVat,
                        hostingAttributes.Value.UpgradeFromAccountId.Value));

                _invoiceCorrectionService.CreateBasketItems(invoiceCorrection, basket, domainAttributes.DomainName, basketItem);
            }
        }
    }
}