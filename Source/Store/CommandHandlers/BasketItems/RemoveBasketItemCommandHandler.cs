using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Messaging.CommandBus;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Model.Exceptions;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.Validation;
using Store.Warnings;

namespace Store.CommandHandlers.BasketItems
{
    public class RemoveBasketItemCommandHandler : ICommandHandler<RemoveBasketItemCommand>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;

        public RemoveBasketItemCommandHandler(
            ISqlCommandHandler sqlCommandHandler)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            

            _sqlCommandHandler = sqlCommandHandler;
           

        }

        public void Handle(RemoveBasketItemCommand command)
        {
            var basket = command.Basket;
            var basketItem = command.BasketItem;

            if (basketItem.ParentId.HasValue && basketItem.ProductCode != "combell-mover")
                throw new ChildBasketItemDeletionException(basketItem.BasketItemId, basketItem.ParentId.Value);

            _sqlCommandHandler.Execute(new RemoveBasketItem(basketItem.BasketItemId, command.DeletionById));

            var childs = basket.GetChildBasketItems(basketItem.BasketItemId).ToList();
            foreach (var child in childs)
                _sqlCommandHandler.Execute(new RemoveBasketItem(child.BasketItemId, command.DeletionById));

            basket.Items.Remove(basketItem);
            basket.Items.RemoveAll(i => childs.Contains(i));
            
        }
    }
}