﻿using System;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Threading;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Commands.BasketItems;
using Store.Proxies;
using Store.Services;

namespace Store.CommandHandlers.BasketItems
{
    public class AddAddonUpgradeCompensationCommandHandler : ICommandHandler<AddAddonUpgradeCompensationCommand>
    {
        private readonly IInvoiceCorrectionProxy _invoiceCorrectionProxy;
        private readonly IInvoiceCorrectionService _invoiceCorrectionService;

        public AddAddonUpgradeCompensationCommandHandler(
            IInvoiceCorrectionProxy invoiceCorrectionProxy,
            IInvoiceCorrectionService invoiceCorrectionService)
        {
            if (invoiceCorrectionProxy == null) throw new ArgumentNullException("invoiceCorrectionProxy");
            if (invoiceCorrectionService == null) throw new ArgumentNullException("invoiceCorrectionService");

            _invoiceCorrectionProxy = invoiceCorrectionProxy;
            _invoiceCorrectionService = invoiceCorrectionService;
        }

        public void Handle(AddAddonUpgradeCompensationCommand command)
        {
            var basket = command.Basket;
            var basketItem = command.BasketItem;

            var uacAccountAddonAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<UacAccountAddonAttributes>(AttributeKey.UacAccountAddon);
            if (!basket.IsAnonymousBasket &&
                uacAccountAddonAttributes.HasValue &&
                uacAccountAddonAttributes.Value.UpgradeFromAddonId.HasValue)
            {
                var domainAttributes = basketItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);

                var invoiceCorrection = AsyncPump.Await(async () => await _invoiceCorrectionProxy.GetCachingAddonInvoiceCorrection(
                        basket.CustomerIdentifier,
                        domainAttributes.DomainName,
                        uacAccountAddonAttributes.Value.UpgradeFromAddonId.Value,
                        basket.Provider.ResellerId,
                        basketItem.Price.ExclVat));

                _invoiceCorrectionService.CreateBasketItems(invoiceCorrection, basket, domainAttributes.DomainName, basketItem);
            }
        }
    }
}