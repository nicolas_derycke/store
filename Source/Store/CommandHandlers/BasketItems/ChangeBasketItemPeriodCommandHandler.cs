using System;
using System.Linq;
using Intelligent.Shared.Messaging.CommandBus;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Model.Exceptions;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;

namespace Store.CommandHandlers.BasketItems
{
    public class ChangeBasketItemPeriodCommandHandler : ICommandHandler<ChangeBasketItemPeriodCommand>
    {
        private readonly IBasketItemPricingService _basketItemPricingService;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly ICommandBus _commandBus;

        public ChangeBasketItemPeriodCommandHandler(
            IBasketItemPricingService basketItemPricingService,
            ISqlCommandHandler sqlCommandHandler,
            ICommandBus commandBus)
        {
            if (basketItemPricingService == null) throw new ArgumentNullException("basketItemPricingService");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (commandBus == null) throw new ArgumentNullException("commandBus");

            _basketItemPricingService = basketItemPricingService;
            _sqlCommandHandler = sqlCommandHandler;
            _commandBus = commandBus;
        }

        public void Handle(ChangeBasketItemPeriodCommand command)
        {
            var basket = command.Basket;

            var basketItemToUpdate = basket.GetBasketItem(command.BasketItemId);

            if (basketItemToUpdate.IsFixedPrice)
                throw new InvalidBasketItemPeriodException(command.NewPeriod, basketItemToUpdate.ProductCode);

            // we don't execute the live validators, no live validator takes the period into account

            basketItemToUpdate.ChangePeriod(command.NewPeriod);
            _basketItemPricingService.SetProductInfoAndPrice(basketItemToUpdate, basket.CustomerIdentifier, basket.Provider, basket.Language, basket.VatRatePercentage);

            _sqlCommandHandler.Execute(new UpdateBasketItem(basketItemToUpdate, command.LastUpdatedById));

            // re-apply compensation for hosting products which are upgraded
            _commandBus.Send(new AddHostingUpgradeCompensationCommand(basketItemToUpdate, basket));

            // re-apply compensation for addon upgrades which are upgraded
            _commandBus.Send(new AddAddonUpgradeCompensationCommand(basketItemToUpdate, basket));

            // every child which is recurring (e.g. not a one time fee) should also change its period
            foreach (var child in basket.GetChildBasketItems(basketItemToUpdate.BasketItemId).Where(c => c.IsRecurring))
                _commandBus.Send(new ChangeBasketItemPeriodCommand(basket, child.BasketItemId, command.NewPeriod, command.ProviderId, command.LastUpdatedById));
        }
    }
}