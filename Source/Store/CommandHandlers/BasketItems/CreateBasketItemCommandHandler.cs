﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Collections;
using Intelligent.Shared.Core;
using Intelligent.Shared.Messaging.CommandBus;
using Intelligent.Shared.Threading;
using Store.API.Contracts.Baskets;
using Store.Commands.BasketItems;
using Store.Commands.Baskets;
using Store.Model;
using Store.Model.Exceptions;
using Store.Providers.Attributes;
using Store.Services;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.Validation;
using Store.Warnings;
using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Store.SqlQueries;
using Store.API.Contracts;

namespace Store.CommandHandlers.BasketItems
{
    public class CreateBasketItemCommandHandler : ICommandHandler<CreateBasketItemCommand>
    {
        private const int PremiumBasketItemPeriodInMonths = 12;

        private readonly ICommandBus _commandBus;
        private readonly IEnumerable<IAttributeProvider> _attributeProviders;
        private readonly ISqlCommandHandler _sqlCommandHandler;
        private readonly IEnumerable<IWarningProvider> _warningProviders;
        private readonly IEnumerable<ILiveBasketItemValidator> _liveValidators;
        private readonly IPremiumDomainPriceService _premiumDomainPriceService;
        private readonly IBasketItemPricingService _basketItemPricingService;
        private readonly IQueryHandler _queryHandler;

        public CreateBasketItemCommandHandler(
            ICommandBus commandBus,
            IEnumerable<IAttributeProvider> attributeProviders,
            ISqlCommandHandler sqlCommandHandler,
            IEnumerable<IWarningProvider> warningProviders,
            IEnumerable<ILiveBasketItemValidator> liveValidators,
            IPremiumDomainPriceService premiumDomainPriceService,
            IBasketItemPricingService basketItemPricingService,
            IQueryHandler queryHandler)
        {
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (attributeProviders == null) throw new ArgumentNullException("attributeProviders");
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");
            if (warningProviders == null) throw new ArgumentNullException("warningProviders");
            if (liveValidators == null) throw new ArgumentNullException("liveValidators");
            if (premiumDomainPriceService == null) throw new ArgumentNullException("premiumDomainPriceService");
            if (basketItemPricingService == null) throw new ArgumentNullException("basketItemPricingService");
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            _commandBus = commandBus;
            _attributeProviders = attributeProviders;
            _sqlCommandHandler = sqlCommandHandler;
            _warningProviders = warningProviders;
            _liveValidators = liveValidators;
            _premiumDomainPriceService = premiumDomainPriceService;
            _basketItemPricingService = basketItemPricingService;
            _queryHandler = queryHandler;
        }

        public void Handle(CreateBasketItemCommand command)
        {
            var basket = command.Basket;
            var basketItem = command.BasketItem;

            basket.MakeIdentifierUnique(basketItem);
            if (basket.ContainsDuplicateBasketItem(basketItem))
                throw new DuplicateBasketItemException(basketItem.ProductCode);

            if (basketItem.IsUpgrade && basket.BasketType == BasketType.Regular)
                throw new InvalidBasketTypeException("Upgrade items can only be added to an upgrade basket.");

            _basketItemPricingService.SetProductInfoAndPrice(basketItem, basket.CustomerIdentifier, basket.Provider, basket.Language, basket.VatRatePercentage);

            QuantityValidator.ThrowIfInvalid(basketItem, basketItem.Quantity);

            DuplicateSslHostnameValidator.ThrowIfInvalid(basket, basketItem);

            if (basketItem.ProductType == ProductTypes.DomainName && basketItem.ProductCode != "pre-registration")
            {
                var domainName = new DomainName(basketItem.Attributes.Identifier);
                var domainProductExtension = _queryHandler.Execute(new GetExtensionByProductCode(basketItem.ProductCode));
                if (!domainProductExtension.HasValue || domainProductExtension.Value.ExtensionWithDot != domainName.Extension.NameWithDot)
                    throw new InvalidBasketItemAttributeException(AttributeKey.Domain, string.Format("Extension '{0}' does not match the extension of the product '{1}'", domainName.Extension.NameWithDot, basketItem.ProductCode));
            }

            Optional<PremiumDomainPrice> premiumDomainPrice = null;
            if (basketItem.ProductType == ProductTypes.DomainName && !basketItem.ParentId.HasValue && basketItem.ProductCode != "pre-registration")
            {
                var rrpProxyDomainPrice = AsyncPump.Await(async () => await _premiumDomainPriceService.GetRrpProxyDomainPrice(basketItem, basket.Provider));

                if (rrpProxyDomainPrice.HasValue && rrpProxyDomainPrice.Value.IsPremium)
                {
                    // premiums only allow period 1 year
                    basketItem.Period = PremiumBasketItemPeriodInMonths;
                    // recalc default price for 12 months
                    _basketItemPricingService.SetProductInfoAndPrice(basketItem, basket.CustomerIdentifier, basket.Provider, basket.Language, basket.VatRatePercentage);
                    // calculate premium domain price
                    premiumDomainPrice = _premiumDomainPriceService.CalculatePremiumDomainPrice(rrpProxyDomainPrice.Value, basketItem.Price.ExclVat, PremiumBasketItemPeriodInMonths);
                    // set price
                    _basketItemPricingService.SetPremiumDomainFixedPrice(basketItem, premiumDomainPrice.Value.PeriodPrice);
                }
            }

            foreach (var attributeProvider in _attributeProviders)
                attributeProvider.Get(basket, basketItem).WhenHasValue(attribute => basketItem.Attributes.Add(attribute));

            basketItem.SetBasketItemWarnings(_warningProviders.SelectMany(w => w.Validate(basketItem, basket)).ToList());

            basketItem.UpdateBasketItemValidationErrors(_liveValidators.SelectMany(v => v.Validate(basketItem, basket)).ToList());

            if (!basket.Items.Contains(basketItem))
                basket.Items.Add(basketItem);

            // store basket item and attributes in db
            _sqlCommandHandler.Execute(new CreateBasketItem(basket, basketItem, command.CreationById));

            foreach (var attribute in basketItem.Attributes.ToSafeEnumerable())
                _sqlCommandHandler.Execute(new CreateBasketItemAttribute(basketItem.BasketItemId, attribute.Key, attribute.Value, command.CreationById));

            // apply compensation for hosting products which are upgraded
            _commandBus.Send(new AddHostingUpgradeCompensationCommand(basketItem, basket));

            // apply compensation for addon upgrades which are upgraded
            _commandBus.Send(new AddAddonUpgradeCompensationCommand(basketItem, basket));

            // add fixed costs
            _commandBus.Send(new AddFixedCostsCommand(basketItem, basket));

            _commandBus.Send(new AddPremiumDomainOneTimeFeeCommand(premiumDomainPrice, basket, basketItem, command.CreationById));
        }
    }
}