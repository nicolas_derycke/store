﻿using System;
using Intelligent.Shared.Messaging.CommandBus;
using Store.Commands.BasketItems;
using Store.SqlCommands;
using Store.SqlCommands.BasketItems;
using Store.Validation;

namespace Store.CommandHandlers.BasketItems
{
    public class ChangeBasketItemQuantityCommandHandler : ICommandHandler<ChangeBasketItemQuantityCommand>
    {
        private readonly ISqlCommandHandler _sqlCommandHandler;

        public ChangeBasketItemQuantityCommandHandler(ISqlCommandHandler sqlCommandHandler)
        {
            if (sqlCommandHandler == null) throw new ArgumentNullException("sqlCommandHandler");

            _sqlCommandHandler = sqlCommandHandler;
        }

        public void Handle(ChangeBasketItemQuantityCommand command)
        {
            var basketItemToUpdate = command.BasketItem;

            QuantityValidator.ThrowIfInvalid(basketItemToUpdate, command.NewQuantity);

            basketItemToUpdate.ChangeQuantity(command.NewQuantity);

            _sqlCommandHandler.Execute(new UpdateBasketItem(basketItemToUpdate, command.LastUpdatedById));
        }
    }
}
