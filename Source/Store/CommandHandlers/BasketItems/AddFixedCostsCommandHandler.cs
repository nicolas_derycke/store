﻿using System;
using Intelligent.Core.Users;
using Intelligent.Shared.Core.Queries;
using Intelligent.Shared.Messaging.CommandBus;
using Store.API.Contracts;
using Store.Commands.BasketItems;
using Store.Model.Baskets;
using Store.SqlQueries;
using System.Linq;

namespace Store.CommandHandlers.BasketItems
{
    public class AddFixedCostsCommandHandler : ICommandHandler<AddFixedCostsCommand>
    {
        private readonly IQueryHandler _queryHandler;
        private readonly ICommandBus _commandBus;

        public AddFixedCostsCommandHandler(
            IQueryHandler queryHandler,
            ICommandBus commandBus)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");
            if (commandBus == null) throw new ArgumentNullException("commandBus");

            _queryHandler = queryHandler;
            _commandBus = commandBus;
        }

        public void Handle(AddFixedCostsCommand command)
        {
            var basket = command.Basket;
            var basketItem = command.BasketItem;
            var identifierAttribute = basketItem.Attributes.IdentifierItemAttributeKeyValuePair;
            var customerId = 0;
            if (basket.CustomerIdentifier != null)
                customerId = basket.CustomerIdentifier.CustomerId;

            var fixedCostProductCodes = _queryHandler.Execute(new GetFixedCostByProductCodeQuery(basketItem.ActualProductCode));

            foreach (var fixedCostProductCode in fixedCostProductCodes)
            {
                if (identifierAttribute == null)
                    throw new Exception(string.Format("Basket item {0} has no identifier", basketItem.BasketItemId));

                var productDetails = 
                    _queryHandler.Execute(
                        new GetProductDetailsQuery(
                            basket.Language, 
                            fixedCostProductCode,
                            customerId,
                            basket.Provider.Id, 
                            identifierAttribute.Item2.Identifier));
                if (!productDetails.Any())
                    continue;

                var fixedCostBasketItem = 
                    new BasketItem(
                        fixedCostProductCode,
                        null,
                        0,
                        1,
                        new ItemAttributeDictionary() { new ItemAttributeKeyValuePair(identifierAttribute.Item1, identifierAttribute.Item2) },
                        basket.VatRatePercentage,
                        false,
                        basketItem.BasketItemId);

                basket.Items.Add(fixedCostBasketItem);

                _commandBus.Send(new CreateBasketItemCommand(basket, fixedCostBasketItem, SystemUser.Id));
            }
        }
    }
}
