﻿using System;
using System.Collections.Generic;
using System.Linq;
using Combell.RCS.Webservice.Interface;
using Intelligent.Core.Models;
using Intelligent.Shared.ServiceModel.Client;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Warnings
{
    public class TransferTradeWarningProvider : IWarningProvider
    {
        private readonly IServiceInvoker<IRegistryService> _registryService;

        public TransferTradeWarningProvider(IServiceInvoker<IRegistryService> registryService)
        {
            if (registryService == null) throw new ArgumentNullException("registryService");

            this._registryService = registryService;
        }
        public IEnumerable<BasketItemWarning> Validate(BasketItem basketItem, Basket basket)
        {
            if (basketItem.ProductType != ProductTypes.DomainName)
                yield break;

            var domainAttributes = basketItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
            var domain = new DomainName(domainAttributes.DomainName);

            if (!basketItem.Attributes.ContainsKey(AttributeKey.TransferDomain))
                yield break;

            var extensionsSupportingTransferTrade = _registryService.Invoke(rcs => rcs.GetExtensionsSupportingTransferTrade());

            if (!extensionsSupportingTransferTrade.Any(ext => ext == domain.Extension.NameWithDot))
                yield return BasketItemWarning.TransferTradeNotSupported;
        }
    }
}