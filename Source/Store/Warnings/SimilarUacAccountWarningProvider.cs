﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Threading;
using Store.API.Contracts.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using UAC.Queries.Model.Entities;

namespace Store.Warnings
{
    public class SimilarUacAccountWarningProvider : IWarningProvider
    {
        private readonly IUacProxy _uacProxy;

        private static readonly Dictionary<string, ResourceSetAndWarningTuple> ProductTypeResourceSetAndWarningTupleMapping = new Dictionary<string, ResourceSetAndWarningTuple>()
            {
                {ProductTypes.BasicEmail, new ResourceSetAndWarningTuple(ResourceName.BusinessEmail, BasketItemWarning.BusinessEmailAccountExists)},
                {ProductTypes.BusinessEmail, new ResourceSetAndWarningTuple(ResourceName.BasicEmail, BasketItemWarning.BasicEmailAccountExists)},
                {ProductTypes.LinuxSharedHosting, new ResourceSetAndWarningTuple(ResourceName.WindowsHosting, BasketItemWarning.WindowsAccountExists)},
                {ProductTypes.WindowsSharedHosting, new ResourceSetAndWarningTuple(ResourceName.LinuxHosting, BasketItemWarning.LinuxAccountExists)}
            };

        public SimilarUacAccountWarningProvider(IUacProxy uacProxy)
        {
            if (uacProxy == null) throw new ArgumentNullException("uacProxy");

            _uacProxy = uacProxy;
        }

        public IEnumerable<BasketItemWarning> Validate(BasketItem basketItem, Basket basket)
        {
            if (basket.IsAnonymousBasket)
                yield break;

            var identifier = basketItem.Attributes.Identifier;

            if (identifier == null)
                yield break;

            if (ProductTypeResourceSetAndWarningTupleMapping.ContainsKey(basketItem.ProductType))
            {
                var resourceSetAndWarningTuple = ProductTypeResourceSetAndWarningTupleMapping[basketItem.ProductType];
                var customers = AsyncPump.Await(async () => await _uacProxy.GetCustomersByResourceSetAndIdentifier(resourceSetAndWarningTuple.ResourceSetName, identifier));

                if (customers.Any(c => c == basket.CustomerIdentifier))
                    yield return resourceSetAndWarningTuple.Warning;
            }
        }

        private class ResourceSetAndWarningTuple
        {
            public ResourceName ResourceSetName { get; private set; }
            public BasketItemWarning Warning { get; private set; }

            public ResourceSetAndWarningTuple(ResourceName resourceSetName, BasketItemWarning warning)
            {
                ResourceSetName = resourceSetName;
                Warning = warning;
            }
        }
    }
}