﻿using System.Collections.Generic;
using Store.API.Contracts.Baskets;
using Store.Model.Baskets;

namespace Store.Warnings
{
    public interface IWarningProvider
    {
        IEnumerable<BasketItemWarning> Validate(BasketItem basketItem, Basket basket);
    }
}