﻿using System;
using System.Collections.Generic;
using System.Linq;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Providers;

namespace Store.Warnings
{
    public class ExtraFieldsRequiredWarningProvider : IWarningProvider
    {
        private readonly IRequiredExtraFieldsProvider _requiredExtraFieldsProvider;

        public ExtraFieldsRequiredWarningProvider(IRequiredExtraFieldsProvider requiredExtraFieldsProvider)
        {
            if (requiredExtraFieldsProvider == null) throw new ArgumentNullException("requiredExtraFieldsProvider");

            _requiredExtraFieldsProvider = requiredExtraFieldsProvider;
        }

        public IEnumerable<BasketItemWarning> Validate(BasketItem basketItem, Basket basket)
        {
            if (basketItem.ProductType != ProductTypes.DomainName)
                yield break;

            var registrantAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<RegistrantAttributes>(AttributeKey.Registrant);
            if (!registrantAttributes.HasValue)
                yield break;

            var domainAttributes = basketItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
            if (_requiredExtraFieldsProvider.Get(domainAttributes.DomainName, registrantAttributes.Value).Any())
                yield return BasketItemWarning.ExtraFieldsRequired;
        }
    }
}
