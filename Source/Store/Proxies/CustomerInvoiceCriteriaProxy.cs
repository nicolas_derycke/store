﻿using System;
using System.Runtime.Caching;
using Billing.API.Common.Interface.Contracts.Data;
using Billing.API.Common.Interface.Contracts.Requests;
using Billing.API.Common.Interface.Contracts.Services;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;
using Intelligent.Shared.ServiceModel.Client;

namespace Store.Proxies
{
    public class CustomerInvoiceCriteriaProxy : ICustomerInvoiceCriteriaProxy
    {
        private readonly IServiceInvoker<ICustomerService> _customerService;
        private const int CacheExpirationInMinutes = 10;

        public CustomerInvoiceCriteriaProxy(IServiceInvoker<ICustomerService> customerService)
        {
            if (customerService == null) throw new ArgumentNullException("customerService");

            _customerService = customerService;
        }

        public Optional<CustomerInvoiceCriteriaDataContract> GetByCustomer(CustomerIdentifier customerIdentifier)
        {
            if (customerIdentifier == null)
                return Optional<CustomerInvoiceCriteriaDataContract>.Empty;

            var cacheKey = GetCacheKey(customerIdentifier);
            if (MemoryCache.Default.Contains(cacheKey))
                return (CustomerInvoiceCriteriaDataContract)MemoryCache.Default.Get(cacheKey);

            var result = _customerService.Invoke(cs => cs.GetCustomerInvoiceCriteria(new GetCustomerInvoiceCriteriaRequest(customerIdentifier.CustomerId)));
            MemoryCache.Default.Add(cacheKey, result, new CacheItemPolicy()
                {
                    AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(CacheExpirationInMinutes)
                });
            return result;
        }

        public void ClearCache(CustomerIdentifier customerIdentifier)
        {
            var cacheKey = GetCacheKey(customerIdentifier);
            if (MemoryCache.Default.Contains(cacheKey))
                MemoryCache.Default.Remove(cacheKey);
        }

        private static string GetCacheKey(CustomerIdentifier customerIdentifier)
        {
            return string.Format("CustomerInvoiceCriteria.{0}", customerIdentifier.CustomerNumber);
        }
    }
}