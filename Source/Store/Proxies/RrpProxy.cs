﻿using System.Threading.Tasks;
using Store.Proxies.Data;

namespace Store.Proxies
{
    public class RrpProxy : IRrpProxy
    {
        private readonly RrpProxyHttpClient _rrpProxyHttpClient;

        public RrpProxy()
        {
            this._rrpProxyHttpClient = new RrpProxyHttpClient();
        }

        public async Task<RrpProxyDomainPrice> GetRrpProxyDomainPrice(string domainName)
        {
            var properties = await _rrpProxyHttpClient.ExecuteRrpProxyCall("DomainPrice", new RrpProxyParameter("Domain", domainName));

            return new RrpProxyDomainPrice(
                properties.GetValueAsDecimal("[annual][0]"),
                properties.GetValueAsDecimal("[transfer][0]"),
                properties.GetValueAsDecimal("[trade][0]"),
                properties.GetValueAsDecimal("[setup][0]"),
                properties.GetValueAsInteger("[period][0]"),
                properties.GetValueAsInteger("[premium][0]") == 1,
                properties.GetValueAsString("[currency][0]"));
        }
    }
}