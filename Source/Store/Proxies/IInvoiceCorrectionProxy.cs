using System.Threading.Tasks;
using Billing.API.Contracts;
using Intelligent.Core.Models;

namespace Store.Proxies
{
    public interface IInvoiceCorrectionProxy
    {
        Task<InvoiceCorrectionContract> GetInvoiceCorrection(CustomerIdentifier customerIdentifier, string domainName, int newProductPeriodId, decimal newPrice, int accountId);
        Task<InvoiceCorrectionContract> GetCachingAddonInvoiceCorrection(CustomerIdentifier customerIdentifier, string domainName, int currentAddonId, int resellerId, decimal newPrice);
    }
}