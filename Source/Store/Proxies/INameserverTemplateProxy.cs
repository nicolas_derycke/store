using System.Collections.Generic;
using System.Threading.Tasks;
using DomainManagement.Api.NameserverTemplates.Contracts;
using Intelligent.Core.Models;

namespace Store.Proxies
{
    public interface INameserverTemplateProxy
    {
        Task<IEnumerable<NameserverTemplateContract>> GetDefault(CustomerIdentifier customerIdentifier);
    }
}