﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Intelligent.Shared.Net.Http;
using Trademarks.API.Contract;

namespace Store.Proxies
{
    public class TrademarkExistanceProxy : ITrademarkExistanceProxy
    {
        private readonly IIntelliPropertiesProvider _propertiesProvider;

        public TrademarkExistanceProxy(IIntelliPropertiesProvider propertiesProvider)
        {
            if (propertiesProvider == null) throw new ArgumentNullException("propertiesProvider");
            _propertiesProvider = propertiesProvider;
        }

        public async Task<TrademarkCheck> Get(string domainName)
        {
            var client = StronglyTypedHttpClient
                .Create()
                .WithDynamicHeaders(_propertiesProvider.IntelliPropertiesAccessor)
                .Build();
            var url = string.Format("{0}/trademarkCheck/{1}", ConfigurationManager.AppSettings["TrademarkAPI"], domainName);
            return await client.GetAsync<TrademarkCheck>(url);
        }
    }
}
