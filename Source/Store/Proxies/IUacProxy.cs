using System.Collections.Generic;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using UAC.API.Contracts;
using UAC.Queries.Model.Entities;

namespace Store.Proxies
{
    public interface IUacProxy
    {
        Task<Account> GetAccount(int accountId);

        Task<IEnumerable<int>> GetResellerProfileIdsByCustomer(CustomerIdentifier customer);

        Task<IEnumerable<CustomerIdentifier>> GetCustomersByResourceSetAndIdentifier(ResourceName resourceSetName, string identifier);
    }
}