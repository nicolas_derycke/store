﻿using System.Threading.Tasks;
using Customer.API.Contracts;
using Intelligent.Core.Models;

namespace Store.Proxies
{
    public interface ICustomerProxy
    {
        Task<MinimalCustomerContract> GetMinimalCustomer(CustomerIdentifier customerIdentifier);

        Task<bool> IsNewCustomer(CustomerIdentifier customerIdentifier);

        Task<decimal> GetVatRatePercentage(CustomerIdentifier customerIdentifier);
    }
}