using System.Threading.Tasks;
using Store.Proxies.Data;

namespace Store.Proxies
{
    public interface IRrpProxy
    {
        Task<RrpProxyDomainPrice> GetRrpProxyDomainPrice(string domainName);
    }
}