﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Store.Proxies.Data
{
    public class RrpProxyResponse : Dictionary<string, string>
    {
        public RrpProxyResponse(IDictionary<string, string> data)
            : base(data)
        {
        }

        public static decimal ParseRrpProxyDecimal(string s)
        {
            return decimal.Parse(s, CultureInfo.InvariantCulture);
        }

        public decimal GetValueAsDecimal(string key)
        {
            return GetValue(key, RrpProxyResponse.ParseRrpProxyDecimal);
        }

        public int GetValueAsInteger(string key)
        {
            return GetValue(key, int.Parse);
        }

        public string GetValueAsString(string key)
        {
            return GetValue(key, s => s);
        }

        public T GetValue<T>(string key, Func<string, T> parse)
        {
            return !ContainsKey(key) ? default(T) : parse.Invoke(this[key]);
        }
    }
}