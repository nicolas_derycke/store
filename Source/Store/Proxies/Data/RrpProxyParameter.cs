﻿namespace Store.Proxies.Data
{
    public class RrpProxyParameter
    {
        public string Key { get; private set; }
        public string Value { get; private set; }
        
        public RrpProxyParameter(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}