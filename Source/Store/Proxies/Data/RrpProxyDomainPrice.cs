﻿namespace Store.Proxies.Data
{
    public class RrpProxyDomainPrice
    {
        public decimal AnnualPrice { get; private set; }
        public decimal TransferPrice { get; private set; }
        public decimal TradePrice { get; private set; }
        public decimal SetupCost { get; private set; }
        public int Period { get; private set; }
        public bool IsPremium { get; private set; }
        public string Currency { get; private set; }
        
        public RrpProxyDomainPrice(decimal annualPrice, decimal transferPrice, decimal tradePrice, decimal setupCost, int period, bool isPremium, string currency)
        {
            AnnualPrice = annualPrice;
            TransferPrice = transferPrice;
            TradePrice = tradePrice;
            SetupCost = setupCost;
            Period = period;
            IsPremium = isPremium;
            Currency = currency;
        }
    }
}