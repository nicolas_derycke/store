﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using DomainManagement.Api.NameserverTemplates.Contracts;
using Intelligent.Core.Models;

namespace Store.Proxies
{
    public class NameserverTemplateProxy : INameserverTemplateProxy
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public NameserverTemplateProxy(IHttpClientFactory httpClientFactory)
        {
            if (httpClientFactory == null) throw new ArgumentNullException("httpClientFactory");

            this._httpClientFactory = httpClientFactory;
        }

        public async Task<IEnumerable<NameserverTemplateContract>> GetDefault(CustomerIdentifier customerIdentifier)
        {
            var client = _httpClientFactory.BuildStoreClient();
            var url = string.Format("{0}/customers/{1}/nameservertemplates?defaultOnly=true", ConfigurationManager.AppSettings["DomainmanagementAPI"], customerIdentifier.CustomerNumber);
            return await client.GetAsync<IEnumerable<NameserverTemplateContract>>(url);
        }
    }
}
