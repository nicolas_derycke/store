﻿using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Store.Proxies.Data;

namespace Store.Proxies
{
    public class RrpProxyHttpClient
    {
        private readonly string _rrpProxyBaseUrl;

        public RrpProxyHttpClient()
        {
            _rrpProxyBaseUrl = ConfigurationManager.AppSettings["RrpProxyApi"];
            _rrpProxyBaseUrl = string.Format(_rrpProxyBaseUrl, ConfigurationManager.AppSettings["RrpProxyUserName"], ConfigurationManager.AppSettings["RrpProxyPassword"]);
        }

        public Task<RrpProxyResponse> ExecuteRrpProxyCall(string command, RrpProxyParameter parameter)
        {
            return ExecuteRrpProxyCall(command, new[] { parameter });
        }

        public async Task<RrpProxyResponse> ExecuteRrpProxyCall(string command, RrpProxyParameter[] parameters)
        {
            var url = new StringBuilder();
            url.AppendFormat("{0}&command={1}", _rrpProxyBaseUrl, command);
            foreach (var parameter in parameters)
                url.AppendFormat("&{0}={1}", parameter.Key, parameter.Value);

            var client = new HttpClient();
            var response = await client.GetStringAsync(url.ToString());

            var matches = Regex.Matches(response, @"(.*) = (.*)");
            var properties = new RrpProxyResponse(matches.Cast<Match>().Where(m => m.Groups.Count == 3).ToDictionary(m => m.Groups[1].Value.Replace("property", string.Empty), m => m.Groups[2].Value));

            var responseCode = properties.GetValue("code", int.Parse);
            var description = properties.GetValue("description", s => s);
            if (responseCode != 200)
                throw new Exception(string.Format("RrpProxy returned an error: '{0}'", description));

            return properties;
        }
    }
}
