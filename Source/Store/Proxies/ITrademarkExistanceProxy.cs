using System.Threading.Tasks;
using Trademarks.API.Contract;

namespace Store.Proxies
{
    public interface ITrademarkExistanceProxy
    {
        Task<TrademarkCheck> Get(string domainName);
    }
}