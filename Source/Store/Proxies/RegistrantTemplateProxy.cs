﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using DomainManagement.Api.ContactTemplates.Contracts;
using Intelligent.Core.Models;
using Store.Proxies;

namespace Store.Repositories
{
    public class RegistrantTemplateProxy : IRegistrantTemplateProxy
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public RegistrantTemplateProxy(IHttpClientFactory httpClientFactory)
        {
            if (httpClientFactory == null) throw new ArgumentNullException("httpClientFactory");

            this._httpClientFactory = httpClientFactory;
        }

        public async Task<IEnumerable<ContactTemplateContract>> GetDefault(CustomerIdentifier customerIdentifier)
        {
            var client = _httpClientFactory.BuildStoreClient();
            var url = string.Format("{0}/customers/{1}/contacttemplates?defaultOnly=true", ConfigurationManager.AppSettings["DomainmanagementAPI"], customerIdentifier.CustomerNumber);
            return await client.GetAsync<IEnumerable<ContactTemplateContract>>(url);
        }
    }
}