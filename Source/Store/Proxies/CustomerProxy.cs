﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Customer.API.Contracts;
using Intelligent.Core.Models;

namespace Store.Proxies
{
    public class CustomerProxy : ICustomerProxy
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public CustomerProxy(IHttpClientFactory httpClientFactory)
        {
            if (httpClientFactory == null) throw new ArgumentNullException("httpClientFactory");

            _httpClientFactory = httpClientFactory;
        }

        public async Task<decimal> GetVatRatePercentage(CustomerIdentifier customerIdentifier)
        {
            if (customerIdentifier == null)
                return 21;

            var client = _httpClientFactory.BuildStoreClient();
            var url = string.Format("{0}/customers/{1}/vatratepercentage", ConfigurationManager.AppSettings["CustomerAPI"], customerIdentifier.CustomerNumber);
            return await client.GetAsync<decimal>(url);
        }

        public async Task<bool> IsNewCustomer(CustomerIdentifier customerIdentifier)
        {
            var client = _httpClientFactory.BuildStoreClient();
            var url = string.Format("{0}/customers/{1}/isnewcustomer", ConfigurationManager.AppSettings["CustomerAPI"], customerIdentifier.CustomerNumber);
            return await client.GetAsync<bool>(url);
        }

        public async Task<MinimalCustomerContract> GetMinimalCustomer(CustomerIdentifier customerIdentifier)
        {
            var client = _httpClientFactory.BuildStoreClient();
            var url = string.Format("{0}/customers/{1}?view=minimal", ConfigurationManager.AppSettings["CustomerAPI"], customerIdentifier.CustomerNumber);
            return await client.GetAsync<MinimalCustomerContract>(url);
        }
    }
}