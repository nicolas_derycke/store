using Billing.API.Common.Interface.Contracts.Data;
using Intelligent.Core.Models;
using Intelligent.Shared.Core;

namespace Store.Proxies
{
    public interface ICustomerInvoiceCriteriaProxy
    {
        Optional<CustomerInvoiceCriteriaDataContract> GetByCustomer(CustomerIdentifier customerIdentifier);

        void ClearCache(CustomerIdentifier customerIdentifier);
    }
}