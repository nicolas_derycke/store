﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Intelligent.Core.Models;
using Intelligent.Shared.Net.Http;
using UAC.Queries.Model.Entities;
using Uac = UAC.API.Contracts;

namespace Store.Proxies
{
    public class UacProxy : IUacProxy
    {
        private readonly StronglyTypedHttpClient _client;

        public UacProxy(IIntelliPropertiesProvider propertiesProvider)
        {
            if (propertiesProvider == null) throw new ArgumentNullException("propertiesProvider");
            _client = StronglyTypedHttpClient.Create().WithDynamicHeaders(propertiesProvider.IntelliPropertiesAccessor).Build();
        }

        public async Task<IEnumerable<CustomerIdentifier>> GetCustomersByResourceSetAndIdentifier(ResourceName resourceSetName, string identifier)
        {
            var url = string.Format("{0}/product/{1}/{2}/customers", ConfigurationManager.AppSettings["UacAPI"], resourceSetName.ToString(), identifier);
            var customers = await _client.GetAsync<IEnumerable<Uac.Customer>>(url);
            return customers.Select(c => CustomerIdentifier.FromCustomerNumber(c.CustomerNumber));
        }

        public async Task<IEnumerable<int>> GetResellerProfileIdsByCustomer(CustomerIdentifier customer)
        {
            var url = string.Format("{0}/customers/{1}/serviceprofiles", ConfigurationManager.AppSettings["UacAPI"], customer.CustomerId);
            var resellerProfiles = await _client.GetAsync<IEnumerable<Uac.ResellerProfileContract>>(url);
            return resellerProfiles.Select(rp => rp.Id);
        }

        public async Task<Uac.Account> GetAccount(int accountId)
        {
            var url = string.Format("{0}/accounts/{1}", ConfigurationManager.AppSettings["UacAPI"], accountId);
            return await _client.GetAsync<Uac.Account>(url);
        }
    }
}