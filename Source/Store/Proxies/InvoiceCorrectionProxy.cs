﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Billing.API.Contracts;
using Core.Owin;
using Intelligent.Core.Models;
using Intelligent.Shared.Net.Http;
using Intelligent.Shared.Owin;
using Store.Helpers;

namespace Store.Proxies
{
    public class InvoiceCorrectionProxy : IInvoiceCorrectionProxy
    {
        private readonly IAppSettingsConfiguration _appSettingsConfiguration;
        private readonly IIntelliPropertiesProvider _propertiesProvider;

        public InvoiceCorrectionProxy(IAppSettingsConfiguration appSettingsConfiguration, IIntelliPropertiesProvider propertiesProvider)
        {
            if (appSettingsConfiguration == null) throw new ArgumentNullException("appSettingsConfiguration");
            if (propertiesProvider == null) throw new ArgumentNullException("propertiesProvider");

            this._appSettingsConfiguration = appSettingsConfiguration;
            _propertiesProvider = propertiesProvider;
        }

        public async Task<InvoiceCorrectionContract> GetInvoiceCorrection(CustomerIdentifier customerIdentifier, string domainName, int newProductPeriodId, decimal newPrice, int accountId)
        {
            var url = string.Format("{0}/api/customer/{1}/linuxhostingpackage/{2}/invoicecorrections?newProductPeriodId={3}&newPrice={4}&accountId={5}",
               _appSettingsConfiguration.BillingApiUrl,
               customerIdentifier.CustomerId,
               domainName,
               newProductPeriodId,
               newPrice.ToString(CultureInfo.InvariantCulture),
               accountId);

            var client = StronglyTypedHttpClient
                .Create()
                .WithHeader("User-Agent", _appSettingsConfiguration.UserAgent)
                .WithDynamicHeaders(_propertiesProvider.IntelliPropertiesAccessor)
                .Build();
            return await client.GetAsync<InvoiceCorrectionContract>(url);
        }

        public async Task<InvoiceCorrectionContract> GetCachingAddonInvoiceCorrection(CustomerIdentifier customerIdentifier, string domainName, int currentAddonId, int resellerId, decimal newPrice)
        {
            var url = string.Format("{0}/api/customer/{1}/cachingAddon/{2}/invoicecorrections?currentAddonId={3}&resellerId={4}&newPrice={5}",
               _appSettingsConfiguration.BillingApiUrl,
               customerIdentifier.CustomerId,
               domainName,
               currentAddonId,
               resellerId,
               newPrice.ToString(CultureInfo.InvariantCulture));

            var client = StronglyTypedHttpClient.Create().WithHeader("User-Agent", _appSettingsConfiguration.UserAgent).Build();
            return await client.GetAsync<InvoiceCorrectionContract>(url);
        }
    }
}