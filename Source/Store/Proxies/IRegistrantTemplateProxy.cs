using System.Collections.Generic;
using System.Threading.Tasks;
using DomainManagement.Api.ContactTemplates.Contracts;
using Intelligent.Core.Models;

namespace Store.Proxies
{
    public interface IRegistrantTemplateProxy
    {
        Task<IEnumerable<ContactTemplateContract>> GetDefault(CustomerIdentifier customerIdentifier);
    }
}