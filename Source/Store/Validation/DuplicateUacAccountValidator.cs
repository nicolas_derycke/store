﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Threading;
using Store.API.Contracts.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using UAC.Queries.Model.Entities;

namespace Store.Validation
{
    public class DuplicateUacAccountValidator : IBasketItemValidator, ILiveBasketItemValidator
    {
        private readonly IUacProxy _uacProxy;

        private static readonly Dictionary<string, ResourceName> ProductTypeResourceSetMapping = new Dictionary<string, ResourceName>
        {
            {ProductTypes.DomainName, ResourceName.Domainnames},
            {ProductTypes.LinuxSharedHosting, ResourceName.LinuxHosting},
            {ProductTypes.WindowsSharedHosting, ResourceName.WindowsHosting},
            {ProductTypes.Sitebuilder, ResourceName.Websplanet}
        };

        public DuplicateUacAccountValidator(IUacProxy uacProxy)
        {
            if (uacProxy == null) throw new ArgumentNullException("uacProxy");

            _uacProxy = uacProxy;
        }

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            if (basket.IsAnonymousBasket)
                yield break;

            if (basketItem.IsCompensation)
                yield break;

            if (basketItem.IsUpgrade)
                yield break;

            var identifier = basketItem.Attributes.Identifier;

            if (identifier == null)
                yield break;

            if (ProductTypeResourceSetMapping.ContainsKey(basketItem.ProductType))
            {
                var resourceSetName = ProductTypeResourceSetMapping[basketItem.ProductType];
                var customers = AsyncPump.Await(async () => await _uacProxy.GetCustomersByResourceSetAndIdentifier(resourceSetName, identifier));

                if (customers.Any(c => c == basket.CustomerIdentifier))
                    yield return BasketItemValidationError.UacAccountAlreadyExists;
            }
        }
    }
}