﻿using Intelligent.Core.Models;
using Intelligent.Shared.Core.Queries;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model.Baskets;
using Store.SqlQueries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.Validation
{
    public class DuplicatePreregistrationValidator : IBasketItemValidator, ILiveBasketItemValidator
    {
        private readonly IQueryHandler _queryHandler;

        public DuplicatePreregistrationValidator(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._queryHandler = queryHandler;
        }

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            if (basketItem.ProductCode != "pre-registration")
                yield break;
            
            var domainAttributes = basketItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
            var domainName = new DomainName(domainAttributes.DomainName);
            var preregistrations = _queryHandler.Execute(new GetPreregistrationsByNameQuery(domainName));
            if (preregistrations.Count() == 0)
                yield break;
            
            var isGoLive = false;
            var isLandRush = false;
            var preregistrationAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<PreregistrationAttributes>(AttributeKey.Preregistration);
            if(preregistrationAttributes.HasValue)
            {
                isGoLive = preregistrationAttributes.Value.IsGoLive;
                isLandRush = preregistrationAttributes.Value.IsLandRush;
            }
            
            //golive preregistrations: validates if any customer already did a golive preregistration for the given stageDate
            if (isGoLive && preregistrations.Any(p => p.QueuedForGoliveDate == preregistrationAttributes.Value.StageDate))
                yield return BasketItemValidationError.DuplicatePreregistration;
            //landrush preregistrations: validates if any customer already did a landrush preregistration for the given stageDate
            else if (isLandRush && preregistrations.Any(p => p.QueuedForLandrushDate == preregistrationAttributes.Value.StageDate))
                yield return BasketItemValidationError.DuplicatePreregistration;
            //normal preregistrations (without attributes, or both isLandrush and isGoLive is false): validates if the customer already did a preregistration for the given domainname
            else if ((!preregistrationAttributes.HasValue || !(isGoLive || isLandRush)) && basket.CustomerIdentifier != null && preregistrations.Any(p => p.CustomerId == basket.CustomerIdentifier.CustomerId))
                yield return BasketItemValidationError.DuplicatePreregistration;
        }
    }
}