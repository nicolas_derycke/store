﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Threading;
using Store.API.Contracts.Baskets;
using Store.Model.Baskets;
using Store.Providers;
using Store.Proxies;

namespace Store.Validation
{
    public class DuplicateResellerProfileValidator : IBasketItemValidator, ILiveBasketItemValidator
    {
        private static readonly string[] ResellerPlatformProductCodes = { "dual-platform", "linux-reseller-hosting" };

        private readonly IProductResellerProfileProvider _productResellerProfileProvider;
        private readonly IUacProxy _uacProxy;

        public DuplicateResellerProfileValidator(IProductResellerProfileProvider productResellerProfileProvider, IUacProxy uacProxy)
        {
            if (productResellerProfileProvider == null) throw new ArgumentNullException("productResellerProfileProvider");
            if (uacProxy == null) throw new ArgumentNullException("uacProxy");

            _productResellerProfileProvider = productResellerProfileProvider;
            _uacProxy = uacProxy;
        }

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            if (!basket.IsAnonymousBasket &&
                ResellerPlatformProductCodes.Any(code => code == basketItem.ProductType))
            {
                var resellerProfileId = _productResellerProfileProvider.GetResellerProfileIdByProductId(basketItem.ProductId);
                var customersResellerProfileIds = AsyncPump.Await(async () => await _uacProxy.GetResellerProfileIdsByCustomer(basket.CustomerIdentifier));

                if (customersResellerProfileIds.Any(id => id == resellerProfileId))
                    yield return BasketItemValidationError.ResellerProfileAlreadyExists;
            }
        }
    }
}