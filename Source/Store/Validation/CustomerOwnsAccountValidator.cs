﻿using System;
using System.Collections.Generic;
using System.Linq;
using Intelligent.Shared.Threading;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model.Baskets;
using Store.Proxies;

namespace Store.Validation
{
    public class CustomerOwnsAccountValidator : IBasketItemValidator, ILiveBasketItemValidator
    {
        private readonly IUacProxy _uacProxy;

        public CustomerOwnsAccountValidator(IUacProxy uacProxy)
        {
            if (uacProxy == null) throw new ArgumentNullException("uacProxy");

            _uacProxy = uacProxy;
        }

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            if (basket.IsAnonymousBasket)
                yield break;

            var accountIds = new List<int?>();

            var hostingAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<HostingAttributes>(AttributeKey.Hosting);
            if (hostingAttributes.HasValue)
                accountIds.Add(hostingAttributes.Value.UpgradeFromAccountId);

            var uacAccountAddonAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<UacAccountAddonAttributes>(AttributeKey.UacAccountAddon);
            if (uacAccountAddonAttributes.HasValue)
                accountIds.Add(uacAccountAddonAttributes.Value.AccountId);

            if (!accountIds.Any())
                yield break;

            foreach (var accountId in accountIds.Where(id => id.HasValue).Select(id => id.Value))
            {
                var account = AsyncPump.Await(async () => await _uacProxy.GetAccount(accountId));

                if (account.CustomerId != basket.CustomerIdentifier.CustomerId)
                    yield return BasketItemValidationError.AccountBelongsToOtherCustomer;
            }
        }
    }
}