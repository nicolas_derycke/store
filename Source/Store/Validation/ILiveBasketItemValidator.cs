﻿using System.Collections.Generic;
using Store.API.Contracts.Baskets;
using Store.Model.Baskets;

namespace Store.Validation
{
    public interface ILiveBasketItemValidator
    {
        IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket);
    }
}