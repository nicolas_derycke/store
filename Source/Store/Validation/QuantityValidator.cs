﻿using System.Collections.Generic;
using System.Linq;
using Store.Model;
using Store.Model.Baskets;
using Store.Model.Exceptions;

namespace Store.Validation
{
    public static class QuantityValidator
    {
        private static readonly IEnumerable<string> ProductTypesThatCanBeOrderedMoreThanOnceForSameBasketItem =
            new List<string>
            {
                ProductTypes.BasicEmail,
                ProductTypes.BusinessEmail,
                ProductTypes.MsSqlDb,
                ProductTypes.MysqlDb,
                ProductTypes.VeamCloudConnect
            };

        public static void ThrowIfInvalid(BasketItem basketItem, int quantity)
        {
            if (quantity > 1 && !ProductTypesThatCanBeOrderedMoreThanOnceForSameBasketItem.Contains(basketItem.ProductType))
                throw new InvalidBasketItemQuantityException(basketItem.BasketItemId, quantity);

            if (quantity == 0)
                throw new InvalidBasketItemQuantityException(basketItem.BasketItemId, quantity);

            if (quantity < 0 && basketItem.IsRecurring)
                throw new InvalidBasketItemQuantityException(basketItem.BasketItemId, quantity);
        }
    }
}