﻿using System.Collections.Generic;
using Store.API.Contracts.Baskets;
using Store.Model.Baskets;

namespace Store.Validation
{
    public interface IBasketItemValidator
    {
        IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket);
    }
}
