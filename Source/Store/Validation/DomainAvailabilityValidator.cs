﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Core.Owin;
using Intelligent.Shared.Net.Http;
using Intelligent.Shared.Owin;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Validation
{
    public class DomainAvailabilityValidator : IBasketItemValidator
    {
        private readonly IIntelliPropertiesProvider _intelliProperties;

        public DomainAvailabilityValidator(IIntelliPropertiesProvider intelliProperties)
        {
            if (intelliProperties == null) throw new ArgumentNullException("intelliProperties");
            _intelliProperties = intelliProperties;
        }

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            if (basketItem.ProductType != ProductTypes.DomainName)
                yield break;

            var domainAttributes = basketItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);

            var httpClient = StronglyTypedHttpClient
                .Create()
                .WithDynamicHeaders(_intelliProperties.IntelliPropertiesAccessor)
                .Build();
            var url = string.Format("{0}domainnameavailability/{1}", ConfigurationManager.AppSettings["DnaServiceUrl"], domainAttributes.DomainName);
            var availabilty = (string)httpClient.GetAsync<dynamic>(url).Result.status.name.ToString().ToLower();

            // we could not determine the status, this is an issue on our side and should not result in an invalid basket for the customer
            if (availabilty == "lookuperror")
                yield break;

            var previousAvailability = basketItem.Attributes.ContainsKey(AttributeKey.TransferDomain) ? "taken" : "free";

            if (previousAvailability != availabilty)
                yield return BasketItemValidationError.DomainAvailabilityChanged;
        }
    }
}
