﻿using System;
using System.Collections.Generic;
using Intelligent.Shared.Messaging.CommandBus;
using Store.API.Contracts.Baskets;
using Store.Commands.Baskets;
using Store.Model.Baskets;
using Store.Services;

namespace Store.Validation
{
    public class PricingRecalculator : IBasketItemValidator
    {
        private readonly IBasketItemPricingService _basketItemPricingService;
        private readonly ICommandBus _commandBus;

        public PricingRecalculator(IBasketItemPricingService basketItemPricingService, ICommandBus commandBus)
        {
            if (basketItemPricingService == null) throw new ArgumentNullException("basketItemPricingService");
            if (commandBus == null) throw new ArgumentNullException("commandBus");

            _basketItemPricingService = basketItemPricingService;
            _commandBus = commandBus;
        }

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            _basketItemPricingService.SetProductInfoAndPrice(basketItem, basket.CustomerIdentifier, basket.Provider, basket.Language, basket.VatRatePercentage);
            _commandBus.Send(new EvaluateTaxPercentageCommand(basket));

            yield break;
        }
    }
}