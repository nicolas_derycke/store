﻿using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model.Baskets;
using Store.Model.Exceptions;
using System.Linq;

namespace Store.Validation
{
    public static class DuplicateSslHostnameValidator
    {
        public static void ThrowIfInvalid(Basket basket, BasketItem basketItem)
        {
            var parentBasketItem = basketItem;
            if (basketItem.ParentId.HasValue)
                parentBasketItem = basket.GetBasketItem(basketItem.ParentId.Value);

            if (!parentBasketItem.Attributes.ContainsKey(AttributeKey.Certificate))
                return;

            var hostnames = parentBasketItem.Attributes.GetBasketItemAttribute<CertificateAttributes>(AttributeKey.Certificate).Hostnames.ToList();

            var childBasketItems = basket.GetChildBasketItems(parentBasketItem.BasketItemId);
            foreach (var childBasketItem in childBasketItems)
            {
                var certificateAttribute = childBasketItem.Attributes.GetOptionalBasketItemAttribute<CertificateAttributes>(AttributeKey.Certificate);
                if (certificateAttribute.HasValue)
                    hostnames.AddRange(certificateAttribute.Value.Hostnames);
            }

            if (hostnames.Distinct().Count() != hostnames.Count())
                // duplicate hostname
                throw new InvalidBasketItemAttributeException(
                    AttributeKey.Certificate,
                    string.Format("A duplicate hostname was found in basket item '{0}' and/or its childs.", parentBasketItem.BasketItemId));
        }
    }
}
