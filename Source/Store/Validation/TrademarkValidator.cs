﻿using System;
using System.Collections.Generic;
using Intelligent.Shared.Threading;
using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model;
using Store.Model.Baskets;
using Store.Proxies;
using Trademarks.API.Contract;

namespace Store.Validation
{
    public class TrademarkValidator : IBasketItemValidator
    {
        private readonly ITrademarkExistanceProxy _trademarkExistanceProxy;

        public TrademarkValidator(ITrademarkExistanceProxy trademarkExistanceProxy)
        {
            if (trademarkExistanceProxy == null) throw new ArgumentNullException("trademarkExistanceProxy");

            _trademarkExistanceProxy = trademarkExistanceProxy;
        }

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            if (basketItem.ProductType == ProductTypes.DomainName)
            {
                var domainAttributes = basketItem.Attributes.GetBasketItemAttribute<DomainItemAttributes>(AttributeKey.Domain);
                var trademarkAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<TrademarkAttributes>(AttributeKey.Trademark);
                var trademarkCheck = AsyncPump.Await(async () => await _trademarkExistanceProxy.Get(domainAttributes.DomainName));

                var originalTrademarkResult = trademarkAttributes.HasValue ? ClaimExistance.Exists : ClaimExistance.DoesntExist;
                if (trademarkCheck.ClaimExistanceValue != ClaimExistance.Error &&
                    originalTrademarkResult != trademarkCheck.ClaimExistanceValue)
                    yield return BasketItemValidationError.TrademarkChanged;
            }
        }
    }
}
