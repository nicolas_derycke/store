﻿using System;
using System.Collections.Generic;
using Intelligent.Shared.Core.Queries;
using Store.API.Contracts.Baskets;
using Store.Model;
using Store.Model.Baskets;
using Store.SqlQueries;

namespace Store.Validation
{
    public class InactiveProductValidator : IBasketItemValidator
    {
        private readonly IQueryHandler _queryHandler;

        public InactiveProductValidator(IQueryHandler queryHandler)
        {
            if (queryHandler == null) throw new ArgumentNullException("queryHandler");

            this._queryHandler = queryHandler;
        }

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            var optionalPeriodVisibility = _queryHandler.Execute(new IsProductPeriodVowQuery(basketItem.ActualProductCode, basketItem.Period, ProductAction.Renewal, basket.Provider));

            if (!optionalPeriodVisibility.HasValue)
            {
                yield return BasketItemValidationError.InactiveProduct;
                yield break;
            }

            var periodVisibility = optionalPeriodVisibility.Value;

            if (!periodVisibility.IsProductActive ||
                !periodVisibility.IsProductGroupTreeVow ||
                !periodVisibility.IsProductVow)
                yield return BasketItemValidationError.InactiveProduct;

            if (!periodVisibility.IsProductPeriodVow)
                yield return BasketItemValidationError.InactiveProductPeriod;
        }
    }
}
