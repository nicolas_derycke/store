﻿using Store.API.Client;
using Store.API.Contracts;
using Store.API.Contracts.Baskets;
using Store.API.Contracts.TypedItemAttributes;
using Store.Model.Baskets;
using System.Collections.Generic;

namespace Store.Validation
{
    public class UnacceptedTrademarkValidator : ILiveBasketItemValidator
    {


        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            var trademarkAttributes = basketItem.Attributes.GetOptionalBasketItemAttribute<TrademarkAttributes>(AttributeKey.Trademark);

            if (!trademarkAttributes.HasValue)
                yield break;

            if (!trademarkAttributes.Value.IsAcceptedByCustomer)
                yield return BasketItemValidationError.UnacceptedTrademark;
        }
    }
}