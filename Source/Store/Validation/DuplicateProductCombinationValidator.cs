﻿using System.Collections.Generic;
using System.Linq;
using Store.API.Contracts.Baskets;
using Store.Model;
using Store.Model.Baskets;

namespace Store.Validation
{
    public class DuplicateProductCombinationValidator : IBasketItemValidator, ILiveBasketItemValidator
    {
        private static readonly List<List<string>> UnallowedProductTypesCombinations = new List<List<string>>()
        {
            { new List<string>() { ProductTypes.LinuxSharedHosting, ProductTypes.WindowsSharedHosting, ProductTypes.Sitebuilder, ProductTypes.Drupal, ProductTypes.Joomla, ProductTypes.Magento, ProductTypes.Wordpress } },
            { new List<string>() { ProductTypes.BasicEmail, ProductTypes.BusinessEmail} },
            {new List<string>() {ProductTypes.ResellerPlatform } }
        };

        public IEnumerable<BasketItemValidationError> Validate(BasketItem basketItem, Basket basket)
        {
            if (basketItem.IsCompensation)
                yield break;

            var identifier = basketItem.Attributes.Identifier;

            //if (identifier == null)
            //    yield break;

            foreach (var unallowedProductTypeCombination in UnallowedProductTypesCombinations.Where(c => c.Any(pt => pt == basketItem.ProductType)))
            {
                if (identifier != null)
                {
                    if (basket.Items.Any(i =>
                        i.Attributes.Identifier == identifier &&
                        i != basketItem &&
                        unallowedProductTypeCombination.Contains(basketItem.ProductType) &&
                        unallowedProductTypeCombination.Contains(i.ProductType)))
                        yield return BasketItemValidationError.DuplicateProductCombination;

                    //TODO:This code will probably never execute. This needs to be checked if it can be removed.
                    if (basket.Items.Any(i =>
                        i.Attributes.Identifier == identifier &&
                        i != basketItem &&
                        basketItem.ProductCode == i.ProductCode))
                        yield return BasketItemValidationError.DuplicateProductCombination;
                }
                else
                {
                    if (basket.Items.Any(i =>
                        i != basketItem &&
                        unallowedProductTypeCombination.Where(upt => upt != basketItem.ProductType)
                            .Contains(i.ProductType)))
                        yield return BasketItemValidationError.DuplicateProductCombination;

                    if (basket.Items.Any(i =>
                        i != basketItem &&
                        basketItem.ProductCode == i.ProductCode))
                        yield return BasketItemValidationError.DuplicateProductCombination;
                }
            }
        }
    }
}